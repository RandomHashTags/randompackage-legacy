package me.randomHashTags.RandomPackage.api;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.api.events.conquest.ConquestDamageEvent;
import me.randomHashTags.RandomPackage.utils.classes.conquests.ConquestChest;
import me.randomHashTags.RandomPackage.utils.classes.conquests.ConquestMob;
import me.randomHashTags.RandomPackage.utils.classes.conquests.LivingConquestChest;
import me.randomHashTags.RandomPackage.utils.classes.conquests.LivingConquestMob;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Conquest extends RandomPackageAPI implements Listener, CommandExecutor {

    public boolean isEnabled = false;
    private static Conquest instance;
    public static Conquest getConquest() {
        if(instance == null) instance = new Conquest();
        return instance;
    }
    public FileConfiguration config;
    private List<Integer> tasks = new ArrayList<>();
    private int defaultMaxHealth = 0;
    private LivingConquestChest last;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(args.length == 0) {
            viewLast(sender);
        } else {
            final String a = args[0];
            if(a.equals("help")) {
                viewHelp(sender);
            } else if(a.equals("stop")) {
                destroyConquests();
            } else if(sender instanceof Player && a.equals("spawn")) {
                spawn((Player) sender);
            }
        }
        return true;
    }

    public void enable() {
        final long started = System.currentTimeMillis();
        if(isEnabled) return;
        save("Features", "conquests.yml");
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "conquests.yml"));

        defaultMaxHealth = config.getInt("conquests.default settings.max hp");
        final String defaultSpawnRegion = config.getString("conquests.default settings.spawn region");
        final int defaultSpawnedHealth = config.getInt("conquests.default settings.spawned hp"), defaultDmgPerHit = config.getInt("conquests.default settings.dmg per hit"), defaultDmgDelay = config.getInt("conquests.default settings.dmg delay"), defaultSpawnInterval = config.getInt("conquests.default settings.spawn interval"), defaultHealthRadius = config.getInt("conquests.default settings.messages.health radius"), defaultFirstAnnounced = config.getInt("conquests.default settings.messages.first announced"), defaultAnnounceIntervalAfterSpawned = config.getInt("conquests.default settings.messages.announce interval after spawned"), defaultDespawnDelay = config.getInt("conquests.default settings.messages.despawn after");
        final ItemStack defaultPlacedBlock = d(null, config.getString("conquests.default settings.placed block"), 0);
        final List<String> defaultSpawnMsg = config.getStringList("conquests.default settings.messages.spawn"), defaultWillSpawnMsg = config.getStringList("conquests.default settings.messages.will spawn"), defaultStillAliveMsg = config.getStringList("conquests.default settings.messages.still alive"), defaultHealthMsg = config.getStringList("conquests.default settings.messages.health"), defaultUnlockedMsg = config.getStringList("conquests.default settings.messages.unlocked"), defaultHitAttributes = config.getStringList("conquests.default settings.hit attributes");

        int loaded = 0, loadedB = 0;
        for(String s : config.getConfigurationSection("bosses").getKeys(false)) {
            final String p = "bosses." + s + ".";
            new ConquestMob(s, config.getString(p + "type").toUpperCase(), ChatColor.translateAlternateColorCodes('&', config.getString(p + "name")), config.getStringList(p + "attributes"), config.getStringList(p + "equipment"), config.getStringList(p + "drops"));
            loadedB += 1;
        }

        for(String s : config.getConfigurationSection("conquests").getKeys(false)) {
            if(!s.equals("default settings")) {
                loaded += 1;
                final String p = "conquests." + s + ".";
                final int maxHealth = config.getInt(p + "max hp"), spawnedHealth = config.getInt(p + "spawned hp"), dmgPerHit = config.getInt(p + "dmg per hit"), dmgDelay = config.getInt(p + "dmg delay"), spawnInterval = config.getInt(p + "spawn interval"), healthRadius = config.getInt(p + "messages.health radius"), firstAnnounced = config.getInt(p + "messages.first announced"), announceIntervalAfterSpawned = config.getInt(p + "messages.announce interval after spawned"), despawnDelay = config.getInt(p + "messages.despawn delay");
                final ItemStack placedBlock = d(null, config.getString(p + "placed block"), 0);
                final String spawnRegion = config.getString(p + "spawn region"), rewardSize = config.getString(p + "reward size");
                final List<String> rewards = config.getStringList(p + "rewards"), spawnMsg = config.getStringList(p + "messages.spawn"), willSpawnMsg = config.getStringList(p + "messages.will spawn"), stillAliveMsg = config.getStringList(p + "messages.still alive"), healthMsg = config.getStringList(p + "messages.health"), unlockedMsg = config.getStringList(p + "messages.unlocked"), hitAttributes = config.getStringList(p + "hit attributes");
                final HashMap<ConquestMob, String> spawnedMobs = new HashMap<>();
                for(String S : config.getStringList(p + "spawned bosses"))
                    spawnedMobs.put(ConquestMob.valueOf(S.split(";")[0]), S.split(";")[1]);
                final ConquestChest cc = new ConquestChest(s, spawnInterval != 0 ? spawnInterval : defaultSpawnInterval, spawnRegion != null ? spawnRegion : defaultSpawnRegion, maxHealth != 0 ? maxHealth : defaultMaxHealth, spawnedHealth != 0 ? spawnedHealth : defaultSpawnedHealth, dmgPerHit != 0 ? dmgPerHit : defaultDmgPerHit, dmgDelay != 0 ? dmgDelay : defaultDmgDelay, spawnedMobs, placedBlock != null ? placedBlock : defaultPlacedBlock, rewardSize, rewards, !hitAttributes.isEmpty() ? hitAttributes : defaultHitAttributes, !spawnMsg.isEmpty() ? spawnMsg : defaultSpawnMsg, !willSpawnMsg.isEmpty() ? willSpawnMsg : defaultWillSpawnMsg, firstAnnounced != 0 ? firstAnnounced : defaultFirstAnnounced, announceIntervalAfterSpawned != 0 ? announceIntervalAfterSpawned : defaultAnnounceIntervalAfterSpawned, !stillAliveMsg.isEmpty() ? stillAliveMsg : defaultStillAliveMsg, healthRadius != 0 ? healthRadius : defaultHealthRadius, !healthMsg.isEmpty() ? healthMsg : defaultHealthMsg, !unlockedMsg.isEmpty() ? unlockedMsg : defaultUnlockedMsg, despawnDelay != 0 ? despawnDelay : defaultDespawnDelay);
                final int spawninterval = cc.spawnInterval*20;
                tasks.add(scheduler.scheduleSyncRepeatingTask(randompackage, () -> {
                    final String[] sr = cc.spawnRegion.split(";");
                    final World w = Bukkit.getWorld(sr[0]);
                    final int xMin = Integer.parseInt(sr[1].split(":")[0]), xMax = Integer.parseInt(sr[1].split(":")[1]), x = xMin + random.nextInt(xMax-xMin+1), zMin = Integer.parseInt(sr[2].split(":")[0]), zMax = Integer.parseInt(sr[2].split(":")[1]), z = zMin + random.nextInt(zMax-zMin+1);
                    final Location l = new Location(w, x, 256, z);
                    l.setY(w.getHighestBlockYAt(l));
                    last = cc.spawn(l);
                }, spawninterval, spawninterval));
            }
        }
        final List<String> conquests = otherdata.getStringList("conquests");
        if(conquests != null && !conquests.isEmpty())
            for(String s : conquests)
                new LivingConquestChest(toLocation(s.split(":")[0]), ConquestChest.valueOf(s.split(":")[3]), Integer.parseInt(s.split(":")[2]), Long.parseLong(s.split(":")[1]), false, false);
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " conquest chests and " + loadedB + " bosses &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        ConquestChest.types.clear();
        LivingConquestMob.living.clear();
        final List<LivingConquestChest> lc = LivingConquestChest.living;
        final List<String> con = new ArrayList<>();
        for(LivingConquestChest c : lc) {
            con.add(toString(c.location) + ":" + c.spawnedTime + ":" + c.health + ":" + c.type.path + ":" + c.conquerer);
        }
        for(int i = 0; i < lc.size(); i++) {
            lc.get(i).delete(false);
            i -= 1;
        }
        otherdata.set("conquests", con);
        saveOtherData();
        for(int i : tasks) scheduler.cancelTask(i);
        tasks.clear();
        HandlerList.unregisterAll(this);
    }
    public void destroyConquests() {
        for(LivingConquestChest l : LivingConquestChest.living)
            l.delete(false);
    }

    @EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
        final Block c = event.getClickedBlock();
        if(c != null) {
            final Location l = c.getLocation();
            final LivingConquestChest cc = LivingConquestChest.valueOf(l);
            if(cc != null) {
                final Player player = event.getPlayer();
                event.setCancelled(true);
                player.updateInventory();
                if(!event.getAction().equals(Action.LEFT_CLICK_BLOCK)) return;
                final ConquestDamageEvent cde = new ConquestDamageEvent(player, cc, cc.type.dmgPerHit);
                pluginmanager.callEvent(cde);
                if(!cde.isCancelled())
                    cc.damage(player, l, cde.damage, false);
            }
        }
    }
    @EventHandler
    private void blockBreakEvent(BlockBreakEvent event) {
        final LivingConquestChest c = LivingConquestChest.valueOf(event.getBlock().getLocation());
        if(c != null) {
            event.setCancelled(true);
            event.getPlayer().updateInventory();
        }
    }

    @EventHandler
    private void entityDeathEvent(EntityDeathEvent event) {
        final LivingConquestMob l = LivingConquestMob.valueOf(event.getEntity().getUniqueId());
        if(l != null)
            l.kill(event);
    }

    @EventHandler
    private void chunkUnloadEvent(ChunkUnloadEvent event) {
        final Chunk c = event.getChunk();
        final LivingConquestChest l = LivingConquestChest.valueOf(c);
        if(!event.isCancelled() && l != null) {
            event.setCancelled(true);
        }
    }

    public void viewLast(CommandSender sender) {
        if(hasPermission(sender, "RandomPackage.conquest", true)) {
            final HashMap<String, String> replacements = new HashMap<>();
            final boolean j = last != null;
            final Location L = j ? last.location : null;
            replacements.put("{LAST}", j ? (System.currentTimeMillis()-last.spawnedTime) + "ms" : "N/A");
            replacements.put("{LOCATION}", j ? L.getBlockX() + "x " + L.getBlockY() + "y " + L.getBlockZ() + "z" : "N/A");
            replacements.put("{CONQUERER}", j && last.conquerer != null ? last.conquerer : "N/A");
            sendStringListMessage(sender, config.getStringList("messages.command"), replacements);
        }
    }
    public void viewHelp(CommandSender sender) {
        if(hasPermission(sender, "RandomPackage.conquest.help", true)) {
            final HashMap<String, String> replacements = new HashMap<>();
            replacements.put("{CHEST_HP}", Double.toString(defaultMaxHealth));
            sendStringListMessage(sender, config.getStringList("messages.help"), replacements);
        }
    }
    public void spawn(Player player) {
        if(hasPermission(player, "RandomPackage.conquest.spawn", true)) {
            final List<ConquestChest> chests = ConquestChest.types;
            final Location L = player.getLocation(), l = new Location(L.getWorld(), L.getBlockX(), L.getBlockY(), L.getBlockZ());
            last = chests.get(random.nextInt(chests.size())).spawn(l);
        }
    }
}

package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.randomHashTags.RandomPackage.api.events.customboss.CustomBossDamageByEntityEvent;
import me.randomHashTags.RandomPackage.api.events.customenchant.CustomEnchantEntityDamageByEntityEvent;
import me.randomHashTags.RandomPackage.api.events.mobstacker.MobStackDepleteEvent;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.utils.classes.ArmorSet;
import me.randomHashTags.RandomPackage.api.events.ArmorSetEquipEvent;
import me.randomHashTags.RandomPackage.api.events.ArmorSetUnequipEvent;
import me.randomHashTags.RandomPackage.api.events.customenchant.CEAApplyPotionEffectEvent;
import me.randomHashTags.RandomPackage.api.events.customenchant.CustomEnchantProcEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerArmorEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerArmorEvent.ArmorEventReason;

public class CustomArmor extends CustomEnchants implements Listener {
	
	public boolean isEnabled = false;
	private static CustomArmor instance;
	public static final CustomArmor getCustomArmor() {
		if(instance == null) instance = new CustomArmor();
		return instance;
	}
	
	public FileConfiguration config;
	public ItemStack equipmentLootbox;
	private List<Player> inEquipmentLootbox = new ArrayList<>();

	public void enable() {
		final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "custom armor.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "custom armor.yml"));
		equipmentLootbox = d(config, "items.equipment-lootbox", 0);
		final List<ItemStack> sets = new ArrayList<>();
		int loaded = 0;
		for(String s : config.getConfigurationSection("sets").getKeys(false)) {
			List<ArrayList<String>> o = new ArrayList<>();
			for(int i = 1; i <= 6; i++) {
				ArrayList<String> O = new ArrayList<>();
				final String type = i == 1 ? "armor" : i == 2 ? "helmet" : i == 3 ? "chestplate" : i == 4 ? "leggings" : i == 5 ? "boots" : "weapon";
				if(config.get("sets." + s + "." + type + "-lore") != null) {
					for(String l : config.getStringList("sets." + s + "." + type + "-lore")) O.add(ChatColor.translateAlternateColorCodes('&', l));
				}
				o.add(O);
			}
			final List<String> armorlore = o.get(0), helmetLore = armorlore != null ? armorlore : o.get(1), chestplateLore = armorlore != null ? armorlore : o.get(2), leggingsLore = armorlore != null ? armorlore : o.get(3), bootsLore = armorlore != null ? armorlore : o.get(4), weaponLore = o.get(5);
			new ArmorSet(s, helmetLore, chestplateLore, leggingsLore, bootsLore, weaponLore, config.getStringList("sets." + s + ".attributes"), config.getStringList("sets." + s + ".activate-message"));
			loaded += 1;
			for(int k = 1; k <= 4; k++) {
				item = k == 1 ? new ItemStack(Material.DIAMOND_HELMET, 1) : k == 2 ? new ItemStack(Material.DIAMOND_CHESTPLATE, 1) : k == 3 ? new ItemStack(Material.DIAMOND_LEGGINGS, 1) : new ItemStack(Material.DIAMOND_BOOTS, 1);
				itemMeta = item.getItemMeta(); lore.clear();
				lore.addAll(k == 1 ? helmetLore : k == 2 ? chestplateLore : k == 3 ? leggingsLore : k == 4 ? bootsLore : weaponLore);
				itemMeta.setLore(lore); lore.clear();
				item.setItemMeta(itemMeta);
				sets.add(item);
			}
		}
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " Armor Sets &e(took " + (System.currentTimeMillis()-started) + "ms)"));
		addGivedpCategory(sets, UMaterial.DIAMOND_HELMET, "Custom Armor", "Givedp: Custom Armor");
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		ArmorSet.sets.clear();
		HandlerList.unregisterAll(this);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void playerArmorEvent(PlayerArmorEvent event) {
		if(!event.isCancelled()) {
			final Player player = event.getPlayer();
			final String n = event.getReason().name();
			if(n.contains("_EQUIP")) {
				Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> {
					final ArmorSet W = ArmorSet.valueOf(player);
					if(W != null) {
						final ArmorSetEquipEvent e = new ArmorSetEquipEvent(player, W);
						pluginmanager.callEvent(e);
						sendStringListMessage(player, W.getActivateMessage());
						procCustomArmor(e, W);
					}
				}, 0);
			} else if(n.contains("_UNEQUIP")) {
				final ArmorSet W = ArmorSet.valueOf(player);
				if(W != null) {
					final ArmorSetUnequipEvent e = new ArmorSetUnequipEvent(player, W);
					pluginmanager.callEvent(e);
					procCustomArmor(e, W);
				}
			} else if(n.equals("BREAK")) {
				final ArmorSet W = ArmorSet.valueOf(player);
				if(W != null) procCustomArmor(event, W);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if(event.getEntity() instanceof Player) procCustomArmor(event, ArmorSet.valueOf((Player) event.getEntity()));
		if(event.getDamager() instanceof Player) procCustomArmor(event, ArmorSet.valueOf((Player) event.getDamager()));
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void entityDamageEvent(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) procCustomArmor(event, ArmorSet.valueOf((Player) event.getEntity()));
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void foodLevelChangeEvent(FoodLevelChangeEvent event) {
		if(event.getEntity() instanceof Player) procCustomArmor(event, ArmorSet.valueOf((Player) event.getEntity()));
	}

	public ItemStack getPiece(ArmorSet set, String type) {
		final String p = set.getPath();
		item = d(config, "sets." + p + "." + type.toLowerCase(), 0).clone(); itemMeta = item.getItemMeta(); lore.clear();
		if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
		for(String s : config.getStringList("sets." + p + ".armor-lore")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
		itemMeta.setLore(lore); lore.clear();
		item.setItemMeta(itemMeta);
		return item;
	}

	public void procCustomArmor(Event event, ArmorSet set) {
		if(set == null) return;
		for(String attr : set.getAttributes()) {
            final String A = attr.split(";")[0].toLowerCase();
            final boolean isPlayer = event instanceof EntityDamageByEntityEvent && ((EntityDamageByEntityEvent) event).getDamager() instanceof Player || event instanceof EntityDamageEvent && ((EntityDamageEvent) event).getEntity() instanceof Player;
            if(event instanceof PlayerArmorEvent && A.equals("armorequip") && ((PlayerArmorEvent) event).getReason().name().contains("_EQUIP")
                || event instanceof PlayerArmorEvent && A.equals("armorunequip") && ((PlayerArmorEvent) event).getReason().name().contains("_UNEQUIP")
                || event instanceof PlayerArmorEvent && A.equals("armorpiecebreak") && ((PlayerArmorEvent) event).getReason().equals(ArmorEventReason.BREAK)

				|| event instanceof ArmorSetEquipEvent && A.equals("armorsetequip")
				|| event instanceof ArmorSetUnequipEvent && A.equals("armorsetunequip")

                || event instanceof EntityDamageByEntityEvent && A.equals("damagedealt") && isPlayer
                || event instanceof EntityDamageByEntityEvent && A.equals("pva") && isPlayer
                || event instanceof EntityDamageByEntityEvent && A.equals("pvp") && isPlayer && ((EntityDamageByEntityEvent) event).getEntity() instanceof Player
                || event instanceof EntityDamageByEntityEvent && A.equals("pve") && isPlayer && ((EntityDamageByEntityEvent) event).getEntity() instanceof LivingEntity && !(((EntityDamageByEntityEvent) event).getEntity() instanceof Player)
                || event instanceof EntityDamageByEntityEvent && A.equals("isdamaged") && ((EntityDamageByEntityEvent) event).getEntity() instanceof Player
                || event instanceof EntityDamageByEntityEvent && A.equals("hitbyarrow") && ((EntityDamageByEntityEvent) event).getEntity() instanceof Player && ((EntityDamageByEntityEvent) event).getDamager() instanceof Arrow
                || event instanceof EntityDamageByEntityEvent && A.equals("arrowhit") && ((EntityDamageByEntityEvent) event).getDamager() instanceof Arrow && ((Arrow) ((EntityDamageByEntityEvent) event).getDamager()).getShooter() instanceof Player && shotbows.keySet().contains(((EntityDamageByEntityEvent) event).getDamager().getUniqueId())
                || event instanceof EntityDamageEvent && A.startsWith("damagedby(") && isPlayer && A.toUpperCase().contains(((EntityDamageEvent) event).getCause().name())
                || event instanceof EntityDamageByEntityEvent && A.startsWith("damagedby(") && ((EntityDamageByEntityEvent) event).getEntity() instanceof Player && A.toUpperCase().contains(((EntityDamageByEntityEvent) event).getCause().name())

                || event instanceof CustomEnchantEntityDamageByEntityEvent && A.startsWith("ceentityisdamaged")
                || event instanceof CustomBossDamageByEntityEvent && A.startsWith("custombossisdamaged")

                || event instanceof BlockPlaceEvent && A.equals("blockplace")
                || event instanceof BlockBreakEvent && A.equals("blockbreak")

                || event instanceof FoodLevelChangeEvent && A.equals("foodlevelgained") && ((FoodLevelChangeEvent) event).getEntity() instanceof Player && ((FoodLevelChangeEvent) event).getFoodLevel() > ((Player) ((FoodLevelChangeEvent) event).getEntity()).getFoodLevel()
                || event instanceof FoodLevelChangeEvent && A.equals("foodlevellost") && ((FoodLevelChangeEvent) event).getEntity() instanceof Player && ((FoodLevelChangeEvent) event).getFoodLevel() < ((Player) ((FoodLevelChangeEvent) event).getEntity()).getFoodLevel()

                || event instanceof PlayerItemDamageEvent && A.equals("isdurabilitydamaged")

                || event instanceof PlayerInteractEvent && A.equals("playerinteract")
                || event instanceof ProjectileHitEvent && A.equals("arrowland") && ((ProjectileHitEvent) event).getEntity() instanceof Arrow && getHitEntity((ProjectileHitEvent) event) != null
                || event instanceof EntityShootBowEvent && A.equals("shootbow")

                || event instanceof PlayerDeathEvent && A.equals("playerdeath")
                || event instanceof PlayerDeathEvent && A.equals("killedplayer")
                || event instanceof EntityDeathEvent && A.equals("killedentity") && !(((EntityDeathEvent) event).getEntity() instanceof Player)

                || event instanceof CustomEnchantProcEvent && A.equals("enchantproc")
                || event instanceof CEAApplyPotionEffectEvent && A.equals("ceapplypotioneffect")

                || event instanceof MobStackDepleteEvent && A.equals("mobstackdeplete")

                || event instanceof PluginEnableEvent && A.startsWith("timer(")

                || mcmmoIsEnabled && event instanceof com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent && A.equals("mcmmoxpgained")
                || mcmmoIsEnabled && event instanceof com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent && A.equals("mcmmoxpgained:" + ((com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent) event).getSkill().name().toLowerCase())
            ) {
                executeAttributes(event, attr);
            }
		}
	}
	
	public void executeAttributes(Event event, String attribute) {
		for(String a : attribute.substring(attribute.split(";")[0].length()).split(";")) {
			if(event != null && a.toLowerCase().startsWith("cancel")) {
				((Cancellable) event).setCancelled(true);
				if(event instanceof EntityDamageByEntityEvent && ((EntityDamageByEntityEvent) event).getDamager() instanceof Arrow) {
					((EntityDamageByEntityEvent) event).getDamager().remove();
				}
				return;
			} else {
				w(null, event, null, getRecipients(event, a.contains("[") ? a.split("\\[")[1].split("]")[0] : a, null), a, attribute, -1, null);
			}
		}
	}

	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		final ItemStack i = event.getItem();
		if(i != null && i.hasItemMeta() && i.getItemMeta().equals(equipmentLootbox.getItemMeta()) && i.getType().equals(equipmentLootbox.getType())) {
			final Player player = event.getPlayer();
			event.setCancelled(true);
			player.updateInventory();
			removeItem(player, i, 1);
			giveItem(player, getRandomEquipmentLootboxLoot());
		}
	}

	public ItemStack getRandomEquipmentLootboxLoot() {
		final List<String> r = config.getStringList("items.equipment-lootbox.rewards");
		String l = r.get(random.nextInt(r.size()));
		if(l.contains("||")) l = l.split("\\|\\|")[random.nextInt(l.split("\\|\\|").length)];
		return givedpitem.valueOf(l).clone();
	}
}

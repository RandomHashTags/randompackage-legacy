package me.randomHashTags.RandomPackage.api;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import org.bukkit.*;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.io.File;
import java.util.*;

/*
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Zombie;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
*/
@SuppressWarnings({"deprecation"})
public class CustomEnchantments extends RandomPackageAPI {
	/*
	private final String prefix = "[RandomPackage] ";
	//
	private int alchemistCostSlot = -1;
	//
	private String
		mcmmo = null, alchemistcurrency
		;
	
	private boolean shootfireworks = false, alchemistAccept = false, tinkererAccept = false;
	//
	private FileConfiguration enchantments, enchantmentsInfo, cemessages;
	private final String folder = "plugins" + File.separator + "RandomPackage" + File.separator + "Custom Enchantments" + File.separator;
	//
	private String[] enchantstrings = new String[10000];
	private Double[] doubles = new Double[10000];
	//
	private HashMap<String, String> enchants = new HashMap<String, String>();
	private HashMap<Player, Double> marksman = new HashMap<Player, Double>();
	private HashMap<Player, List<Player>> aegis = new HashMap<Player, List<Player>>();
	private HashMap<Player, ItemStack> commander = new HashMap<Player, ItemStack>(), protection = new HashMap<Player, ItemStack>(), implants = new HashMap<Player, ItemStack>(), teleblock = new HashMap<Player, ItemStack>();
	private ArrayList<ItemStack> bows = new ArrayList<ItemStack>();
	private ArrayList<UUID> soulmode = new ArrayList<UUID>(), dominate = new ArrayList<UUID>(), repairguard = new ArrayList<UUID>(), bowsp = new ArrayList<UUID>(), Teleblock = new ArrayList<UUID>(), phoenix = new ArrayList<UUID>(), divineimmolation = new ArrayList<UUID>();
	private ArrayList<Integer> dominatee = new ArrayList<Integer>();
	//
	//
	private void sendStringListMessage(UUID uuid, List<String> message, double d) {
		Player player = Bukkit.getPlayer(uuid);
		if(message.size() > 0 && !(message.get(0).equals("")))
		for(String string : message) {
			if(string.contains("{AMOUNT}")) string = string.replace("{AMOUNT}", Double.toString(d));
			string = ChatColor.translateAlternateColorCodes('&', string);
			if(player == null) Bukkit.getConsoleSender().sendMessage(string);
			else player.sendMessage(string);
		}
	}
	//
	private void applyEnchantmentOrb(InventoryClickEvent event, String type) {
		int previous = -1;
		boolean first = false;
		ArrayList<ItemStack> t = type.equals("armor-enchantment-orb") ? armorenchantmentorbs : weaponenchantmentorbs;
		if(event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().hasLore()) {
			for(int i = 1; i <= t.size(); i++) {
				if(event.getCurrentItem().getItemMeta().getLore().contains(ChatColor.translateAlternateColorCodes('&', itemsConfig.getString(type + ".apply")).replace("{SLOTS}", "" + itemsConfig.getInt(type + "." + i + ".slots")).replace("{ADD_SLOTS}", "" + i)))
					previous = i - 1;
			}
		}
		if(previous == -1) first = true;
		if(previous + 1 >= t.size() || !(event.getCursor().getItemMeta().getDisplayName().equals(t.get(previous + 1).getItemMeta().getDisplayName()))) return;
			
		item = event.getCurrentItem(); itemMeta = item.getItemMeta(); lore.clear();
		if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
		int percent = -1;
		previous++;
		if(first) previous = 1;
		for(int i = 0; i < itemsConfig.getStringList(type + "." + previous + ".lore").size(); i++)
			if(itemsConfig.getStringList(type + "." + previous + ".lore").get(i).contains("{PERCENT}"))
				percent = getRemainingInt(event.getCursor().getItemMeta().getLore().get(i));
		previous--;
		if(first) previous = -1;
		if(random.nextInt(100) <= percent) {
			if(previous == -1) {
				lore.add(ChatColor.translateAlternateColorCodes('&', itemsConfig.getString(type + ".apply").replace("{SLOTS}", "" + itemsConfig.getInt(type + ".1.slots")).replace("{ADD_SLOTS}", "1")));
			} else {
				boolean did = false;
				previous++;
				for(int i = 0; i < lore.size(); i++) {
					if(!(did) && lore.get(i).equals(ChatColor.translateAlternateColorCodes('&', itemsConfig.getString(type + ".apply").replace("{SLOTS}", "" + itemsConfig.getInt(type + "." + previous + ".slots")).replace("{ADD_SLOTS}", "" + (previous))))) {
						did = true;
						lore.set(i, ChatColor.translateAlternateColorCodes('&', itemsConfig.getString(type + ".apply").replace("{SLOTS}", "" + itemsConfig.getInt(type + "." + (previous + 1) + ".slots")).replace("{ADD_SLOTS}", "" + (previous + 1))));
					}
				}
				if(!(did)) return;
			}
			playSuccess((Player) event.getWhoClicked());
		} else {
			playDestroy((Player) event.getWhoClicked());
			event.getWhoClicked().sendMessage("[CustomEnchantments] previous=" + previous + "; percent=" + percent + " - Report how you got this to RandomHashTags!");
			return;
		}
		
		itemMeta.setLore(lore); lore.clear();
		item.setItemMeta(itemMeta);
		event.setCancelled(true);
		event.setCurrentItem(item);
		if(event.getCursor().getAmount() == 1) { event.setCursor(new ItemStack(Material.AIR)); } else { event.getCursor().setAmount(event.getCursor().getAmount() - 1); }
		((Player) event.getWhoClicked()).updateInventory();
		return;
	}
	//@EventHandler(priority = EventPriority.HIGHEST)
	private void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if(event.isCancelled()) {
			if(event.getEntity() instanceof Player && event.getDamager().getType().name().contains("ARROW") && ((Projectile) event.getDamager()).getShooter() instanceof Player) {
				Player shooter = (Player) ((Projectile) event.getDamager()).getShooter(), victim = (Player) event.getEntity();
				for(int i = 0; i < bowsp.size(); i++)
					if(bowsp.get(i).equals(shooter.getUniqueId())) {
						ArrayList<String> damagerenchants = getEnchantmentsOnItem(bows.get(i));
						for(String string : damagerenchants) {
							String OEN = string.split(" ")[0];
							if(OEN.equals("Teleportation"))
								if(fapi.relationIsAlly(shooter, victim) || fapi.relationIsMember(shooter, victim))
									Teleportation((Projectile) event.getDamager(), shooter, victim);
						}
				}
			}
			
			
			return;
		} else {
			procEnchants(event);
		}
	}
	private boolean doublestrike = false;
	public void procEnchants(EntityDamageByEntityEvent event) {
		if(!(Bukkit.getPluginManager().getPlugin("WorldEdit") == null) && !(Bukkit.getPluginManager().getPlugin("WorldGuard") == null)
				&& event.getEntity() instanceof Player
				&& RP_WorldEditWorldGuard.getInstance().locationHasPvPAllowed(event.getEntity().getWorld(), event.getEntity().getLocation()) == false
				|| event.getEntity() instanceof Player && event.getDamager() instanceof Player
				&& !(fapi.relationIsEnemyOrNull((Player) event.getEntity(), (Player) event.getDamager()))
				&& !(fapi.relationIsNeutral((Player) event.getEntity(), (Player) event.getDamager()))
				) {
			event.setCancelled(true);
			return;
		} else
		// Player vs Entity
		if(event.getEntity() instanceof IronGolem && !(event.getEntity().getCustomName() == null) && event.getDamager() instanceof Player) {
			String playername = ChatColor.stripColor(event.getEntity().getCustomName().replace(ChatColor.stripColor(guardiansname.replace("{PLAYER}", "")), ""));
			if(!(Bukkit.getPlayer(playername) == null) && Bukkit.getPlayer(playername).isOnline() && !(Bukkit.getPlayer(playername).getName().equalsIgnoreCase(playername))) {
				for(ItemStack is : Bukkit.getPlayer(playername).getInventory().getArmorContents()) {
					if(!(is == null) && !(is.getType().equals(Material.AIR)) && is.hasItemMeta() && is.getItemMeta().hasLore()) {
						for(String enchant : getEnchantmentsOnItem(is)) {
							if(enchant.split(" ")[0].equals("BloodLink") && random.nextInt(100) <= getProckChance("BloodLink", Integer.parseInt(enchant.split(" ")[1]), Bukkit.getPlayer(playername))) {
								BloodLink(Bukkit.getPlayer(playername));
							}
						}
					}
				}
			}
			return;
		} else 
		// Player vs Player OR Player vs Entity
		if(event.getEntity() instanceof LivingEntity && event.getDamager() instanceof Player && !(getItemInHand((Player) event.getDamager()) == null)
				&& getItemInHand((Player) event.getDamager()).hasItemMeta() && getItemInHand((Player) event.getDamager()).getItemMeta().hasLore()) {
			Player damager = (Player) event.getDamager();
			LivingEntity victim = (LivingEntity) event.getEntity();
			if(dominate.contains(damager.getUniqueId())) {
				int d = 0;
				for(int z = 0; z < dominate.size(); z++) if(dominate.get(z).equals(damager.getUniqueId()) && d == 0) d = z;
				event.setDamage(event.getDamage() * (event.getDamage() * dominatee.get(d) * doubles[7]));
			}
			int shackle = 0;
			// Weapon enchants
			for(String enchant : getEnchantmentsOnItem(getItemInHand(damager))) {
				String OEN = enchant.split(" ")[0];
				int level = Integer.parseInt(enchant.split(" ")[1]), chance = 0;
				if(!(OEN.equals("Dodge")))
					chance = getProckChance(OEN, level, damager);
				//Bukkit.broadcastMessage("evaluateProckChance=" + chance + ",getProckChance=" + getProckChance(OEN, level, 0));
				if(random.nextInt(100) <= chance || OEN.equals("Silence") || OEN.equals("Disarmor") || OEN.equals("DivineImmolation")) {
					if(OEN.equals("Assassin")) Assassin(event, level);
					else if(OEN.equals("Barbarian")) Barbarian(event, level, damager);
					/ else if(OEN.equals("Bleed") && victim instanceof Player) {
						bleed.add(victim.getName());
						if(Bukkit.getVersion().contains("1.9") || Bukkit.getVersion().contains("1.10") || Bukkit.getVersion().contains("1.11")) {
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "particle blockcrack " + victim.getEyeLocation().getX() + " " + victim.getLocation().getY() + " " + victim.getEyeLocation().getZ() + " 0.65 0.8 0.65 1 100 normal " + victim.getName() + " 38");
						}
						Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
							public void run() {
								bleed.remove(victim.getName());
							}
						}, 20 * 6);
					else if(OEN.equals("Demonforged") && victim instanceof Player) Demonforged((Player) victim, level, chance);
					else if(OEN.equals("DivineImmolation") && soulmode.contains(event.getDamager().getUniqueId())) DivineImmolation(event, level);
					else if(OEN.equals("Berserk")) Berserk(damager, level);
					else if(OEN.equals("Blacksmith")) Blacksmith(event, damager, level);
					else if(OEN.equals("Blessed")) Blessed(damager);
					else if(OEN.equals("Blind")) Blind(event, level);
					else if(OEN.equals("Cleave")) Cleave(damager, victim, level, event);
					else if(OEN.equals("Confusion") && victim instanceof Player) Confusion((Player) victim, level);
					else if(OEN.equals("DeepWounds") && victim instanceof Player) DeepWounds(event, (Player) victim, level);
					else if(OEN.equals("Disarmor") && victim instanceof Player && ((Player) victim).getHealth() <= 8) {
						boolean did = false;
						for(ItemStack is : ((Player) victim).getInventory().getArmorContents()) {
							for(String enchants : getEnchantmentsOnItem(is)) {
								if(enchants.split(" ")[0].equals("Sticky") && did == false) {
									chance = chance - getProckChance(enchants, level, damager); did = true;
								}
							}
						}
						if(random.nextInt(100) <= chance) Disarmor((Player) victim);
					} else if(OEN.equals("Dominate") && victim instanceof Player) Dominate(event, level);
					else if(OEN.equals("DoubleStrike") && doublestrike == false) {
						procEnchants(event);
						doublestrike = true;
						sendStringListMessage(damager.getUniqueId(), cemessages.getStringList("enchants.DoubleStrike"), 0);
						playSound(sounds, "enchants.DoubleStrike", damager, damager.getLocation(), false);
					} else if(OEN.equals("Epicness") && victim instanceof Player) Epicness((Player) victim, level);
					else if(OEN.equals("Enrage"))                                 Enrage(event, damager, level);
					else if(OEN.equals("Execute"))                                Execute(damager, level);
					else if(OEN.equals("Featherweight"))                          Featherweight(damager, level);
					else if(OEN.equals("Frozen") && victim instanceof Player)     Frozen((Player) victim, level);
					else if(OEN.equals("Greatsword") && victim instanceof Player) Greatsword(event, (Player) victim, level);
					else if(OEN.equals("IceAspect") && victim instanceof Player)  IceAspect((Player) victim, level);
					else if(OEN.equals("IceFreeze") && victim instanceof Player)  IceFreeze((Player) victim, level);
					else if(OEN.equals("Insanity") && victim instanceof Player)   Insanity(damager, level, (Player) victim, event);
					else if(OEN.equals("Insomnia") && victim instanceof Player)   Insomnia((Player) victim, level, event);
					else if(OEN.equals("Lifesteal"))                              Lifesteal(event, damager);
					else if(OEN.equals("Obliterate"))                             Obliterate(event, level);
					else if(OEN.equals("Paralyze") && victim instanceof Player)   Paralyze((Player) victim, level);
					else if(OEN.equals("Poison"))                                 Poison(victim, level);
					else if(OEN.equals("Pummel") && victim instanceof Player)     Pummel(damager, (Player) victim, level);
					else if(OEN.equals("Rage"))                                   Rage(event, damager, level);
					else if(OEN.equals("Ravenous")) { Ravenous(damager, level);
					} else if(OEN.equals("Shackle") && !(victim instanceof Player)) { Shackle(event, level);
					} else if(OEN.equals("Silence") && victim instanceof Player) {
						int solitude = 0;
						for(String str : getEnchantmentsOnItem(getItemInHand(damager))) {
							if(str.equals("Solitude")) { solitude = getProckChance(str, level, damager);
							}
						}
						chance = solitude + chance;
						if(random.nextInt(100) <= chance) { Silence((Player) victim, level); }
					} else if(OEN.equals("SkillSwipe") && victim instanceof Player) { SkillSwipe((Player) victim, damager, level);
					} else if(OEN.equals("ThunderingBlow")) { ThunderingBlow(event, level);
					} else if(OEN.equals("Trap") && victim instanceof Player) { Trap((Player) victim, level);
					} else if(OEN.equals("Trickster") && victim instanceof Player) { Trickster((Player) victim);
					} else if(OEN.equals("Vampire")) { Vampire(event, damager);
					}
				}
			}
			doublestrike = false;
			// Armor enchants
			int lucky = 0;
			for(ItemStack is : damager.getInventory().getArmorContents()) {
				if(!(is == null) && is.hasItemMeta() && is.getItemMeta().hasLore())
					for(String enchant : getEnchantmentsOnItem(is)) {
						String OEN = enchant.split(" ")[0];
						int level = Integer.parseInt(enchant.split(" ")[1]), chance = 0;
						if(!(OEN.equals("Dodge"))) chance = getProckChance(OEN, level, damager);
						if(random.nextInt(100) <= chance) {
							if(OEN.equals("PlanetaryDeathbringer"))  PlanetaryDeathbringer(event);
							else if(OEN.equals("Leadership"))        Leadership(event, level);
						}
					}
			}
			//
		}
		// Player is damaged by a living entity or Player
		if(event.getEntity() instanceof Player && event.getDamager() instanceof Player || event.getEntity() instanceof Player && event.getDamager() instanceof LivingEntity) {
			Player victim = (Player) event.getEntity();
			LivingEntity damager = (LivingEntity) event.getDamager();
			if(damager instanceof Projectile && damager.getType().name().contains("ARROW"))
				if(((Projectile) damager).getShooter() instanceof Player) {
					damager = (Player) ((Projectile) damager).getShooter();
				}
				else return;
			boolean phoeniX = false; int phoenix = 1000 + random.nextInt(101);
			item = getSoulGemInPlayerInventory(victim, true);
			// Victim's Armor
			for(ItemStack ap : victim.getInventory().getArmorContents()) {
				if(!(ap == null) && ap.hasItemMeta() && ap.getItemMeta().hasLore()) {
					for(String enchant : getEnchantmentsOnItem(ap)) {
						int souls = 0;
						if(!(item == null)) souls = getRemainingInt(item.getItemMeta().getDisplayName());
						String OEN = enchant.split(" ")[0];
						int level = Integer.parseInt(enchant.split(" ")[enchant.split(" ").length - 1]), chance = 0;
						if(!(OEN.equals("Dodge"))) {
							chance = getProckChance(OEN, level, victim);
						}
						if(random.nextInt(100) <= chance || OEN.equals("Dodge")
								|| OEN.equals("Immortal") || OEN.equals("Phoenix")
								) {
							//
							if(OEN.equals("Aegis") && damager instanceof Player) Aegis(event, (Player) damager, level);
							else if(OEN.equals("Angelic")) { Angelic(victim, level);
							} else if(OEN.equals("Armored") && damager instanceof Player) { Armored(victim, (Player) damager, level, event);
							} else if(OEN.equals("ArrowDeflect") && damager.getType().name().contains("ARROW")) { ArrowDeflect(event, victim, level);
							} else if(OEN.equals("Cactus")) { Cactus(damager, level);
							} else if(OEN.equals("CreeperArmor") && event.getDamager().getType().equals(EntityType.CREEPER) || OEN.equals("CreeperArmor") && event.getDamager().getType().equals(EntityType.PRIMED_TNT)) { CreeperArmor(event, victim, level);
							} else if(OEN.equals("Curse") && victim.getHealth() <= doubles[10]) { Curse(victim, level);
							} else if(OEN.equals("Deathbringer")) { Deathbringer(victim, level);
							} else if(OEN.equals("DeathGod"))     { DeathGod(event, level);
							} else if(OEN.equals("Dodge")) {
								if(victim.isSneaking()) { chance = getProckChance(enchantments.getString("chances." + OEN + ".if-sneaking"), level, victim);
								} else { chance = getProckChance(enchantments.getString("chances." + OEN + ".not-sneaking"), level, victim); }
								if(random.nextInt(100) <= chance) { Dodge(victim, event); }
							} else if(OEN.equals("EnderShift")) { EnderShift(victim, level);
							} else if(OEN.equals("Enlighted")) { Enlighted(victim);
							} else if(OEN.equals("Guardians")) { Guardians(event, victim, level);
							} else if(OEN.equals("Immortal") && !(item == null) && souls - 5 - level >= 0) {
								Immortal(victim, ap);
								depleteSouls(victim, item, souls, 5 + (random.nextInt(level * 4)));
								victim.updateInventory();
							} else if(OEN.equals("Inversion")) { Inversion(event, victim, level);
							} else if(OEN.equals("Molten")) { Molten(damager, level);
							} else if(OEN.equals("NaturesWrath") && !(item == null) && souls - 75 >= 0) {
								NaturesWrath(event, victim, level);
								depleteSouls(victim, item, souls, 75);
							} else if(OEN.equals("Phoenix") && !(item == null) && victim.getHealth() - event.getFinalDamage() <= 0.00 && souls - phoenix >= 0 && phoeniX == false) {
								phoeniX = true;
								Phoenix(event, victim, level);
								depleteSouls(victim, item, souls, phoenix);
							} else if(OEN.equals("Poisoned")) { Poisoned(damager, level);
							} else if(OEN.equals("Ragdoll")) { Ragdoll(victim);
							} else if(OEN.equals("Shockwave") && event.getDamager() instanceof Player) { Shockwave((Player) event.getDamager(), level);
							} else if(OEN.equals("SmokeBomb")) { SmokeBomb(damager, level);
							} else if(OEN.equals("SpiritLink")) { SpiritLink(victim, level);
							} else if(OEN.equals("Spirits")) { Spirits(victim, level);
							} else if(OEN.equals("Stormcaller")) { Stormcaller(damager);
							} else if(OEN.equals("RocketEscape")) { RocketEscape(victim, level);
							} else if(OEN.equals("Tank") && event.getDamager() instanceof Player) { Tank(victim, (Player) event.getDamager(), level, event);
							} else if(OEN.equals("Valor") && event.getDamager() instanceof Player && ((Player) event.getDamager()).getInventory().getItemInHand().getType().name().contains("SWORD")) { Valor(event, level);
							} else if(OEN.equals("Voodoo")) { Voodoo(damager, level);
							} else if(OEN.equals("Wither")) { Wither(damager, level);
							} else if(OEN.equals("UndeadRuse")) { UndeadRuse(victim, level, damager);
							}
						}
					}
				}
			}
			if(repairguard.contains(victim.getUniqueId())) event.setDamage(event.getFinalDamage() / 2);
			// Victim's Item In Hand
			if(!(getItemInHand(victim) == null) && getItemInHand(victim).getType().equals(Material.BOW) && getItemInHand(victim).hasItemMeta() && getItemInHand(victim).getItemMeta().hasLore()) {
				for(String enchant : getEnchantmentsOnItem(getItemInHand(victim))) {
					String OEN = enchant.split(" ")[0];
					int level = Integer.parseInt(enchant.split(" ")[1]), chance = getProckChance(enchant, level, victim);
					if(random.nextInt(100) <= chance) {
						if(OEN.equals("Farcast")) { Farcast(damager, level);
						}
					}
				}
			}
		}
		// Player is shot by an arrow
		if(event.getEntity() instanceof Player && event.getDamager().getType().name().contains("ARROW") && ((Projectile) event.getDamager()).getShooter() instanceof Player) {
			Player damager = (Player) ((Projectile) event.getDamager()).getShooter(), victim = (Player) event.getEntity();
			boolean heavy = false;
			if(!(getItemInHand(victim) == null) && getItemInHand(victim).hasItemMeta() && getItemInHand(victim).getItemMeta().hasLore()) {
				item = getItemInHand(victim);
				for(String enchant : getEnchantmentsOnItem(item)) {
					String OEN = getOriginalEnchantmentName(enchant);
					int level = Integer.parseInt(enchant.split(" ")[1]), chance = getProckChance(OEN, level, victim);
					if(random.nextInt(100) <= chance) {
						if(OEN.equals("ArrowBreak") && item.getType().name().endsWith("_AXE")) { ArrowBreak(event, victim);
						}
					}
				}
			}
			for(ItemStack is : victim.getInventory().getArmorContents()) {
				if(!(is == null)) {
					for(String enchant : getEnchantmentsOnItem(is)) {
						String OEN = enchant.split(" ")[0];
						if(OEN.equals("Heavy") || OEN.equals("Marksman")) {
							int chance = getProckChance(OEN, Integer.parseInt(enchant.split(" ")[1]), victim);
							if(random.nextInt(100) <= chance) {
								if(OEN.equals("Heavy") && heavy == false) { heavy = true; Heavy(event, victim);
								} else if(OEN.equals("Marksman") && !(marksman.get(damager) == null)) { Marksman(event, marksman.get(damager)); marksman.remove(damager);
								}
							}
						}
					}
				}
			}
			heavy = false;
			if(unfocus.contains(victim.getName())) { event.setDamage(event.getDamage() * 0.5); }
			if(bowsp.contains(damager)) {
				for(int i = 0; i < bowsp.size(); i++) {
					if(bowsp.get(i) == damager.getUniqueId()) {
						for(String enchant : getEnchantmentsOnItem(bows.get(i))) {
							String OEN = enchant.split(" ")[0];
							int level = Integer.parseInt(enchant.split(" ")[1]), chance = getProckChance(OEN, level, damager);
							if(random.nextInt(100) <= chance) {
								if(OEN.equals("ArrowLifesteal")) { ArrowLifesteal(victim, damager);
								} else if(OEN.equals("Longbow") && !(getItemInHand(victim) == null) && getItemInHand(victim).getType().equals(Material.BOW)) { event.setDamage(event.getDamage() + level);
								} else if(OEN.equals("Piercing")) { Piercing(event, level);
								} else if(OEN.equals("Snare")) { Snare(victim, level);
								} else if(OEN.equals("Sniper")) { Sniper(event, damager, victim, level);
							} else if(OEN.equals("Unfocus") && !(unfocus.contains(victim.getName()))) {
									unfocus.add(victim.getName());
									sendStringListMessage(victim.getUniqueId(), cemessages.getStringList("enchants.Unfocus"), 0);
									playSound(sounds, "enchants.Unfocus", victim, victim.getLocation(), false);
									playSound(sounds, "enchants.Unfocus", damager, damager.getLocation(), false);
									Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
										public void run() {
											if(victim.isOnline()) {
												unfocus.remove(victim.getName());
												sendStringListMessage(victim.getUniqueId(), cemessages.getStringList("enchants.Refocus"), 0);
											}
										}
									}, 20 * level * 2);
								}
							}
						}
					}
				}
			}
			return;
		}
		//
		if(event.getEntity() instanceof LivingEntity && !(event.getEntity().getCustomName() == null) && event.getEntity().getType().equals(EntityType.IRON_GOLEM) && event.getDamager().getType().name().endsWith("ARROW") && ((Projectile) event.getDamager()).getShooter() instanceof Player && bowsp.contains((Player) ((Projectile) event.getDamager()).getShooter())) {
			for(int i = 0; i < bowsp.size(); i++) {
				if(bowsp.get(i) == ((Player) ((Projectile) event.getDamager()).getShooter()).getUniqueId()) {
					for(String enchant : getEnchantmentsOnItem(bows.get(i))) {
						String OEN = enchant.split(" ")[0];
						if(OEN.equals("Hijack") && random.nextInt(100) <= getProckChance(OEN, Integer.parseInt(enchant.split(" ")[1]), (Player) ((Projectile) event.getDamager()).getShooter())) {
							for(Player players : Bukkit.getWorld(event.getEntity().getWorld().getName()).getPlayers()) {
								if(ChatColor.stripColor(event.getEntity().getCustomName()).contains(players.getName())) {
									Hijack((IronGolem) event.getEntity(), (Player) (((Projectile) event.getDamager()).getShooter()), players);
									return;
								}
							}
						}
					}
				}
			}
		} else { return; }
	}
	//
	//@EventHandler
	private void playerDropItemEvent(PlayerDropItemEvent event) {
		if(!(event.isCancelled()) && soulmode.contains(event.getPlayer().getUniqueId()) && event.getItemDrop().getItemStack().hasItemMeta() && event.getItemDrop().getItemStack().getItemMeta().hasLore() && event.getItemDrop().getItemStack().getItemMeta().getLore().equals(items.getSound("soul-gem").getItemMeta().getLore())) {
			soulmode.remove(event.getPlayer().getUniqueId());
			sendStringListMessage(event.getPlayer().getUniqueId(), messages.getStringList("Givedp.soul-mode.deactivate.dropped"), 0);
		}
	}
	//@EventHandler(priority = EventPriority.HIGHEST)
	private void blockBreakEvent(BlockBreakEvent event) {
		if(event.isCancelled() || getItemInHand(event.getPlayer()) == null || !(getItemInHand(event.getPlayer()).hasItemMeta()) || !(getItemInHand(event.getPlayer()).getItemMeta().hasLore()) || event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) { return;
		} else {
			Player player = (Player) event.getPlayer();
			for(String enchant : getEnchantmentsOnItem(getItemInHand(event.getPlayer()))) {
				String OEN = enchant.split(" ")[0];
				int level = Integer.parseInt(enchant.split(" ")[1]), chance = getProckChance(OEN, level, player);
				if(random.nextInt(100) <= chance) {
					if(OEN.equals("AutoSmelt")) { AutoSmelt(event, event.getPlayer(), event.getBlock().getType(), level);
					} else if(OEN.equals("Detonate")) { Detonate(event, player, event.getBlock().getType());
					} else if(OEN.equals("Experience")) { Experience(event, player, level);
					} else if(OEN.equals("Oxygenate")) { Oxygenate(player);
					} else if(OEN.equals("Telepathy")) {
						if(!(event.getBlock().getType().equals(Material.MOB_SPAWNER))) {
							for(ItemStack is : event.getBlock().getDrops()) giveItem(player, is);
							event.setCancelled(true);
							event.getBlock().setType(Material.AIR);
						}
					}
				}
			}
			return;
		}
	}
	//@EventHandler
	private void playerItemHeldEvent(PlayerItemHeldEvent event) {
		if(divineimmolation.contains(event.getPlayer().getUniqueId())) divineimmolation.remove(event.getPlayer().getUniqueId());
		if(soulmode.contains(event.getPlayer().getUniqueId())
				&& event.getPlayer().getInventory().getItem(event.getNewSlot()) != null
				&& event.getPlayer().getInventory().getItem(event.getNewSlot()).getType().name().endsWith("SWORD")) {
			for(String enchant : getEnchantmentsOnItem(event.getPlayer().getInventory().getItem(event.getNewSlot()))) {
				if(enchant.split(" ")[0].equals("DivineImmolation")) {
					divineimmolation.add(event.getPlayer().getUniqueId());
					return;
				}
			}
		}
	}
	private void DivineImmolation(EntityDamageByEntityEvent event, int level) {
		event.getEntity().setFireTicks(level * 20 + 40);
		event.setDamage(event.getDamage() * (1 + (level * 0.65)));
		for(Entity entity : event.getEntity().getNearbyEntities(level * 1.5, level * 1.5, level * 1.5)) {
			if(!(entity instanceof Player) || entity instanceof Player && !(entity == event.getDamager()) && fapi.relationIsEnemyOrNull((Player) event.getDamager(), (Player) entity)) {
				sendStringListMessage(entity.getUniqueId(), cemessages.getStringList("enchants.DivineImmolation"), 0);
				entity.setFireTicks(level * 20 + 40);
				if(entity instanceof Player) {
					((Damageable) entity).damage(level * 1.5);
				} else if(entity instanceof Damageable) {
					((Damageable) entity).damage(level * 3.5);
				}
			}
		}
	}
	private void Immortal(Player victim, ItemStack is) { is.setDurability((short) (is.getDurability() - 1)); }
	private void NaturesWrath(EntityDamageByEntityEvent event, Player victim, int level) {
		for(Entity entity : victim.getNearbyEntities(45, 45, 45)) {
			if(entity instanceof Player && fapi.relationIsEnemyOrNull(victim, (Player) entity)
					|| !(entity instanceof Player) && entity instanceof Damageable) {
				for(int i = 0; i <= 11; i++) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
						public void run() {
							sendStringListMessage(victim.getUniqueId(), cemessages.getStringList("enchants.NaturesWrath"), 0);
							if(!(entity.isDead())) {
								sendStringListMessage(entity.getUniqueId(), cemessages.getStringList("enchants.NaturesWrath"), 0);
								victim.getWorld().strikeLightning(entity.getLocation());
							}
						}
					}, 20 * i);
				}
				if(entity instanceof Player) {
					final Player Player = (Player) entity;
					Player.setWalkSpeed(0f);
					Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
						public void run() {
							Player.setWalkSpeed(0.2f);
						}
					}, 20 * 11);
				}
			}
		}
	}
	//@EventHandler
	private void playerJoinEvent(PlayerJoinEvent event) {
		if(event.getPlayer().getWalkSpeed() != 0.2f) {
			event.getPlayer().setWalkSpeed(0.2f);
		}
	}
	private void Phoenix(EntityDamageByEntityEvent event, Player victim, int level) {
		if(!(phoenix.contains(victim))) {
			phoenix.add(victim.getUniqueId());
			event.setCancelled(true);
			victim.setHealth(victim.getMaxHealth());
			playSound(sounds, "enchants.Phoenix", victim, victim.getLocation(), true);
			Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
				public void run() {
					phoenix.remove(victim);
				}
			}, 20 * 60 * 10 - (20 * 60 * level));
		}
	}
	//@EventHandler(priority = EventPriority.HIGHEST)
	private void projectileLaunchEvent(ProjectileLaunchEvent event) {
		if(event.getEntityType().equals(EntityType.ENDER_PEARL) && event.getEntity().getShooter() instanceof Player && Teleblock.contains(event.getEntity().getShooter())) {
			event.setCancelled(true);
			giveItem((Player) event.getEntity().getShooter(), new ItemStack(Material.ARROW, 1));
			((Player) event.getEntity().getShooter()).updateInventory();
		}
	}
	private void Teleblock(Player victim, int level) {
		if(!(Teleblock.contains(victim))) {
			Teleblock.add(victim.getUniqueId());
			Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() { public void run() { Teleblock.remove(victim); } }, 20 * level + 100);
		}
		if(!(victim.getInventory().first(Material.ENDER_PEARL) == -1)) {
			removeItem(victim, new ItemStack(Material.ENDER_PEARL, 1), level + random.nextInt(12));
		}
	}
	//
	private void depleteSouls(Player player, ItemStack is, int currentSouls, int depleteBy) {
		if(currentSouls - depleteBy <= 0) {
			sendStringListMessage(player.getUniqueId(), messages.getStringList("Givedp.soul-mode.deactivate.ran-out"), 0);
			depleteBy = currentSouls;
			soulmode.remove(player.getUniqueId());
		}
		itemMeta = is.getItemMeta();
		itemMeta.setDisplayName(itemMeta.getDisplayName().replace("" + currentSouls, "" + (currentSouls - depleteBy)));
		is.setItemMeta(itemMeta);
		return;
	}
	private ItemStack getSoulGemInPlayerInventory(Player player, boolean checkWholeInv) {
		int size = 0;
		if(checkWholeInv) { size = player.getInventory().getSize(); } else { size = 9; }
		for(int i = 0; i < size; i++) {
			item = player.getInventory().getItem(i);
			if(item != null && !(item.getType().equals(Material.AIR)) && item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().hasLore() && item.getItemMeta().getLore().equals(items.getSound("soul-gem").getItemMeta().getLore())) {
				return item;
			}
		}
		return null;
	}
	//
	//@EventHandler
	private void entityShootBowEvent(EntityShootBowEvent event) {
		if(event.isCancelled()
				|| !(event.getEntity() instanceof Player)
				|| !(Bukkit.getPluginManager().getPlugin("WorldEdit") == null) && !(Bukkit.getPluginManager().getPlugin("WorldGuard") == null)
				&& RP_WorldEditWorldGuard.getInstance().locationHasPvPAllowed(event.getEntity().getWorld(), event.getProjectile().getLocation()) == false) { return;
		} else {
			Player player = (Player) event.getEntity();
			if(event.getBow().hasItemMeta() && event.getBow().getItemMeta().hasLore()) {
				for(String enchant : getEnchantmentsOnItem(event.getBow())) {
					String OEN = enchant.split(" ")[0];
					int level = Integer.parseInt(enchant.split(" ")[1]), chance = getProckChance(OEN, Integer.parseInt(enchant.split(" ")[1]), player);
					if(random.nextInt(100) <= chance || OEN.equals("Teleblock")) {
						if(OEN.equals("Cowification")) {
							//Cowification(event);
						} else if(OEN.equals("Teleblock") && soulmode.contains(player.getUniqueId())) {
							item = getSoulGemInPlayerInventory(player, false);
							depleteSouls(player, item, getRemainingInt(item.getItemMeta().getDisplayName()), level * 6);
							teleblock.put(player, event.getBow());
						}
					}
				}
			}
			double marksmanDamage = 0.00;
			for(ItemStack is : player.getInventory().getArmorContents()) {
				if(!(is == null) && is.hasItemMeta() && is.getItemMeta().hasLore()) {
					for(String enchant : getEnchantmentsOnItem(is)) {
						String OEN = enchant.split(" ")[0];
						int level = Integer.parseInt(enchant.split(" ")[1]);
						if(OEN.equals("Marksman")) { marksmanDamage += level * 0.25;
						}
					}
				}
			}
			if(!(marksmanDamage == 0.00)) {
				marksman.put(player, marksmanDamage);
			}
			bows.add(event.getBow());
			bowsp.add(event.getEntity().getUniqueId());
			return;
		}
	}
	//@EventHandler
	private void projectileHitEvent(ProjectileHitEvent event) {
		if(event.getEntity().getShooter() instanceof Player && event.getEntity().getType().name().contains("ARROW")) {
			Player shooter = (Player) event.getEntity().getShooter();
			Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
				public void run() {
					Entity h = null;
					if(!(event.getEntity().getNearbyEntities(0.0, 0.0, 0.0).size() == 0))
						h = event.getEntity().getNearbyEntities(0.0, 0.0, 0.0).get(0);
					if(!(shooter == null) && bowsp.contains(shooter.getUniqueId())) {
						for(int i = 0; i < bowsp.size(); i++) {
							if(bowsp.get(i) == shooter.getUniqueId()) {
								for(String enchant : getEnchantmentsOnItem(bows.get(i))) {
									String OEN = enchant.split(" ")[0];
									int level = Integer.parseInt(enchant.split(" ")[1]), chance = getProckChance(OEN, level, shooter);
									if(random.nextInt(100) <= chance
											|| OEN.equals("Teleblock") && soulmode.contains(shooter.getUniqueId()) && !(teleblock.get(shooter) == null) && event.getEntity().getNearbyEntities(0.00, 0.00, 0.00).size() >= 1 && event.getEntity().getNearbyEntities(0.00, 0.00, 0.00).get(0) instanceof Player) {
										if(OEN.equals("EagleEye") && !(h == null) && h instanceof Player) EagleEye((Player) h);
										else if(OEN.equals("DimensionRift")) DimensionRift(h, level);
										else if(OEN.equals("Explosive"))     Explosive(event.getEntity().getLocation(), event.getEntity());
										else if(OEN.equals("Hellfire"))      Hellfire(event.getEntity(), shooter, level);
										else if(OEN.equals("Lightning"))     Lightning(event.getEntity().getLocation(), event.getEntity());
										else if(OEN.equals("Teleblock"))     Teleblock((Player) event.getEntity().getNearbyEntities(0.00, 0.00, 0.00).get(0), level);
									}
								}
								marksman.remove(shooter);
								teleblock.remove(shooter);
								bowsp.remove(i);
								bows.remove(i);
								return;
							}
						}
					}
				}
			}, 1);
		}
	}
	
	//@EventHandler
	private void entityTargetLivingEntityEvent(EntityTargetLivingEntityEvent event) {
		if(!(event.getEntity().getCustomName() == null) && event.getTarget() instanceof Player)
			if(event.getEntityType().equals(EntityType.IRON_GOLEM) && event.getEntity().getCustomName().equals(guardiansname.replace("{PLAYER}", event.getTarget().getName()))
					|| event.getEntityType().equals(EntityType.ZOMBIE) && event.getEntity().getCustomName().equals(undeadrusename.replace("{PLAYER}", event.getTarget().getName()))
					|| event.getEntityType().equals(EntityType.BLAZE) && event.getEntity().getCustomName().equals(spiritsname.replace("{PLAYER}", event.getTarget().getName())))
				event.setCancelled(true);
	}
	private void Aegis(EntityDamageByEntityEvent event, Player damager, int level) {
		if(!(aegis.keySet().contains(event.getEntity()))) aegis.put((Player) event.getEntity(), new ArrayList<Player>());
		if(!(aegis.get(event.getEntity()).isEmpty()) && aegis.get(event.getEntity()).contains(damager)
				&& (8 - level) >= aegis.get(event.getEntity()).size()) {
			event.setDamage(event.getDamage() * doubles[2]);
		} else {
			aegis.get(event.getEntity()).add(damager);
			Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
				public void run() {
					aegis.get(event.getEntity()).remove(damager);
				}
			}, 20 * 10);
		}
	}
	private void Angelic(Player victim, int level) { givePotionEffects(null, victim, "Angelic", level); }
	private void ArrowBreak(EntityDamageByEntityEvent event, Player victim) {
		event.setCancelled(true); event.getDamager().remove();
		sendStringListMessage(victim.getUniqueId(), cemessages.getStringList("enchants.ArrowBreak"), 0);
		playSound(sounds, "enchants.ArrowBreak", victim, victim.getLocation(), true);
	}
	private void ArrowDeflect(EntityDamageByEntityEvent event, Player victim, int level) {
		if(arrowdeflect.contains(victim.getName() + level)) {
			event.setCancelled(true);
		} else {
			arrowdeflect.add(victim.getName() + level);
			Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
				public void run() {
					arrowdeflect.remove(victim.getName() + level);
				}
			}, 40 - (int) (0.4 * level));
		}
	}
	private void ArrowLifesteal(Player victim, Player damager) {
		if(victim.getHealth() - 1 <= 0.00) { victim.setHealth(0); } else { victim.setHealth(victim.getHealth() - 1); }
		damager.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20, 2));
		playSound(sounds, "enchants.ArrowLifesteal", damager, damager.getLocation(), false);
	}
	private void Armored(Player victim, Player damager, int level, EntityDamageByEntityEvent event) {
		if(getItemInHand(damager) == null || !(getItemInHand(damager).getType().name().endsWith("_SWORD"))) { return; }
		double dividedDamage = 0.0;
		for(ItemStack item : victim.getInventory().getArmorContents()) {
			for(String enchant : getEnchantmentsOnItem(item)) {
				if(enchant.split(" ")[0].equals("Armored")) {
					dividedDamage = dividedDamage + 1.85 * level;
				}
			}
		}
		event.setDamage(event.getDamage() / dividedDamage);
	}
	private void Assassin(EntityDamageByEntityEvent event, int level) {
		if(event.getEntity().getLocation().distance(event.getDamager().getLocation()) <= doubles[3]) {
			event.setDamage(event.getDamage() * (1 + (level * doubles[4])));
		} else {
			event.setDamage(event.getDamage() * doubles[5]);
		}
	}
	private void AutoSmelt(BlockBreakEvent event, Player player, Material material, int level) {
		if(material.equals(Material.GOLD_ORE) || material.equals(Material.IRON_ORE)) {
			event.setCancelled(true);
			event.getBlock().setType(Material.AIR);
			for(Enchantment enchant : getItemInHand(player).getEnchantments().keySet()) {
				if(enchant.equals(Enchantment.LOOT_BONUS_BLOCKS)) {
					int fortune = random.nextInt(getItemInHand(player).getEnchantmentLevel(enchant) + 1);
					if(fortune == 0) { fortune = 1; }
					level = level * fortune;
				}
			}
			giveItem(player, new ItemStack(Material.getMaterial(material.name().replace("ORE", "INGOT")), level, (byte) 0));
			playSound(sounds, "enchants.AutoSmelt", player, event.getBlock().getLocation(), true);
		}
	}
	private void Barbarian(EntityDamageByEntityEvent event, int level, Player player) {
		if(event.getEntity() instanceof Player && !(getItemInHand((Player) event.getEntity()) == null) && getItemInHand((Player) event.getEntity()).getType().name().endsWith("_AXE")) {
			event.setDamage(event.getDamage() * (1.10 + (level * 0.05)));
			playSound(sounds, "enchants.Barbarian", player, player.getLocation(), true);
		}
	}
	private void Berserk(Player player, int level) { givePotionEffects(null, player, "Berserk", level); }
	private void Blacksmith(EntityDamageByEntityEvent event, Player damager, int level) {
		event.setDamage(event.getDamage() * 0.5);
		int amount = random.nextInt(2) + 1;
		ItemStack piece = new ItemStack(Material.LEATHER_BOOTS, 1);
		for(ItemStack is : damager.getInventory().getArmorContents()) if(!(is == null) && is.getDurability() > piece.getDurability()) piece = is;
		piece.setDurability((short) (piece.getDurability() - amount < 0 ? 0 : piece.getDurability() - amount));
		sendStringListMessage(damager.getUniqueId(), cemessages.getStringList("enchants.Blacksmith"), 0);
		playSound(sounds, "enchants.Blacksmith", damager, damager.getLocation(), true);
		damager.updateInventory();
	}
	public void Blessed(Player player) {
		for(PotionEffect pe : player.getActivePotionEffects()) {
			PotionEffectType type = pe.getType();
			if(type.equals(PotionEffectType.BLINDNESS)
					|| type.equals(PotionEffectType.CONFUSION)
					|| type.equals(PotionEffectType.HARM)
					|| type.equals(PotionEffectType.HUNGER)
					|| type.equals(PotionEffectType.POISON)
					|| type.equals(PotionEffectType.SLOW)
					|| type.equals(PotionEffectType.SLOW_DIGGING)
					|| type.equals(PotionEffectType.WEAKNESS)
					|| type.equals(PotionEffectType.WITHER)
					|| !v.contains("1.8") && type.equals(PotionEffectType.getByName("UNLUCK"))
					|| !v.contains("1.8") && type.equals(PotionEffectType.getByName("LEVITATION"))
					|| !v.contains("1.8") && type.equals(PotionEffectType.getByName("GLOWING"))) {
				player.removePotionEffect(type);
			}
		}
		sendStringListMessage(player.getUniqueId(), cemessages.getStringList("enchants.Blessed"), 0);
	}
	private void Blind(EntityDamageByEntityEvent event, int level) {
		((LivingEntity) event.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * level + 40, level - 1));
	}
	private void BloodLink(Player player) {
		int randomhealth = random.nextInt(2) + 1;
		if(player.getHealth() + randomhealth <= player.getMaxHealth()) { player.setHealth(player.getHealth() + randomhealth); }
		sendStringListMessage(player.getUniqueId(), cemessages.getStringList("enchants.BloodLink"), randomhealth);
	}
	private void Cactus(LivingEntity damager, int level) { damager.damage(level); }
	private void Cleave(Player damager, LivingEntity victim, int level, EntityDamageByEntityEvent event) {
		playSound(sounds, "enchants.Cleave", damager, damager.getLocation(), true);
		String[] area = enchantments.getString("Cleave.area").replace("level", "" + level).replace("\\s", "").replace(" ", "").split("\\:");
		for(Entity entity : victim.getNearbyEntities(evaluate(area[0]), evaluate(area[1]), evaluate(area[2]))) {
			if(!(entity instanceof Player) && entity instanceof Damageable || entity instanceof Player && fapi.relationIsEnemyOrNull(damager, (Player) entity) == true) {
				((Damageable) entity).damage(event.getFinalDamage() * 0.25);
				if(getItemInHand(damager).getItemMeta().hasEnchant(Enchantment.FIRE_ASPECT) && random.nextInt(100) <= enchantments.getInt("Cleave.fire-aspect")) { entity.setFireTicks(10 * level); }
			}
		}
	}
	private void Commander(Player player, int level) {
		player.getWorld().playEffect(new Location(player.getWorld(), player.getEyeLocation().getX(), player.getEyeLocation().getY() + 0.5, player.getEyeLocation().getZ()), Effect.STEP_SOUND, Material.DIAMOND_BLOCK);
		String[] string = enchantments.getString("Commander.area").toLowerCase().replace("level", "" + level).replace(" ", "").split("\\:");
		for(Entity entity : player.getNearbyEntities(evaluate(string[0]), evaluate(string[1]), evaluate(string[2]))) {
			if(entity instanceof Player && fapi.relationIsAlly(player, (Player) entity) == true) {
				givePotionEffects(null, (Player) entity, "Commander", level);
			}
		}
	}
	private void Confusion(Player victim, int level) { givePotionEffects(null, victim, "Confusion", level); }
	
	private void Cowification(EntityShootBowEvent event) {
		if(event.getForce() == (float) 1.0 && event.getEntity() instanceof Player) {
			event.getProjectile().setCustomName("CowificationArrow"); event.getProjectile().setCustomNameVisible(false);
			Cow cow = event.getProjectile().getWorld().spawn(event.getProjectile().getLocation().add(0.0, 1.25, 0.0), Cow.class);
			cow.setAgeLock(true); cow.setVelocity(event.getProjectile().getVelocity()); cow.setCustomName("CowificationCow"); cow.setCustomNameVisible(false);
		}
	}
	private void CreeperArmor(EntityDamageByEntityEvent event, Player victim, int level) {
		event.setDamage(event.getDamage() / level);
		event.setCancelled(true);
	}
	private void Curse(Player victim, int level) {
		givePotionEffects(victim, null, "Curse", level);
	}
	private void Deathbringer(Player victim, int level) { givePotionEffects(null, victim, "Deathbringer", level); }
	private void DeathGod(EntityDamageByEntityEvent event, int level) {
		Player victim = (Player) event.getEntity();
		if(victim.getHealth() <= level + 4) {
			event.setDamage(0.00);
			victim.setHealth(victim.getHealth() + 5);
		}
	}
	//@EventHandler
	private void playerDeathEvent(PlayerDeathEvent event) {
		if(bowsp.contains(event.getEntity())) { for(int i = 0; i < bowsp.size(); i++) { if(bowsp.get(i).equals(event.getEntity())) bowsp.remove(i); bows.remove(i); } }
		//
		if(soulmode.contains(event.getEntity().getUniqueId())) soulmode.remove(event.getEntity().getUniqueId());
		if(divineimmolation.contains(event.getEntity().getUniqueId())) divineimmolation.remove(event.getEntity().getUniqueId());
		//
		if(event.getEntity().getKiller() instanceof Player && !(getItemInHand(event.getEntity().getKiller()) == null) && getItemInHand(event.getEntity().getKiller()).hasItemMeta() && getItemInHand(event.getEntity().getKiller()).getItemMeta().hasLore()) {
			item = getItemInHand(event.getEntity().getKiller());
			for(String enchant : getEnchantmentsOnItem(item)) {
				String OEN = enchant.split(" ")[0];
				if(OEN.equals("Decapitation") || OEN.equals("Lifebloom")) {
					int level = Integer.parseInt(enchant.split(" ")[1]), chance = getProckChance(OEN, level, event.getEntity());
					if(random.nextInt(100) <= chance) {
						if(OEN.equals("Decapitation") || OEN.equals("Headless")) { DecapitationHeadless(event.getEntity());
						} else if(OEN.equals("Lifebloom")) { Lifebloom(event.getEntity(), level);
						}
					}
				}
			}
		}
	}
	private void DecapitationHeadless(Player victim) {
		item = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta skullMeta = (SkullMeta) item.getItemMeta();
		skullMeta.setOwner(victim.getName());
		item.setItemMeta(skullMeta);
		victim.getWorld().dropItem(victim.getLocation(), item);
	}
	private void DeepWounds(EntityDamageByEntityEvent event, Player victim, int level) { givePotionEffects(null, victim, "DeepWounds", level); }
	private void Demonforged(Player victim, int level, int chance) {
		for(ItemStack item : victim.getInventory().getArmorContents()) {
			if(!(item == null)) {
				item.setDurability((short) (item.getDurability() - Math.subtractExact(chance, 9)));
			}
		}
		playSound(sounds, "enchants.Demonforged", victim, victim.getLocation(), false);
	}
	private void Detonate(BlockBreakEvent event, Player player, Material material) {
		for(int i = 1; i <= 7; i++) {
			Location loc = new Location(event.getPlayer().getWorld(), 0.0, 0.0, 0.0);
			if(i == 1) { material = event.getBlock().getType(); loc = event.getBlock().getLocation();
			} else if(i == 2) {
				material = event.getBlock().getWorld().getBlockAt(event.getBlock().getLocation().getBlockX(), event.getBlock().getLocation().getBlockY() + 1, event.getBlock().getLocation().getBlockZ()).getType();
				loc = new Location(event.getBlock().getWorld(), event.getBlock().getLocation().getBlockX(), event.getBlock().getLocation().getBlockY() + 1, event.getBlock().getLocation().getBlockZ());
			} else if(i == 3) {
				material = event.getBlock().getWorld().getBlockAt(event.getBlock().getLocation().getBlockX(), event.getBlock().getLocation().getBlockY() - 1, event.getBlock().getLocation().getBlockZ()).getType();
				loc = new Location(event.getBlock().getWorld(), event.getBlock().getLocation().getBlockX(), event.getBlock().getLocation().getBlockY() - 1, event.getBlock().getLocation().getBlockZ());
			} else if(i == 4) {
				material = event.getBlock().getWorld().getBlockAt(event.getBlock().getLocation().getBlockX() + 1, event.getBlock().getLocation().getBlockY(), event.getBlock().getLocation().getBlockZ()).getType();
				loc = new Location(event.getBlock().getWorld(), event.getBlock().getLocation().getBlockX() + 1, event.getBlock().getLocation().getBlockY(), event.getBlock().getLocation().getBlockZ());
			} else if(i == 5) {
				material = event.getBlock().getWorld().getBlockAt(event.getBlock().getLocation().getBlockX() - 1, event.getBlock().getLocation().getBlockY(), event.getBlock().getLocation().getBlockZ()).getType();
				loc = new Location(event.getBlock().getWorld(), event.getBlock().getLocation().getBlockX() - 1, event.getBlock().getLocation().getBlockY(), event.getBlock().getLocation().getBlockZ());
			} else if(i == 6) {
				material = event.getBlock().getWorld().getBlockAt(event.getBlock().getLocation().getBlockX(), event.getBlock().getLocation().getBlockY(), event.getBlock().getLocation().getBlockZ() + 1).getType();
				loc = new Location(event.getBlock().getWorld(), event.getBlock().getLocation().getBlockX(), event.getBlock().getLocation().getBlockY(), event.getBlock().getLocation().getBlockZ() + 1);
			} else if(i == 7) {
				material = event.getBlock().getWorld().getBlockAt(event.getBlock().getLocation().getBlockX(), event.getBlock().getLocation().getBlockY(), event.getBlock().getLocation().getBlockZ() - 1).getType();
				loc = new Location(event.getBlock().getWorld(), event.getBlock().getLocation().getBlockX(), event.getBlock().getLocation().getBlockY(), event.getBlock().getLocation().getBlockZ() - 1); }
			if(detonate.contains(material.name())) { return; }
			loc.getWorld().getBlockAt(loc).breakNaturally();
			loc.getWorld().playEffect(event.getBlock().getLocation(), Effect.EXPLOSION_LARGE, 1);
			playSound(sounds, "enchants.Detonate", player, event.getBlock().getLocation(), true);
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "execute " + event.getPlayer().getName() + " ~ ~ ~ particle flame " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ() + " 0.25 0.5 0.25 0 25");
		}
	}
	private void DimensionRift(Entity entity, int level) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				Location loc = new Location(entity.getWorld(), entity.getLocation().getBlockX(), entity.getLocation().getBlockY() - 1, entity.getLocation().getBlockZ()),
						loc2 = new Location(entity.getWorld(), entity.getLocation().getBlockX(), entity.getLocation().getBlockY(), entity.getLocation().getBlockZ());
				Material prev = entity.getWorld().getBlockAt(loc).getType(), prev2 = entity.getWorld().getBlockAt(loc2).getType();
				boolean did = false;
				if(random.nextInt(100) <= doubles[13]) did = true;
				entity.getWorld().getBlockAt(loc).setType(Material.SOUL_SAND);
				if(did) entity.getWorld().getBlockAt(loc2).setType(Material.WEB);
				final boolean o = did;
				Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
					public void run() {
						entity.getWorld().getBlockAt(loc).setType(Material.AIR);
						entity.getWorld().getBlockAt(loc).setType(prev);
						if(o) {
							entity.getWorld().getBlockAt(loc2).setType(Material.AIR);
							entity.getWorld().getBlockAt(loc2).setType(prev2);
						}
					}
				}, (int) (20 * doubles[14]));
			}
		}, 20 * 1);
	}
	private void Diminish(EntityDamageByEntityEvent event) {
		
	}
	private void Disarmor(Player victim) {
		int randomarmor = random.nextInt(4);
		if(randomarmor == 0 && !(victim.getInventory().getHelmet() == null) && !(victim.getInventory().getHelmet().getType().equals(Material.AIR))) {
			item = victim.getInventory().getHelmet(); victim.getInventory().setHelmet(new ItemStack(Material.AIR));
		} else if(randomarmor == 1 && !(victim.getInventory().getChestplate() == null) && !(victim.getInventory().getChestplate().getType().equals(Material.AIR))) {
			item = victim.getInventory().getChestplate(); victim.getInventory().setChestplate(new ItemStack(Material.AIR));
		} else if(randomarmor == 2 && !(victim.getInventory().getLeggings() == null) && !(victim.getInventory().getLeggings().getType().equals(Material.AIR))) {
			item = victim.getInventory().getLeggings(); victim.getInventory().setLeggings(new ItemStack(Material.AIR));
		} else if(randomarmor == 3 && !(victim.getInventory().getBoots() == null) && !(victim.getInventory().getBoots().getType().equals(Material.AIR))) {
			item = victim.getInventory().getBoots(); victim.getInventory().setBoots(new ItemStack(Material.AIR));
		} else { return; }
		if(item.hasItemMeta() && item.getItemMeta().hasLore()) {
			for(String enchant : getEnchantmentsOnItem(item)) {
				String OEN = enchant.split(" ")[0];
				if(potioneffectEnchants.contains(OEN))
					potionEffects(victim, OEN, true);
			}
		}
		playSound(sounds, "enchants.Disarmor", victim, victim.getLocation(), false);
		sendStringListMessage(victim.getUniqueId(), cemessages.getStringList("enchants.Disarmor"), 0);
		giveItem(victim, item);
	}
	private void Dodge(Player victim, EntityDamageByEntityEvent event) {
		event.setCancelled(true);
		sendStringListMessage(victim.getUniqueId(), cemessages.getStringList("enchants.Dodge"), 0);
		playSound(sounds, "enchants.Dodge", victim, victim.getLocation(), true);
	}
	private void Dominate(EntityDamageByEntityEvent event, int level) {
		dominate.add(event.getEntity().getUniqueId());
		int j = dominatee.size();
		dominatee.add(level);
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				dominate.remove(event.getEntity().getUniqueId());
				dominatee.remove(j);
			}
		}, (long) (20 * level * doubles[7]));
	}
	private void EagleEye(Player victim) {
		short r = (short) (1 + random.nextInt(doubles[8].intValue() - 1));
		for(ItemStack is : victim.getInventory().getArmorContents())
			if(!(is == null))
				is.setDurability((short) (is.getDurability() + r));
	}
	private void EnderShift(Player victim, int level) { givePotionEffects(null, victim, "EnderShift", level); }
	private void Enlighted(Player victim) { if(!(victim.getHealth() + 2 > victim.getMaxHealth())) { victim.setHealth(victim.getHealth() + 2); } }
	//@EventHandler
	private void EnderWalker(EntityDamageEvent event) {
		if(event.isCancelled() || !(event.getEntity() instanceof Player) || !(event.getCause().equals(DamageCause.POISON)) && !(event.getCause().equals(DamageCause.WITHER))) { return;
		} else {
			Player player = (Player) event.getEntity();
			if(!(player.getInventory().getBoots() == null)) {
				for(String enchant : getEnchantmentsOnItem(player.getInventory().getBoots())) {
					if(enchant.split(" ")[0].equals("EnderWalker")) {
						int level = Integer.parseInt(enchant.split(" ")[1]);
						event.setCancelled(true);
						if(random.nextInt(100) <= getProckChance("EnderWalker", level, player)) { if(player.getHealth() + level <= player.getMaxHealth()) { player.setHealth(player.getHealth() + level); } }
					}
				}
			}
		}
	}
	private void Enrage(EntityDamageByEntityEvent event, Player damager, int level) {
		double multiplier = (damager.getMaxHealth() - damager.getHealth()) / 3;
		if(multiplier == 0.0) { return; }
		if(multiplier < 1.0) { event.setDamage(event.getDamage() * (1.0 + multiplier));
		} else { event.setDamage(event.getDamage() * multiplier / 2); }
	}
	private void Epicness(Player victim, int level) {
		if(level == 1) { Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "execute " + victim.getName() + " ~ ~ ~ particle largesmoke " + victim.getLocation().getX() + " " + victim.getLocation().getY() + " " + victim.getLocation().getZ() + " 0.5 1 0.5 1 25 1");
		} else if(level == 2) { Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "execute " + victim.getName() + " ~ ~ ~ particle magicCrit " + victim.getLocation().getX() + " " + victim.getLocation().getY() + " " + victim.getLocation().getZ() + " 0.5 1 0.5 1 25 1");
		} else if(level == 3) { Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "execute " + victim.getName() + " ~ ~ ~ particle cloud " + victim.getLocation().getX() + " " + victim.getLocation().getY() + " " + victim.getLocation().getZ() + " 0.5 1 0.5 1 25 1"); }
	}
	private void Execute(Player player, int level) { givePotionEffects(player, null, "Execute", level); }
	private void Experience(BlockBreakEvent event, Player player, int level) {
		int mulitplier = random.nextInt(level + 1);
		if(mulitplier == 0) { mulitplier = 1; }
		event.setExpToDrop(event.getExpToDrop() * mulitplier);
	}
	private void Explosive(Location location, Projectile proj) { location.getWorld().createExplosion(location.getX(), location.getY(), location.getZ(), 2, false, false); proj.remove(); }
	private void Farcast(LivingEntity damager, int level) {
		double knockback = level * 0.25;
		damager.setVelocity(new Vector(-damager.getEyeLocation().getDirection().getX() + knockback, 0.0, -damager.getEyeLocation().getDirection().getZ() + knockback));
	}
	private void Featherweight(Player player, int level) { givePotionEffects(player, null, "Featherweight", level); }
	private void Frozen(Player victim, int level) { givePotionEffects(null, victim, "Frozen", level); }
	private void Greatsword(EntityDamageByEntityEvent event, Player victim, int level) {
		double damage = event.getDamage() * (1 + (level * 0.15));
		if(!(getItemInHand(victim) == null) && getItemInHand(victim).getType().equals(Material.BOW)) {
			if(victim.getHealth() - damage > 0)
				event.setDamage(damage);
			else
				victim.setHealth(0.0);
		}
		return;
	}
	private void Guardians(EntityDamageByEntityEvent event, Player victim, int level) {
		IronGolem ig = (IronGolem) victim.getWorld().spawnEntity(victim.getLocation(), EntityType.IRON_GOLEM);
		ig.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 99999, 4, false));
		ig.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 99999, 4, false));
		ig.setCustomName(guardiansname.replace("{PLAYER}", victim.getName()));
		if(event.getDamager() instanceof LivingEntity) ig.setTarget((LivingEntity) event.getDamager());
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				if(!(ig.isDead()))
					ig.remove();
			}
		}, 20 * 16);
	}
	// Hardened & Reforged
	//@EventHandler
	private void HardenedReforged(PlayerItemDamageEvent event) {
		if(!(event.isCancelled()) && !(event.getItem() == null) && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasLore()) {
			for(String enchant : getEnchantmentsOnItem(event.getItem())) {
				String OEN = enchant.split(" ")[0];
				if(OEN.equals("Hardened") || OEN.equals("Reforged")) {
					if(random.nextInt(100) <= getProckChance(OEN, Integer.parseInt(enchant.split(" ")[1]), event.getPlayer())) {
						event.setCancelled(true);
					}
				}
			}
		} else { return; }
	}
	private void Haste(PlayerInteractEvent event, int level) { givePotionEffects(event.getPlayer(), null, "Haste", level); }
	private void Heavy(EntityDamageByEntityEvent event, Player victim) {
		double percent = 0.02;
		for(ItemStack item : victim.getInventory().getArmorContents()) {
			for(String enchant : getEnchantmentsOnItem(item)) {
				if(enchant.split(" ")[0].equals("Heavy")) {
					percent = percent + (0.02 * Integer.parseInt(enchant.split(" ")[1]));
				}
			}
		}
		event.setDamage(event.getDamage() * (1.00 - percent));
		playSound(sounds, "enchants.Heavy", victim, victim.getLocation(), true);
	}
	private void Hijack(IronGolem g, Player player, Player gOwner) {
		if(player.getName().equalsIgnoreCase(gOwner.getName())) { return; }
		IronGolem ig = g.getWorld().spawn(g.getLocation(), IronGolem.class);
		g.remove();
		ig.setTarget(gOwner);
		ig.setCustomName(guardiansname.replace("{PLAYER}", player.getName()));
		ig.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 99999, 1, false));
		ig.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 99999, 1, false));
	}
	private void Hellfire(Projectile proj, Player player, int level) {
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "execute " + player.getName() + " " + proj.getLocation().getX() + " " + proj.getLocation().getY() + " " + proj.getLocation().getZ() + " particle flame " + proj.getLocation().getX() + " " + (proj.getLocation().getY() + 1) + " " + proj.getLocation().getZ() + " 2.5 1 2.5 0 400 0");
		proj.remove();
		for(Entity entity : player.getNearbyEntities(5, 4, 5)) {
			if(!(entity instanceof Player) || entity instanceof Player && fapi.relationIsEnemyOrNull(player, (Player) entity)) {
				entity.setFireTicks(level * 40);
			}
		}
	}
	private void IceAspect(Player victim, int level) {
		givePotionEffects(null, victim, "IceAspect", level);
		victim.getWorld().playEffect(victim.getEyeLocation(), Effect.STEP_SOUND, Material.ICE);
	}
	private void IceFreeze(Player victim, int level) {
		float walkspeed = victim.getWalkSpeed();
		victim.setWalkSpeed(0f);
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				victim.setWalkSpeed(walkspeed);
			}
		}, (int) (20 * doubles[9]));
	}
	private void Implants(Player player) {
		if(!(player.getFoodLevel() == 20)) { player.setFoodLevel(player.getFoodLevel() + 1); }
		if(random.nextInt(100) <= 2 && !(player.getHealth() + 1 > player.getMaxHealth())) { player.setHealth(player.getHealth() + 1); }
	}
	//@EventHandler(priority = EventPriority.HIGHEST)
	private void Inquisitive(EntityDeathEvent event) {
		if(event.getEntity() instanceof LivingEntity && !(event.getEntity() instanceof Player)) {
			if(event.getEntity().getKiller() instanceof Player && !(getItemInHand(event.getEntity().getKiller()) == null)) {
				item = getItemInHand(event.getEntity().getKiller());
				if(!(event.getEntity().getCustomName() == null)) {
					if(event.getEntityType().equals(EntityType.IRON_GOLEM) && ChatColor.stripColor(event.getEntity().getCustomName()).endsWith(ChatColor.stripColor(guardiansname.replace("{PLAYER}", "")))
							|| event.getEntityType().equals(EntityType.ZOMBIE) && ChatColor.stripColor(event.getEntity().getCustomName()).endsWith(ChatColor.stripColor(undeadrusename.replace("{PLAYER}", "")))
							|| event.getEntityType().equals(EntityType.BLAZE) && ChatColor.stripColor(event.getEntity().getCustomName()).endsWith(ChatColor.stripColor(spiritsname.replace("{PLAYER}", "")))) {
						event.setDroppedExp(0); event.getDrops().clear();
						return;
					}
				}
				if(event.getEntity().getKiller() instanceof Player && !(item == null) && mobRage.contains(event.getEntity().getKiller().getName())) {
					event.getEntity().setVelocity(new Vector(event.getEntity().getKiller().getLocation().getDirection().getX() * .45, 0.25, event.getEntity().getKiller().getLocation().getDirection().getZ() * .45));
					if(event.getEntity().getNearbyEntities(0.0, 0.0, 0.0).size() >= 1) {
						for(Entity entity : event.getEntity().getNearbyEntities(0.0, 0.0, 0.0)) {
							if(entity.getType().equals(event.getEntity().getType())) {
								entity.setVelocity(new Vector(-(event.getEntity().getKiller().getLocation().getDirection().getX()) * .25, 0.0, -(event.getEntity().getKiller().getLocation().getDirection().getZ()) * .25));
							}
						}
					}
				}
				//
				if(item.hasItemMeta() && item.getItemMeta().hasLore()) {
					for(String enchant : getEnchantmentsOnItem(item)) {
						String OEN = enchant.split(" ")[0];
						int level = Integer.parseInt(enchant.split(" ")[1]), chance = getProckChance(OEN, level, event.getEntity().getKiller());
						if(random.nextInt(100) <= chance) {
							if(OEN.equals("Inquisitive")) {
								String equ = null;
								if(level == 1) equ = enchantstrings[0]; else equ = enchantstrings[1];
								equ = equ.replace(" ", "").replace("xp", "" + event.getDroppedExp()).replace("level", "" + level);
								event.setDroppedExp(getProckChance(equ, level, event.getEntity().getKiller()));
							} else if(OEN.equals("MasterInquisitive")) {
								String equ = null;
								if(level == 1) equ = enchantstrings[2]; else equ = enchantstrings[3];
								equ = equ.replace(" ", "").replace("xp", "" + event.getDroppedExp()).replace("level", "" + level);
								event.setDroppedExp(getProckChance(equ, level, event.getEntity().getKiller()));
							}
						}
					}
				}
			}
			return;
		} else { return; }
	}
	private void Insanity(Player damager, int level, Player victim, EntityDamageByEntityEvent event) {
		if(!(getItemInHand(victim) == null) && getItemInHand(victim).getType().name().endsWith("_SWORD")) {
			String damage = enchantments.getString("Insanity.damage").replace("damage", "" + event.getDamage()).replace("level", "" + level);
			event.setDamage(evaluate(damage));
			playSound(sounds, "enchants.Insanity", damager, damager.getLocation(), false);
		}
	}
	private void Insomnia(Player player, int level, EntityDamageByEntityEvent event) {
		givePotionEffects(player, null, "Insomnia", level);
		if(random.nextInt(101) <= 25) { event.setDamage(event.getDamage() * 2); }
	}
	private void Inversion(EntityDamageByEntityEvent event, Player victim, int level) {
		event.setDamage(0);
		int amount = random.nextInt(3) + 1;
		if(!(victim.getHealth() + amount > victim.getMaxHealth())) { victim.setHealth(victim.getHealth() + amount); }
		for(String string : cemessages.getStringList("enchants.inversion")) {
			victim.sendMessage(ChatColor.translateAlternateColorCodes('&', string.replace("{AMOUNT}", "" + amount)));
		}
	}
	private void Leadership(EntityDamageByEntityEvent event, int level) {
		int nearbyallies = 0;
		for(Entity entity : event.getDamager().getNearbyEntities(doubles[11], doubles[11], doubles[11]))
			if(entity instanceof Player && fapi.relationIsAlly((Player) event.getDamager(), (Player) entity))
				nearbyallies++;
		event.setDamage(event.getDamage() * (1 + (doubles[12] * nearbyallies)));
	}
	private void Lifebloom(Player victim, int level) {
		victim.playEffect(victim.getLocation(), Effect.STEP_SOUND, Material.EMERALD_BLOCK);
		playSound(sounds, "enchants.Lifebloom", victim, victim.getLocation(), true);
		int area = evaluate(enchantments.getString("Lifebloom.area").replace("level", "" + level));
		for(Entity entity : victim.getNearbyEntities(area, area, area)) {
			if(entity instanceof Player && fapi.relationIsAlly(victim, (Player) entity) == true
					|| entity instanceof Player && fapi.relationIsMember(victim, (Player) entity) == true) {
				((Player) entity).setHealth(((Player) entity).getMaxHealth());
			}
		}
	}
	private void Lifesteal(EntityDamageByEntityEvent event, Player player) {
		if(!(player.getHealth() + 1 > 20.0)) { player.setHealth(player.getHealth() + 1); }
		if(!(((LivingEntity) event.getEntity()).getHealth() - 1 <= 0)) { ((LivingEntity) event.getEntity()).setHealth(((LivingEntity) event.getEntity()).getHealth() - 1); }
		playSound(sounds, "enchants.Lifesteal", player, player.getLocation(), true);
	}
	private void Lightning(Location location, Projectile proj) { proj.getWorld().strikeLightning(location); proj.remove(); }
	private void Marksman(EntityDamageByEntityEvent event, double damage) {
		event.setDamage(event.getDamage() + damage);
	}
	private void Molten(LivingEntity damager, int level) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				damager.setFireTicks(40 * level + 60);
			}
		}, 1);
	}
	private void Obliterate(EntityDamageByEntityEvent event, int level) {
		double multiplier = level + 0.25;
		event.getEntity().setVelocity(new Vector(-event.getEntity().getLocation().getDirection().getX() * multiplier, event.getEntity().getVelocity().getY(), -event.getEntity().getLocation().getDirection().getZ() * multiplier));
	}
	private void ObsidianDestroyer(PlayerInteractEvent event, int level) {
		if(event.getClickedBlock().getType().equals(Material.OBSIDIAN)) {
			event.getClickedBlock().breakNaturally(); event.getClickedBlock().getWorld().playEffect(event.getClickedBlock().getLocation(), Effect.STEP_SOUND, Material.OBSIDIAN);
		}
	}
	private void Oxygenate(Player player) { if(player.getRemainingAir() + 40 <= 300) player.setRemainingAir(player.getRemainingAir() + 40); }
	private void Paralyze(Player victim, int level) {
		victim.getWorld().strikeLightning(victim.getLocation());
		if(random.nextInt(100) <= level * 15) {
			givePotionEffects(null, victim, "Paralyze", level);
		}
	}
	private void Piercing(EntityDamageByEntityEvent event, int level) { event.setDamage(event.getFinalDamage() * (1 + (level * .25))); }
	private void PlanetaryDeathbringer(EntityDamageByEntityEvent event) {
		event.setDamage(event.getDamage() * doubles[0]);
	}
	private void Poison(LivingEntity victim, int level) { victim.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 40 * level + 40, level - 1)); }
	private void Poisoned(LivingEntity damager, int level) { damager.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 40 * level + 40, level - 1)); }
	private void Protection(Player player, int level) {
		String[] string = enchantments.getString("Protection.area").toLowerCase().replace(" ", "").replace("level", "" + level).split("\\:");
		for(Entity entity : player.getNearbyEntities(evaluate(string[0]), evaluate(string[1]), evaluate(string[2]))) {
			if(entity instanceof Player && fapi.relationIsAlly(player, (Player) entity) == true) {
				((Player) entity).addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 40 * level, level / 2));
				((Player) entity).addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 10 * level, 1));
			}
		}
	}
	private void Pummel(Player damager, Player victim, int level) {
		givePotionEffects(null, victim, "Pummel", level);
		String[] string = enchantments.getString("Pummel.area").replace(" ", "").toLowerCase().replace("level", "" + level).split("\\:");
		for(Entity entity : victim.getNearbyEntities(evaluate(string[0]), evaluate(string[1]), evaluate(string[2]))) {
			if(entity instanceof Player && fapi.relationIsEnemyOrNull(damager, victim) == true) {
				givePotionEffects(null, ((Player) entity), "Pummel", level);
			}
		}
		return;
	}
	private void Ragdoll(Player victim) { victim.setVelocity(victim.getLocation().getDirection().multiply(-random.nextDouble() - 0.5)); }
	
	private ArrayList<String> playerRage = new ArrayList<String>(), mobRage = new ArrayList<String>();
	
	private void Rage(EntityDamageByEntityEvent event, Player damager, int level) {
		LivingEntity entity = (LivingEntity) event.getEntity();
		double multiplier = 1.000;
		if(entity instanceof Player && enchantments.getBoolean("Rage.player-particles")
				|| !(entity instanceof Player) && enchantments.getBoolean("Rage.mob-particles")) {
			entity.getWorld().playEffect(entity.getLocation(), Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
			entity.getWorld().playEffect(entity.getEyeLocation(), Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
		}
		ArrayList<String> type = null;
		if(entity instanceof Player) { type = playerRage; } else { type = mobRage; }
		type.add(damager.getName());
		for(String string : type) {
			if(string.equals(damager.getName())) {
				if(!(multiplier + 0.111 > 1.000 + (level * 0.111))) {
					multiplier = multiplier + 0.111;
				}
			}
		}
		entity.setVelocity(new Vector(-(event.getDamager().getLocation().getDirection().getX()) * 0.35, 0.0, -(event.getDamager().getLocation().getDirection().getZ()) * .35));
		event.setDamage(event.getDamage() * multiplier);
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				playerRage.remove(damager.getName());
				mobRage.remove(damager.getName());
			}
		}, 20 * 4);
	}
	private void Ravenous(Player player, int level) { givePotionEffects(null, player, "Ravenous", level); }
	private void RocketEscape(Player victim, int level) { if(victim.getHealth() <= (level * 2) + 4) { givePotionEffects(null, victim, "RocketEscape", level); } }
	private void Shackle(EntityDamageByEntityEvent event, int level) {
		if(level <= 3 || level == 2 && !(event.getEntity().getType().equals(EntityType.MAGMA_CUBE)) || level == 1 && !(event.getEntity().getType().equals(EntityType.BLAZE)) && !(event.getEntity().getType().equals(EntityType.MAGMA_CUBE))) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
				public void run() {
					event.getEntity().setVelocity(new Vector(-(event.getDamager().getLocation().getDirection().getX() * .25), 0.0, -(event.getDamager().getLocation().getDirection().getZ() * .25)));
				}
			}, 1);
		}
	}
	private void Shockwave(Player damager, int level) { damager.setVelocity(new Vector(-damager.getLocation().getDirection().getX() * 3, damager.getVelocity().getY(), -damager.getLocation().getDirection().getZ() * 3)); }
	private void Silence(Player victim, int level) {
		Collection<PotionEffect> pes = victim.getActivePotionEffects(), potioneffects = victim.getActivePotionEffects();
		for(PotionEffect pe : pes) { victim.removePotionEffect(pe.getType()); }
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				for(PotionEffect pe : potioneffects) {
					victim.addPotionEffect(pe);
				}
			}
		}, 20 * level);
	}
	private void SkillSwipe(Player victim, Player damager, int level) {
		int taken_xp = (random.nextInt(10) + 1) * level;
		if(victim.getTotalExperience() >= taken_xp) {
			int remaining_xp = getTotalExperience(victim) - taken_xp;
			setTotalExperience(victim, remaining_xp);
			setTotalExperience(damager, getTotalExperience(damager) + taken_xp);
		}
	}
	private void SmokeBomb(LivingEntity damager, int level) { damager.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * level + 100, level - 1)); }
	private void Snare(Player victim, int level) {
		givePotionEffects(null, victim, "Snare", level);
	}
	private void Sniper(EntityDamageByEntityEvent event, Player damager, Player victim, int level) {
		if(event.getDamager().getLocation().getY() > victim.getLocation().getY()) {
			double multiplier = 1.0;
			for(int i = 1; i <= level; i++) { multiplier = multiplier + 0.5; }
			double damage = event.getFinalDamage() * multiplier;
			event.setDamage(0.00);
			if(victim.getHealth() - damage <= 0.00) {
				victim.setHealth(0.00);
			} else {
				victim.setHealth(victim.getHealth() - damage);
			}
			playSound(sounds, "enchants.Sniper", damager, damager.getLocation(), false);
			sendStringListMessage(damager.getUniqueId(), cemessages.getStringList("enchants.Sniper"), 0);
		}
	}
	private void SpiritLink(Player victim, int level) {
		String[] string = enchantments.getString("SpiritLink.area").toLowerCase().replace(" ", "").replace("level", "" + level).split("\\:");
		for(Entity entity : victim.getNearbyEntities(evaluate(string[0]), evaluate(string[1]), evaluate(string[2]))) {
			if(entity instanceof Player && fapi.relationIsAlly(victim, (Player) entity) == true
					|| entity instanceof Player && fapi.relationIsMember(victim, (Player) entity) == true) {
				Player nearbyplayer = (Player) entity;
				if(!(nearbyplayer.getHealth() + 1 > nearbyplayer.getMaxHealth())) { nearbyplayer.setHealth(nearbyplayer.getHealth() + 1); }
			}
		}
	}
	private void Spirits(Player victim, int level) {
		playSound(sounds, "enchants.Spirits", victim, victim.getLocation(), true);
		for(Entity entity : victim.getNearbyEntities(level + (level / 2), level + (level / 2), level + (level / 2))) {
			if(entity instanceof Player && fapi.relationIsAlly(victim, (Player) entity) == true) {
				givePotionEffects(null, ((Player) entity), "Spirits", level);
			}
		}
		for(int i = 1; i <= level; i++) {
			Blaze blaze = (Blaze) victim.getWorld().spawnEntity(victim.getLocation(), EntityType.BLAZE);
			blaze.setCustomName(spiritsname.replace("{PLAYER}", victim.getName()));
			blaze.setCanPickupItems(false);
			blaze.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 99999, level));
			Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() { public void run() { blaze.remove(); } }, 300);
		}
	}
	private void Stormcaller(LivingEntity damager) { damager.getWorld().strikeLightning(damager.getLocation()); }
	private void Tank(Player victim, Player damager, int level, EntityDamageByEntityEvent event) {
		if(getItemInHand(damager) == null || !(getItemInHand(damager).getType().name().endsWith("_AXE"))) { return; }
		double dividedDamage = 0.0;
		for(ItemStack item : victim.getInventory().getArmorContents()) {
			for(String enchant : getEnchantmentsOnItem(item)) {
				if(enchant.split(" ")[0].equals("Tank")) {
					dividedDamage = dividedDamage + 1.85 * level;
				}
			}
		}
		event.setDamage(event.getDamage() / dividedDamage);
	}
	private void Teleportation(Projectile proj, Player player1, Player player2) {
		player1.teleport(player2.getLocation());
		proj.remove();
	}
	private void ThunderingBlow(EntityDamageByEntityEvent event, int level) {
		for(int i = 1; i <= 3; i++) event.getEntity().getWorld().strikeLightning(event.getEntity().getLocation());
	}
	private void Trap(Player victim, int level) { givePotionEffects(null, victim, "Trap", level); }
	private void Trickster(Player victim) {
		Location location = victim.getLocation();
		if(victim.getLocation().getYaw() >= 0) { location.setYaw(victim.getLocation().getYaw() - 180);
		} else { location.setYaw(victim.getLocation().getYaw() + 180); }
		victim.teleport(location);
	}
	private void UndeadRuse(Player victim, int level, LivingEntity damager) {
		for(int i = 1; i <= level; i++) {
			Zombie zombie = (Zombie) victim.getWorld().spawnEntity(victim.getLocation(), EntityType.ZOMBIE);
			zombie.setCustomName(undeadrusename.replace("{PLAYER}", victim.getName()));
			zombie.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 99999, 3));
			zombie.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 99999, 1));
			zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 99999, 1));
			zombie.setCanPickupItems(false); zombie.setTarget(damager);
			Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
				public void run() {
					if(!(zombie.isDead() == true)) {
						zombie.remove(); 
					}
				}
			}, 20 * 20);
		}
	}
	private void Valor(EntityDamageByEntityEvent event, int level) {
		event.setDamage(0.045 * level);
	}
	private void Vampire(EntityDamageByEntityEvent event, Player damager) {
		if(damager.getHealth() + event.getDamage() / 2 > damager.getMaxHealth()) { damager.setHealth(damager.getMaxHealth());
		} else { damager.setHealth(damager.getHealth() + event.getDamage() / 2); return; }
	}
	private void Voodoo(LivingEntity damager, int level) { damager.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 40 * level + 40, level - 1)); }
	private void Wither(LivingEntity damager, int level) { damager.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 40 * level + 40, level - 1)); }
	*/
}

package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.EnvoyCrate;
import me.randomHashTags.RandomPackage.api.events.player.PlayerClaimEnvoyCrateEvent;

public class Envoy extends RandomPackageAPI implements CommandExecutor, Listener {
	
	public boolean isEnabled = false;
	private static Envoy instance;
	public static final Envoy getEnvoy() {
		if(instance == null) instance = new Envoy();
		return instance;
	}
	
	public FileConfiguration config;
	public ItemStack envoySummon, presetLocationPlacer;
	private int spawnTask, task;
	private String type;
	public final List<Location> preset = new ArrayList<>();
	private final List<Player> settingPreset = new ArrayList<>();

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		final int l = args.length;
		if(l == 0) {
			
		} else {
		    final String a = args[0];
			if(a.equals("help")) viewHelp(sender);
			else if(a.equals("spawn") || a.equals("summon") || a.equals("begin") || a.equals("start")) {
				if(hasPermission(sender, "RandomPackage.envoy.start", true))
					spawnEnvoy(ChatColor.translateAlternateColorCodes('&', config.getString("messages.default-summon-type")), false, l == 1 ? type : args[1].toUpperCase());
			} else if(a.equals("stop") || a.equals("end")) {
				if(hasPermission(sender, "RandomPackage.envoy.stop", true)) EnvoyCrate.stopAllEnvoys();
            } else if(player != null && a.equals("preset")) {
			    enterEditMode(player);
            }
		}
		return true;
	}

	public void enable() {
		final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "envoy.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		final List<String> c = otherdata.getStringList("envoy.preset");
		if(c != null && !c.isEmpty())
			for(String s : c)
				preset.add(toLocation(s));
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "envoy.yml"));
		type = config.getString("settings.type");
		envoySummon = d(config, "items.envoy-summon", 0);

		presetLocationPlacer = new ItemStack(Material.BEDROCK);
		itemMeta = presetLocationPlacer.getItemMeta();
		itemMeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Preset EnvoyCrate Location");
		itemMeta.setLore(Arrays.asList(ChatColor.GRAY + "Place me to add a preset envoy location for", ChatColor.GRAY + "a chance for an EnvoyCrate to spawn at this location."));
		presetLocationPlacer.setItemMeta(itemMeta);

		final String defaultBlock = config.getString("tiers.default-settings.block"), defaultRewardSize = config.getString("tiers.default-settings.reward-size");
		final String[] fb = config.getString("tiers.default-settings.falling-block").split(":");
		final ItemStack defaultFallingBlock = new ItemStack(Material.valueOf(fb[0].toUpperCase()));
		final boolean defaultDropsFromSky = config.getBoolean("tiers.default-settings.drops-from-sky"), defaultCanRepeatRewards = config.getBoolean("tiers.default-settings.can-repeat-rewards");
		final List<String> defaultCannotLandAbove = config.getStringList("tiers.default-settings.cannot-land-above");
		final List<ItemStack> tiers = new ArrayList<>();
		int loaded = 0;
		for(String s : config.getConfigurationSection("tiers").getKeys(false)) {
			if(!s.equals("default-settings")) {
				final Object cannotLandAbove = config.get("tiers." + s + ".cannot-land-above"), dropsFromSky = config.get("tiers." + s + ".drops-from-sky"), rewardSize = config.get("tiers." + s + ".reward-size"), fallingblock = config.get("tiers." + s + ".falling-block"), canRepeatRewards = config.get("tiers." + s + ".can-repeat-rewards");
				final ItemStack physicalItem = d(config, "tiers." + s, 0);
				final String f = config.getString("tiers." + s + ".firework"), block = config.getString("tiers." + s + ".block");
				final Firework firework = f != null ? spawnFirework(Type.valueOf(f.split(":")[0].toUpperCase()), getColor(f.split(":")[1].toUpperCase()), getColor(f.split(":")[2].toUpperCase()), Integer.parseInt(f.split(":")[3])) : null;
				final EnvoyCrate crate = new EnvoyCrate(s, block != null ? block : defaultBlock, physicalItem, config.getInt("tiers." + s + ".chance"), rewardSize != null ? (String) rewardSize : defaultRewardSize, config.getStringList("tiers." + s + ".rewards"), dropsFromSky != null ? (boolean) dropsFromSky : defaultDropsFromSky, fallingblock != null ? new ItemStack(Material.valueOf(((String) fallingblock).split(":")[0].toUpperCase()), 1, Byte.parseByte(((String) fallingblock).split(":")[1])) : defaultFallingBlock, firework, canRepeatRewards != null ? (boolean) canRepeatRewards : defaultCanRepeatRewards, cannotLandAbove != null ? (List<String>) cannotLandAbove : defaultCannotLandAbove);
				if(firework != null) firework.remove();
				tiers.add(crate.getPhysicalItem().clone());
				loaded += 1;
			}
		}
		addGivedpCategory(tiers, UMaterial.ENDER_CHEST, "Envoy Tiers", "Givedp: Envoy Tiers");
		final String defaul = ChatColor.translateAlternateColorCodes('&', config.getString("messages.default-summon-type"));

		spawnTask = scheduler.scheduleSyncDelayedTask(randompackage, () -> spawnEnvoy(defaul, true, type), getRandomTime());
		task = scheduler.scheduleSyncRepeatingTask(randompackage, () -> {
			final HashMap<Integer, HashMap<Location, EnvoyCrate>> crates = EnvoyCrate.activeCrates;
			for(int i : crates.keySet())
				for(Location l : crates.get(i).keySet())
					spawnFirework(crates.get(i).get(l).getFirework(), l);
		}, 0, 20*config.getInt("settings.firework-delay"));
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " envoy tiers &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
	public void disable() {
		if(!isEnabled) return;
		List<String> p = new ArrayList<>();
		for(Location l : preset) p.add(toString(l));
		otherdata.set("envoy.preset", p);
		saveOtherData();
		isEnabled = false;
		scheduler.cancelTask(spawnTask);
		scheduler.cancelTask(task);
		EnvoyCrate.stopAllEnvoys();
		EnvoyCrate.activeCrates.clear();
		EnvoyCrate.crates.clear();
		if(!settingPreset.isEmpty()) {
			for(Player player : settingPreset) player.getInventory().remove(presetLocationPlacer);
			for(Location l : preset) l.getWorld().getBlockAt(l).setType(Material.AIR);
		}
		settingPreset.clear();
		preset.clear();
		HandlerList.unregisterAll(this);
	}
	
	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		final ItemStack i = event.getItem();
		final EnvoyCrate ec = EnvoyCrate.valueOf(i);
		final Player player = event.getPlayer();
		if(ec != null) {
			event.setCancelled(true);
			player.updateInventory();
			removeItem(player, i, 1);
			final List<String> rewards = ec.getRandomRewards();
			for(String s : rewards) giveItem(player, d(null, s, 0));
		} else if(i != null && i.hasItemMeta() && i.getItemMeta().equals(envoySummon.getItemMeta())) {
			event.setCancelled(true);
			player.updateInventory();
			removeItem(player, i, 1);
			spawnEnvoy(ChatColor.translateAlternateColorCodes('&', config.getString("messages.item-summon-type")), false, type);
		} else if(event.getClickedBlock() != null) {
			final Location l = event.getClickedBlock().getLocation();
			final EnvoyCrate c = EnvoyCrate.valueOf(l);
			if(c != null) {
				final PlayerClaimEnvoyCrateEvent e = new PlayerClaimEnvoyCrateEvent(player, l, c);
				pluginmanager.callEvent(e);
				if(!e.isCancelled()) {
					event.setCancelled(true);
					player.updateInventory();
					EnvoyCrate.destroy(EnvoyCrate.getEnvoy(l), l, true);
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
    private void blockPlaceEvent(BlockPlaceEvent event) {
	    if(!event.isCancelled()) {
	        final Player player = event.getPlayer();
	        if(settingPreset.contains(player) && player.getItemInHand().equals(presetLocationPlacer)) {
	            preset.add(event.getBlockPlaced().getLocation());
            }
        }
    }
    @EventHandler
	private void blockBreakEvent(BlockBreakEvent event) {
		if(!event.isCancelled()) {
			final Player player = event.getPlayer();
			final Location l = event.getBlock().getLocation();
			if(settingPreset.contains(player) && preset.contains(l)) {
				preset.remove(l);
			}
		}
	}

    public void enterEditMode(Player player) {
		if(hasPermission(player, "RandomPackage.envoy.preset", true)) {
			if(!settingPreset.contains(player)) {
				if(settingPreset.isEmpty()) for(Location l : preset) l.getWorld().getBlockAt(l).setType(Material.BEDROCK);
				settingPreset.add(player);
				player.getInventory().addItem(presetLocationPlacer);
			} else {
				settingPreset.remove(player);
				player.getInventory().remove(presetLocationPlacer);
				if(settingPreset.isEmpty()) for(Location l : preset) l.getWorld().getBlockAt(l).setType(Material.AIR);
			}
		}
    }
	
	public void spawnEnvoy(String summonType, boolean natural, String where) {
		for(String s : config.getStringList("messages.broadcast")) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{SUMMON_TYPE}", summonType)));
		}
		EnvoyCrate.spawnEnvoy(where, getRandomAmountSpawned());
		if(natural) {
			scheduler.scheduleSyncDelayedTask(randompackage, () -> spawnEnvoy(ChatColor.translateAlternateColorCodes('&', config.getString("messages.default-summon-type")), true, where), getRandomTime());
		}
	}
	private int getRandomTime() {
		final String r = config.getString("settings.repeats");
		final int min = r.contains("-") ? Integer.parseInt(r.split("-")[0]) : 0, t = r.contains("-") ? min+random.nextInt(Integer.parseInt(r.split("-")[1])-min+1) : Integer.parseInt(r);
		return t*20;
	}
	private int getRandomAmountSpawned() {
		final String as = config.getString("settings.amount-spawned");
		final boolean hyphen = as.contains("-");
		final int min = Integer.parseInt(hyphen ? as.split("-")[0] : as);
		return hyphen ? min+random.nextInt(Integer.parseInt(as.split("-")[1])-min+1) : min;
	}
	public void viewHelp(CommandSender sender) {
		if(hasPermission(sender, "RandomPackage.envoy.help", true))
			sendStringListMessage(sender, config.getStringList("messages.envoy-help"));
	}
}

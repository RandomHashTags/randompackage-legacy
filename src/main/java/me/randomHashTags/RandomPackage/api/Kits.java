package me.randomHashTags.RandomPackage.api;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;
import me.randomHashTags.RandomPackage.utils.classes.kits.*;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Kits extends RandomPackageAPI implements Listener, CommandExecutor, TabCompleter {

    public boolean gkitsAreEnabled = false, vkitsAreEnabled = false, mkitsAreEnabled = false;
    private boolean isRegistered = false;
    private enum KitType { GLOBAL, EVOLUTION, MASTERY, }

    private static Kits instance;
    public static final Kits getKits() {
        if(instance == null) instance = new Kits();
        return instance;
    }

    private gkitevents gkitEvents;
    private vkitevents vkitEvents;
    private mkitevents mkitEvents;
    public YamlConfiguration gkits, vkits, mkits;
    private Inventory gkit, vkit, mkit, gkitPreview, vkitPreview, mkitPreview;


    private ItemStack gkitCooldown, gkitPreviewBackground, vkitCooldown, vkitPreviewBackground, vkitLocked, mkitBackground;
    public boolean heroicEnchantedEffect = false, gkitUsesTiers, tierZeroEnchantEffect;
    public ItemStack heroicFallenHeroBundle;
    public HashMap<GlobalKit, ItemStack> gkitGems = new HashMap<>(), gkitFallenHeroes = new HashMap<>();
    public HashMap<EvolutionKit, ItemStack> vkitGems = new HashMap<>(), vkitFallenHeroes = new HashMap<>();
    public ArrayList<FallenHero> gkitHeroicFallenHeroes = new ArrayList<>();

    private ArrayList<String> gkitPaths = new ArrayList<>(), vkitPaths = new ArrayList<>();
    private HashMap<EditedKit, String> editing = new HashMap<>();

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        final String c = cmd.getName();
        if(args.length == 0 && player != null) {
            if(hasPermission(sender, "RandomPackage." + c, true))
                view(player, c.equals("gkit") ? KitType.GLOBAL : c.equals("vkit") ? KitType.EVOLUTION : KitType.MASTERY);
        } else if(args.length == 2 && args[0].equals("reset")) {
            if(hasPermission(sender, "RandomPackage." + c + ".reset", true))
                resetAll(player, args[1], c.equals("gkit") ? KitType.GLOBAL : c.equals("vkit") ? KitType.EVOLUTION : KitType.MASTERY);
        } else if(args.length == 3 && args[0].equals("reset")) {
            if(hasPermission(sender, "RandomPackage." + c + ".reset-kit", true))
                reset(player, args[1], c.equals("gkit") ? KitType.GLOBAL : c.equals("vkit") ? KitType.EVOLUTION : KitType.MASTERY, args[2]);
        }
        if(c.equals("gkit") && args.length == 2) {
            final String arg = args[0], argg = args[1];
            if(arg.equals("edit") && hasPermission(sender, "RandomPackage.gkit.edit", true))
                edit(player, GlobalKit.valueOf(argg), true);
        }
        return true;
    }
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        List<String> paths = new ArrayList<>();
        final String n = cmd.getName();
        if((n.equals("gkit") || n.equals("vkit")) && args.length > 0 && args[0].equals("edit")) {
            paths = new ArrayList<>(n.equals("gkit") ? this.gkitPaths : this.vkitPaths);
            if(args.length == 1) {
                paths.clear();
                for(Player p : Bukkit.getOnlinePlayers()) {
                    final String nn = p.getName();
                    if(nn.toLowerCase().startsWith(args[0]))
                        paths.add(nn);
                }
            } else if(args.length == 2) {
                if(!args[1].isEmpty()) {
                    for(int i = 0; i < paths.size(); i++) {
                        final String s = paths.get(i);
                        if(!s.startsWith(args[1].toLowerCase())) {
                            paths.remove(s);
                            i -= 1;
                        }
                    }
                }
            }
        } else if((n.equals("gkit") || n.equals("vkit")) && args.length > 0 && args[0].equals("reset")) {
            for(Player p : Bukkit.getOnlinePlayers()) {
                final String pn = p.getName();
                if(args.length == 1 || args.length == 2 && pn.toLowerCase().startsWith(args[1].toLowerCase()))
                    paths.add(p.getName());
            }
        }
        return paths;
    }

    private void tryRegistering() {
        if(isRegistered) return;
        pluginmanager.registerEvents(this, randompackage);
        isRegistered = true;
    }

    public void enableGkits() {
        final long started = System.currentTimeMillis();
        if(gkitsAreEnabled) return;
        save("Features", "kits global.yml");
        gkitEvents = new gkitevents();
        pluginmanager.registerEvents(gkitEvents, randompackage);
        tryRegistering();
        gkitsAreEnabled = true;
        gkits = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "kits global.yml"));
        gkitCooldown = d(gkits, "items.cooldown", 0);
        heroicFallenHeroBundle = d(gkits, "items.heroic-fallen-hero-bundle", 0);
        heroicEnchantedEffect = gkits.getBoolean("items.heroic.enchanted-effect");
        gkit = Bukkit.createInventory(null, gkits.getInt("gui.size"), ChatColor.translateAlternateColorCodes('&', gkits.getString("gui.title")));
        gkitPreview = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', gkits.getString("items.preview.title")));
        gkitPreviewBackground = d(gkits, "items.preview", 0);
        gkitUsesTiers = gkits.getBoolean("gui.settings.use-tiers");
        tierZeroEnchantEffect = gkits.getBoolean("gui.settings.tier-zero-enchant-effect");
        final boolean defaultHeroic = gkits.getBoolean("gui.defaults.heroic");
        final String heroicprefix = ChatColor.translateAlternateColorCodes('&', gkits.getString("items.heroic.prefix"));
        final int defaultMaxTier = gkits.getInt("gui.defaults.max-tier"), defaultCooldown = gkits.getInt("gui.defaults.cooldown");
        final ItemStack defaultSpawnItem = d(gkits, "fallen-heroes.default.spawn-item", 0).clone(), defaultGem = d(gkits, "fallen-heroes.default.gem", 0).clone();

        int loaded = 0;
        for(String s : gkits.getConfigurationSection("kits").getKeys(false)) {
            Object maxtier = gkits.get("kits." + s + ".max-tier"), cooldown = gkits.get("kits." + s + ".cooldown"), heroic = gkits.get("kits." + s + ".heroic");
            final String fh = gkits.get("kits." + s + ".fallen-hero") != null ? gkits.getString("kits." + s + ".fallen-hero") : "default";
            final GlobalKit k = new GlobalKit(s, gkits.getInt("kits." + s + ".slot"), maxtier != null ? (int) maxtier : defaultMaxTier, cooldown != null ? (int) cooldown : defaultCooldown, d(gkits, "kits." + s, 0), heroic != null ? (boolean) heroic : defaultHeroic, fh);
            final List<KitItem> kititems = new ArrayList<>();
            for(String i : gkits.getConfigurationSection("kits." + s + ".items").getKeys(false)) {
                final String t = gkits.getString("kits." + s + ".items." + i + ".item");
                if(t != null) {
                    final int chance = gkits.get("kits." + s + ".items." + i + ".chance") != null ? gkits.getInt("kits." + s + ".items." + i + ".chance") : 100;
                    kititems.add(new KitItem(k, i, gkits.getString("kits." + s + ".items." + i + ".item"), gkits.getString("kits." + s + ".items." + i + ".name"), gkits.getStringList("kits." + s + ".items." + i + ".lore"), chance));
                }
            }
            k.items = kititems;
            gkitPaths.add(k.path);
            final String n = k.getDisplayedItem().getItemMeta().getDisplayName(), N = heroicprefix.replace("{NAME}", n);
            final boolean isheroic = k.isHeroic;
            gkit.setItem(k.slot, k.getDisplayedItem());
            item = fh.equals("default") ? defaultGem.clone() : d(gkits, "fallen-heroes." + fh + ".gem", 0).clone(); itemMeta = item.getItemMeta(); lore.clear();
            if(item.hasItemMeta()) {
                itemMeta.setDisplayName(itemMeta.hasDisplayName() ? isheroic ? itemMeta.getDisplayName().replace("{NAME}", N) : n : n);
                if(itemMeta.hasLore()) for(String q : itemMeta.getLore()) lore.add(q.replace("{NAME}", isheroic ? N : n));
                itemMeta.setLore(lore); lore.clear();
                item.setItemMeta(itemMeta);
            }
            gkitGems.put(k, item);

            item = fh.equals("default") ? defaultSpawnItem.clone() : d(gkits, "fallen-heroes." + fh + ".spawn-item", 0); itemMeta = item.getItemMeta(); lore.clear();
            if(item.hasItemMeta()) {
                if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{NAME}", isheroic ? N : n));
                if(itemMeta.hasLore())
                    for(String E : itemMeta.getLore()) lore.add(E.replace("{NAME}", isheroic ? N : n));
            }
            itemMeta.setLore(lore); lore.clear();
            item.setItemMeta(itemMeta);
            gkitFallenHeroes.put(k, item);

            final ItemStack gem = d(gkits, "fallen-heroes." + fh + ".gem", 0); itemMeta = gem.getItemMeta(); lore.clear();
            if(gem.hasItemMeta()) {
                if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{NAME}", isheroic ? N : n));
                for(String q : itemMeta.getLore()) lore.add(q.replace("{NAME}", isheroic ? N : n));
                itemMeta.setLore(lore); lore.clear();
                gem.setItemMeta(itemMeta);
            }
            final FallenHero fallenhero = new FallenHero(fh, gkits.getString("fallen-heroes." + fh + ".spawn-item.spawnable"), k, gkits.getString("fallen-heroes." + fh + ".type").toUpperCase(), gkitFallenHeroes.get(k), gkits.getInt("fallen-heroes." + fh + ".gem.chance"), gem);
            if(isheroic) gkitHeroicFallenHeroes.add(fallenhero);
            loaded += 1;
        }
        addGivedpCategory(new ArrayList<>(gkitGems.values()), UMaterial.DIAMOND, "Gkit Gems", "Givedp: Gkit Gems");
        addGivedpCategory(new ArrayList<>(gkitFallenHeroes.values()), UMaterial.BONE, "Gkit Fallen Heroes", "Givedp: Gkit Fallen Heroes");
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " global kits, gems, & fallen heroes &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disableGkits() {
        if(!gkitsAreEnabled) return;
        gkitsAreEnabled = false;
        for(int i = 0; i < EditedKit.editing.size(); i++) {
            final EditedKit e = EditedKit.editing.get(i);
            final Player p = e.player;
            if(e.original.get(0).kit instanceof GlobalKit) {
                e.delete();
                p.closeInventory();
                i -= 1;
            }
        }
        for(int i = 0; i < FallenHero.heroes.size(); i++) {
            final FallenHero f = FallenHero.heroes.get(i);
            if(f.globalkitORevolutionkit instanceof GlobalKit) {
                FallenHero.heroes.remove(f);
                i -= 1;
            }
        }
        GlobalKit.kits.clear();
        gkitFallenHeroes.clear();
        gkitGems.clear();
        gkitPaths.clear();
        gkitHeroicFallenHeroes.clear();
        HandlerList.unregisterAll(gkitEvents);
    }

    public void enableVkits() {
        final long started = System.currentTimeMillis();
        if(vkitsAreEnabled) return;
        save("Features", "kits evolution.yml");
        vkitsAreEnabled = true;
        vkits = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "kits evolution.yml"));
        vkitEvents = new vkitevents();
        pluginmanager.registerEvents(vkitEvents, randompackage);
        tryRegistering();
        vkitCooldown = d(vkits, "items.cooldown", 0);
        vkit = Bukkit.createInventory(null, vkits.getInt("gui.size"), ChatColor.translateAlternateColorCodes('&', vkits.getString("gui.title")));
        vkitPreview = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', vkits.getString("items.preview.title")));
        vkitPreviewBackground = d(vkits, "items.preview", 0);
        vkitLocked = d(vkits, "permissions.locked", 0);
        final ItemStack defaultSpawnItem = d(vkits, "fallen-heroes.default.spawn-item", 0).clone(), defaultGem = d(vkits, "fallen-heroes.default.gem", 0);
        final int defaultMaxLevel = vkits.getInt("gui.defaults.max-level"), defaultCooldown = vkits.getInt("gui.defaults.cooldown"), defaultUpgradeChance = vkits.getInt("gui.defaults.upgrade-chance");
        int loaded = 0;
        for(String s : vkits.getConfigurationSection("kits").getKeys(false)) {
            Object maxlevel = vkits.get("kits." + s + ".max-level"), upgradechance = vkits.get("kits." + s + ".upgrade-chance"), cooldown = vkits.get("kits." + s + ".cooldown");
            final int slot = vkits.getInt("kits." + s + ".slot");
            final ItemStack gui = d(vkits, "kits." + s, 0);
            final String fh = vkits.get("kits." + s + ".fallen-hero") != null ? vkits.getString("kits." + s + ".fallen-hero") : "default";
            final EvolutionKit k = new EvolutionKit(s, slot, maxlevel != null ? (int) maxlevel : defaultMaxLevel, upgradechance != null ? (int) upgradechance : defaultUpgradeChance, cooldown != null ? (int) cooldown : defaultCooldown, gui, null);
            final List<KitItem> kititems = new ArrayList<>();
            for(String i : vkits.getConfigurationSection("kits." + s + ".items").getKeys(false)) {
                final String t = vkits.getString("kits." + s + ".items." + i + ".item");
                if(t != null) {
                    final int chance = vkits.get("kits." + s + ".items." + i + ".chance") != null ? vkits.getInt("kits." + s + ".items." + i + ".chance") : 100;
                    kititems.add(new KitItem(k, i, vkits.getString("kits." + s + ".items." + i + ".item"), vkits.getString("kits." + s + ".items." + i + ".name"), vkits.getStringList("kits." + s + ".items." + i + ".lore"), chance, "1", false, vkits.getInt("kits." + s + ".items." + i + ".reqlevel")));
                }
            }
            k.items = kititems;
            vkitPaths.add(s);
            vkit.setItem(slot, gui);
            final String n = gui.getItemMeta().getDisplayName();
            item = fh.equals("default") ? defaultGem.clone() : d(vkits, "fallen-heroes." + fh + ".gem", 0).clone(); itemMeta = item.getItemMeta(); lore.clear();
            if(item.hasItemMeta()) {
                itemMeta.setDisplayName(n != null ? itemMeta.hasDisplayName() ? itemMeta.getDisplayName().replace("{NAME}", n) : n : null);
                if(itemMeta.hasLore()) for(String q : itemMeta.getLore()) lore.add(q.replace("{NAME}", n));
                itemMeta.setLore(lore); lore.clear();
                item.setItemMeta(itemMeta);
            }
            vkitGems.put(k, item);

            item = fh.equals("default") ? defaultSpawnItem.clone() : d(vkits, "fallen-heroes." + fh + ".spawn-item", 0); itemMeta = item.getItemMeta(); lore.clear();
            if(item.hasItemMeta()) {
                if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{NAME}", n));
                if(itemMeta.hasLore())
                    for(String L : itemMeta.getLore()) lore.add(L.replace("{NAME}", n));
            }
            itemMeta.setLore(lore); lore.clear();
            item.setItemMeta(itemMeta);
            vkitFallenHeroes.put(k, item);

            final ItemStack gem = d(vkits, "fallen-heroes." + fh + ".gem", 0); itemMeta = gem.getItemMeta(); lore.clear();
            if(gem.hasItemMeta()) {
                if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{NAME}", n));
                for(String q : itemMeta.getLore()) lore.add(q.replace("{NAME}", n));
                itemMeta.setLore(lore); lore.clear();
                gem.setItemMeta(itemMeta);
            }
            new FallenHero(fh, vkits.getString("fallen-heroes." + fh + ".spawn-item.spawnable"), k, vkits.getString("fallen-heroes." + fh + ".type").toUpperCase(), vkitFallenHeroes.get(k), vkits.getInt("fallen-heroes." + fh + ".gem.chance"), gem);
            loaded += 1;
        }
        addGivedpCategory(new ArrayList<>(vkitGems.values()), UMaterial.DIAMOND, "Vkit Gems", "Givedp: Vkit Gems");
        addGivedpCategory(new ArrayList<>(vkitFallenHeroes.values()), UMaterial.BONE, "Vkit Fallen Heroes", "Givedp: Vkit Fallen Heroes");
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " evolution kits, gems, & fallen heroes &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disableVkits() {
        if(!vkitsAreEnabled) return;
        vkitsAreEnabled = false;
        vkits = null;
        HandlerList.unregisterAll(vkitEvents);
        vkitEvents = null;
        vkitFallenHeroes.clear();
        vkitGems.clear();
        for(int i = 0; i < EditedKit.editing.size(); i++) {
            final EditedKit e = EditedKit.editing.get(i);
            final Player p = e.player;
            if(e.original.get(0).kit instanceof EvolutionKit) {
                e.delete();
                p.closeInventory();
                i -= 1;
            }
        }
        for(int i = 0; i < FallenHero.heroes.size(); i++) {
            final FallenHero f = FallenHero.heroes.get(i);
            if(f.globalkitORevolutionkit instanceof EvolutionKit) {
                FallenHero.heroes.remove(f);
                i -= 1;
            }
        }
    }

    public void enableMkits() {
        final long started = System.currentTimeMillis();
        if(mkitsAreEnabled) return;
        save("Features", "kits mastery.yml");
        mkitsAreEnabled = true;
        mkits = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "kits mastery.yml"));
        mkitEvents = new mkitevents();
        pluginmanager.registerEvents(mkitEvents, randompackage);
        tryRegistering();
        mkit = Bukkit.createInventory(null, mkits.getInt("gui.size"), ChatColor.translateAlternateColorCodes('&', mkits.getString("gui.title")));
        mkitBackground = d(mkits, "gui.background", 0);

        int loaded = 0;
        for(String s : mkits.getConfigurationSection("mkits").getKeys(false)) {
            final int slot = mkits.getInt("mkits." + s + ".slot");
            final ItemStack display = d(mkits, "mkits." + s, 0);
            final MasteryKit m = new MasteryKit(s, slot, display, null, true);
            loaded += 1;
            mkit.setItem(slot, display);
        }
        for(int i = 0; i < mkit.getSize(); i++)
            if(mkit.getItem(i) == null)
                mkit.setItem(i, mkitBackground);

        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " mastery kits &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disableMkits() {
        if(!mkitsAreEnabled) return;
        mkitsAreEnabled = false;
        mkits = null;
        HandlerList.unregisterAll(mkitEvents);
        mkitEvents = null;
    }


    public void view(Player player, KitType type) {
        player.closeInventory();
        final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());

        if(type.equals(KitType.EVOLUTION) && vkitsAreEnabled) {
            player.openInventory(Bukkit.createInventory(player, vkit.getSize(), vkit.getTitle()));
            player.getOpenInventory().getTopInventory().setContents(vkit.getContents());
            player.updateInventory();
            final HashMap<String, Integer> tiers = pdata.vkitLevels;
            final HashMap<String, Long> cooldowns = pdata.vkitCooldowns;
            for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
                item = player.getOpenInventory().getTopInventory().getItem(i);
                if(item != null) {
                    item = item.clone();
                    final EvolutionKit v = EvolutionKit.valueOf(i);
                    if(v != null) {
                        final String n = v.path;
                        final int lvl = tiers.keySet().contains(n) ? tiers.get(n) : player.hasPermission("RandomPackage.vkit." + n) ? 1 : 0;
                        final boolean hasPerm = hasPermissionToObtain(player, v), cooldown = cooldowns.keySet().contains(n) && cooldowns.get(n) > System.currentTimeMillis();
                        if(!hasPerm) item = vkitLocked.clone();
                        else if(cooldown) setCooldown(player, v);
                        if(!cooldown) {
                            itemMeta = item.getItemMeta(); lore.clear();
                            if(!hasPerm) {
                                final ItemMeta is = player.getOpenInventory().getTopInventory().getItem(i).getItemMeta();
                                itemMeta.setDisplayName(is.getDisplayName());
                                itemMeta.setLore(is.getLore());
                            }
                            if(itemMeta.hasLore())
                                for(String s : itemMeta.getLore()) {
                                    lore.add(s.replace("{LEVEL}", Integer.toString(lvl)));
                                }
                            if(hasPerm) {
                                if(!cooldown)
                                    for(String s : vkits.getStringList("permissions.unlocked"))
                                        lore.add(ChatColor.translateAlternateColorCodes('&', s));
                                for(String s : vkits.getStringList("permissions.preview")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
                            } else {
                                for(String s : vkitLocked.getItemMeta().getLore())
                                    lore.add(ChatColor.translateAlternateColorCodes('&', s));
                            }
                            itemMeta.setLore(lore); lore.clear();
                            item.setItemMeta(itemMeta);
                            player.getOpenInventory().getTopInventory().setItem(i, item);
                        }

                    }
                }
            }
        } else if(type.equals(KitType.GLOBAL) && gkitsAreEnabled) {
            player.openInventory(Bukkit.createInventory(player, gkit.getSize(), gkit.getTitle()));
            player.getOpenInventory().getTopInventory().setContents(gkit.getContents());
            player.updateInventory();
            final HashMap<String, Integer> tiers = pdata.gkitTiers;
            final HashMap<String, Long> cooldowns = pdata.gkitCooldowns;
            final String heroicprefix = ChatColor.translateAlternateColorCodes('&', gkits.getString("items.heroic.prefix"));
            for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
                item = player.getOpenInventory().getTopInventory().getItem(i);
                if(item != null) {
                    final GlobalKit k = GlobalKit.valueOf(i);
                    if(k != null) {
                        final String n = k.path;
                        final boolean has = tiers.keySet().contains(n) || player.hasPermission("RandomPackage.gkit." + n);
                        itemMeta = item.getItemMeta(); lore.clear();
                        if(cooldowns.keySet().contains(n) && cooldowns.get(n) > System.currentTimeMillis()) {
                            setCooldown(player, k);
                        } else {
                            final int tier = tiers.keySet().contains(n) ? tiers.get(n) : has ? 1 : 0;
                            final boolean isheroic = k.isHeroic, q = isheroic && heroicEnchantedEffect && (has || tierZeroEnchantEffect && tiers.keySet().contains(n) && !(tier < 1));
                            if(itemMeta.hasDisplayName() && isheroic) itemMeta.setDisplayName(heroicprefix.replace("{NAME}", itemMeta.getDisplayName()));
                            if(gkitUsesTiers)
                                for(String s : gkits.getStringList("gui.settings.pre-lore"))
                                    lore.add(ChatColor.translateAlternateColorCodes('&', s.replace("{TIER}", toRoman(tier)).replace("{MAX_TIER}", toRoman(k.maxTier))));
                            if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
                            for(String s : gkits.getStringList("gui.settings." + (has ? "un" : "") + "locked")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
                            for(String s : gkits.getStringList("items.preview.added-gui-lore")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
                            itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                            if(q) itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                            itemMeta.setLore(lore); lore.clear();
                            item.setItemMeta(itemMeta);
                            if(q) item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
                        }
                    }
                }
            }
        } else if(type.equals(KitType.MASTERY)) {
            player.openInventory(Bukkit.createInventory(player, mkit.getSize(), mkit.getTitle()));
            player.getOpenInventory().getTopInventory().setContents(mkit.getContents());
            player.updateInventory();
            final Inventory top = player.getOpenInventory().getTopInventory();
            for(int i = 0; i < top.getSize(); i++) {
                final MasteryKit m = MasteryKit.valueOf(i);
                if(m != null) {
                    item = top.getItem(i); itemMeta = item.getItemMeta(); lore.clear();
                    if(itemMeta.hasLore()) {
                        for(String s : itemMeta.getLore()) {
                            if(s.contains("{") && s.contains("}")) {
                                final String t = s.split("\\{")[1].split("}")[0];
                                final GlobalKit gk = GlobalKit.valueOf(t);
                                final EvolutionKit vk = gk == null ? EvolutionKit.valueOf(t) : null;
                                if(gk != null) {
                                    s = s.replace("{" + gk.path + "}", FallenHero.valueOf(gk).getFallenHeroName() );
                                }
                                if(vk != null) {
                                    s = s.replace("{" + vk.path + "}", vk.getDisplayedItem().getItemMeta().getDisplayName());
                                }
                            }
                            lore.add(s);
                        }
                    }
                    itemMeta.setLore(lore); lore.clear();
                    item.setItemMeta(itemMeta);
                }
            }
        } else return;
        player.updateInventory();
    }

    public void setCooldown(UUID player, Object kit) {
        if(player != null && kit != null) {
            final GlobalKit gkit = kit instanceof GlobalKit ? (GlobalKit) kit : null;
            final EvolutionKit vkit = gkit == null && kit instanceof EvolutionKit ? (EvolutionKit) kit : null;
            final MasteryKit mkit = vkit == null && kit instanceof MasteryKit ? (MasteryKit) kit : null;
            if(gkit == null && vkit == null && mkit == null) return;

            final boolean g = gkit != null, v = vkit != null;
            final String n = g ? gkit.path : v ? vkit.path : mkit.getPath();
            final int cooldown = g ? gkit.cooldown : v ? vkit.cooldown : mkit.cooldown;
            final RPPlayerData pdata = RPPlayerData.get(player);
            (g ? pdata.gkitCooldowns : v ? pdata.vkitCooldowns : null).put(n, System.currentTimeMillis()+cooldown*1000);
        }
    }
    private void setCooldown(Player player, Object kit) {
        if(player != null && kit != null) {
            final GlobalKit gkit = kit instanceof GlobalKit ? (GlobalKit) kit : null;
            final EvolutionKit vkit = gkit == null && kit instanceof EvolutionKit ? (EvolutionKit) kit : null;
            final MasteryKit mkit = vkit == null && kit instanceof MasteryKit ? (MasteryKit) kit : null;
            if(gkit == null && vkit == null && mkit == null) return;

            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            final boolean g = gkit != null, v = vkit != null, hasPerm = hasPermissionToObtain(player, kit);
            final YamlConfiguration yml = g ? gkits : v ? vkits : mkits;
            final String n = g ? gkit.path : v ? vkit.path : mkit.getPath();
            final long t = System.currentTimeMillis();
            final boolean cooldown = g && pdata.getGkitCooldown(n)-t <= 0 || v && pdata.getVkitCooldown(n)-t <= 0;
            final int slot = g ? gkit.slot : v ? vkit.slot : mkit.getSlot(), tier = g ? pdata.getGkitTier(n) : v ? pdata.getVkitLevel(n) : 0;
            final ItemStack displayed = g ? gkit.getDisplayedItem() : v ? vkit.getDisplayedItem() : mkit.getDisplay();
            final HashMap<String, Long> cooldowns = g ? pdata.gkitCooldowns : v ? pdata.vkitCooldowns : null;
            item = (g ? gkitCooldown : v ? vkitCooldown : null).clone(); itemMeta = item.getItemMeta(); lore.clear();
            int sec = (int) TimeUnit.MILLISECONDS.toSeconds(cooldowns.get(n)-System.currentTimeMillis()), min = sec/60, hr = min/60, d = hr/24;
            hr -= d*24;
            min -= (hr*60)+(d*60*24);
            sec -= (min*60)+(hr*60*60)+(d*60*60*24);
            for(String s : itemMeta.getLore()) {
                if(s.equals("{LORE}"))
                    for(String q : displayed.getItemMeta().getLore())
                        lore.add(q.replace("{LEVEL}", Integer.toString(tier)));
                else
                    lore.add(s.replace("{LEVEL}", Integer.toString(tier)).replace("{DAYS}", Integer.toString(d)).replace("{HOURS}", Integer.toString(hr)).replace("{MINUTES}", Integer.toString(min)).replace("{SECONDS}", Integer.toString(sec)));
            }
            if(hasPerm) {
                if(!cooldown)
                    for(String s : yml.getStringList("permissions.unlocked"))
                        lore.add(ChatColor.translateAlternateColorCodes('&', s));
            } else {
                for(String s : yml.getStringList("permissions.locked"))
                    lore.add(ChatColor.translateAlternateColorCodes('&', s));
            }
            for(String s : yml.getStringList("permissions.preview")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
            itemMeta.setLore(lore); lore.clear();
            item.setItemMeta(itemMeta);
            player.getOpenInventory().getTopInventory().setItem(slot, item);
            player.updateInventory();
        }
    }
    public void edit(Player player, GlobalKit kit, boolean editOriginalItems) {
        final String k = kit.path;
        final List<KitItem> items = kit.items;
        player.openInventory(Bukkit.createInventory(player, 54, "Edit gkit: " + k));
        final Inventory top = player.getOpenInventory().getTopInventory();
        for(KitItem ki : items) {
            final String chances = ki.stringChances;
            if(chances == null) {
                top.setItem(top.firstEmpty(), d(gkits, "kits." + kit.path + ".items." + ki.path, ((GlobalKit) ki.kit).maxTier));
            }
        }
        final ItemStack I = kit.getDisplayedItem();
        itemMeta = I.getItemMeta();
        itemMeta.setDisplayName(ChatColor.GOLD + "Settings");
        lore.clear();
        lore.addAll(Arrays.asList(" ", ChatColor.GRAY + "Left click to edit", ChatColor.GRAY + "Right click to remove", " ", ChatColor.GRAY + "Click items in your inventory to add to gkit"));
        itemMeta.setLore(lore); lore.clear();
        I.setItemMeta(itemMeta);
        top.setItem(49, I);
        final EditedKit ek = EditedKit.valueOf(player);
        if(editOriginalItems) {
            if(ek != null) ek.delete();
            final List<KitItem> i = kit.items;
            new EditedKit(player, i, new ArrayList<>(i));
        } else if(ek != null) {
            ek.selected = -1;
        }
    }
    public void editKitItem(Player player, EditedKit ek, int slot) {
        ek.selected = slot;
        final KitItem ki = ek.edited.get(slot);
        final GlobalKit gkit = (GlobalKit) ki.kit;
        final ItemStack it = d(gkits, "kits." + gkit.path + ".items." + ki.path, gkit.maxTier);
        player.openInventory(Bukkit.createInventory(player, 9, "Editing kit item: " + ki.path));
        final Inventory top = player.getOpenInventory().getTopInventory();
        top.setItem(0, it);

        for(int i = 0; i < top.getSize(); i++) {
            item = i == 2 ? UMaterial.GOLD_NUGGET.getItemStack() : i == 3 ? UMaterial.NAME_TAG.getItemStack() : i == 4 ? UMaterial.SIGN.getItemStack() : i == 5 ? UMaterial.GLOWSTONE_DUST.getItemStack() : i == 6 ? UMaterial.MAP.getItemStack() : i == 8 ? UMaterial.ARROW.getItemStack() : null;
            if(item != null) {
                itemMeta = item.getItemMeta();
                final String n = i == 2 ? ChatColor.GREEN + "Set new amount" : i == 3 ? ChatColor.AQUA + "Set new name" : i == 4 ? ChatColor.YELLOW + "Set new lore" : i == 5 ? ChatColor.GOLD + "Set new chance" : i == 6 ? ChatColor.GOLD + "Set Givedp Item" : i == 8 ? ChatColor.GRAY + "Back" : null;
                itemMeta.setDisplayName(n);
                lore.clear();
                if(i == 4) {
                    lore.add(ChatColor.GRAY + "Current:");
                    for(String s : ki.lore) {
                        lore.add(ChatColor.RESET + (s.length() >= 60 ? s.substring(0, 60) + "..." : s));
                    }
                } else {
                    lore.addAll(
                            i == 2 ? Arrays.asList(ChatColor.GRAY + "Current:", ChatColor.RESET + ki.amount)
                                    : i == 3 ? Arrays.asList(ChatColor.GRAY + "Current:", ChatColor.RESET + (ki.stringName != null ? ki.stringName : ChatColor.RESET + "N/A"))
                                    : i == 5 ? Arrays.asList(ChatColor.GRAY + "Current: " + ChatColor.RESET + ki.chance + "%")
                                    : i == 6 ? Arrays.asList(ChatColor.GRAY + "Set this item to a Givedp Item")
                                    : new ArrayList<>()
                    );
                }
                itemMeta.setLore(lore); lore.clear();
                item.setItemMeta(itemMeta);
                top.setItem(i, item);
            }
            player.updateInventory();
        }
    }
    public void editKitAmount(EditedKit ek) { enterEdit(ek, "AMOUNT"); }
    public void editKitName(EditedKit ek) { enterEdit(ek, "NAME"); }
    public void editKitLore(EditedKit ek) { enterEdit(ek, "LORE"); }
    public void editKitChance(EditedKit ek) { enterEdit(ek, "CHANCE"); }
    private void enterEdit(EditedKit ek, String type) {
        final Player player = ek.player;
        player.closeInventory();
        editing.put(ek, type);
    }

    @EventHandler
    private void playerChatEvent(AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();
        final EditedKit ek = EditedKit.valueOf(player);
        if(ek != null) {
            event.setCancelled(true);
            final String msg = event.getMessage();
            final KitItem target = ek.edited.get(ek.selected);
            final String type = editing.get(ek);
            if(type.equals("AMOUNT")) {
                target.amount = msg;
            } else if(type.equals("NAME")) {
                target.stringName = msg;
            } else if(type.equals("LORE")) {

            } else if(type.equals("CHANCE")) {
                target.chance = Integer.parseInt(msg);
            } else return;
            editing.remove(ek);
            editKitItem(player, ek, ek.selected);
        }
    }



    public boolean hasPermissionToObtain(Player player, Object kit) {
        if(player != null && kit != null) {
            final GlobalKit gkit = kit instanceof GlobalKit ? (GlobalKit) kit : null;
            final EvolutionKit vkit = gkit == null && kit instanceof EvolutionKit ? (EvolutionKit) kit : null;
            final MasteryKit mkit = gkit == null && kit instanceof MasteryKit ? (MasteryKit) kit : null;
            if(gkit == null && vkit == null && mkit == null) return false;

            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            final String n = gkit != null ? gkit.path : vkit != null ? vkit.path : mkit.getPath();
            return gkit != null && (pdata.gkitTiers.keySet().contains(n) || player.hasPermission("RandomPackage.gkit." + n))
                    || vkit != null && (pdata.vkitLevels.keySet().contains(n) || player.hasPermission("RandomPackage.vkit." + n));
        } else {
            return false;
        }
    }
    public void resetAll(CommandSender sender, String target, KitType type) {
        final RPPlayerData pdata = r(sender, target, type);
        if(pdata != null) {
            if(type.equals(KitType.EVOLUTION)) pdata.vkitCooldowns.clear();
            else if(type.equals(KitType.GLOBAL)) pdata.gkitCooldowns.clear();
        }
    }
    public void reset(CommandSender sender, String target, KitType type, String kitName) {
        final RPPlayerData pdata = r(sender, target, type);
        if(pdata != null) {
            if(type.equals(KitType.EVOLUTION)) pdata.vkitCooldowns.put(kitName, 0l);
            else if(type.equals(KitType.GLOBAL)) pdata.gkitCooldowns.put(kitName, 0l);
        }
    }
    private RPPlayerData r(CommandSender sender, String target, KitType type) {
        final OfflinePlayer p = Bukkit.getOfflinePlayer(target);
        final RPPlayerData pdata = RPPlayerData.get(p.getUniqueId());
        final YamlConfiguration yml = type.equals(KitType.EVOLUTION) ? vkits : type.equals(KitType.GLOBAL) ? gkits : null;

        if(pdata == null) {
            sendStringListMessage(sender, yml.getStringList("messages.target-doesnt-exist"));
        } else {
            for(String s : yml.getStringList("messages.success")) sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{PLAYER}", p.getName())));
        }
        return pdata;
    }
    public void preview(Player player, GlobalKit kit, int tier) {
        if(player == null || kit == null) return;
        player.closeInventory();
        final List<ItemStack> rewards = new ArrayList<>();
        final String pn = player.getName(), t = Integer.toString(tier), mt = Integer.toString(kit.maxTier);
        for(KitItem ki : kit.items) {
            final ItemStack is = d(gkits, "kits." + kit.path + ".items." + ki.path, tier);
            if(is != null && is.hasItemMeta()) {
                itemMeta = is.getItemMeta();
                if(itemMeta.hasDisplayName()) {
                    itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{PLAYER}", pn));
                }
                if(itemMeta.hasLore()) {
                    lore.clear();
                    for(String s : itemMeta.getLore()) {
                        lore.add(s.replace("{TIER}", t).replace("{MAX_TIER}", mt));
                    }
                    itemMeta.setLore(lore);
                }
                is.setItemMeta(itemMeta);
            }
            rewards.add(is);
        }
        int s = rewards.size();
        s = s == 9 || s == 18 || s == 27 || s == 36 || s == 45 || s == 54 ? s : s > 54 ? 54 : ((s+9)/9)*9;
        player.openInventory(Bukkit.createInventory(player, s, gkitPreview.getTitle()));
        final Inventory top = player.getOpenInventory().getTopInventory();
        for(ItemStack i : rewards)
            top.setItem(top.firstEmpty(), i);
        for(int i = 0; i < top.getSize(); i++) {
            item = top.getItem(i);
            if(item == null || item.getType().name().contains("AIR"))
                top.setItem(i, gkitPreviewBackground.clone());
        }
        player.updateInventory();
    }
    public void give(Player player, Object kit, int tier, boolean allItems, boolean addCooldown) {
        if(player != null && kit != null) {
            final GlobalKit gkit = kit instanceof GlobalKit ? (GlobalKit) kit : null;
            final EvolutionKit vkit = gkit == null && kit instanceof EvolutionKit ? (EvolutionKit) kit : null;
            final MasteryKit mkit = gkit == null && kit instanceof MasteryKit ? (MasteryKit) kit : null;
            if(gkit == null && vkit == null && mkit == null) return;

            final boolean g = gkit != null, v = vkit != null;
            final int max = g ? gkit.maxTier : v ? vkit.maxLevel : 0;
            final String p = g ? gkit.path : v ? vkit.path : mkit.getPath();
            final YamlConfiguration yml = g ? gkits : v ? vkits : mkits;
            final List<KitItem> kitItems = g ? gkit.items : v ? vkit.items : null;
            if(kitItems == null) return;
            final String pn = player.getName(), t = Integer.toString(tier), mt = Integer.toString(max);
            for(KitItem ki : kitItems) {
                if(allItems || ki.chance == 100 || ki.chance > random.nextInt(100)) {
                    final ItemStack is = d(yml, "kits." + p + ".items." + ki.path, tier);
                    if(is != null && is.hasItemMeta()) {
                        itemMeta = is.getItemMeta();
                        if(itemMeta.hasDisplayName()) {
                            itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{LEVEL}", t).replace("{PLAYER}", pn));
                        }
                        if(itemMeta.hasLore()) {
                            lore.clear();
                            for(String s : itemMeta.getLore()) {
                                lore.add(s.replace("{LEVEL}", t).replace("{TIER}", t).replace("{MAX_TIER}", mt));
                            }
                            itemMeta.setLore(lore); lore.clear();
                        }
                        is.setItemMeta(itemMeta);
                    }
                    giveItem(player, is);
                }
            }
            if(addCooldown) setCooldown(player.getUniqueId(), kit);
        }
    }
    public void give(Player player, GlobalKit kit, int tier, boolean addCooldown) { give(player, kit, tier, false, addCooldown); }
    public void give(Player player, EvolutionKit vkit, boolean preview) {
        if(vkit == null) return;
        final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
        final HashMap<String, Integer> lvls = pdata.vkitLevels;
        final String n = vkit.path;
        final int vkitlvl = lvls.keySet().contains(n) ? lvls.get(n) : player.hasPermission("RandomPackage.vkit." + n) ? 1 : 0;
        final List<ItemStack> rewards = new ArrayList<>();
        for(KitItem ki : vkit.items)
            if(preview || ki.reqLevel <= 0 || vkitlvl >= ki.reqLevel)
                rewards.add(d(vkits, "kits." + n + ".items." + ki.path, vkitlvl));
        if(preview) {
            int s = rewards.size();
            s = s == 9 || s == 18 || s == 27 || s == 36 || s == 45 || s == 54 ? s : s > 54 ? 54 : ((s+9)/9)*9;
            player.openInventory(Bukkit.createInventory(player, s, vkitPreview.getTitle()));
        }
        for(ItemStack is : rewards) {
            if(is != null) {
                item = is.clone(); itemMeta = item.getItemMeta(); lore.clear();
                if(item.hasItemMeta()) {
                    if(itemMeta.hasDisplayName()) {
                        itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{PLAYER}", player.getName()).replace("{LEVEL}", Integer.toString(vkitlvl)));
                    }
                    if(itemMeta.hasLore()) {
                        for(String s : itemMeta.getLore()) {
                            if(s.startsWith("{") && s.contains("reqlevel=")) {
                                final int level = getRemainingInt(s.split(":")[0]), reqlevel = getRemainingInt(s.split("reqlevel=")[1].split(":")[0]);
                                final CustomEnchant enchant = CustomEnchant.valueOf(s.split("\\{")[1].split("}")[0].replace("" + level, ""));
                                if(enchant != null && vkitlvl >= reqlevel) {
                                    lore.add(enchant.getRarity().getApplyColors() + enchant.getName() + " " + toRoman(level != -1 ? level : 1+random.nextInt(enchant.getMaxLevel())));
                                }
                            } else if(s.startsWith("{") && s.contains(":") && s.endsWith("}")) {
                                final String r = s.split(":")[random.nextInt(s.split(":").length)];
                                int level = getRemainingInt(s.split("\\{")[1].split("}")[0]);
                                final CustomEnchant enchant = CustomEnchant.valueOf(r.split("\\{")[1].split("}")[0].replace("" + level, ""));

                                if(enchant != null) {
                                    if(level == -1) level = random.nextInt(enchant.getMaxLevel());
                                    lore.add(enchant.getRarity().getApplyColors() + enchant.getName() + " " + toRoman(level != 0 ? level : 1));
                                }
                            } else
                                lore.add(s.replace("{LEVEL}", Integer.toString(vkitlvl)));
                        }
                        itemMeta.setLore(lore); lore.clear();
                    }
                    item.setItemMeta(itemMeta);
                }
                if(preview) player.getOpenInventory().getTopInventory().addItem(item);
                else        giveItem(player, item);
            }
        }
        final int fe = player.getOpenInventory().getTopInventory().firstEmpty();
        if(preview && fe > -1)
            for(int i = fe; i < player.getOpenInventory().getTopInventory().getSize(); i++)
                player.getOpenInventory().getTopInventory().setItem(i, vkitPreviewBackground.clone());
        if(!preview)
            pdata.vkitCooldowns.put(n, System.currentTimeMillis()+(vkit.cooldown*1000));
        player.updateInventory();
        int upgradechance = vkit.upgradeChance, a = (int) (fapi.getVkitLevelingChance(fapi.getFaction(player))*100);
        upgradechance += a;
        if(!preview && random.nextInt(100) <= upgradechance) {
            final int newlvl = vkitlvl+1;
            if(newlvl > vkit.maxLevel) return;
            final String name = vkit.getDisplayedItem().getItemMeta().getDisplayName();
            pdata.vkitLevels.put(n, newlvl);
            for(String s : vkits.getStringList("messages.upgrade"))
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{LEVEL}", Integer.toString(newlvl)).replace("{VKIT}", name)));
            for(String s : vkits.getStringList("messages.upgrade-broadcast"))
                Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{PLAYER}", player.getName()).replace("{VKIT}", name).replace("{LEVEL}", Integer.toString(newlvl))));
        }
    }

    @EventHandler
    private void entityDeathEvent(EntityDeathEvent event) {
        if(!(event.getEntity() instanceof Player)) {
            final FallenHero f = FallenHero.valueOf(event.getEntity().getUniqueId());
            if(f != null) {
                f.kill(event.getEntity(), event.getEntity().getKiller());
                event.setDroppedExp(0);
                event.getDrops().clear();
            }
        }
    }
    @EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        FallenHero f = !event.isCancelled() && event.getAction().equals(Action.RIGHT_CLICK_BLOCK) ? FallenHero.valueOf(event.getItem()) : null;
        if(f != null) {
            final String spawnloc = f.getSpawnLocation().toLowerCase();
            if(!spawnloc.equals("anywhere")) {
                if(spawnloc.equals("warzone") && !fapi.locationIsWarZone(event.getClickedBlock()))
                    return;
            }
            removeItem(player, event.getItem(), 1);
            final Location c = event.getClickedBlock().getLocation();
            f.spawn(player, new Location(c.getWorld(), c.getX(), c.getY()+1, c.getZ()));
        } else {
            f = FallenHero.valueOF(event.getItem());
            if(f != null) {
                final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
                final GlobalKit g = f.getGlobalKit();
                final String n = g != null ? g.path : f.getEvolutionKit().path;
                final HashMap<String, String> replacements = new HashMap<>();
                if(g != null) {
                    final HashMap<String, Integer> tiers = pdata.gkitTiers;
                    if(!tiers.keySet().contains(n) && player.hasPermission("RandomPackage.gkit." + n)) tiers.put(n, 1);
                    if(tiers.keySet().contains(n)) {
                        if(tiers.get(n) != g.maxTier) tiers.put(n, tiers.get(n)+1);
                        else return;
                    } else {
                        replacements.put("{NAME}", f.getFallenHeroName());
                        tiers.put(n, 1);
                        sendStringListMessage(player, Kits.getKits().gkits.getStringList("messages.redeem"), replacements);
                    }
                } else {
                    final HashMap<String, Integer> levels = pdata.vkitLevels;
                    final EvolutionKit v = f.getEvolutionKit();
                    if(levels.keySet().contains(n)) {
                        if(levels.get(n) != v.maxLevel) levels.put(n, levels.get(n)+1);
                        else {
                            sendStringListMessage(player, Kits.getKits().vkits.getStringList("messages.already-have-max"));
                            return;
                        }
                    } else {
                        replacements.put("{NAME}", f.getFallenHeroName());
                        levels.put(n, 1);
                        sendStringListMessage(player, Kits.getKits().vkits.getStringList("messages.redeem"), replacements);
                    }
                }
                removeItem(player, event.getItem(), 1);
            }
        }
    }

    private class gkitevents extends RandomPackageAPI implements Listener {

        @EventHandler
        private void inventoryClickEvent(InventoryClickEvent event) {
            if(!event.isCancelled() && event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR) && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
                final Player player = (Player) event.getWhoClicked();
                final String t = event.getWhoClicked().getOpenInventory().getTopInventory().getTitle();
                final int r = event.getRawSlot();
                if(t.equals(gkit.getTitle()) || t.equals(gkitPreview.getTitle())) {
                    event.setCancelled(true);
                    player.updateInventory();
                    final GlobalKit gkit = GlobalKit.valueOf(r);
                    if(gkit == null || r < 0 || r >= player.getOpenInventory().getTopInventory().getSize()) return;

                    final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
                    final int tier = pdata.getGkitTier(gkit.path);
                    if(t.equals(gkitPreview.getTitle())) {
                        player.closeInventory();
                        sendStringListMessage(player, gkits.getStringList("messages.cannot-withdraw"));
                    } else if(event.getClick().name().contains("RIGHT")) {
                        preview(player, gkit, tier);
                    } else {
                        final String n = gkit.path;
                        final HashMap<String, Long> cooldowns = pdata.gkitCooldowns;
                        final HashMap<String, Integer> tiers = pdata.gkitTiers;
                        final boolean hasPerm = hasPermissionToObtain(player, gkit);
                        if(!hasPerm) {
                            sendStringListMessage(player, gkits.getStringList("messages.not-unlocked-kit"));
                        } else if(tiers.keySet().contains(n) && !cooldowns.keySet().contains(n)
                                || !tiers.keySet().contains(n) && player.hasPermission("RandomPackage.gkit." + n) && !cooldowns.keySet().contains(n)
                                || cooldowns.keySet().contains(n) && cooldowns.get(n) <= System.currentTimeMillis()) {
                            cooldowns.put(n, System.currentTimeMillis()+(gkit.cooldown*1000));
                            give(player, gkit, tier, false);
                            setCooldown(player, gkit);
                        }
                    }
                } else {
                    final EditedKit ek = EditedKit.valueOf(player);
                    if(ek != null) {
                        event.setCancelled(true);
                        player.updateInventory();
                        if(ek.selected == -1) editKitItem(player, ek, r);
                        else if(r == 2) editKitAmount(ek);
                        else if(r == 3) editKitName(ek);
                        else if(r == 4) editKitLore(ek);
                        else if(r == 5) editKitChance(ek);
                        else if(r == 8) {
                            final GlobalKit gkit = (GlobalKit) ek.original.get(0).kit;
                            ek.delete();
                            edit(player, gkit, false);
                        }
                    }
                }
            }
        }

        @EventHandler
        private void playerInteractEvent(PlayerInteractEvent event) {
            if(event.getItem() != null && event.getItem().hasItemMeta() && event.getItem().getItemMeta().equals(heroicFallenHeroBundle.getItemMeta())) {
                event.setCancelled(true);
                removeItem(event.getPlayer(), event.getItem(), 1);
                final List<String> s = gkits.getStringList("items.heroic-fallen-hero-bundle.reveals");
                for(int i = 1; i <= gkits.getInt("items.heroic-fallen-hero-bundle.reveal-amount"); i++) {
                    giveItem(event.getPlayer(), FallenHero.valueOf(GlobalKit.valueOf(s.get(random.nextInt(s.size())))).getSpawnItem().clone());
                }
            }
        }
    }

    private class vkitevents extends RandomPackageAPI implements Listener {

        @EventHandler
        private void inventoryClickEvent(InventoryClickEvent event) {
            if(!event.isCancelled() && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
                final String t = event.getWhoClicked().getOpenInventory().getTopInventory().getTitle();
                if(t.equals(vkit.getTitle()) || t.equals(vkitPreview.getTitle())) {
                    final Player player = (Player) event.getWhoClicked();
                    event.setCancelled(true);
                    player.updateInventory();
                    if(event.getRawSlot() >= player.getOpenInventory().getTopInventory().getSize()) return;

                    final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
                    if(t.equals(vkitPreview.getTitle())) {
                        player.closeInventory();
                        sendStringListMessage(player, vkits.getStringList("messages.cannot-withdraw"));
                    } else if(event.getClick().name().contains("RIGHT")) {
                        player.closeInventory();
                        final EvolutionKit vkit = EvolutionKit.valueOf(event.getRawSlot());
                        give(player, vkit, true);
                    } else {
                        final EvolutionKit vkit = EvolutionKit.valueOf(event.getRawSlot());
                        if(vkit == null) return;
                        final String n = vkit.path;
                        final HashMap<String, Long> cooldowns = pdata.vkitCooldowns;
                        final HashMap<String, Integer> levels = pdata.vkitLevels;
                        final boolean hasPerm = hasPermissionToObtain(player, vkit);
                        final long time = System.currentTimeMillis();
                        if(!hasPerm) {
                            sendStringListMessage(player, vkits.getStringList("messages.not-unlocked-kit"));
                        } else if(!cooldowns.keySet().contains(n) && (levels.keySet().contains(n) || !levels.keySet().contains(n) && player.hasPermission("RandomPackage.vkit." + n))
                                || cooldowns.keySet().contains(n) && cooldowns.get(n) <= time) {
                            give(player, vkit, false);
                            cooldowns.put(n, time+(vkit.cooldown*1000));
                            setCooldown(player, vkit);
                        }
                    }
                }
            }
        }
    }

    private class mkitevents extends RandomPackageAPI implements Listener {

        @EventHandler
        private void inventoryClickEvent(InventoryClickEvent event) {
            final Player player = (Player) event.getWhoClicked();
            if(!event.isCancelled() && player.getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
                final String t = player.getOpenInventory().getTopInventory().getTitle();
                if(t.equals(mkit.getTitle())) {
                    event.setCancelled(true);
                    player.updateInventory();
                    final int r = event.getRawSlot();
                    final String c = event.getClick().name();
                    final MasteryKit m = MasteryKit.valueOf(r);
                    if(r < 0 || r >= player.getOpenInventory().getTopInventory().getSize() || !c.contains("LEFT") && !c.contains("RIGHT") || event.getCurrentItem() == null || m == null) return;
                    final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
                    if(pdata.masterykits.keySet().contains(m)) {

                    }
                }
            }
        }
    }
}

package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.randomHashTags.RandomPackage.utils.classes.custombosses.*;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.SlimeSplitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.DisplaySlot;

import me.randomHashTags.RandomPackage.RandomPackageAPI;

public class CustomBosses extends RandomPackageAPI implements Listener {
	public boolean isEnabled = false;
	private static CustomBosses instance;
	public static final CustomBosses getCustomBosses() {
		if(instance == null) instance = new CustomBosses();
		return instance;
	}
	public YamlConfiguration config;
	private HashMap<UUID, LivingCustomBoss> deadBosses = new HashMap<>();

	public void enable() {
		if(isEnabled) return;
		save("Features", "custom bosses.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features", "custom bosses.yml"));
		final List<ItemStack> j = new ArrayList<>();
		for(String s : config.getConfigurationSection("bosses").getKeys(false)) {
			final CustomMinion minion = new CustomMinion(config.getInt("bosses." + s + ".minion.max"), config.getString("bosses." + s + ".minion.type").toUpperCase(), ChatColor.translateAlternateColorCodes('&', config.getString("bosses." + s + ".minion.name")), config.getStringList("bosses." + s + ".minion.attributes"));
			List<CustomBossAttack> attacks = new ArrayList<>();
			for(int i = 0; i <= 100; i++)
				if(config.get("bosses." + s + ".attacks." + i) != null)
					attacks.add(new CustomBossAttack(config.getInt("bosses." + s + ".attacks." + i + ".chance"), config.getInt("bosses." + s + ".attacks." + i + ".radius"), config.getStringList("bosses." + s + ".attacks." + i + ".attack")));
			HashMap<Integer, List<String>> messages = new HashMap<>();
			messages.put(-5, config.getStringList("bosses." + s + ".messages.summon"));
			messages.put(-4, config.getStringList("bosses." + s + ".messages.summon-broadcast"));
			messages.put(-3, config.getStringList("bosses." + s + ".messages.defeated"));
			messages.put(-2, config.getStringList("bosses." + s + ".messages.defeated-broadcast"));
			for(int i = 0; i <= 100; i++) if(config.get("bosses." + s + ".messages." + i) != null) messages.put(i, config.getStringList("bosses." + s + ".messages." + i));
			final CustomBoss c = new CustomBoss(s, d(config, "bosses." + s + ".spawn-item", 0), config.getString("bosses." + s + ".type").toUpperCase(), ChatColor.translateAlternateColorCodes('&', config.getString("bosses." + s + ".name")), ChatColor.translateAlternateColorCodes('&', config.getString("bosses." + s + ".scoreboard.title")), DisplaySlot.valueOf(config.getString("bosses." + s + ".scoreboard.display-slot").toUpperCase()), config.getStringList("bosses." + s + ".scoreboard.scores"), config.getStringList("bosses." + s + ".attributes"), config.getStringList("bosses." + s + ".rewards"), attacks, config.getInt("bosses." + s + ".messages.radius"), messages, minion, config.getInt("bosses." + s + ".minion.max"));
			j.add(c.getSpawnItem());
		}
		loadBackup();
		addGivedpCategory(j, UMaterial.SPIDER_SPAWN_EGG, "Custom Bosses", "Givedp: Custom Bosses");
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		backup();
		LivingCustomBoss.living.clear();
		LivingCustomMinion.living.clear();
		CustomBoss.bosses.clear();
		CustomMinion.minions.clear();
		HandlerList.unregisterAll(this);
	}

	public void backup() {
		otherdata.set("bosses", null);
		for(LivingCustomBoss l : LivingCustomBoss.living) {
			final UUID u = l.entity.getUniqueId();
			otherdata.set("bosses." + u + ".type", l.type.path);
			final HashMap<UUID, Double> damagers = l.damagers;
			for(UUID d : damagers.keySet())
				otherdata.set("bosses." + u + ".damagers." + d, damagers.get(d));
		}
		saveOtherData();
	}
	public void loadBackup() {
		final ConfigurationSection bosses = otherdata.getConfigurationSection("bosses");
		if(bosses != null) {
			for(String s : bosses.getKeys(false)) {
				final UUID u = UUID.fromString(s);
				final Entity e = getEntity(u);
				if(e != null && !e.isDead()) {
					final LivingCustomBoss l = new LivingCustomBoss(null, (LivingEntity) e, CustomBoss.valueOf(otherdata.getString("bosses." + s + ".type")));
					final ConfigurationSection d = otherdata.getConfigurationSection("bosses." + s + ".damagers");
					if(d != null)
						for(String a : d.getKeys(false))
							l.damagers.put(UUID.fromString(a), otherdata.getDouble("bosses." + s + ".damagers." + a));
				}
			}
		}
	}

	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		final ItemStack I = event.getItem();
		if(I != null && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			final CustomBoss c = CustomBoss.valueOf(I);
			if(c != null) {
				final Player player = event.getPlayer();
				event.setCancelled(true);
				player.updateInventory();
				removeItem(player, I, 1);
				final Location l = event.getClickedBlock().getLocation();
				c.spawn(player, new Location(l.getWorld(), l.getX(), l.getY()+1, l.getZ()));
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if(!event.isCancelled()) {
			final LivingCustomBoss c = LivingCustomBoss.valueOf(event.getEntity().getUniqueId());
			if(c != null) {
				final LivingEntity entity = (LivingEntity) event.getEntity();
				c.damage(entity, event.getDamager(), event.getFinalDamage());
			}
		}
	}
	@EventHandler
	private void slimeSplitEvent(SlimeSplitEvent event) {
		final UUID u = event.getEntity().getUniqueId();
		if(deadBosses.keySet().contains(u)) {
			for(String a : deadBosses.get(u).type.attributes) {
				if(a.toLowerCase().startsWith("split=false"))
					event.setCancelled(true);
			}
			deadBosses.remove(u);
		}
	}
	@EventHandler
	private void entityDeathEvent(EntityDeathEvent event) {
		final LivingEntity e = event.getEntity();
		final UUID u = e.getUniqueId();
		final LivingCustomBoss c = LivingCustomBoss.valueOf(u);
		final LivingCustomMinion m = c == null ? LivingCustomMinion.valueOf(u) : null;
		if(c != null || m != null) {
			final EntityDamageEvent ede = e.getLastDamageCause();
			event.setDroppedExp(0); event.getDrops().clear();
			if(c != null) {
				deadBosses.put(u, c);
				c.kill(e, ede);
			} else {
				m.kill(e, ede);
			}
		}
	}
}

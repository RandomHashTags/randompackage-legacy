package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.*;

import me.randomHashTags.RandomPackage.RandomPackage;
import me.randomHashTags.RandomPackage.utils.classes.globalchallenges.GlobalChallengePrize;
import me.randomHashTags.RandomPackage.utils.supported.MCMMOAPI;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.coinflips.CoinFlipMatch;
import me.randomHashTags.RandomPackage.utils.classes.globalchallenges.GlobalChallenge;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;
import me.randomHashTags.RandomPackage.api.events.AlchemistExchangeEvent;
import me.randomHashTags.RandomPackage.api.events.CoinFlipChallengeEndEvent;
import me.randomHashTags.RandomPackage.api.events.EnchanterPurchaseEvent;
import me.randomHashTags.RandomPackage.api.events.FundDepositEvent;
import me.randomHashTags.RandomPackage.api.events.GlobalChallengeEndEvent;
import me.randomHashTags.RandomPackage.api.events.TinkererTradeEvent;
import me.randomHashTags.RandomPackage.api.events.customenchant.CustomEnchantProcEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerApplyCustomEnchantEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerGlobalChallengeParticipateEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerRevealCustomEnchantEvent;
import static java.util.stream.Collectors.*;

@SuppressWarnings({"deprecation"})
public class GlobalChallenges extends RandomPackageAPI implements CommandExecutor, Listener {

	public boolean isEnabled = false;
	private static GlobalChallenges instance;
	public static final GlobalChallenges getChallenges() {
		if(instance == null) instance = new GlobalChallenges();
		return instance;
	}
	
	public YamlConfiguration config;
	private Inventory inv, leaderboard, claimPrizes;
	private int topPlayersSize = 54;

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(args.length == 0 && player != null)
		    viewCurrent(player);
		else if(args.length >= 2 && args[0].equals("stop")) {
			if(hasPermission(player, "RandomPackage.globalchallenges.stop", true))
				stopChallenge(GlobalChallenge.valueOf(args[1].replace("_", " ")), false);
		} else if(player != null && args.length >= 1 && args[0].equals("claim"))
			viewPrizes(player);
		else if(args.length >= 1 && args[0].equals("reload")) {
			if(hasPermission(sender, "RandomPackage.globalchallenges.reload", true))
				reloadChallenges();
		} else if(args.length >= 3 && args[0].equals("giveprize")) {
			if(hasPermission(sender, "RandomPackage.globalchallenges.giveprize", true)) {
				final OfflinePlayer op = Bukkit.getOfflinePlayer(args[1]);
				final int placing = getRemainingInt(args[2]);
				if(op != null && placing != -1) {
					RPPlayerData.get(op.getUniqueId()).addGlobalChallengePrize(GlobalChallengePrize.valueOf(placing));
				}
			}
		}
		return true;
	}

	public void enable() {
	    final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "global challenges.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		if(RandomPackage.mcmmo != null) MCMMOAPI.getMCMMOAPI().gcIsEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "global challenges.yml"));
		inv = Bukkit.createInventory(null, config.getInt("gui.size"), ChatColor.translateAlternateColorCodes('&', config.getString("gui.title")));
		topPlayersSize = config.getInt("challenge leaderboard.how many");
		leaderboard = Bukkit.createInventory(null, ((topPlayersSize+9)/9)*9, ChatColor.translateAlternateColorCodes('&', config.getString("challenge leaderboard.title")));
		claimPrizes = Bukkit.createInventory(null, 9, ChatColor.translateAlternateColorCodes('&', config.getString("rewards.title")));
		int loaded = 0, loadedPrizes = 0;
		for(String s : config.getConfigurationSection("challenges").getKeys(false)) {
			new GlobalChallenge(s, config.getString("challenges." + s + ".type"), config.getString("challenges." + s + ".tracks"), d(config, "challenges." + s, 0), new ArrayList<>());
			loaded += 1;
		}
		for(String s : config.getConfigurationSection("rewards").getKeys(false)) {
			if(!s.equals("title")) {
				new GlobalChallengePrize(d(config, "rewards." + s + ".prize", 0), config.getInt("rewards." + s + ".amount"), Integer.parseInt(s), config.getStringList("rewards." + s + ".prizes"));
				loadedPrizes += 1;
			}
		}
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " global challenges and " + loadedPrizes + " prizes &e(took " + (System.currentTimeMillis()-started) + "ms)"));
		reloadChallenges();
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		otherdata.set("active global challenges", null);
		for(GlobalChallenge c : GlobalChallenge.active) {
			final String p = c.path;
			otherdata.set("active global challenges." + p + ".started", c.started);
			final HashMap<UUID, Double> participants = getParticipants(c);
			for(UUID u : participants.keySet())
				otherdata.set("active global challenges." + p + ".participants." + u, participants.get(u));
		}
		saveOtherData();
		GlobalChallenge.active.clear();
		GlobalChallenge.challenges.clear();
		GlobalChallengePrize.prizes.clear();
		HandlerList.unregisterAll(this);
	}
	public void reloadChallenges() {
		final int max = config.getInt("challenge settings.max at once");
		int maxAtOnce = max;
		final ConfigurationSection EEE = otherdata.getConfigurationSection("active global challenges");
		if(EEE != null) {
		    final long started = System.currentTimeMillis();
			int loadeD = 0;
			for(String s : EEE.getKeys(false)) {
				if(maxAtOnce > 0) {
					loadeD += 1;
					final GlobalChallenge g = GlobalChallenge.valueOf(s);
					if(g != null) {
						final List<UUID> participants = new ArrayList<>();
						final ConfigurationSection partic = otherdata.getConfigurationSection("active global challenges." + s + ".participants");
						if(partic != null) {
							for(String u : partic.getKeys(false)) {
								final UUID uuid = UUID.fromString(u);
								participants.add(uuid);
								RPPlayerData.get(uuid).getGlobalChallenges().put(g, otherdata.getDouble("active global challenges." + s + ".participants." + u));
							}
						}
						maxAtOnce -= 1;
						g.begin(Long.parseLong(otherdata.getString("active global challenges." + s + ".started")), participants);
					}
				}
			}
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aStarted " + loadeD + " pre-exiting global challenges &e(took " + (System.currentTimeMillis()-started) + "ms)"));
		}
		if(maxAtOnce > 0) {
		    final long started = System.currentTimeMillis();
			for(int i = 1; i <= maxAtOnce; i++) {
				final GlobalChallenge gc = getRandomChallenge();
				if(!gc.isActive()) gc.begin(started);
				else i-=1;
			}
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aStarted " + maxAtOnce + " new global challenges &e(took " + (System.currentTimeMillis()-started) + "ms)"));
		}
		reloadInventory();
	}

	private void reloadInventory() {
		int f = 0;
		for(int i = 0; i < inv.getSize(); i++) {
			if(config.get("gui." + i) != null) {
				final String p = config.getString("gui." + i + ".item");
				if(p.toUpperCase().equals("{CHALLENGE}")) {
					GlobalChallenge z = f < GlobalChallenge.active.size() ? GlobalChallenge.active.get(f) : null;
					if(z == null) z = getRandomChallenge().begin(System.currentTimeMillis());
					item = z.getDisplayItem().clone();
					itemMeta = item.getItemMeta(); lore.clear();
					if(f < GlobalChallenge.active.size()) {
						final String n = z.name;
						for(String s : config.getStringList("challenge settings.added lore")) {
							if(s.contains("{TYPE}")) s = s.replace("{TYPE}", n);
							lore.add(ChatColor.translateAlternateColorCodes('&', s));
						}
					}
					itemMeta.setLore(lore); lore.clear();
					item.setItemMeta(itemMeta);
					f += 1;
				} else {
					item = d(config, "gui." + i, 0);
				}
				inv.setItem(i, item);
			}
		}
	}


	public void viewPrizes(Player player) {
		if(hasPermission(player, "RandomPackage.globalchallenges.claim", true)) {
			final HashMap<GlobalChallengePrize, Integer> prizes = RPPlayerData.get(player.getUniqueId()).globalChallengePrizes;
			int size = (prizes.size()/9)*9;
			size = size == 0 ? 9 : size > 54 ? 54 : size;
			player.openInventory(Bukkit.createInventory(player, size, claimPrizes.getTitle()));
			final Inventory top = player.getOpenInventory().getTopInventory();
			player.updateInventory();
			for(GlobalChallengePrize prize : prizes.keySet()) {
				item = prize.getDisplay(); item.setAmount(prizes.get(prize));
				top.addItem(item);
			}
		}
	}
	public void givePrize(UUID player, GlobalChallengePrize prize, boolean sendMessage) {
		final OfflinePlayer op = Bukkit.getOfflinePlayer(player);
		if(op != null && op.isOnline()) {
			final Player p = op.getPlayer();
			final HashMap<String, ItemStack> rewards = prize.getRandomRewards();
			for(String s : rewards.keySet()) {
				item = rewards.get(s);
				final String S = s.toLowerCase();
				if(S.contains("spawner") && item.hasItemMeta() && item.getItemMeta().hasLore() && item.getItemMeta().getLore().size() == 1 && !S.contains("mysterymobspawner")) {
					giveSpawner(p, item.getItemMeta().getLore().get(0), null, item.getAmount());
					item = null;
				} else if(S.contains("xpbottle:") || S.contains("banknote:")) {
					itemMeta = item.getItemMeta(); lore.clear();
					if(itemMeta.hasLore()) {
						for(String l : itemMeta.getLore()) {
							if(S.contains("banknote:")) {
								l = l.replace("{PLAYER}", "Global Challenge Prize");
							} else if(S.contains("xpbottle:")) {
								l = l.replace("{BOTTLER_NAME}", "Global Challenge Prize");
							}
							lore.add(l);
						}
						itemMeta.setLore(lore); lore.clear();
					}
					item.setItemMeta(itemMeta);
				}
				if(item != null)
					giveItem(p, item);
			}
			if(sendMessage) {
				final String placing = prize.getPlacement() + "";
				for(String s : config.getStringList("messages.claimed prize"))
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{PLACING}", placing)));
			}
		}
	}
	public String getRemainingTime(GlobalChallenge chall) {
		final String timeformat = ChatColor.translateAlternateColorCodes('&', config.getString("challenge settings.time left format"));
		final long timeleft = chall.getRemainingTime()/1000;
		int sec = (int) timeleft, min = sec/60;
		sec -= min*60;
		int hr = min/60;
		min -= hr*60;
		int days = hr/24;
		hr -= days*24;
		return timeformat.replace("{HOUR}", Integer.toString(hr)).replace("{MIN}", Integer.toString(min)).replace("{SEC}", Integer.toString(sec));
	}
	public HashMap<UUID, Double> getParticipants(GlobalChallenge challenge) {
		final HashMap<UUID, Double> participants = new HashMap<>();
		for(UUID u : challenge.participants)
			participants.put(u, RPPlayerData.get(u).getGlobalChallenges().get(challenge));
		return participants;
	}
	public Map<UUID, Double> getPlacing(HashMap<UUID, Double> participants) {
		return participants.entrySet().stream().sorted(Map.Entry.<UUID, Double> comparingByValue().reversed()).collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}
	public int getRanking(UUID player, GlobalChallenge g) {
		final Map<UUID, Double> byValue = getParticipants(g).entrySet().stream().sorted(Map.Entry.<UUID, Double> comparingByValue().reversed()).collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		int placement = indexOf(byValue.keySet(), player);
		placement = placement == -1 ? byValue.keySet().size() : placement+1;
		return placement;
	}
	public String getRanking(int rank) {
		String ranking = formatInt(rank);
		ranking = ranking + (ranking.endsWith("1") ? "st" : ranking.endsWith("2") ? "nd" : ranking.endsWith("3") ? "rd" : ranking.equals("0") ? "" : "th");
		return ranking;
	}
	public HashMap<Integer, UUID> getRankings(GlobalChallenge g) {
		final List<UUID> participants = g.participants;
		final HashMap<Integer, UUID> rankings = new HashMap<>();
		for(UUID u : participants)
			rankings.put(getRanking(u, g), u);
		return rankings;
	}
	public void viewCurrent(Player player) {
		if(hasPermission(player, "RandomPackage.globalchallenges", true)) {
			final UUID u = player.getUniqueId();
			final HashMap<GlobalChallenge, Double> challenges = RPPlayerData.get(u).getGlobalChallenges();
			player.openInventory(Bukkit.createInventory(player, inv.getSize(), inv.getTitle()));
			final Inventory top = player.getOpenInventory().getTopInventory();
			top.setContents(inv.getContents());
			player.updateInventory();
			for(int i = 0; i < top.getSize(); i++) {
				item = top.getItem(i);
				if(item != null) {
					final GlobalChallenge g = GlobalChallenge.valueOf(item);
					if(g != null) {
						final HashMap<UUID, Double> participants = getParticipants(g);
						final Map<UUID, Double> placings = getPlacing(participants);
						int topp = 0;
						UUID ranked = topp < placings.size() ? (UUID) placings.keySet().toArray()[topp] : null;
						final String remainingtime = getRemainingTime(g);
						itemMeta = item.getItemMeta(); lore.clear();
						if(item.hasItemMeta()) {
							if(itemMeta.hasLore()) {
								final String ranking = getRanking(getRanking(u, g));
								for(String s : itemMeta.getLore()) {
									if(s.contains("{DATE}")) s = s.replace("{DATE}", toReadableDate(new Date(g.started), config.getString("challenge settings.date format")));
									if(s.contains("{YOUR_VALUE}")) s = s.replace("{YOUR_VALUE}", formatDouble(g.participants.contains(u) ? challenges.get(g) : 0));
									if(s.contains("{YOUR_RANKING}")) s = s.replace("{YOUR_RANKING}", ranking);
									if(s.contains("{TIME_LEFT}")) s = s.replace("{TIME_LEFT}", remainingtime);
									if(s.contains("{TOP}")) {
										if(ranked == null) {
											s = s.replace("{TOP}", "None").replace("{VALUE}", "0");
										} else {
											s = s.replace("{TOP}", Bukkit.getOfflinePlayer(ranked).getName());
											if(s.contains("{VALUE}")) {
												s = s.replace("{VALUE}", formatDouble(participants.get(ranked)));
											}
										}
										topp += 1;
										ranked = topp < placings.size() ? (UUID) placings.keySet().toArray()[topp] : null;
									}
									lore.add(s);
								}
							}
							itemMeta.setLore(lore); lore.clear();
							item.setItemMeta(itemMeta);
						}
					}
				}
			}
			player.updateInventory();
		}
	}
	public void stopChallenge(GlobalChallenge chall, boolean giveRewards) { chall.end(giveRewards ,3); }
	public void viewTopPlayers(Player player, GlobalChallenge active) {
		player.closeInventory();
		player.openInventory(Bukkit.createInventory(player, leaderboard.getSize(), leaderboard.getTitle()));
		final Inventory top = player.getOpenInventory().getTopInventory();
		final String n = ChatColor.translateAlternateColorCodes('&', config.getString("challenge leaderboard.name")), N = active.name;
		final HashMap<Integer, UUID> rankings = getRankings(active);
		for(int i = 0; i < topPlayersSize && i < rankings.keySet().size(); i++) {
			final RPPlayerData pdata = RPPlayerData.get(rankings.get(i+1));
			final OfflinePlayer offlineplayer = pdata.getOfflinePlayer();
			final String ranking = getRanking(i+1), name = offlineplayer.getName(), value = formatDouble(pdata.getGlobalChallenges().get(active));
			item = new ItemStack(UMaterial.PLAYER_HEAD_ITEM.getMaterial(), i+1, (byte) 3);
			final SkullMeta skm = (SkullMeta) item.getItemMeta();
			skm.setDisplayName(n.replace("{PLAYER}", name));
			skm.setOwner(name); lore.clear();
			for(String s : config.getStringList("challenge leaderboard.lore")) {
				if(s.contains("{RANKING}")) s = s.replace("{RANKING}", ranking);
				if(s.contains("{CHALLENGE}")) s = s.replace("{CHALLENGE}", N);
				if(s.contains("{VALUE}")) s = s.replace("{VALUE}", value);
				lore.add(ChatColor.translateAlternateColorCodes('&', s));
			}
			skm.setLore(lore); lore.clear();
			item.setItemMeta(skm);
			top.setItem(i, item);
		}
		player.updateInventory();
	}
	public GlobalChallenge getRandomChallenge() { final List<GlobalChallenge> g = GlobalChallenge.challenges; return g.get(random.nextInt(g.size())); }
	
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getWhoClicked() == event.getWhoClicked().getOpenInventory().getTopInventory().getHolder()) {
			final Player player = (Player) event.getWhoClicked();
			final String t = event.getWhoClicked().getOpenInventory().getTopInventory().getTitle();
			if(t.equals(inv.getTitle()) || t.equals(leaderboard.getTitle()) || t.equals(claimPrizes.getTitle())) {
				event.setCancelled(true);
				player.updateInventory();
				
				if(event.getRawSlot() >= event.getWhoClicked().getOpenInventory().getTopInventory().getSize() || event.getRawSlot() < 0 || !event.getClick().equals(ClickType.LEFT) && !event.getClick().equals(ClickType.RIGHT) || event.getCurrentItem() == null) return;
				if(t.equals(inv.getTitle())) {
					final GlobalChallenge g = GlobalChallenge.valueOf(event.getCurrentItem());
					if(g != null) {
						player.closeInventory();
						viewTopPlayers(player, g);
					}
				} else if(t.equals(claimPrizes.getTitle())) {
					final GlobalChallengePrize prize = GlobalChallengePrize.valueOf(event.getCurrentItem());
					givePrize(player.getUniqueId(), prize, true);
					item = event.getCurrentItem().clone();
					item = item.getAmount() == 1 ? new ItemStack(Material.AIR) : item;
					player.getOpenInventory().getTopInventory().setItem(event.getRawSlot(), item);
				}
				player.updateInventory();
			}
		}
	}
	@EventHandler(priority = EventPriority.LOWEST)
	private void globalChallengeEndEvent(GlobalChallengeEndEvent event) {
		for(int i = 0; i < inv.getSize(); i++) {
			item = inv.getItem(i);
			if(item != null && !item.getType().equals(Material.AIR)) {
				final GlobalChallenge gc = GlobalChallenge.valueOf(item);
				if(gc != null) {
					reloadInventory();
				}
			}
		}
	}
	
	public void increase(Event event, String input, UUID player, double increaseBy) {
		input = input.toLowerCase();
		for(GlobalChallenge g : GlobalChallenge.active) {
			final String t = g.tracks.split(";")[0].toLowerCase();
			if(t.equals(input)
					|| t.startsWith("blocksminedbymaterial_") && input.startsWith("blocksminedbymaterial_") && input.split("blocksminedbymaterial_")[1].endsWith(t.split("blocksminedbymaterial_")[1])
					|| t.startsWith("customenchantmprocs_") && input.startsWith("customenchantmprocs_") && input.split("customenchantmprocs_")[1].endsWith(t.split("customenchantmprocs")[1])) {
				final PlayerGlobalChallengeParticipateEvent e = new PlayerGlobalChallengeParticipateEvent(g, event, input, increaseBy);
				pluginmanager.callEvent(e);
				if(!e.isCancelled())
					RPPlayerData.get(player).increaseGlobalChallenge(g, increaseBy);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if(!event.isCancelled()) {
			final UUID damager = event.getDamager().getUniqueId();
			if(event.getDamager() instanceof Player) {
				final double dmg = event.getFinalDamage();
				increase(event, "pvadamage", damager, dmg);
				final Entity e = event.getEntity();
				if(e instanceof Player) increase(event, "pvpdamage", damager, dmg);
				else if(e instanceof LivingEntity) increase(event, "pvedamage", damager, dmg);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void entityDeathEvent(EntityDeathEvent event) {
		final LivingEntity e = event.getEntity();
		final UUID u = e.getUniqueId();
		if(e instanceof Player) {
			increase(event, "playerdeaths", u, 1);
		} else {
		    final Player killer = e.getKiller();
            if(killer != null) {
                final UUID k = killer.getUniqueId();
                if(e instanceof Monster) increase(event, "hostilemobskilled", k, 1);
                else increase(event, "passivemobskilled", k, 1);
                increase(event, "mobskilled", k, 1);
            }
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void blockPlaceEvent(BlockPlaceEvent event) {
		if(!event.isCancelled()) {
			final UUID player = event.getPlayer().getUniqueId();
			increase(event, "blocksplaced", player, 1);
			final Block b = event.getBlock();
			increase(event, b.getType().name() + ":" + b.getData() + "_placed", player, 1);
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void blockBreakEvent(BlockBreakEvent event) {
		if(!event.isCancelled()) {
			final UUID player = event.getPlayer().getUniqueId();
			increase(event, "blocksmined", player, 1);
			final Block b = event.getBlock();
			increase(event, b.getType().name() + ":" + b.getData() + "_mined", player, 1);
			final String m = getItemInHand(event.getPlayer()).getType().name();
			increase(event, "blocksminedbymaterial_" + m, player, 1);
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void playerFishEvent(PlayerFishEvent event) {
		final State s = event.getState();
		final UUID player = event.getPlayer().getUniqueId();
		if(s.equals(State.CAUGHT_FISH)) {
			increase(event, "fishcaught", player, 1);
			if(event.getCaught() instanceof Fish) increase(event, event.getCaught().getType().name() + "_Caught", player, 1);
		} else if(s.equals(State.CAUGHT_ENTITY)) {
			increase(event, "treasurecaught", player, 1);
			if(event.getCaught() instanceof Item) {
				final ItemStack i = ((Item) event.getCaught()).getItemStack();
				increase(event, i.getType().name() + ":" + i.getData().getData() + "_caught", player, 1);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void playerExpChangeEvent(PlayerExpChangeEvent event) {
		if(event.getAmount() > 0) {
			increase(event, "expgained", event.getPlayer().getUniqueId(), event.getAmount());
		}
	}
	/*
	 * RandomPackage Events
	 */
	@EventHandler
	private void alchemistExchangeEvent(AlchemistExchangeEvent event) {
		if(!event.isCancelled()) {
			increase(event, "alchemistexchanges", event.getPlayer().getUniqueId(), 1);
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void playerRevealCustomEnchantEvent(PlayerRevealCustomEnchantEvent event) {
		if(!event.isCancelled()) {
			final UUID player = event.getPlayer().getUniqueId();
			increase(event, "customenchantsrevealed", player, 1);
			increase(event, "customenchantsrevealed_" + event.getEnchant().getRarity().getName(), player, 1);
		}
	}
	@EventHandler(priority = EventPriority.LOWEST)
	private void coinFlipChallengeEndEvent(CoinFlipChallengeEndEvent event) {
		final UUID winner = event.getWinner().getUniqueId(), loser = event.getLoser().getUniqueId();
		increase(event, "coinflipswon", winner, 1);
		increase(event, "coinflipslost", loser, 1);
		final CoinFlipMatch match = event.getMatch().getMatch();
		increase(event, "$wonincoinflip", winner, event.getWonCash());
		increase(event, "$lostincoinflip", loser, match.getWager()-match.getTax());
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void fundDepositEvent(FundDepositEvent event) {
		if(!event.isCancelled()) {
			increase(event, "$funddeposited", event.getPlayer().getUniqueId(), event.getAmount());
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void customEnchantProcEvent(CustomEnchantProcEvent event) {
		if(event.getPlayer() != null && !event.isCancelled()) {
			final UUID player = event.getPlayer().getUniqueId();
			final CustomEnchant enchant = event.getEnchant();
			increase(event, "customenchantprocs", player, 1);
			increase(event, "customenchantprocs_" + enchant.getRarity().getName(), player, 1);
			increase(event, "customenchantproc'd_" + enchant.getPath(), player, 1);
			final ItemStack i = event.getItem();
			increase(event, "customenchantmprocs_" + i.getType().name() + ":" + i.getData().getData(), player, 1);
		}
	}
	@EventHandler(priority = EventPriority.LOWEST)
	private void customEnchantApplyEvent(PlayerApplyCustomEnchantEvent event) {
		final UUID player = event.getPlayer().getUniqueId();
		increase(event, "customenchantsapplied", player, 1);
		increase(event, "customenchantsapplied_" + event.getEnchant().getRarity().getName(), player, 1);
	}
	@EventHandler(priority = EventPriority.LOWEST)
	private void enchanterPurchaseEvent(EnchanterPurchaseEvent event) {
		if(!event.isCancelled()) {
			final UUID player = event.getPlayer().getUniqueId();
			increase(event, "enchanterpurchases", player, 1);
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void tinkererTradeEvent(TinkererTradeEvent event) {
		if(!event.isCancelled()) {
			final UUID player = event.getPlayer().getUniqueId();
			increase(event, "tinkerertrades", player, 1);
			increase(event, "tinkereritemtrades", player, event.getTrades().keySet().size());
		}
	}
}

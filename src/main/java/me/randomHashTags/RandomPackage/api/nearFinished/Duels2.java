package me.randomHashTags.RandomPackage.api.nearFinished;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.duels.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Duels2 extends RandomPackageAPI implements Listener, CommandExecutor {

    private static Duels2 instance;
    public static final Duels2 getDuels() {
        if(instance == null) instance = new Duels2();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;

    private Inventory type, settings, godset, collectionbin;

    private HashMap<Player, DuelSettings> sent = new HashMap<>(), joiningQueue = new HashMap<>();
    private HashMap<String, Integer> settingSlots = new HashMap<>();
    private List<String> commands = new ArrayList<>();


    public boolean onCommand(CommandSender sender, Command cmd, String comamndLabel, String[] args) {
        final Player player = sender instanceof Player ? (Player) sender : null;
        final int l = args.length;
        if(l == 0) {
            if(player != null) viewTypes(player);
        } else {
            final String a = args[0];
            if(a.equals("help")) viewHelp(sender);
            else if(player != null) {
                if(a.equals("collect")) viewCollectionBin(player);
                else if(a.equals("toggle")) toggleNotifications(player);
                else if(a.equals("custom")) editGodset(player);
                else if(a.equals("leave")) leave(player);
                else create(player, a);
            }
        }
        return true;
    }
    public void enable() {
        if(isEnabled) return;
        save("Features", "duels.yml");
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features", "duels.yml"));
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;

        type = Bukkit.createInventory(null, config.getInt("type gui.size"), ChatColor.translateAlternateColorCodes('&', config.getString("type gui.title")));
        settings = Bukkit.createInventory(null, config.getInt("settings gui.size"), ChatColor.translateAlternateColorCodes('&', config.getString("settings gui.title")));
        godset = Bukkit.createInventory(null, config.getInt("godset gui.size"), ChatColor.translateAlternateColorCodes('&', config.getString("godset gui.title")));
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        sent.clear();
        settingSlots.clear();
        commands.clear();
        for(DuelArena a : DuelArena.arenas) a.queue = 0;
        DuelArena.arenas.clear();
        DuelSettings.duelSettings.clear();
        for(ActiveDuel d : ActiveDuel.activeDuels) {
        }
        ActiveDuel.activeDuels.clear();
        HandlerList.unregisterAll(this);
    }

    public void viewHelp(CommandSender sender) {
        if(hasPermission(sender, "RandomPackage.duel.help", true)) {
            sendStringListMessage(sender, config.getStringList("messages.help"));
        }
    }
    public void viewTop(CommandSender sender) {
        if(hasPermission(sender, "RandomPackage.duel.top", true)) {

        }
    }
    public void viewTypes(Player player) {
        if(hasPermission(player, "RandomPackage.duel.view", true)) {
            player.closeInventory();
            player.openInventory(Bukkit.createInventory(player, type.getSize(), type.getTitle()));
            final Inventory top = player.getOpenInventory().getTopInventory();
            top.setContents(type.getContents());
            player.updateInventory();
        }
    }

    private boolean passedCheck(Player player) {

        return true;
    }
    public void editGodset(Player player) {
        if(hasPermission(player, "RandomPackage.duel.godset", true)) {
            final int s = godset.getSize();
            player.openInventory(Bukkit.createInventory(player, godset.getSize(), godset.getTitle()));
            final Inventory top = player.getOpenInventory().getTopInventory();
            top.setContents(godset.getContents());
            for(int i = 0; i < s; i++) {
            }
            player.updateInventory();
        }
    }
    public void joinQueue(Player player, DuelType type) {
        final QueuedDuel q = QueuedDuel.valueOf(player);
        final ActiveDuel a = q == null ? ActiveDuel.valueOf(player) : null;
        if(q != null) {

        } else if(a != null) {

        } else {
            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            final boolean ranked = type.equals(DuelType.RANKED);
            final List<QueuedDuel> queue = ranked ? QueuedDuel.ranked : QueuedDuel.unranked;
            if(queue.size() == 0) {
                sendStringListMessage(player, config.getStringList("messages.queue create duel"));
                joiningQueue.put(player, pdata.lastDuelSettings);
            }
        }
    }
    private void acceptJoiningQueue(Player player, DuelType type) {
        if(joiningQueue.containsKey(player)) {
            final boolean ranked = type.equals(DuelType.RANKED);
            final List<QueuedDuel> queue = ranked ? QueuedDuel.ranked : QueuedDuel.unranked;
            final QueuedDuel queued = new QueuedDuel(player, joiningQueue.get(player), type);
            joiningQueue.remove(player);
            final HashMap<String, String> replacements = new HashMap<>();
            replacements.put("{QUEUE}", Integer.toString(queue.size()));
            sendStringListMessage(player, config.getStringList("messages." + (ranked ? "" : "un") + "ranked queue joined"), replacements);
        }
    }
    public void create(Player player, String target) {
        if(hasPermission(player, "RandomPackage.duel.create", true)) {
            final HashMap<String, String> replacements = new HashMap<>();
            replacements.put("{TARGET}", target);
            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            if(player.getName().toLowerCase().equals(target.toLowerCase())) {
                sendStringListMessage(player, config.getStringList("messages.cannot duel self"), replacements);
            } else if(!pdata.receivesDuelInvites) {
                sendStringListMessage(player, config.getStringList("messages.duels toggled off"), replacements);
            } else if(Bukkit.getPlayer(target) == null || !Bukkit.getPlayer(target).isOnline()) {
                sendStringListMessage(player, config.getStringList("messages.target doesnt exist"), replacements);
            } else {
                final Player t = Bukkit.getPlayer(target);
                final ActiveDuel a = ActiveDuel.valueOf(t);
                if(sent.containsKey(t)) {
                    sendStringListMessage(player, config.getStringList("messages.already creating"), replacements);
                } else if(a != null) {
                    replacements.put("{OPPONENT", a.requester.equals(t) ? a.target.getName() : t.getName());
                    sendStringListMessage(player, config.getStringList("messages.target already in a duel"), replacements);
                } else {
                    player.closeInventory();
                    final DuelSettings last = RPPlayerData.get(player.getUniqueId()).lastDuelSettings;
                    final boolean l = last == null;
                    final boolean gapples = l || last.gapples, mcmmo = l || last.mcmmo, potions = l || last.potions, bows = l || last.bows, healing = l || last.healing, foodloss = l || last.foodloss, enderpearls = l || last.enderpearls, risksInventory = l || last.risksInventory, armor = l || last.armor, weapons = l || last.weapons, envoy = l || last.envoy, deathCertificates = l || last.deathCertificates;
                    final List<String> allowedCommands = l ? commands : last.allowedCommands;
                    final DuelKit kit = l ? null : last.kit;
                    final DuelSettings d = new DuelSettings(player, t, null, gapples, mcmmo, potions, bows, healing, foodloss, enderpearls, risksInventory, armor, weapons, allowedCommands, envoy, deathCertificates, kit);
                    sent.put(player, d);

                    player.openInventory(Bukkit.createInventory(player, settings.getSize(), settings.getTitle()));
                    final Inventory top = player.getOpenInventory().getTopInventory();
                    top.setContents(settings.getContents());
                    player.updateInventory();
                    for(int i = 0; i < top.getSize(); i++) {
                        item = top.getItem(i);
                        if(item != null) {

                        }
                    }
                    player.updateInventory();
                }

            }
        }
    }
    private void createRequest(Player player) {
        if(sent.containsKey(player)) {

        }
    }
    public void startDuel(Player requester, Player accepter, DuelSettings settings) {
        final DuelArena arena = settings.arena;
        final DuelKit kit = settings.kit;
        sent.remove(requester);
        new ActiveDuel(requester, accepter, settings, new ArrayList<>(), DuelType.REQUESTED);
    }
    public void toggleNotifications(Player player) {
        if(hasPermission(player, "RandomPackage.duel.toggle", true)) {
            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            pdata.receivesDuelInvites = !pdata.receivesDuelInvites;
            sendStringListMessage(player, config.getStringList("messages.toggle notifications o" + (pdata.receivesDuelInvites ? "n" : "ff")));
        }
    }
    public void viewCollectionBin(Player player) {
        if(hasPermission(player, "RandomPackage.duel.collect", true)) {
            final List<ItemStack> items = RPPlayerData.get(player.getUniqueId()).duelStakeCollectionBin;
            if(items.isEmpty()) {
                sendStringListMessage(player, config.getStringList("messages.stake collection bin empty"));
            } else {
                player.openInventory(Bukkit.createInventory(player, collectionbin.getSize(), collectionbin.getTitle()));
                final Inventory top = player.getOpenInventory().getTopInventory();
                top.setContents(collectionbin.getContents());
                player.updateInventory();
                for(ItemStack i : items)
                    top.setItem(top.firstEmpty(), i);
            }
        }
    }
    public void leaveQueue(Player player) {
        final QueuedDuel q = QueuedDuel.valueOf(player);
        if(q != null) {
            sendStringListMessage(player, config.getStringList(""));
            q.delete();
        } else {

        }
    }
    public void leave(Player player) {
        if(hasPermission(player, "RandomPackage.duel.leave", true)) {
            final ActiveDuel a = ActiveDuel.valueOf(player);
            if(a != null) {
                a.end(a.requester.equals(player) ? a.target : player);
                sendStringListMessage(player, config.getStringList("messages.leave"));
            } else {
                sendStringListMessage(player, config.getStringList("messages.leave not in one"));
            }
        }
    }

    @EventHandler
    private void inventoryCloseEvent(InventoryCloseEvent event) {
        final Player player = (Player) event.getPlayer();
        sent.remove(player);
    }
    @EventHandler
    private void playerQuitEvent(PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        if(sent.keySet().contains(player)) {
            sent.remove(player);
        }
        final ActiveDuel a = ActiveDuel.valueOf(player);
        if(a != null) {
            final Player r = a.requester, t = a.target;
            a.end(player.equals(r) ? t : r);
        }
    }
}

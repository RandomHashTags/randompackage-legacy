package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.api.events.FundDepositEvent;

public class Fund extends RandomPackageAPI implements CommandExecutor, Listener {
	
	public boolean isEnabled = false;
	private static Fund instance;
	public static final Fund getFund() {
		if(instance == null) instance = new Fund();
		return instance;
	}
	
	public FileConfiguration config;
	private HashMap<String, String> unlockstring = new HashMap<>();
	private HashMap<String, Double> needed_unlocks = new HashMap<>();
	
	public double maxfund = 0.00, total = 0.00;
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if(args.length == 0) view(sender);
		else if(args[0].equals("help")) viewHelp(sender);
		else if(args[0].equals("reset")) reset(sender);
		else if(sender instanceof Player && args.length >= 2 && args[0].equals("deposit")) deposit((Player) sender, args[1]);
		return true;
	}

	public void enable() {
		final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "fund.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "fund.yml"));
		for(String s : config.getStringList("unlock")) {
			needed_unlocks.put(s.split(";")[2], Double.parseDouble(s.split(";")[1]));
			unlockstring.put(s.split(";")[0], s);
			if(Double.parseDouble(s.split(";")[1]) > maxfund) maxfund = Double.parseDouble(s.split(";")[1]);
		}
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded Server Fund &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
	public void disable() {
		if(!isEnabled) return;
		total = 0.00;
		isEnabled = false;
		unlockstring.clear();
		needed_unlocks.clear();
		HandlerList.unregisterAll(this);
	}
	
	public void deposit(Player player, String arg) {
		if(hasPermission(player, "RandomPackage.fund.deposit", true)) {
			if(total >= maxfund)
				sendStringListMessage(player, config.getStringList("messages.already-complete"));
			else if(arg.contains(".") && !config.getBoolean("allows-decimals"))
				sendStringListMessage(player, config.getStringList("messages.cannot-include-decimals"));
			else {
				final double q = getRemainingDouble(arg), min = config.getDouble("min-deposit");
				if(q == -1) return;
				final String a = q < config.getDouble("min-deposit") ? "less-than-min" : q > eco.getBalance(player) ? "need-more-money" : null;
				if(a != null) {
					sendMessage(player, a, null, a.equals("less-than-min") ? min : q, false);
					return;
				}
				final FundDepositEvent e = new FundDepositEvent(player, q);
				pluginmanager.callEvent(e);
				if(!e.isCancelled()) {
					final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
					pdata.fundDeposits += q;
					eco.withdrawPlayer(player, q);
					total += q;
					sendMessage(player, "deposited", null, q, true);
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	private void playerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
		if(!event.isCancelled()) {
			final String cmd = event.getMessage().contains(":") ? event.getMessage().split(":")[1].split(" ")[0].toLowerCase() : event.getMessage().substring(1).split(" ")[0].toLowerCase();
			final PluginCommand pl = Bukkit.getPluginCommand(cmd);
			if(pl != null) {
				for(String s : unlockstring.keySet()) {
					if(total < Double.parseDouble(unlockstring.get(s).split(";")[1])) {
						if(s.startsWith("/" + pl.getName()) && pl.getAliases().contains(cmd) && !s.contains(" ")
								|| s.startsWith("/" + pl.getName()) && pl.getName().equals(cmd) && !s.contains(" ")
								|| event.getMessage().toLowerCase().equals(s.toLowerCase())
								|| event.getMessage().toLowerCase().equals(s.replace(pl.getName(), cmd).toLowerCase())) {
							if(hasPermission(event.getPlayer(), "RandomPackage.fund.bypass", false)) return;
							event.setCancelled(true);
							sendMessage(event.getPlayer(), "needs-to-reach", unlockstring.get(s), 0, false);
							return;
						}
					}
				}
			}
		}
	}
	private void sendMessage(CommandSender sender, String path, String unlockstring, double q, boolean broadcasted) {
		for(String ss : config.getStringList("messages." + path)) {
			if(ss.contains("{PLAYER}")) ss = ss.replace("{PLAYER}", sender.getName());
			if(ss.contains("{ARG1}")) ss = ss.replace("{ARG1}", unlockstring.split(";")[3]);
			if(ss.contains("{ARG2}")) ss = ss.replace("{ARG2}", unlockstring.split(";")[0]);
			if(ss.contains("{REQ}"))  ss = ss.replace("{REQ}", getAbbreviation(Double.parseDouble(unlockstring.split(";")[1])));
			if(ss.contains("{REQ$}")) ss = ss.replace("{REQ$}", formatInt(Integer.parseInt(unlockstring.split(";")[1])));
			if(ss.contains("{AMOUNT}")) ss = ss.replace("{AMOUNT}", formatDouble(q).split("\\.")[0]);
			if(broadcasted) Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', ss));
			else            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ss));
			return;
		}
	}

	public void reset(CommandSender sender) {
		if(hasPermission(sender, "RandomPackage.fund.reset", true)) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!)&r &e" + (sender != null ? sender.getName() : "CONSOLE") + " &chas reset the server fund!"));
			total = 0.00;
			for(RPPlayerData pdata : RPPlayerData.players.values()) pdata.fundDeposits = 0.00;
		}
	}
	public void view(CommandSender sender) {
		if(hasPermission(sender, "RandomPackage.fund", true)) {
			final int length = config.getInt("messages.progress-bar.length"), pdigits = config.getInt("messages.unlock-percent-digits");
			final String symbol = config.getString("messages.progress-bar.symbol"), achieved = ChatColor.translateAlternateColorCodes('&', config.getString("messages.progress-bar.achieved")), notachieved = ChatColor.translateAlternateColorCodes('&', config.getString("messages.progress-bar.not-achieved"));
			for(String s : config.getStringList("messages.view")) {
				if(s.contains("{BALANCE}")) s = s.replace("{BALANCE}", formatDouble(total).split("\\.")[0]);
				if(s.equals("{CONTENT}")) {
					for(String i : config.getStringList("unlock")) {
						final double req = needed_unlocks.get(i.split(";")[2]);
						final int q = (int) req;
						final String percent = roundDoubleString((total / req) * 100 > 100.000 ? 100 : (total / req) * 100, pdigits);
						for(String k : config.getStringList("messages.content")) {
							if(k.contains("{COMPLETED}")) k = k.replace("{COMPLETED}", total >= req ? ChatColor.translateAlternateColorCodes('&', config.getString("messages.completed")) : "");
							if(k.contains("{UNLOCK}")) k = k.replace("{UNLOCK}", i.split(";")[2]);
							if(k.contains("{UNLOCK%}")) k = k.replace("{UNLOCK%}", percent);
							if(k.contains("{PROGRESS_BAR}")) {
								String u = "";
								for(int a = 1; a <= length; a++) u = u + (Double.parseDouble(percent) >= a ? achieved : notachieved) + symbol;
								k = k.replace("{PROGRESS_BAR}", u);
							}
							if(k.contains("{REQ}")) k = k.replace("{REQ}", getAbbreviation(req));
							if(k.contains("{REQ$}")) k = k.replace("{REQ$}", formatInt(q));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', k));
						}
					}
				}
				if(!s.equals("{CONTENT}")) sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
			}
		}
	}
	public void viewHelp(CommandSender sender) {
		if(hasPermission(sender, "RandomPackage.fund.help", true))
			sendStringListMessage(sender, config.getStringList("messages.help"));
	}
	private String getAbbreviation(double input) {
		final int l = Integer.toString((int) input).length();
		String ll = formatDouble(input);
		if(ll.contains(",")) ll = ll.split(",")[0] + "." + ll.split(",")[1];
		String d = Double.toString(Double.parseDouble(ll));
		if(d.endsWith(".0") && d.split("\\.")[1].length() == 1) d = d.split("\\.")[0];
		return d + ChatColor.translateAlternateColorCodes('&', config.getString("messages." + (l >= 13 && l <= 15 ? "trillion" : l >= 10 && l <= 12 ? "billion" : l >= 7 && l <= 9 ? "million" : l >= 4 && l <= 6 ? "thousand" : "")));
	}
}

package me.randomHashTags.RandomPackage.api.unfinished;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.server.PluginEnableEvent;

import java.io.File;

public class Disguises extends RandomPackageAPI implements Listener, CommandExecutor {

    public boolean isEnabled = false;
    private static Disguises instance;
    public static Disguises getDisguises() {
        if(instance == null) instance = new Disguises();
        return instance;
    }

    private YamlConfiguration config;

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        final String c = cmd.getName();
        final Player player = sender instanceof Player ? (Player) sender : null;
        if(c.equals("disguises")) {
            if(args.length == 0) {

            }
        } else if(c.equals("undisguise")) {

        }
        return true;
    }
    @EventHandler
    private void pluginEnableEvent(PluginEnableEvent event) {
        if(event.getPlugin().equals(randompackage)) {
            config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "disguises.yml"));
        }
    }

    @EventHandler
    private void entityDamageEvent(EntityDamageEvent event) {
        final RPPlayerData pdata = event.getEntity() instanceof Player ? RPPlayerData.get(event.getEntity().getUniqueId()) : null;
        if(pdata != null) {

        }
    }
}

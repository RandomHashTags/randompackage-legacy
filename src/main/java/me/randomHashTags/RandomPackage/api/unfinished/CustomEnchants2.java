package me.randomHashTags.RandomPackage.api.unfinished;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.api.events.customboss.CustomBossDamageByEntityEvent;
import me.randomHashTags.RandomPackage.api.events.customenchant.CEAApplyPotionEffectEvent;
import me.randomHashTags.RandomPackage.api.events.customenchant.CustomEnchantProcEvent;
import me.randomHashTags.RandomPackage.api.events.customenchant.PvAnyEvent;
import me.randomHashTags.RandomPackage.api.events.customenchant.isDamagedEvent;
import me.randomHashTags.RandomPackage.api.events.mobstacker.MobStackDepleteEvent;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchantEntity;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.RarityGem;
import org.bukkit.*;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class CustomEnchants2 extends RandomPackageAPI implements Listener {

    /*private static CustomEnchants2 instance;
    public static final CustomEnchants2 getCustomEnchants2() {
        if(instance == null) instance = new CustomEnchants2();
        return instance;
    }

    public boolean isEnabled = false;
    public YamlConfiguration config;


    public void enable() {
        if(isEnabled) return;
        save("Features", "custom enchants.yml");
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features", "custom enchants.yml"));
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        HandlerList.unregisterAll(this);
    }





    public void procPlayerArmor(Event event, Player player) {
        if(player != null) {
            for(ItemStack is : player.getInventory().getArmorContents()) {
                if(is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
                    for(String s : is.getItemMeta().getLore()) {
                        final CustomEnchant e = CustomEnchant.valueOf(s);
                        if(e != null) {
                            procEnchant(event, e, getEnchantmentLevel(s), is, player);
                        }
                    }
                }
            }
        }
    }
    public void procPlayerItem(Event event, Player player, ItemStack is) {
        if(player != null) {
            final ItemStack h = is == null ? player.getInventory().getItemInHand() : is;
            if(h != null && h.hasItemMeta() && h.getItemMeta().hasLore()) {
                for(String s : h.getItemMeta().getLore()) {
                    final CustomEnchant e = CustomEnchant.valueOf(s);
                    if(e != null) {
                        procEnchant(event, e, getEnchantmentLevel(s), h, player);
                    }
                }
            }
        }

    }
    public void tryProcEnchant(Event event, Player player, CustomEnchant enchant) {
        if(player != null) {
            for(ItemStack is : player.getInventory().getArmorContents()) {
                if(is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
                    for(String s : is.getItemMeta().getLore()) {
                        final CustomEnchant e = CustomEnchant.valueOf(s);
                        if(e != null && e.equals(enchant)) {
                            procEnchant(event, e, getEnchantmentLevel(s), is, player);
                        }
                    }
                }
            }
        }
    }
    public void procEnchant(Event event, CustomEnchant enchant, int level, ItemStack itemWithEnchant, Player P) {
        final CustomEnchantProcEvent e = new CustomEnchantProcEvent(event, enchant, level, itemWithEnchant, P);
        pluginmanager.callEvent(e);
        if(!e.isCancelled() && (!(e.getEvent() instanceof Cancellable) || e.getEvent() instanceof Cancellable && !((Cancellable) e.getEvent()).isCancelled()) || e.getEvent() instanceof PluginEnableEvent) {
            executeAttributes(e.getPlayer(), event, enchant.getAttributes());
        }
    }
    public boolean isOnCorrectItem(CustomEnchant enchant, ItemStack is) {
        final String i = is != null ? is.getType().name() : null;
        if(enchant != null && i != null) for(String s : enchant.getAppliesTo()) if(i.endsWith(s.toUpperCase())) return true;
        return false;
    }

    public void executeAttributes(Player player, Event event, List<String> attributes) {
        if(player == null || event == null) return;
        final List<String> attributess = new ArrayList<>();
        for(String s : attributes) attributess.add(replaceBasic(s));

        for(String attribute : attributess) {
            final String type = attribute.split(";")[0].toLowerCase();
            final int chance = attribute.split(";")[1].toLowerCase().startsWith("chance=") ? Integer.parseInt(attribute.split(";")[1].split("=")[1]) : 100;
            if(chance < random.nextInt(100)) {
                for(String a : attribute.substring(type.length()).split(";")) {
                    final String A = a.toLowerCase();
                    if(A.startsWith("cancel") && event instanceof Cancellable) {
                        ((Cancellable) event).setCancelled(true);
                        player.updateInventory();
                    } else {
                        final List<LivingEntity> recipients = new ArrayList<>();
                        final String did = tryBasicAttribute(event, recipients, a, null, -1);
                        if(did == null) {
                            if(event instanceof PvAnyEvent && (A.equals("pva") || A.equals("pvp") && ((PvAnyEvent) event).getVictim() instanceof Player || A.equals("pve") && !(((PvAnyEvent) event).getVictim() instanceof Player) || A.equals("arrowhit") && ((PvAnyEvent) event).getProjectile() != null && ((PvAnyEvent) event).getProjectile() instanceof Arrow && shotbows.keySet().contains(((PvAnyEvent) event).getProjectile().getUniqueId())))
                                executeAttributes(player, (PvAnyEvent) event, a);
                            else if(event instanceof isDamagedEvent && (A.equals("isdamaged") || A.equals("hitbyarrow") && ((isDamagedEvent) event).getDamager() instanceof Arrow || A.startsWith("damagedby(") && ((isDamagedEvent) event).getCause() != null && A.toUpperCase().contains(((isDamagedEvent) event).getCause().name())))
                                executeAttributes(player, (isDamagedEvent) event, a);
                        }
                    }
                }
            }
        }
    }
    private void executeAttributes(Player player, PvAnyEvent event, String attribute) {
        final String d = Double.toString(event.getDamage());
        attribute = attribute.replace("dmg", d);
        if(attribute.startsWith("setdamage{")) {
            final double dmg = oldevaluate(attribute.split("\\{")[1].split("}")[0]);
            event.setDamage(dmg);
        }
        //w(null, event, null, getRecipients(event, a.contains("[") ? a.split("\\[")[1].split("]")[0] : a, null), a, attribute, -1, player);
    }
    private void executeAttributes(Player player, isDamagedEvent event, String attribute) {
        final String d = Double.toString(event.getDamage());
        attribute = attribute.replace("dmg", d);
        if(attribute.startsWith("setdamage{")) {
            final double dmg = oldevaluate(attribute.split("\\{")[1].split("}")[0]);
            event.setDamage(dmg);
        }
        //w(null, event, null, getRecipients(event, a.contains("[") ? a.split("\\[")[1].split("]")[0] : a, null), a, attribute, -1, player);
    }

    private String replaceBasic(String attribute) {
        if(attribute.contains("random{")) {
            final String e = attribute.split("random\\{")[1].split("}")[0];
            final int min = (int) oldevaluate(e.split(":")[0]), max = (int) oldevaluate(e.split(":")[1].split("}")[0]);
            int r = min + random.nextInt(max - min + 1);
            attribute = attribute.replace("random{" + e + "}", Integer.toString(r));
        }
        return attribute;
    }
    private String tryBasicAttribute(Event event, List<LivingEntity> recipients, String attribute, CustomEnchant enchant, int level) {
        final String A = attribute;
        attribute = attribute.toLowerCase();
        if(attribute.startsWith("removepotioneffect{")) {
            final PotionEffectType type = getPotionEffectType((attribute.contains("]") ? attribute.split("]")[1] : attribute.split("\\{")[1]).split(":")[0].toUpperCase());
            for(LivingEntity l : recipients)
                removePotionEffect(l, getPotionEffect(l, type), attribute.contains(":true") ? config.getStringList("messages.remove-potion-effect") : null, enchant, level);
        } else if(attribute.startsWith("damage{")) {
            for(LivingEntity l : recipients)
                damage(l, oldevaluate(attribute.split("]")[1].split("}")[0].replace("h", "")));
        } else if(attribute.startsWith("sendmessage{")) {
            for (LivingEntity l : recipients)
                sendMessage(l, enchant, attribute.split("]")[1]);
        } else if(attribute.startsWith("heal{")) {
            for(LivingEntity l : recipients)
                heal(l, oldevaluate(attribute.split("]")[1].split("}")[0].replace("h", "")));
        } else if(attribute.startsWith("sethealth{")) {
            for(LivingEntity l : recipients)
                setHealth(l, (int) oldevaluate(attribute.split("]")[1].split("}")[0]));
        } else if(attribute.startsWith("refillair{")) {
            for(LivingEntity l : recipients)
                refillAir(l, Integer.parseInt(attribute.split("]")[1].split("}")[0]));
        } else if(attribute.startsWith("setxp{")) {
            final int value = (int) oldevaluate(attribute.toLowerCase().split("setxp\\{")[1].split("}")[0]);
            for(LivingEntity l : recipients)
                setXP(l, value);
        } else if(attribute.startsWith("healhunger{")) {
            final int h = (int) oldevaluate(attribute.split("]")[1].split("}")[0]);
            for(LivingEntity l : recipients)
                healHunger(l, h);
        } else if(A.startsWith("dropItem{")) {
            String y = A.split("dropItem\\{")[1].split("]")[1];
            if(event instanceof PlayerInteractEvent) y = y.replace("player", ((PlayerInteractEvent) event).getPlayer().getName());
            for(LivingEntity l : recipients)
                dropItem(l, d(null, y, 0));
        } else if(attribute.startsWith("playsound{")) {
            final String[] s = attribute.split(":");
            final String sound = s[0];
            final int pitch = Integer.parseInt(s[1]), volume = Integer.parseInt(s[2]), playtimes = Integer.parseInt(s[3]);
            final boolean globalsound = attribute.endsWith(":true");
            for(LivingEntity l : recipients)
                playSound(sound, pitch, volume, l, playtimes, globalsound);
        } else if(attribute.startsWith("executecommand{")) {
            final String cmd = A.split("\\{")[1].split("}")[0];
            final ConsoleCommandSender sender = Bukkit.getConsoleSender();
            for(LivingEntity l : recipients) {
                final Location loc = l.getLocation();
                final String c = cmd
                        .replace("%RECIPIENT%", l.getName())
                        .replace("%RECIPIENT_CN%", l.getCustomName())
                        .replace("%RECIPIENT_UUID%", l.getUniqueId().toString())
                        .replace("%RECIPIENT_TYPE%", l.getType().name())
                        .replace("%RECIPIENT_HP%", Double.toString(l.getHealth()))
                        .replace("%RECIPIENT_X%", Integer.toString(loc.getBlockX()))
                        .replace("%RECIPIENT_Y%", Integer.toString(loc.getBlockY()))
                        .replace("%RECIPIENT_Z%", Integer.toString(loc.getBlockZ()))
                ;
                Bukkit.dispatchCommand(sender, c);
            }
        } else return null;
        return A;
    }



    public void w(CustomEnchantProcEvent ev, Event event, CustomEnchant enchant, List<LivingEntity> recipients, String a, String attribute, int b, Player P) {
        try {
            executeAttributes(ev, event, enchant, recipients, a, attribute, b, P);
        } catch (Exception e) {
            System.out.print(" ");
            System.out.print("[RandomPackage] Custom Enchant Exception caught. Below is the info that caused the error.");
            System.out.print("[RandomPackage] Version: " + randompackage.getDescription().getVersion() + ". User: %%__USER__%%");
            System.out.print("[RandomPackage] CustomEnchantProcEvent = " + ev);
            System.out.print("[RandomPackage] Event = " + event);
            System.out.print("[RandomPackage] Custom Enchant = " + (enchant != null ? enchant.getName() : "null"));
            System.out.print("[RandomPackage] recipients = " + recipients);
            System.out.print("[RandomPackage] a = " + a);
            System.out.print("[RandomPackage] attribute = " + attribute);
            System.out.print("[RandomPackage] b = " + b);
            System.out.print("[RandomPackage] P = " + P);
            System.out.print(" ");
            e.printStackTrace();
        }
    }
    private void executeAttributes(CustomEnchantProcEvent ev, Event event, CustomEnchant enchant, List<LivingEntity> recipients, String a, String attribute, int b, Player P) {
        if(ev != null && !ev.didProc()) return;
        final boolean isPVAny = event instanceof PvAnyEvent;
        final HashMap<String, LivingEntity> recipientss = getRecipients(event, P);
        for(String s : recipientss.keySet()) {
            if(a.contains(s + "X"))
                a = a.replace(s + "X", Integer.toString(recipientss.get(s).getLocation().getBlockX()));
            if(a.contains(s + "Y"))
                a = a.replace(s + "Y", Integer.toString(recipientss.get(s).getLocation().getBlockY()));
            if(a.contains(s + "Z"))
                a = a.replace(s + "Z", Integer.toString(recipientss.get(s).getLocation().getBlockZ()));
        }
        for(int i = 1; i <= 6; i++) {
            if(a.contains("maxHealthOf(") || a.contains("healthOf(")) {
                String value = a.contains("maxHealthOf(") ? "maxHealthOf" : "healthOf";
                value = value + "(" + a.split(value + "\\(")[1].split("\\)")[0] + ")";
                if(isPVAny) {
                    final PvAnyEvent e = (PvAnyEvent) event;
                    final LivingEntity damager = e.getDamager(), victim = e.getVictim();
                    if(value.contains("DAMAGER"))
                        a = a.replace(value, Double.toString(value.startsWith("max") ? damager.getMaxHealth() : damager.getHealth()));
                    if(value.contains("VICTIM"))
                        a = a.replace(value, victim != null ? Double.toString(value.startsWith("max") ? victim.getMaxHealth() : victim.getHealth()) : "0");
                }
            }
            if(a.contains("getXP(")) {
                String value = a.contains("getXP(") ? "getXP" : "";
                if(!value.equals("")) {
                    value = value + "(" + a.split(value + "\\(")[1].split("\\)")[0] + ")";
                    if(isPVAny) {
                        final PvAnyEvent E = (PvAnyEvent) event;
                        if(value.contains("DAMAGER"))
                            a = a.replace("DAMAGER", Integer.toString(getXP(E.getDamager())));
                        if(value.contains("VICTIM"))
                            a = a.replace("VICTIM", E != null && E.getVictim() instanceof Player ? Integer.toString(getXP(E.getVictim())) : "0");
                    }
                }
            }
        }
        if(event instanceof CustomBossDamageByEntityEvent) {
            a = a.replace("dmg", Double.toString(((CustomBossDamageByEntityEvent) event).getDamage()));
        } else if(event instanceof EntityDamageByEntityEvent) {
            a = a.replace("dmg", Double.toString(((EntityDamageByEntityEvent) event).getDamage()));
        }

        if(a.contains("combo{")) {
            final String e = a.split("combo\\{")[1].split("}")[0], o = e.split(":")[0];
            final Player p = ev.getPlayer();
            final double combo = combos.keySet().contains(p) && combos.get(p).keySet().contains(o) ? combos.get(p).get(o) : 1.00;
            a = a.replace("combo{" + e + "}", Double.toString(combo));
        }
        if(a.contains("direction")) {
            final String type = "direction" + (a.contains("directionXOf") ? "X" : a.contains("directionYOf") ? "Y" : a.contains("directionZOf") ? "Z" : "") + "Of", r = a.split(type + "\\{")[1].split("}")[0];
            final LivingEntity recip = getRecipient(event, r);
            if(recip != null) {
                final Vector direc = recip.getLocation().getDirection();
                a = a.replace(type + "{" + r + "}", Double.toString(type.contains("X") ? direc.getX() : type.contains("Y") ? direc.getY() : type.contains("Z") ? direc.getZ() : 0.00));
            } else {
                Bukkit.broadcastMessage("[RandomPackage] recipient == null. Event=" + event.getEventName());
                return;
            }
        }
        if(a.contains("nearby{") || a.contains("nearbyAllies{") || a.contains("nearbyEnemies{")) {
            final boolean allies = a.contains("nearbyAllies{"), enemies = a.contains("nearbyEnemies{");
            final String e = a.split("nearby" + (allies ? "Allies" : enemies ? "Enemies" : "") + "\\{")[1].split("}")[0];
            final List<LivingEntity> r = new ArrayList<>();
            final LivingEntity who = event instanceof PluginEnableEvent ? P : getRecipient(event, e.split(":")[1]), k = getRecipient(event, e.split(":")[0]);
            if(who != null) {
                for(Entity en : who.getNearbyEntities(oldevaluate(e.split(":")[2]), oldevaluate(e.split(":")[3]), oldevaluate(e.split(":")[4]))) {
                    if(en instanceof LivingEntity && en instanceof Damageable && (k == null || k != null && !en.equals(k)))
                        if(!(en instanceof Player)
                                || who instanceof Player && en instanceof Player && (enemies && fapi.relationIsEnemyOrNull((Player) who, (Player) en) || allies && fapi.relationIsAlly((Player) who, (Player) en)))
                            r.add((LivingEntity) en);
                }
            }
            a = a.replace("nearby" + (allies ? "Allies" : enemies ? "Enemies" : "") + "{" + e + "}", r.toString().replace("\\p{Z}", ""));
            recipients = r;
        }
        if(a.contains("nearbySize{") || a.contains("nearbyAlliesSize{") || a.contains("nearbyEnemiesSize{")) {
            int size = 0;
            final boolean allies = a.contains("nearbyAlliesSize{"), enemies = a.contains("nearbyEnemiesSize{");
            final String e = a.split("nearby" + (allies ? "Allies" : enemies ? "Enemies" : "") + "Size\\{")[1].split("}")[0];
            final LivingEntity who = event instanceof PluginEnableEvent ? P : getRecipient(event, e.split(":")[1]), k = getRecipient(event, e.split(":")[0]);
            for(Entity en : who.getNearbyEntities(oldevaluate(e.split(":")[2]), oldevaluate(e.split(":")[3]), oldevaluate(e.split(":")[4]))) {
                if(en instanceof LivingEntity && en instanceof Damageable && (k == null || k != null && !en.equals(k)))
                    if(!allies && !(en instanceof Player)
                            || who instanceof Player && en instanceof Player && (enemies && fapi.relationIsEnemyOrNull((Player) who, (Player) en) || allies && fapi.relationIsAlly((Player) who, (Player) en)))
                        size += 1;
            }
            a = a.replace("nearby" + (allies ? "Allies" : enemies ? "Enemies" : "") + "Size{" + e + "}", Integer.toString(size));
        }

        if(a.toLowerCase().startsWith("wait{")) {
            wait(ev, event, enchant, attribute, b, (int) oldevaluate(a.split("\\{")[1].split("}")[0]), P);
            return;
        } else if(a.toLowerCase().startsWith("ignite{")) {
            a = a.toLowerCase();
            final int time = (int) oldevaluate(a.split("]")[1].split("}")[0]);
            if(a.split("\\[")[1].split("]")[0].contains("arrow")) {
                if(event instanceof EntityShootBowEvent)
                    (((EntityShootBowEvent) event).getProjectile()).setFireTicks(time);
            }
            for(LivingEntity l : recipients)
                l.setFireTicks(time);
        } else if(a.toLowerCase().startsWith("setdamage{")) {
            a = a.toLowerCase();
            final double dmg = oldevaluate(a.split("\\{")[1].split("}")[0]);
            if(isPVAny) {
                ((PvAnyEvent) event).setDamage(dmg);
            } else if(event instanceof EntityDamageEvent) {
                ((EntityDamageEvent) event).setDamage(dmg);
            }
        } else if(a.toLowerCase().startsWith("setdurability{")) {
            a = a.toLowerCase();
            for(LivingEntity l : recipients) {
                ItemStack p = null;
                double difference = -1;
                if(a.contains("mostdamaged")) {
                    for(ItemStack A : l.getEquipment().getArmorContents())
                        if(A != null && A.getType() != Material.AIR) {
                            double newdif = Double.parseDouble(Short.toString(A.getDurability())) / Double.parseDouble(Short.toString(A.getType().getMaxDurability()));
                            if(difference == -1 || newdif > difference) {
                                difference = newdif;
                                p = A;
                            }
                        }
                } else if(a.contains("helmet") && l.getEquipment().getHelmet() != null) p = l.getEquipment().getHelmet();
                else if(a.contains("chestplate") && l.getEquipment().getChestplate() != null) p = l.getEquipment().getChestplate();
                else if(a.contains("leggings") && l.getEquipment().getLeggings() != null) p = l.getEquipment().getLeggings();
                else if(a.contains("boots") && l.getEquipment().getBoots() != null) p = l.getEquipment().getBoots();
                else if(a.contains("all")) {
                    for(ItemStack q : l.getEquipment().getArmorContents())
                        q.setDurability((short) (oldevaluate(a.split(":")[1].split("}")[0].replace("durability", Short.toString(q.getDurability())))));
                } else if(a.contains("iteminhand")) p = getItemInHand(l);
                else if(a.contains("item")) p = ev.getItem();
                else return;
                if(p != null) {
                    a = a.replace("durability", Short.toString(p.getDurability()));
                    final double dura = oldevaluate(a.split(":")[1].split("}")[0]);
                    p.setDurability((short) (dura < 0 ? 0 : dura));
                }
            }
        } else if(a.toLowerCase().startsWith("setdroppedexp{")) {
            a = a.toLowerCase();
            int dropped = event instanceof BlockBreakEvent ? ((BlockBreakEvent) event).getExpToDrop() : event instanceof EntityDeathEvent ? ((EntityDeathEvent) event).getDroppedExp() : 0;
            final int xp = (int) oldevaluate(a.split("\\{")[1].split("}")[0].replace("droppedxp", Integer.toString(dropped)));
            if(event instanceof BlockBreakEvent)        ((BlockBreakEvent) event).setExpToDrop(xp);
            else if(event instanceof EntityDeathEvent)  ((EntityDeathEvent) event).setDroppedExp(xp);
        } else if(event instanceof PlayerInteractEvent && a.toLowerCase().startsWith("breakhitblock")) {
            breakHitBlock((PlayerInteractEvent) event);
        } else if(a.toLowerCase().startsWith("smite{")) {
            final int amount = (int) oldevaluate(a.split("]")[1].split("}")[0]);
            if(a.toLowerCase().split("\\[")[1].split("]")[0].contains("arrowloc")) {
                if(event instanceof ProjectileHitEvent)
                    smite(((ProjectileHitEvent) event).getEntity().getLocation(), amount);
            }
            for(LivingEntity l : recipients)
                smite(l.getLocation(), amount);
        } else if(a.toLowerCase().startsWith("givedrops{")) {
            if(event instanceof BlockBreakEvent) {
                final BlockBreakEvent bb = (BlockBreakEvent) event;
                final Collection<ItemStack> drops = bb.getBlock().getDrops();
                for(LivingEntity l : recipients) {
                    if(l instanceof Player) {
                        for(ItemStack i : drops) {
                            giveItem((Player) l, i);
                        }
                    }
                }
                bb.setCancelled(true);
                bb.getBlock().setType(Material.AIR);
            }
        } else if(a.toLowerCase().startsWith("setvelocity{")) {
            String U = a.toLowerCase().split("]")[1];
            if(isPVAny) {
                final PvAnyEvent E = (PvAnyEvent) event;
                final Player d = E.getDamager();
                final Vector dv = d.getVelocity();
                final LivingEntity victim = E.getVictim();
                U = U.replace("velocityxof(damager)", Double.toString(dv.getX())).replace("velocityyof(damager)", Double.toString(dv.getY())).replace("velocityzof(damager)", Double.toString(dv.getZ()));
                U = U.replace("velocityof(damager)", dv.getX() + ":" + dv.getY() + ":" + dv.getZ());
                if(victim != null) {
                    final Vector v = victim.getVelocity();
                    U = U.replace("velocityxof(victim)", Double.toString(v.getX())).replace("velocityyof(victim)", Double.toString(v.getY())).replace("velocityzof(victim)", Double.toString(v.getZ()));
                    U = U.replace("velocityof(victim)", v.getX() + ":" + v.getY() + ":" + v.getZ());
                }
            }
            final String[] u = U.split(":");
            final Vector knockback = new Vector(oldevaluate(u[0]), oldevaluate(u[1]), oldevaluate(u[2].split("}")[0]));
            for(LivingEntity l : recipients)
                setVelocity(l, knockback);
        } else if(a.toLowerCase().startsWith("freeze{")) {
            final int time = (int) oldevaluate(a.split("]")[1].split("}")[0]);
            for(LivingEntity l : recipients)
                freeze(l, time);
        } else if(a.toLowerCase().startsWith("procenchants{")) {
            for(LivingEntity l : recipients)
                if(l instanceof Player) {
                    HashMap<ItemStack, HashMap<CustomEnchant, Integer>> enchants = getEnchants((Player) l);
                    for(ItemStack is : enchants.keySet())
                        for(CustomEnchant ce : enchants.get(is).keySet())
                            if(!ce.getAttributes().toString().toLowerCase().contains("procenchants{"))
                                procEnchant(event, ce, enchants.get(is).get(ce), is, P);
                }
        } else if(a.toLowerCase().startsWith("setgainedxp{") && mcmmoIsEnabled) {
            a = a.toLowerCase();
            if(event instanceof com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent) {
                final com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent M = (com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent) event;
                final int xp = (int) oldevaluate(a.split("setgainedxp\\{")[1].split("}")[0].replace("xp", Integer.toString(M.getXpGained())));
                M.setRawXpGained(xp);
                M.setXpGained(xp);
            }
        } else if(a.toLowerCase().startsWith("spawnentity{")) {
            final String s = a.split("]")[1].split("}")[0];
            final CustomEnchantEntity e = CustomEnchantEntity.valueOf(s.split(":")[0]);
            if(e != null)
                for(LivingEntity l : recipients)
                    e.spawn(l, getRecipient(event, s.split(":")[2]), event);
        } else if(a.toLowerCase().startsWith("stopenchant{")) {
            final String J = a.split("]")[1].split("}")[0];
            final int seconds = Integer.parseInt(J.split(":")[1]);
            for(LivingEntity l : recipients) {
                if(l instanceof Player) {
                    final Player p = (Player) l;
                    if(J.toLowerCase().startsWith("all")) {
                        stoppedAllEnchants.add(p);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> stoppedAllEnchants.remove(p), 20*seconds);
                    } else {
                        final CustomEnchant ce = CustomEnchant.valueOf(J.split(":")[0]);
                        if(ce != null) {
                            if(!stoppedEnchants.keySet().contains(p)) stoppedEnchants.put(p, new HashMap<>());
                            if(stoppedEnchants.get(p).keySet().contains(ce)) {
                                Bukkit.getScheduler().cancelTask(stoppedEnchants.get(p).get(ce));
                            }
                            int task = Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> stoppedEnchants.get(p).remove(ce), 20*seconds);
                            stoppedEnchants.get(p).put(ce, task);
                        }
                    }
                }
            }
        } else if(a.toLowerCase().startsWith("breakblocks{")) {
            a = a.toLowerCase();
            final int x1 = (int) oldevaluate(a.split("]")[1].split(":")[0]), y1 = (int) oldevaluate(a.split("]")[1].split(":")[1]), z1 = (int) oldevaluate(a.split("]")[1].split(":")[2]),
                    x2 = (int) oldevaluate(a.split("]")[1].split(":")[3]), y2 = (int) oldevaluate(a.split("]")[1].split(":")[4]), z2 = (int) oldevaluate(a.split("]")[1].split(":")[5].split("}")[0]);
            for(LivingEntity le : recipients)
                if(event instanceof BlockBreakEvent)
                    breakBlocks(getItemInHand(le).getType(), ((BlockBreakEvent) event).getBlock(), x1, y1, z1, x2, y2, z2);
        } else if(a.toLowerCase().startsWith("remove{")) {
            for(LivingEntity l : recipients)
                if(!(l instanceof Player))
                    l.remove();
        } else if(a.toLowerCase().startsWith("replaceblock{")) {
            final String args = a.toLowerCase().split("\\{")[1].split("}")[0];
            final World w = ev.getPlayer().getWorld();
            final int x = (int) oldevaluate(args.split(":")[0]), y = (int) oldevaluate(args.split(":")[1]), z = (int) oldevaluate(args.split(":")[2]);
            final Location l = new Location(w, x, y, z);
            final Material type = Material.valueOf(args.split(":")[3].toUpperCase());
            final Byte data = Byte.parseByte(args.split(":")[4]);
            final int ticks = Integer.parseInt(args.split(":")[5]);
            setTemporaryBlock(l, type, data, ticks);
        } else if(a.toLowerCase().startsWith("stealxp{")) {
            final String arg = a.split("\\{")[1].split("}")[0];
            final LivingEntity receiver = getRecipient(event, arg.split(":")[0]), target = getRecipient(event, arg.split(":")[1]);
            final int amount = Integer.parseInt(arg.split(":")[2]);
            if(receiver != null && receiver instanceof Player && target != null && target instanceof Player) {

            }
        } else if(a.toLowerCase().startsWith("depleteraritygem{")) {
            final Player player = ev.getPlayer();
            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            if (!pdata.hasActiveRarityGem(RarityGem.valueOf(a.split("\\{")[1].split(":")[0]))) {
                ev.setProced(false);
                return;
            }
            final RarityGem gem = RarityGem.valueOf(a.split("\\{")[1].split(":")[0]);
            final ItemStack g = getRarityGem(gem, player);
            if (g != null) {
                itemMeta = g.getItemMeta();
                final int amount = getRemainingInt(itemMeta.getDisplayName());
                final String fn = fapi.getFaction(player);
                int depleteAmount = Integer.parseInt(a.split(":")[1].split("}")[0]);
                double f = fapi.getDecreaseRarityGemPercent(fn, gem);
                depleteAmount -= depleteAmount * f;
                if (amount - depleteAmount <= 0) {
                    depleteAmount = amount;
                    pdata.toggleRarityGem(ev, gem);
                }
                itemMeta = g.getItemMeta();
                itemMeta.setDisplayName(gem.getPhysicalItem().getItemMeta().getDisplayName().replace("{SOULS}", Integer.toString(amount - depleteAmount)));
                g.setItemMeta(itemMeta);
                ev.getPlayer().updateInventory();
            }
        } else if(a.toLowerCase().startsWith("depletestacksize{") && event instanceof MobStackDepleteEvent) {
            final int amount = Integer.parseInt(a.split("\\{")[1].split("}")[0]);
            ((MobStackDepleteEvent) event).setAmount(amount);
        } else if(a.toLowerCase().startsWith("createcombo{")) {
            final String path = a.split("\\{")[1].split("}")[0];
            final CustomEnchant n = CustomEnchant.valueOf(path.split(":")[0]);
            final Player p = ev.getPlayer();
            if(!combos.keySet().contains(p)) combos.put(p, new HashMap<>());
            if(!combos.get(p).keySet().contains(n))
                combos.get(p).put(n, Double.parseDouble(path.split(":")[1]));
        } else if(a.toLowerCase().startsWith("addcombo{")) {
            final String path = a.split("\\{")[1].split("}")[0];
            final CustomEnchant n = CustomEnchant.valueOf(path.split(":")[0]);
            final Player p = ev.getPlayer();
            if(!combos.keySet().contains(p)) combos.put(p, new HashMap<>());
            combos.get(p).put(n, (combos.get(p).keySet().contains(n) ? combos.get(p).get(n) : 0.00)+Double.parseDouble(path.split(":")[1]));
        } else if(a.toLowerCase().startsWith("depletecombo{")) {
            final String path = a.split("\\{")[1].split("}")[0];
            final CustomEnchant n = CustomEnchant.valueOf(path.split(":")[0]);
            final Player p = ev.getPlayer();
            if(combos.keySet().contains(p) && combos.get(p).keySet().contains(n)) {
                final double d = combos.get(p).get(n), l = Double.parseDouble(path.split(":")[1]);
                combos.get(p).put(n, d-l < 0.00 ? 0.00 : round(d-l, 2));
            }
        } else if(a.toLowerCase().startsWith("stopcombo{")) {
            final CustomEnchant n = CustomEnchant.valueOf(a.split("\\{")[1].split("}")[0]);
            final Player p = ev.getPlayer();
            if(combos.keySet().contains(p) && combos.get(p).keySet().contains(n)) {
                combos.get(p).remove(n);
            }
        } else if(a.toLowerCase().startsWith("explode{")) {
            final Location l = getRecipientLoc(event, a.split("\\[")[1].split("]")[0]);
            if(l != null)
                explode(l, Float.parseFloat(a.split("]")[1].split(":")[0]), Boolean.parseBoolean(a.split(":")[1]), Boolean.parseBoolean(a.split(":")[2].split("}")[0]));
        } else if(a.toLowerCase().startsWith("if{")) {
            doIf(ev, event, enchant, ev.getLevel(), a, attribute, b, a.split("\\{")[1], P);
        }
    }


    public void addPotionEffect(Event event, LivingEntity entity, PotionEffect potioneffect) {

    }
    public void addPotionEffect(Event event, Player player, LivingEntity entity, PotionEffect potioneffect, List<String> message, CustomEnchant enchant, int level) {
        final CEAApplyPotionEffectEvent e = new CEAApplyPotionEffectEvent(event, player, entity, enchant, level, potioneffect);
        pluginmanager.callEvent(e);
        if(!e.isCancelled()) {
            entity.addPotionEffect(potioneffect);
            dopotioneffect(entity, potioneffect, message, enchant, level);
        } else if(e != null && entity instanceof Player && e.isCancelled()) {
            procPlayerArmor(e, (Player) entity);
            procPlayerItem(e, (Player) entity, null);
        }
    }
    public void removePotionEffect(LivingEntity entity, PotionEffect potioneffect) {
        this.removePotionEffect(entity, potioneffect, null, null, -1);
    }
    public void removePotionEffect(LivingEntity entity, PotionEffect potioneffect, List<String> message, CustomEnchant enchant, int level) {
        if(potioneffect != null && entity.hasPotionEffect(potioneffect.getType())) {
            entity.removePotionEffect(potioneffect.getType());
            dopotioneffect(entity, potioneffect, message, enchant, level);
        }
    }
    private void dopotioneffect(LivingEntity entity, PotionEffect potioneffect, List<String> message, CustomEnchant enchant, int level) {
        if(message != null) {
            for(String s : message) {
                if(s.contains("{ENCHANT}")) s = s.replace("{ENCHANT}", enchant.getName() + " " + toRoman(level));
                if(s.contains("{POTION_EFFECT}")) s = s.replace("{POTION_EFFECT}", potioneffect.getType().getName() + " " + toRoman(potioneffect.getAmplifier() + 1));
                entity.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
            }
        }
    }
    public void damage(LivingEntity entity, double damage) {
        damage = entity.getHealth() - damage <= 0.00 ? 0.00 : damage;
        entity.damage(damage);
    }
    private void sendMessage(LivingEntity entity, CustomEnchant enchant, String message) {
        final String e = enchant != null ? enchant.getName() : "null";
        if(message.contains("\\n")) for(String s : message.split("\\\\n")) entity.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("}", "").replace("%ENCHANT%", e)));
        else                                                               entity.sendMessage(ChatColor.translateAlternateColorCodes('&', message.replace("}", "").replace("%ENCHANT%", e)));
    }
    private void heal(LivingEntity entity, double by) {
        if(entity.getHealth() != entity.getMaxHealth()) {
            entity.setHealth(entity.getHealth() + by > entity.getMaxHealth() ? entity.getMaxHealth() : entity.getHealth() + by);
        }
    }
    private void refillAir(LivingEntity entity, int addedAir) {
        final int r = entity.getRemainingAir(), m = entity.getMaximumAir();
        entity.setRemainingAir(r + addedAir > m ? m : r + addedAir);
    }
    private void setHealth(LivingEntity entity, double newHealth) {
        if(newHealth < 0.00) entity.setHealth(0.00);
        else entity.setHealth(newHealth);
    }
    private int getXP(LivingEntity entity) {
        return entity instanceof Player ? getTotalExperience((Player) entity) : 0;
    }
    private void setXP(LivingEntity entity, int value) {
        if(entity instanceof Player)
            setTotalExperience((Player) entity, value);
    }
    private void healHunger(LivingEntity entity, int by) {
        final Player player = entity instanceof Player ? (Player) entity : null;
        final int f = player != null ? player.getFoodLevel() : 0;
        if(player != null && f + by <= 20) {
            player.setFoodLevel(f + by);
        }
    }
    private void dropItem(LivingEntity entity, ItemStack is) {
        entity.getWorld().dropItem(entity.getLocation(), is);
    }*/
}

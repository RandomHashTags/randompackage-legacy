package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.api.events.customenchant.*;
import me.randomHashTags.RandomPackage.api.events.masks.MaskEquipEvent;
import me.randomHashTags.RandomPackage.api.events.masks.MaskUnequipEvent;
import me.randomHashTags.RandomPackage.api.events.mobstacker.MobStackDepleteEvent;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackage;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.BlackScroll;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.EnchantRarity;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.EnchantmentOrb;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.Fireball;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.MagicDust;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.RandomizationScroll;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.RarityGem;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.SoulTracker;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchantEntity;
import me.randomHashTags.RandomPackage.api.events.AlchemistExchangeEvent;
import me.randomHashTags.RandomPackage.api.events.ArmorSetEquipEvent;
import me.randomHashTags.RandomPackage.api.events.ArmorSetUnequipEvent;
import me.randomHashTags.RandomPackage.api.events.EnchanterPurchaseEvent;
import me.randomHashTags.RandomPackage.api.events.customboss.CustomBossDamageByEntityEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerApplyCustomEnchantEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerApplyCustomEnchantEvent.CustomEnchantApplyResult;
import me.randomHashTags.RandomPackage.api.events.player.PlayerArmorEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerPreApplyCustomEnchantEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerArmorEvent.ArmorEventReason;
import me.randomHashTags.RandomPackage.api.events.player.PlayerRevealCustomEnchantEvent;
import me.randomHashTags.RandomPackage.utils.supported.VaultAPI;
import net.milkbowl.vault.economy.Economy;

@SuppressWarnings({"deprecation"})
public class CustomEnchants extends RandomPackageAPI {
	
	public boolean isEnabled = false;
	private static CustomEnchants instance;
	public static final CustomEnchants getCustomEnchants() {
		if(instance == null) instance = new CustomEnchants();
		return instance;
	}

	public boolean levelZeroRemoval = false, selfproc = false;
	public FileConfiguration config;
	
	private Inventory alchemist, enchanter, tinkerer;
	private HashMap<Integer, ItemStack> enchanterpurchase = new HashMap<>();
	private HashMap<Integer, Integer> enchantercost = new HashMap<>();
	private ItemStack alchemistaccept, alchemistexchange, alchemistpreview, tinkereraccept;
	public ItemStack mysterydust, transmogscroll, whitescroll;
	private String enchantercurrency, alchemistcurrency;
	
	private HashMap<String, Integer> timerenchants = new HashMap<>();
	private List<String> disabledenchants = new ArrayList<>(), noMoreEnchantsAllowed = new ArrayList<>();
	private List<Player> tinkererAccept = new ArrayList<>(), alchemistAccept = new ArrayList<>();
	private List<OfflinePlayer> frozen = new ArrayList<>();
	private HashMap<String, List<String>> blockbreakblacklist = new HashMap<>();
	private HashMap<Location, HashMap<ItemStack, HashMap<Block, Integer>>> temporayblocks = new HashMap<>(); // <block location <original block, <temporary new block, ticks>>>>

	private enum RomanNumeralValues {
		I(1), X(10), C(100), M(1000), V(5), L(50), D(500);
		private int val;
		RomanNumeralValues(int val) { this.val = val; }
		public int asInt() { return val; }
	}
	
	private String WHITE_SCROLL, SUCCESS, DESTROY, TRANSMOG;
	private HashMap<UUID, ItemStack> shotBows = new HashMap<>();
	public HashMap<UUID, Player> shotbows = new HashMap<>();

	private List<Player> stoppedAllEnchants = new ArrayList<>();
	private HashMap<Player, HashMap<CustomEnchant, Integer>> stoppedEnchants = new HashMap<>();
	private HashMap<Player, HashMap<CustomEnchant, Double>> combos = new HashMap<>();
	private List<UUID> spawnedFromSpawner = new ArrayList<>();
	
	public boolean mcmmoIsEnabled = RandomPackage.mcmmo != null && RandomPackage.mcmmo.isEnabled();
	
	private int successSlot, destroySlot, alchemistCostSlot;

	private Firework doFirework(String rarity) {
		final Firework fw = Bukkit.getWorlds().get(0).spawn(Bukkit.getWorlds().get(0).getSpawnLocation(), Firework.class);
		final FireworkMeta fwm = fw.getFireworkMeta();
		String[] string = config.getString("rarities." + rarity + ".revealed-item.firework").split(":");
		fwm.addEffects(FireworkEffect.builder().trail(true).flicker(true).with(Type.valueOf(string[0].toUpperCase())).withColor(getColor(string[1])).withFade(getColor(string[2])).withFlicker().withTrail().build());
		fwm.setPower(Integer.parseInt(string[3]));
		fw.setFireworkMeta(fwm);
		return fw;
	}


	public void enable() {
	    final long started = System.currentTimeMillis();
	    if(isEnabled) return;
	    pluginmanager.registerEvents(this, randompackage);
	    save("Features", "custom enchants.yml");
	    isEnabled = true;

        for(World world : Bukkit.getServer().getWorlds()) {
            if(!world.getGameRuleValue("commandBlockOutput").equals("false"))  world.setGameRuleValue("commandBlockOutput", "false");
            if(!world.getGameRuleValue("sendCommandFeedback").equals("false")) world.setGameRuleValue("sendCommandFeedback", "false");
        }
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "custom enchants.yml"));

        for(String s : config.getStringList("settings.no more enchants"))
            noMoreEnchantsAllowed.add(ChatColor.translateAlternateColorCodes('&', s));
        selfproc = config.getBoolean("settings.proc self harm");
        levelZeroRemoval = config.getBoolean("settings.level-zero-removal");
        mysterydust = d(config, "dust.mystery", 0);
        transmogscroll = d(config, "items.transmog-scroll", 0);
        whitescroll = d(config, "items.white-scroll", 0);
        WHITE_SCROLL = ChatColor.translateAlternateColorCodes('&', config.getString("items.white-scroll.apply"));
        TRANSMOG = ChatColor.translateAlternateColorCodes('&', config.getString("items.transmog-scroll.apply"));
        SUCCESS = ChatColor.translateAlternateColorCodes('&', config.getString("rarities.values.success"));
        DESTROY = ChatColor.translateAlternateColorCodes('&', config.getString("rarities.values.destroy"));

        alchemistcurrency = config.getString("alchemist.currency").toUpperCase();
        enchantercurrency = config.getString("enchanter.currency").toLowerCase();
        alchemistaccept = d(config, "alchemist.accept", 0);
        for(int i = 0; i < alchemistaccept.getItemMeta().getLore().size(); i++) if(alchemistaccept.getItemMeta().getLore().get(i).contains("{COST}")) alchemistCostSlot = i;
        alchemistexchange = d(config, "alchemist.exchange", 0);
        alchemistpreview = d(config, "alchemist.preview", 0);
        tinkereraccept = d(config, "tinkerer.accept", 0);

        alchemist = Bukkit.createInventory(null, 27, ChatColor.translateAlternateColorCodes('&', config.getString("alchemist.title")));
        tinkerer = Bukkit.createInventory(null, config.getInt("tinkerer.size"), ChatColor.translateAlternateColorCodes('&', config.getString("tinkerer.title")));
        setupInventory(alchemist);
        setupInventory(tinkerer);

        for(String s : config.getConfigurationSection("block-break-blacklist").getKeys(true))
            blockbreakblacklist.put(s.toLowerCase(), config.getStringList("block-break-blacklist." + s));
        final Firework fw = doFirework("RANDOM");
        final EnchantRarity randomrarity = new EnchantRarity("RANDOM", "", "", fw, d(config, "rarities.RANDOM.reveal-item", 0), null, config.getStringList("rarities.RANDOM.reveal-enchant-msg"));
        if(fw != null) fw.remove();
        List<ItemStack> raritybooks = new ArrayList<>();
        raritybooks.add(randomrarity.getRevealItem());
        for(int i = 0; i < config.getStringList("rarities.values.lore-format").size(); i++) {
            if(config.getStringList("rarities.values.lore-format").get(i).equals("{SUCCESS}")) successSlot = i;
            else if(config.getStringList("rarities.values.lore-format").get(i).equals("{DESTROY}")) destroySlot = i;
        }
        int enchantsize = 0;
        final List<EnchantRarity> format = CustomEnchant.format;
        for(String s : config.getConfigurationSection("rarities").getKeys(false)) {
            if(!s.equals("apply-info") && !s.equals("values") && !s.equals("RANDOM")) {
                final Firework firework = doFirework(s);
                final EnchantRarity rar = new EnchantRarity(s, config.getString("rarities." + s + ".revealed-item.name-colors"), config.getString("rarities." + s + ".revealed-item.apply-colors"), firework, d(config, "rarities." + s + ".reveal-item", 0), d(config, "rarities." + s + ".revealed-item", 0), config.getStringList("rarities." + s + ".reveal-enchant-msg"));
                if(firework != null) firework.remove();
                raritybooks.add(rar.getRevealItem());
                if(!format.contains(rar)) format.add(rar);
            }
        }
        final ArrayList<String> missingLore = new ArrayList<>(), missingAttributes = new ArrayList<>(), tinkererXPValues = new ArrayList<>(), alchemistValues = new ArrayList<>(), missingAppliesTo = new ArrayList<>(), enchantTimers = new ArrayList<>();
        final PluginEnableEvent fakeEvent = new PluginEnableEvent(randompackage);
        for(String s : config.getConfigurationSection("enchants").getKeys(false)) {
            enchantsize += 1;
            final int maxlevel = config.getInt("enchants." + s + ".max level");
            final String rarity = config.getString("enchants." + s + ".rarity"), name = config.getString("enchants." + s + ".name"), tinkerer = config.getString("enchants." + s + ".tinkerer"), alchemist = config.getString("enchants." + s + ".alchemist");
            final List<String> appliesto = new ArrayList<>(), attributes = config.getStringList("enchants." + s + ".attributes"), enchantlore = config.getStringList("enchants." + s + ".lore");

            if(alchemist == null || alchemist.split(":").length == 0 && maxlevel != 1 || alchemist.split(":").length != maxlevel-1) alchemistValues.add(s);
            if(tinkerer == null || tinkerer.split(":").length == 0 && maxlevel != 1 || tinkerer.split(":").length != maxlevel) tinkererXPValues.add(s);
            if(attributes == null) missingAttributes.add(s);
            if(enchantlore == null) missingLore.add(s);

            final String t = config.getString("enchants." + s + ".applies to"), requires = config.getString("enchants." + s + ".requires");
            if(t != null)  for(String S : t.split(";")) appliesto.add(S);
            else           missingAppliesTo.add(s);
            String enchantprocvalue = "";
            if(attributes != null) for(String att : attributes) if(att.startsWith("EnchantProc;value=")) enchantprocvalue = att.split("=")[1];
            final CustomEnchant E = new CustomEnchant(s, config.getBoolean("enchants." + s + ".enabled"), name, EnchantRarity.valueOf(rarity), maxlevel, requires, enchantprocvalue, appliesto, attributes, enchantlore, tinkerer, alchemist);
            if(E.isEnabled() && attributes != null) {
                for(String a : attributes) {
                    if(a.toLowerCase().startsWith("timer(")) {
                        final int f = (int) oldevaluate(a.split("\\(")[1].split("\\)")[0]);
                        enchantTimers.add(E.getRarity().getApplyColors() + s + " (" + f + " ticks)");
                        final int w = scheduler.scheduleSyncRepeatingTask(randompackage, new Runnable() {
                            public void run() {
                                for(Player p : Bukkit.getOnlinePlayers()) {
                                    tryProcEnchant(fakeEvent, p, E);
                                }
                            }
                        }, 0, f);
                        timerenchants.put(name, w);
                    }
                }
            } else {
                disabledenchants.add(E.getRarity().getApplyColors() + name);
            }
        }
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &3Started repeating timers for enchants: &f" + enchantTimers));
        if(!alchemistValues.isEmpty()) Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &cMissing, too many, or not enough alchemist values for enchants: &f" + alchemistValues));
        if(!tinkererXPValues.isEmpty()) Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &cMissing, too many, or not enough tinkerer values for enchants: &f" + tinkererXPValues));
        if(!missingAttributes.isEmpty()) Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &cMissing \"&fattributes&c\" for enchants: &f" + missingAttributes));
        if(!missingLore.isEmpty()) Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &cMissing \"&flore&c\" for enchants: &f" + missingLore));
        if(!missingAppliesTo.isEmpty()) Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &cMissing \"&fapplies to&c\" path for enchants: &f" + missingAppliesTo));

        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + enchantsize + " enchants! &e(took " + (System.currentTimeMillis()-started) + "ms)"));

        final HashMap<Integer, String> defaultColors = new HashMap<>();
        defaultColors.put(0, config.getString("rarity gems.default-settings.colors.less-than-100"));
        defaultColors.put(-1, config.getString("rarity gems.default-settings.colors.else"));
        for(int z = 100; z <= 100000; z += 100) {
            final String Z = config.getString("rarity gems.default-settings.colors." + z + "s");
            if(Z != null) defaultColors.put(z, Z);
        }
        // Rarity Gems
        for(String path : config.getConfigurationSection("rarity gems").getKeys(false)) {
            if(!path.equals("default-settings")) {
                if(config.get("rarity gems." + path) != null) {
                    List<EnchantRarity> worksFor = new ArrayList<>();
                    for(String s : config.getString("rarity gems." + path + ".activates").split(";")) worksFor.add(EnchantRarity.valueOf(s));
                    new RarityGem(path, d(config, "rarity gems." + path, 0), worksFor, config.getStringList("rarity gems.split-msg"), config.getInt("rarity gems." + path + ".time-between-same-kills"), defaultColors, config.getStringList("rarity gems." + path + ".toggle-on"), config.getStringList("rarity gems." + path + ".toggle-off.interact"), config.getStringList("rarity gems." + path + ".toggle-off.dropped"), config.getStringList("rarity gems." + path + ".toggle-off.moved"), config.getStringList("rarity gems." + path + ".toggle-off.ran-out"));
                }
            }
        }
        List<ItemStack> orbs = new ArrayList<>();
        for(int WASD = 1; WASD <= 3; WASD++) {
            for(int i = 0; i <= 1000; i++) {
                if(WASD == 1) {
                    // Enchantment Orbs
                    if(config.get("enchantment-orbs." + i) != null) {
                        final ItemStack iii = d(config, "enchantment-orbs." + i, 0);
                        final List<String> appliesto = new ArrayList<>();
                        for(String s : config.getString("enchantment-orbs." + i + ".applies to").split(";")) appliesto.add(s.toUpperCase());
                        final int starting = config.getInt("enchantment-orbs." + i + ".starting-max-slots");
                        final int increment = config.getInt("enchantment-orbs." + i + ".upgrade-increment");
                        int increm = increment;
                        for(int k = starting; k <= config.getInt("enchantment-orbs." + i + ".final-max-slots"); k += increment) {
                            if(k != starting) increm += increment;
                            final String slots = Integer.toString(k), increments = Integer.toString(increm), appliedlore = ChatColor.translateAlternateColorCodes('&', config.getString("enchantment-orbs." + i + ".apply").replace("{SLOTS}", slots).replace("{ADD_SLOTS}", increments));
                            item = iii.clone(); itemMeta = item.getItemMeta(); lore.clear();
                            itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{SLOTS}", slots));
                            if(itemMeta.hasLore()) {
                                for(String s : itemMeta.getLore()) {
                                    lore.add(s.replace("{SLOTS}", slots).replace("{INCREMENT}", increments));
                                }
                            }
                            itemMeta.setLore(lore); lore.clear();
                            item.setItemMeta(itemMeta);
                            new EnchantmentOrb(i, item, appliedlore, appliesto, k, increm);
                            item = item.clone(); itemMeta = item.getItemMeta(); lore.clear();
                            for(String s: itemMeta.getLore()) lore.add(s.replace("{PERCENT}", "100"));
                            itemMeta.setLore(lore); lore.clear();
                            item.setItemMeta(itemMeta);
                            orbs.add(item);
                        }
                    }
                    // Soul Trackers
                    if(config.get("soul trackers." + i) != null) {
                        List<String> appliesto = new ArrayList<>();
                        for(String s : config.getString("soul trackers." + i + ".applies to").split(";")) appliesto.add(s);
                        new SoulTracker(i, RarityGem.valueOf(config.getString("soul trackers." + i + ".converts to gem")), d(config, "soul trackers." + i, 0), config.getString("soul trackers." + i + ".apply"), config.getStringList("soul trackers." + i + ".apply-msg"), appliesto, config.getDouble("soul trackers." + i + ".split-multiplier"), config.getStringList("soul trackers." + i + ".split-msg"));
                    }
                } else if(WASD == 2) {
                    // Black Scrolls
                    if(config.get("black-scrolls." + i) != null) {
                        List<EnchantRarity> appliesto = new ArrayList<>();
                        for(String s : config.getString("black-scrolls." + i + ".applies to").split(";")) appliesto.add(EnchantRarity.valueOf(s));
                        new BlackScroll(i, d(config, "black-scrolls." + i, 0), Integer.parseInt(config.getString("black-scrolls." + i + ".percents").split(";")[0]), Integer.parseInt(config.getString("black-scrolls." + i + ".percents").split(";")[1]), appliesto);
                    }
                    // Dust
                    if(config.get("dust." + i) != null) {
                        List<EnchantRarity> appliesto = new ArrayList<EnchantRarity>();
                        for(String s : config.getString("dust." + i + ".applies to").split(";")) appliesto.add(EnchantRarity.valueOf(s));
                        new MagicDust(i, d(config, "dust." + i, 0), config.getInt("dust." + i + ".chance"), Integer.parseInt(config.getString("dust." + i + ".percents").split(";")[0]), Integer.parseInt(config.getString("dust." + i + ".percents").split(";")[1]), appliesto, config.get("dust." + i + ".upgrades-to") != null ? config.getInt("dust." + i + ".upgrades-to") : -1, config.get("dust." + i + ".upgrade-cost") != null ? config.getInt("dust." + i + ".upgrade-cost") : -1);
                    }
                    // Randomization Scrolls
                    if(config.get("randomization-scrolls." + i) != null) {
                        List<EnchantRarity> appliesto = new ArrayList<>();
                        for(String s : config.getString("randomization-scrolls." + i + ".applies to").split(";")) appliesto.add(EnchantRarity.valueOf(s));
                        new RandomizationScroll(i, d(config, "randomization-scrolls." + i, 0), appliesto);
                    }
                } else if(WASD == 3) {
                    // Fireballs
                    if(config.get("fireballs." + i) != null) {
                        List<EnchantRarity> exchangeablerarities = new ArrayList<>();
                        for(String s : config.getString("fireballs." + i + ".exchangeable-rarities").split(";")) exchangeablerarities.add(EnchantRarity.valueOf(s));
                        List<MagicDust> revealsdust = new ArrayList<>();
                        for(String s : config.getString("fireballs." + i + ".reveals-dust").split(";")) revealsdust.add(MagicDust.valueOf(Integer.parseInt(s)));
                        new Fireball(i, d(config, "fireballs." + i, 0), exchangeablerarities, revealsdust);
                    }
                }
            }
        }
        final List<ItemStack> raritygems = new ArrayList<>();
        for(RarityGem g : RarityGem.gems) {
            item = g.getPhysicalItem().clone(); itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{SOULS}", "100"));
            item.setItemMeta(itemMeta);
            raritygems.add(item);
        }
        addGivedpCategory(raritygems, UMaterial.EMERALD, "Rarity Gems", "Givedp: Rarity Gems");
        addGivedpCategory(orbs, UMaterial.ENDER_EYE, "Enchantment Orbs", "Givedp: Enchantment Orbs");
        final List<String> f = new ArrayList<>();
        for(EnchantRarity r : format) f.add(r.getName());
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded Enchant Rarities: &f" + f.toString().substring(1, f.toString().length() - 1)));

        enchanter = Bukkit.createInventory(null, config.getInt("enchanter.size"), ChatColor.translateAlternateColorCodes('&', config.getString("enchanter.title")));
        for(int i = 0; i < enchanter.getSize(); i++) {
            if(config.get("enchanter." + i) != null) {
                final int cost = config.getInt("enchanter." + i + ".cost");
                enchantercost.put(i, cost);
                enchanterpurchase.put(i, checkStringForRPItem(null, config.getString("enchanter." + i + ".purchase")));
                item = d(config, "enchanter." + i, 0); itemMeta = item.getItemMeta(); lore.clear();
                if(itemMeta.hasLore()) {
                    for(String string : itemMeta.getLore()) {
                        if(string.contains("{COST}")) string = string.replace("{COST}", formatInt(cost));
                        lore.add(string);
                    }
                    itemMeta.setLore(lore); lore.clear();
                    item.setItemMeta(itemMeta);
                }
                enchanter.setItem(i, item);
            }
        }
        addGivedpCategory(raritybooks, UMaterial.BOOK, "Rarity Books", "Givedp: Rarity Books");
        for(int i = 1; i <= 3; i ++) {
            List<ItemStack> W = new ArrayList<>();
            if(i == 1)
                for(RandomizationScroll r : RandomizationScroll.scrolls) {
                    W.add(r.getItemStack().clone());
                }
            else if(i == 2)
                for(SoulTracker r : SoulTracker.trackers) {
                    W.add(r.getItemStack().clone());
                }
            else if(i == 3)
                for(Fireball r : Fireball.fireballs) {
                    W.add(r.getItemStack().clone());
                }
            final String what = i == 1 ? "Randomization Scrolls" : i == 2 ? "Soul Trackers" : "Fireballs";
            addGivedpCategory(W, i == 3 ? UMaterial.FIRE_CHARGE : UMaterial.PAPER, what, "Givedp: " + what);
        }

        boolean dropsItemsUponDeath = config.getBoolean("entities.settings.default-drops-items-upon-death"), canTargetSummoner = config.getBoolean("entities.settings.default-can-target-summoner");
        final HashMap<String, CustomEnchantEntity> entities = CustomEnchantEntity.entities;
        for(String s : config.getConfigurationSection("entities").getKeys(true)) {
            if(!s.startsWith("settings")) {
                final String path = s.split("\\.")[0];
                if(!entities.keySet().contains(path)) {
                    canTargetSummoner = config.get("entities." + path + ".can-target-summoner") != null ? config.getBoolean("entities." + path + ".can-target-summoner") : canTargetSummoner;
                    dropsItemsUponDeath = config.get("entities." + path + ".drops-items-upon-death") != null ? config.getBoolean("entities." + path + ".drops-items-upon-death") : dropsItemsUponDeath;
                    new CustomEnchantEntity(EntityType.valueOf(config.getString("entities." + path + ".type").toUpperCase()), path, config.getString("entities." + path + ".name"), config.getStringList("entities." + path + ".attributes"), canTargetSummoner, dropsItemsUponDeath);
                }
            }
        }
    }
    public void disable() {
	    if(!isEnabled) return;
	    BlackScroll.scrolls.clear();
	    CustomEnchant.enchants.clear();
	    CustomEnchant.enabledRarityEnchants.clear();
	    EnchantmentOrb.enchantmentorbs.clear();
	    EnchantRarity.rarities.clear();
	    Fireball.fireballs.clear();
	    MagicDust.dust.clear();
	    RandomizationScroll.scrolls.clear();
	    RarityGem.gems.clear();
	    SoulTracker.trackers.clear();
	    enchanterpurchase.clear();
	    enchantercost.clear();
	    stoppedEnchants.clear();
	    stoppedAllEnchants.clear();
	    combos.clear();
	    spawnedFromSpawner.clear();
	    shotbows.clear();
	    shotBows.clear();
	    timerenchants.clear();
	    disabledenchants.clear();
	    noMoreEnchantsAllowed.clear();
	    tinkererAccept.clear();
	    alchemistAccept.clear();
	    blockbreakblacklist.clear();
        for(OfflinePlayer p : frozen) p.getPlayer().setWalkSpeed(0.2f);
        frozen.clear();
        for(Location l : temporayblocks.keySet()) {
            final World w = l.getWorld();
            final ItemStack prev = (ItemStack) temporayblocks.get(l).keySet().toArray()[0];
            w.getBlockAt(l).setType(prev.getType());
            w.getBlockAt(l).getState().setRawData(prev.getData().getData());
        }
        temporayblocks.clear();
	    isEnabled = false;
        HandlerList.unregisterAll(this);
    }

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		final String n = cmd.getName();
		if(n.equals("disabledenchants"))
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', disabledenchants.toString()));
		else if(player != null && n.equals("alchemist") && hasPermission(player, "RandomPackage.alchemist", true))viewAlchemist(player);
		else if(player != null && n.equals("enchanter") && hasPermission(player, "RandomPackage.enchanter", true))viewEnchanter(player);
		else if(player != null && n.equals("tinkerer") && hasPermission(player, "RandomPackage.tinkerer", true))  viewTinkerer(player);
		else if(n.equals("enchants") && hasPermission(sender, "RandomPackage.enchants", true)) {
			if(args.length == 0)
				viewEnchants(sender, 1);
			else {
				final int page = getRemainingInt(args[0]);
				viewEnchants(sender, page > 0 ? page : 1);
			}
		} else if(player != null && n.equals("splitsouls") && hasPermission(sender, "RandomPackage.splitsouls", true)) {
			splitsouls(player, args.length == 0 ? -1 : getRemainingInt(args[0]));
		}
		return true;
	}
	public void viewEnchants(CommandSender sender, int page) {
		final ChatEvents cea = ChatEvents.getChatEventsAPI();
		final String format = randompackage.getConfig().getString("enchants.format");
		final int maxpage = CustomEnchant.enchants.size()/10;
		page = page > maxpage ? maxpage : page;
        final int starting = page*10;
		for(String s : randompackage.getConfig().getStringList("enchants.msg")) {
			if(s.equals("{ENCHANTS}")) {
				for(int i = starting; i <= starting+10; i++) {
				    if(CustomEnchant.enchants.size() > i) {
                        final CustomEnchant ce = CustomEnchant.enchants.get(i);
                        final EnchantRarity rarity = ce.getRarity();
                        final HashMap<String, List<String>> replacements = new HashMap<>();
                        replacements.put("{TIER}", Arrays.asList(rarity.getApplyColors() + rarity.getName()));
                        replacements.put("{DESC}", ce.getLore());
                        final String msg = ChatColor.translateAlternateColorCodes('&', format.replace("{MAX}", Integer.toString(ce.getMaxLevel())).replace("{ENCHANT}", rarity.getApplyColors() + ChatColor.BOLD + ce.getName()));
                        if(sender instanceof Player) {
                            lore.clear();
                            for(String ss : randompackage.getConfig().getStringList("enchants.hover"))
                                lore.add(ChatColor.translateAlternateColorCodes('&', ss));
                            if(sender instanceof Player)
                                cea.sendHoverMessage((Player) sender, msg, lore, replacements);
                            lore.clear();
                        } else {
                            sender.sendMessage(msg);
                        }

                    }
				}
			} else {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{MAX_PAGE}", Integer.toString(maxpage)).replace("{PAGE}", Integer.toString(page))));
			}
		}
	}
	public void viewAlchemist(Player player) { openInventory(player, alchemist); }
	public void viewEnchanter(Player player) { openInventory(player, enchanter); }
	public void viewTinkerer(Player player) { openInventory(player, tinkerer); }
	private void openInventory(Player player, Inventory inv) {
		player.openInventory(Bukkit.createInventory(player, inv.getSize(), inv.getTitle()));
		player.getOpenInventory().getTopInventory().setContents(inv.getContents());
		player.updateInventory();
	}
	private void setupInventory(Inventory inv) {
		ItemStack i1 = inv.equals(tinkerer) ? d(config, "tinkerer.divider", 0) : d(config, "alchemist.exchange", 0),
				i2 = inv.equals(alchemist) ? d(config, "alchemist.preview", 0) : null,
				i3 = inv.equals(alchemist) ? d(config, "alchemist.other", 0) : null;
		for(int i = 0; i < inv.getSize(); i++) {
			if(inv.equals(alchemist)) {
				if(i == 3 || i == 5) {}
				else if(i == 13) inv.setItem(i, i2);
				else if(i == 22) inv.setItem(i, i1);
				else inv.setItem(i, i3);
			} else if(inv.equals(tinkerer)) {
				if(i == 4 || i == 13 || i == 22 || i == 31 || i == 40 || i == 49) inv.setItem(i, i1);
				else if(i == 0) inv.setItem(i, d(config, "tinkerer.accept", 0));
				else if(i == 8) inv.setItem(i, d(config, "tinkerer.accept-dupe", 0));
			}
					
		}
	}	
	
	public void stopTimerEnchant(CustomEnchant enchant) {
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage.CustomEnchants] &3Stopped Timer for enchant &7" + enchant.getName()));
		scheduler.cancelTask(timerenchants.get(enchant.getName()));
		timerenchants.remove(enchant.getName());
	}
	public int getEnchantmentLevel(String string) {
		string = ChatColor.stripColor(string.split(" ")[string.split(" ").length - 1].toLowerCase().replace("i", "1").replace("v", "2").replace("x", "3").replaceAll("\\p{L}", "").replace("1", "i").replace("2", "v").replace("3", "x").replaceAll("\\p{N}", "").replaceAll("\\p{P}", "").replaceAll("\\p{S}", "").replaceAll("\\p{M}", "").replaceAll("\\p{Z}", "").toUpperCase());
		return fromRoman(string);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	private void playerDropItemEvent(PlayerDropItemEvent event) {
		if(!event.isCancelled()) {
			final RarityGem gem = RarityGem.valueOf(event.getItemDrop().getItemStack());
			if(gem != null) {
				final RPPlayerData pdata = RPPlayerData.get(event.getPlayer().getUniqueId());
				if(pdata.hasActiveRarityGem(gem)) pdata.toggleRarityGem(event, gem);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void playerArmorEvent(PlayerArmorEvent event) {
		if(!event.isCancelled()) {
			final ItemStack itemstack = event.getItem();
			if(itemstack != null && itemstack.hasItemMeta() && itemstack.getItemMeta().hasLore()) {
				for(String s : itemstack.getItemMeta().getLore()) {
					CustomEnchant enchant = CustomEnchant.valueOf(s);
					if(enchant != null)
						procEnchant(event, enchant, getEnchantmentLevel(s), itemstack, null);
				}
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		if(event.isCancelled()) {
			
		} else if(event.getEntity() instanceof LivingEntity && canProcOn(event.getEntity())) {
			Player damager = event.getDamager() instanceof Player ? (Player) event.getDamager() : null;
			if(damager != null) {
				final PvAnyEvent e = new PvAnyEvent(damager, (LivingEntity) event.getEntity(), event.getDamage());
				pluginmanager.callEvent(e);
				if(!e.isCancelled()) {
					event.setDamage(e.getDamage());
					procPlayerArmor(e, damager);
					procPlayerItem(e, damager, getItemInHand(damager));
				}
			}
			final UUID u = event.getDamager().getUniqueId();
			if(damager == null && event.getDamager() instanceof Arrow && shotbows.keySet().contains(u)) {
				damager = shotbows.get(u);
				final PvAnyEvent e = new PvAnyEvent(damager, (LivingEntity) event.getEntity(), event.getDamage(), (Projectile) event.getDamager());
				pluginmanager.callEvent(e);
				if(!event.isCancelled()) {
					procPlayerArmor(e, damager);
					procPlayerItem(e, damager, shotBows.get(u));
					shotBows.remove(u);
					shotbows.remove(u);
					event.setDamage(e.getDamage());
				}
			}
			if(event.getEntity() instanceof Player && event.getDamager() instanceof LivingEntity && !(event.getDamager() instanceof TNTPrimed) && !(event.getDamager() instanceof Creeper)) {
				final Player victim = (Player) event.getEntity();
				final LivingEntity d = (LivingEntity) event.getDamager();
				final isDamagedEvent e = new isDamagedEvent(victim, d, event.getDamage());
				pluginmanager.callEvent(e);
				if(!e.isCancelled()) {
					event.setDamage(e.getDamage());
					procPlayerArmor(e, victim);
					procPlayerItem(e, victim, getItemInHand(victim));
				}
			}
			if(canProcOn(event.getEntity())) {
				final CustomEnchantEntity cee = CustomEnchantEntity.valueOf(event.getEntity().getUniqueId());
				if(cee != null) {
					final CustomEnchantEntityDamageByEntityEvent e = new CustomEnchantEntityDamageByEntityEvent(cee, event.getDamager(), event.getFinalDamage(), event.getDamage());
					pluginmanager.callEvent(e);
					if(!e.isCancelled()) { 
						final LivingEntity le = cee.getSummoner();
						final Player player = le instanceof Player ? (Player) le : null;
						procPlayerArmor(e, player);
						procPlayerItem(e, player, getItemInHand(player));
					}
				}
			}
			if(damager != null && combos.keySet().contains(damager)) {
				final double d = event.getDamage();
			    for(CustomEnchant enchant : combos.get(damager).keySet()) {
                    event.setDamage(d*combos.get(damager).get(enchant));
                }
            }
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void customBossDamageByEntityEvent(CustomBossDamageByEntityEvent event) {
		if(!event.isCancelled() && event.getDamager() instanceof Player) {
			final Player damager = (Player) event.getDamager();
			procPlayerArmor(event, damager);
			procPlayerItem(event, damager, getItemInHand(damager));
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void mobStackDepleteEvent(MobStackDepleteEvent event) {
		if(!event.isCancelled()) {
			final Player killer = event.getKiller() instanceof Player ? (Player) event.getKiller() : null;
			if(killer != null) {
				procPlayerItem(event, killer, getItemInHand(killer));
				procPlayerArmor(event, killer);
			}
		}
	}
	public void procPlayerArmor(Event event, Player player) {
	    if(player != null) {
            for(ItemStack is : player.getInventory().getArmorContents()) {
                if(is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
                    for(String s : is.getItemMeta().getLore()) {
                        CustomEnchant e = CustomEnchant.valueOf(s);
                        if(e != null) {
                            procEnchant(event, e, getEnchantmentLevel(s), is, player);
                        }
                    }
                }
            }
        }
	}
	public void procPlayerItem(Event event, Player player, ItemStack is) {
	    if(player != null) {
            final ItemStack h = is == null ? player.getInventory().getItemInHand() : is;
            if(h != null && h.hasItemMeta() && h.getItemMeta().hasLore()) {
                for(String s : h.getItemMeta().getLore()) {
                    final CustomEnchant e = CustomEnchant.valueOf(s);
                    if(e != null) {
                        procEnchant(event, e, getEnchantmentLevel(s), h, player);
                    }
                }
            }
        }

	}
	public void tryProcEnchant(Event event, Player player, CustomEnchant enchant) {
	    if(player != null) {
            for(ItemStack is : player.getInventory().getArmorContents()) {
                if(is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
                    for(String s : is.getItemMeta().getLore()) {
                        final CustomEnchant e = CustomEnchant.valueOf(s);
                        if(e != null && e.equals(enchant)) {
                            procEnchant(event, e, getEnchantmentLevel(s), is, player);
                        }
                    }
                }
            }
        }

	}
	public boolean isOnCorrectItem(CustomEnchant enchant, ItemStack is) {
		final String i = is != null ? is.getType().name() : null;
		if(enchant != null && i != null) for(String s : enchant.getAppliesTo()) if(i.endsWith(s.toUpperCase())) return true;
		return false;
	}
	public HashMap<CustomEnchant, Integer> getEnchants(ItemStack is) { // <CustomEnchant, CustomEnchant level>
		final HashMap<CustomEnchant, Integer> enchants = new HashMap<>();
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasLore()) {
			for(String s : is.getItemMeta().getLore()) {
				final CustomEnchant e = CustomEnchant.valueOf(s);
				if(e != null) enchants.put(e, getEnchantmentLevel(s));
			}
		}
		return enchants;
	}
	public HashMap<ItemStack, HashMap<CustomEnchant, Integer>> getEnchants(Player player) { // <Inventory Slot, <CustomEnchant, CustomEnchant level>> 
		HashMap<ItemStack, HashMap<CustomEnchant, Integer>> L = new HashMap<>();
		if(player != null) {
			final ItemStack p = player.getInventory().getItem(player.getInventory().getHeldItemSlot());
			if(p != null) {
				L.put(p, getEnchants(p));
			}
			for(ItemStack is : player.getInventory().getArmorContents()) L.put(is, getEnchants(is));
		}
		return L;
	}
	public ItemStack getRevealedItem(CustomEnchant enchant, int level, int success, int destroy, boolean showEnchantType, boolean showOtherLore) {
		item = enchant.getRarity().getRevealedItem().clone(); itemMeta = item.getItemMeta(); lore.clear();
		itemMeta.setDisplayName(enchant.getRarity().getNameColors() + enchant.getName() + " " + toRoman(level));
		for(String r : config.getStringList("rarities.values.lore-format")) {
			if(r.equals("{SUCCESS}")) {
				if(success != -1) lore.add(ChatColor.translateAlternateColorCodes('&', config.getString("rarities.values.success").replace("{PERCENT}", Integer.toString(success))));
			} else if(r.equals("{DESTROY}")) {
				if(destroy != -1) lore.add(ChatColor.translateAlternateColorCodes('&', config.getString("rarities.values.destroy").replace("{PERCENT}", Integer.toString(destroy))));
			} else if(r.equals("{ENCHANT_LORE}")) {
				for(String s : enchant.getLore())
					lore.add(ChatColor.translateAlternateColorCodes('&', s));
			} else if(r.equals("{ENCHANT_TYPE}") && showEnchantType) {
				final String path = enchant.getAppliesTo().toString().toLowerCase().replace(",", ";").replace("[", "").replace("]", "").replaceAll("\\p{Z}", "");
				lore.add(ChatColor.translateAlternateColorCodes('&', config.getString("rarities.values.enchant-type." + path)));
			} else if(showOtherLore)
				lore.add(ChatColor.translateAlternateColorCodes('&', r));
		}
		itemMeta.setLore(lore); lore.clear();
		item.setItemMeta(itemMeta);
		return item;
	}
	public ItemStack getRandomEnabledEnchant(EnchantRarity rarity) {
		final List<CustomEnchant> rarities = CustomEnchant.getEnabledEnchants(rarity);
		final CustomEnchant enchant = rarities.get(random.nextInt(rarities.size()));
		final int level = random.nextInt(enchant.getMaxLevel()+1);
		item = rarity.getRevealedItem().clone(); itemMeta = item.getItemMeta(); lore.clear();
		itemMeta.setDisplayName(rarity.getNameColors() + enchant.getName() + " " + toRoman(level == 0 ? 1 : level));
		final String appliesto = enchant.getAppliesTo().toString().replace(" ", "").replace(",", ";");
		for(String s : config.getStringList("rarities.values.lore-format")) {
			if(s.equals("{SUCCESS}")) s = config.getString("rarities.values.success");
			if(s.equals("{DESTROY}")) s = config.getString("rarities.values.destroy");
			if(s.contains("{PERCENT}")) s = s.replace("{PERCENT}", Integer.toString(random.nextInt(101)));
			if(s.equals("{ENCHANT_LORE}"))
				for(String ss : enchant.getLore())
					lore.add(ChatColor.translateAlternateColorCodes('&', ss));
			if(s.equals("{ENCHANT_TYPE}")) s = config.getString("rarities.values.enchant-type." + appliesto.substring(1, appliesto.length()-1));
			if(s != null && !s.equals("{ENCHANT_LORE}")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
		}
		itemMeta.setLore(lore); lore.clear();
		item.setItemMeta(itemMeta);
		return item;
	}
	public boolean canProcOn(Entity e) { return config.getStringList("settings.can proc on").contains(e.getType().name()); }
	@EventHandler(priority = EventPriority.HIGH)
	private void entityDamageEvent(EntityDamageEvent event) {
		if(!event.isCancelled() && event.getEntity() instanceof Player && !event.getCause().equals(DamageCause.ENTITY_ATTACK)) {
			final Player victim = (Player) event.getEntity();
			final isDamagedEvent e = new isDamagedEvent(victim, event.getCause());
			pluginmanager.callEvent(e);
			if(!e.isCancelled()) {
				procPlayerArmor(e, victim);
				procPlayerItem(e, victim, null);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void blockBreakEvent(BlockBreakEvent event) {
		if(!event.isCancelled()) {
		    final Player player = event.getPlayer();
			procPlayerArmor(event, player);
			procPlayerItem(event, player, null);
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void blockPlaceEvent(BlockPlaceEvent event) {
		if(!event.isCancelled()) {
			procPlayerArmor(event, event.getPlayer());
			procPlayerItem(event, event.getPlayer(), null);
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void playerJoinEvent(PlayerJoinEvent event) {
		procPlayerArmor(event, event.getPlayer());
		procPlayerItem(event, event.getPlayer(), null);
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void playerInteractEvent(PlayerInteractEvent event) {
	    final ItemStack I = event.getItem();
		final Player player = event.getPlayer();
		if(!event.isCancelled()) {
			item = getItemInHand(player);
			procPlayerArmor(event, player);
			procPlayerItem(event, player, item);
		}
		EnchantRarity rarity = EnchantRarity.valueOf(I);
		final Fireball fireball = rarity == null ? Fireball.valueOf(I) : null;
		final RarityGem gem = fireball == null ? RarityGem.valueOf(I) : null;
		if(rarity != null) {
		    if(rarity.getName().equals("RANDOM")) {
		        final ArrayList<EnchantRarity> rar = new ArrayList<>(EnchantRarity.rarities);
		        rar.remove(EnchantRarity.valueOf("RANDOM"));
		        rarity = rar.get(random.nextInt(rar.size()));
            }
			final ItemStack r = getRandomEnabledEnchant(rarity);
			final String displayname = r.getItemMeta().getDisplayName();
			final CustomEnchant enchant = CustomEnchant.valueOf(r);
			final PlayerRevealCustomEnchantEvent e = new PlayerRevealCustomEnchantEvent(player, I, enchant, getEnchantmentLevel(displayname));
			pluginmanager.callEvent(e);
			if(!e.isCancelled()) {
				event.setCancelled(true);
				removeItem(player, I, 1);
				giveItem(player, r);
				spawnFirework(rarity.getFirework(), player.getLocation());
				player.updateInventory();
				for(String s : rarity.getRevealMessage()) player.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{ENCHANT}", displayname)));
			}
		} else if(fireball != null) {
			event.setCancelled(true);
			removeItem(player, I, 1);
			int percent = -1;
			boolean did = false;
			for(MagicDust d : fireball.getRevealsDust()) {
				if(!did) {
					percent = random.nextInt(100);
					if(percent <= d.getChance()) {
						percent = d.getMinPercent() + random.nextInt(d.getMaxPercent() - d.getMinPercent() + 1);
						did = true;
						item = d.getItemStack().clone();
						playParticle(particles, "reveal dust.defaults.regular", player.getEyeLocation(), 15);
					}
				}
			}
			if(!did) {
				item = mysterydust.clone();
				playParticle(particles, "reveal dust.defaults.mystery", player.getEyeLocation(), 15);
				playSound(sounds, "dust.reveal-dust", player, player.getLocation(), false);
			}
			itemMeta = item.getItemMeta(); lore.clear();
			if(item.hasItemMeta() && itemMeta.hasLore()) for(String s : itemMeta.getLore()) lore.add(s.replace("{PERCENT}", Integer.toString(percent)));
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
			giveItem(player, item);
		} else if(gem != null) {
			event.setCancelled(true);
			player.updateInventory();
			RPPlayerData.get(player.getUniqueId()).toggleRarityGem(event, gem);
		} else if(I != null && I.hasItemMeta() && I.getItemMeta().hasDisplayName() && I.getItemMeta().hasLore()) {
			if(I.getItemMeta().equals(whitescroll.getItemMeta()) || I.getItemMeta().equals(transmogscroll.getItemMeta())) {
				event.setCancelled(true);
				player.updateInventory();
			} else {
				final CustomEnchant enchant = CustomEnchant.valueOf(I);
				if(enchant != null)
					for(String s : config.getStringList("rarities.apply-info")) player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void entityShootBowEvent(EntityShootBowEvent event) {
		if(!event.isCancelled() && event.getEntity() instanceof Player) {
			final Player p = (Player) event.getEntity();
			procPlayerArmor(event, p);
			procPlayerItem(event, p, null);
			final UUID u = event.getProjectile().getUniqueId();
			shotBows.put(u, getItemInHand(p));
			shotbows.put(u, p);
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void projectileHitEvent(ProjectileHitEvent event) {
		final UUID u = event.getEntity().getUniqueId();
		if(event.getEntity().getShooter() instanceof Player && shotBows.keySet().contains(u)) {
			scheduler.scheduleSyncDelayedTask(randompackage, new Runnable() {
				public void run() {
					final ItemStack is = shotBows.get(u);
					procPlayerItem(event, (Player) event.getEntity().getShooter(), is);
					shotBows.remove(u);
					shotbows.remove(u);
				}
			}, 0);
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void playerDeathEvent(PlayerDeathEvent event) {
		final Player p = event.getEntity(), k = p.getKiller();
		procPlayerArmor(event, p);
		procPlayerItem(event, p, null);
		procPlayerArmor(event, k);
		procPlayerItem(event, k, null);
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void playerItemDamageEvent(PlayerItemDamageEvent event) {
		if(!event.isCancelled()) {
			procPlayerItem(event, event.getPlayer(), event.getItem());
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void entityDeathEvent(EntityDeathEvent event) {
		final UUID u = event.getEntity().getUniqueId();
		if(spawnedFromSpawner.contains(u)) spawnedFromSpawner.remove(u);
		if(event.getEntity() instanceof Player) {
			final RPPlayerData pdata = RPPlayerData.get(u);
			for(RarityGem g : pdata.getActiveRarityGems())
				pdata.toggleRarityGem(null, g);
		}
		if(!(event.getEntity() instanceof Player) && event.getEntity().getKiller() != null) {
			procPlayerArmor(event, event.getEntity().getKiller());
			procPlayerItem(event, event.getEntity().getKiller(), null);
		}
		final UUID summoner = CustomEnchantEntity.getSummoner(event.getEntity());
		if(summoner != null && getEntity(summoner) instanceof Player) {
			final CustomEnchantEntity entity = CustomEnchantEntity.valueOf(event.getEntity().getUniqueId());
			if(entity != null && entity.dropsItemsUponDeath()) {
				event.getDrops().clear(); event.setDroppedExp(0);
			}
			final RPPlayerData pdata = RPPlayerData.get(summoner);
			pdata.removeCustomEnchantEntity(event.getEntity().getUniqueId());
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void foodLevelChangeEvent(FoodLevelChangeEvent event) {
		final Player player = event.getEntity() instanceof Player ? (Player) event.getEntity() : null;
		if(!event.isCancelled() && player != null) {
			procPlayerArmor(event, player);
			procPlayerItem(event, player, null);
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	private void creatureSpawnEvent(CreatureSpawnEvent event) {
		if(!event.isCancelled() && event.getSpawnReason().equals(SpawnReason.SPAWNER))
			spawnedFromSpawner.add(event.getEntity().getUniqueId());
	}
	@EventHandler
	private void tinkererClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
			final String t = event.getWhoClicked().getOpenInventory().getTopInventory().getTitle();
			if(t.equals(tinkerer.getTitle())) {
				final Player player = (Player) event.getWhoClicked();
				event.setCancelled(true);
				player.updateInventory();
				final int r = event.getRawSlot();
				int SLOT = player.getOpenInventory().getTopInventory().firstEmpty();
				final ItemStack cur = event.getCurrentItem();
				final String c = event.getClick().name(), cu = cur != null ? cur.getType().name() : null;
				if(r < 0 || !c.contains("LEFT") && !c.contains("RIGHT") || cu == null || cu.equals("AIR")) return;

				final CustomEnchant e = cur.hasItemMeta() && cur.getItemMeta().hasDisplayName() ? CustomEnchant.valueOf(cur.getItemMeta().getDisplayName()) : null;
				if(event.getRawSlot() >= 4 && event.getRawSlot() <= 8
						|| event.getRawSlot() >= 13 && event.getRawSlot() <= 17
						|| event.getRawSlot() >= 22 && event.getRawSlot() <= 26
						|| event.getRawSlot() >= 31 && event.getRawSlot() <= 35
						|| event.getRawSlot() >= 40 && event.getRawSlot() <= 44
						|| event.getRawSlot() >= 49 && event.getRawSlot() <= 53) {
					return;
				} else if(cur.equals(tinkereraccept)) {
					tinkererAccept.add(player);
					player.closeInventory();
					return;
				} else if(r < player.getOpenInventory().getTopInventory().getSize()) {
					giveItem(player, cur);
					item = new ItemStack(Material.AIR);
					SLOT = r;
				} else if(player.getOpenInventory().getTopInventory().firstEmpty() < 0) {
					return;
				} else if(e != null) {
					final ItemStack itemstack = Fireball.valueOf(Arrays.asList(e.getRarity())).getItemStack();
					if(itemstack == null) return;
					item = itemstack.clone();
				} else if(cur.getItemMeta().hasEnchants() && (cu.endsWith("HELMET") || cu.endsWith("CHESTPLATE") || cu.endsWith("LEGGINGS") || cu.endsWith("BOOTS") || cu.endsWith("SWORD") || cu.endsWith("AXE") || cu.endsWith("SPADE") || cu.endsWith("SHOVEL") || cu.endsWith("HOE") || cu.endsWith("BOW"))) {
					int xp = 0;
					for(Enchantment enchant : cur.getEnchantments().keySet())
						xp += Integer.parseInt(config.getString("tinkerer.enchant-values." + enchant.getName().toLowerCase()));
					if(cur.hasItemMeta() && cur.getItemMeta().hasLore()) {
						final HashMap<CustomEnchant, Integer> enchants = getEnchants(cur);
						for(CustomEnchant enchant : enchants.keySet()) xp += enchant.getTinkererValue(enchants.get(enchant));
					}
					if(xp != 0) item = givedpitem.getXpbottle(xp, "Tinkerer").clone();
				} else {
					sendStringListMessage(player, randompackage.getConfig().getStringList("tinkerer.doesnt want that item"));
					return;
				}
				final int first = player.getOpenInventory().getTopInventory().firstEmpty();
				int slot = first <= 3 || r <= 3 ? 4 : 5;
				player.getOpenInventory().getTopInventory().setItem(SLOT + slot, item);
				if(r >= player.getOpenInventory().getTopInventory().getSize())
					player.getOpenInventory().getTopInventory().setItem(first, cur);
				event.setCurrentItem(new ItemStack(Material.AIR));
			}
		}
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		final String title = event.getWhoClicked().getOpenInventory().getTopInventory().getTitle();
		if(!event.isCancelled()
				&& event.getCursor() != null && event.getCurrentItem() != null
				&& event.getCursor().hasItemMeta()
				&& event.getCursor().getItemMeta().hasDisplayName()
				&& event.getCursor().getItemMeta().hasLore()
				|| title.equals(alchemist.getTitle())
				|| title.equals(enchanter.getTitle())
		) {
			final Player player = (Player) event.getWhoClicked();
			if(title.equals(tinkerer.getTitle())) {
				event.setCancelled(true);
				player.updateInventory();
				if(event.getClick().equals(ClickType.NUMBER_KEY)) return;
			/*
			 * Alchemist
			 */
			} else if(title.equals(alchemist.getTitle())) {
				event.setCancelled(true);
				player.updateInventory();
				if(event.getRawSlot() < player.getOpenInventory().getTopInventory().getSize()) {
					if(event.getRawSlot() == 3 || event.getRawSlot() == 5) {
						giveItem(player, event.getCurrentItem());
						player.getOpenInventory().getTopInventory().setItem(event.getRawSlot(), new ItemStack(Material.AIR));

						item = alchemistpreview.clone();
						if(!player.getOpenInventory().getTopInventory().getItem(13).equals(item)) player.getOpenInventory().getTopInventory().setItem(13, item);
						item = alchemistexchange.clone();
						if(!player.getOpenInventory().getTopInventory().getItem(22).equals(item)) player.getOpenInventory().getTopInventory().setItem(22, item);

					} else if(event.getRawSlot() == 22 && player.getOpenInventory().getTopInventory().getItem(3) != null && player.getOpenInventory().getTopInventory().getItem(5) != null
							&& !player.getOpenInventory().getTopInventory().getItem(13).equals(alchemistpreview)) {
						final int cost = getRemainingInt(player.getOpenInventory().getTopInventory().getItem(22).getItemMeta().getLore().get(alchemistCostSlot));
						final AlchemistExchangeEvent e = new AlchemistExchangeEvent(player, player.getOpenInventory().getTopInventory().getItem(3), player.getOpenInventory().getTopInventory().getItem(5), alchemistcurrency, cost, player.getOpenInventory().getTopInventory().getItem(13));
						pluginmanager.callEvent(e);
						if(!e.isCancelled()) {
							if(!player.getGameMode().equals(GameMode.CREATIVE)) {
								boolean notenough = false;
								final Economy eco = VaultAPI.economy;
								if(alchemistcurrency.equals("EXP")) {
									int totalxp = getTotalExperience(player);
									if(totalxp < cost) {
										notenough = true;
										sendStringListMessage(player, config.getStringList("alchemist.messages.not-enough-xp"));
									} else {
										setTotalExperience(player, totalxp - cost);
									}
									playSound(sounds, "alchemist." + (notenough ? "need-more-xp" : "upgrade-via-xp"), player, player.getLocation(), false);
								} else if(eco != null) {
									if(!eco.withdrawPlayer(player, cost).transactionSuccess()) {
										notenough = true;
										sendStringListMessage(player, config.getStringList("alchemist.messages.not-enough-cash"));
									}
                                    playSound(sounds, "alchemist." + (notenough ? "need-more-cash" : "upgrade-via-cash"), player, player.getLocation(), false);
								}
								else return;
								if(notenough) {
									player.closeInventory();
									player.updateInventory();
									return;
								}
							} else playSound(sounds, "alchemist.upgrade-creative", player, player.getLocation(), false);
							item = player.getOpenInventory().getTopInventory().getItem(13).clone(); itemMeta = item.getItemMeta();
							itemMeta.removeEnchant(Enchantment.ARROW_DAMAGE); itemMeta.removeItemFlags(ItemFlag.HIDE_ENCHANTS);
							item.setItemMeta(itemMeta);
							giveItem(player, item);
							alchemistAccept.add(player);
							player.closeInventory();
						}
					}
				} else if(event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().hasDisplayName() && event.getCurrentItem().getItemMeta().hasLore()) {
				    final Inventory top = player.getOpenInventory().getTopInventory();
					final CustomEnchant enchant = CustomEnchant.valueOf(event.getCurrentItem().getItemMeta().getDisplayName());
					final MagicDust dust = enchant == null ? MagicDust.valueOf(event.getCurrentItem()) : null;
					final String suCCess = enchant != null ? "enchant" : dust != null ? "dust" : null;
					final int F = top.firstEmpty();
					if(suCCess != null) {
						boolean upgrade = false;
						int cost = -1;
						if(F == 5 && !top.getItem(3).getItemMeta().getDisplayName().equals(event.getCurrentItem().getItemMeta().getDisplayName())
								|| F == 3
								&& top.getItem(5) != null
								&& !top.getItem(5).getItemMeta().getDisplayName().equals(event.getCurrentItem().getItemMeta().getDisplayName())
								|| F < 0
								) {
							return;
						} else if(F == 3 && top.getItem(5) == null
								|| F == 5 && top.getItem(3) == null) {
							// This is meant to be here :)
							if(dust != null && dust.getUpgradeCost() == 0) return;
						} else {
							final int slot = F == 3 ? 5 : 3;
							if(suCCess.equals("dust")) {
								item = top.getItem(slot).clone(); itemMeta = item.getItemMeta(); lore.clear();
								cost = dust.getUpgradeCost();
								boolean did = false;
								if(cost == -1) return;
								for(int i = 0; i < itemMeta.getLore().size(); i++) {
									if(getRemainingInt(itemMeta.getLore().get(i)) != -1 && !did) {
										did = true;
										int percent = ((getRemainingInt(itemMeta.getLore().get(i)) + getRemainingInt(event.getCurrentItem().getItemMeta().getLore().get(i))) / 2);
										item = MagicDust.valueOf(dust.getUpgradesToNumber()).getItemStack();
										if(item == null) {

											return;
										}
										item = item.clone(); itemMeta = item.getItemMeta();
										for(String s : itemMeta.getLore()) {
											if(s.contains("{PERCENT}")) s = s.replace("{PERCENT}", "" + percent);
											lore.add(s);
										}
										itemMeta.setLore(lore); lore.clear();
										item.setItemMeta(itemMeta);
									}
								}
							} else {
								final String SUCCESS = ChatColor.translateAlternateColorCodes('&', config.getString("rarities.values.success")), DESTROY = ChatColor.translateAlternateColorCodes('&', config.getString("rarities.values.destroy"));
								final int level = getEnchantmentLevel(event.getCurrentItem().getItemMeta().getDisplayName());
								if(level >= enchant.getMaxLevel()) return;
								else                               cost = enchant.getAlchemistUpgradeCost(level);
								item = new ItemStack(top.getItem(slot).getType(), 1, top.getItem(slot).getData().getData());
								itemMeta = item.getItemMeta();
								itemMeta.setDisplayName("randomhashtags was here");
								int success = 0, destroy = 0, higherDestroy = -1;
								for(int i = 0; i <= 100; i++) {
									if(top.getItem(slot).getItemMeta().getLore().contains(SUCCESS.replace("{PERCENT}", "" + i))
											|| event.getCurrentItem().getItemMeta().getLore().contains(SUCCESS.replace("{PERCENT}", "" + i)))
										success = success + (i/4);
									if(top.getItem(slot).getItemMeta().getLore().contains(DESTROY.replace("{PERCENT}", "" + i))
											|| event.getCurrentItem().getItemMeta().getLore().contains(DESTROY.replace("{PERCENT}", "" + i))) {
										if(i > higherDestroy) higherDestroy = i;
										destroy = destroy + i;
									}
								}
								destroy = higherDestroy + (destroy / 4);
								if(destroy > 100) destroy = 100;
								item = getRevealedItem(enchant, level + 1, success, destroy, true, true).clone(); itemMeta = item.getItemMeta();
							}
							upgrade = true;
						}
						if(upgrade) {
							itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
							item.setItemMeta(itemMeta); item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
							event.getWhoClicked().getOpenInventory().getTopInventory().setItem(13, item);
							item = alchemistaccept.clone(); itemMeta = item.getItemMeta();
							for(String string : itemMeta.getLore()) {
								if(string.contains("{COST}")) string = string.replace("{COST}", formatInt(cost));
								lore.add(string);
							}
							itemMeta.setLore(lore); lore.clear();
							item.setItemMeta(itemMeta);
							event.getWhoClicked().getOpenInventory().getTopInventory().setItem(22, item);
						}
						event.getWhoClicked().getOpenInventory().getTopInventory().setItem(event.getWhoClicked().getOpenInventory().getTopInventory().firstEmpty(), event.getCurrentItem());
						event.setCurrentItem(new ItemStack(Material.AIR));
					}
				}
			/*
			 * Enchanter
			 */
			} else if(title.equals(enchanter.getTitle())) {
				event.setCancelled(true);
				if(enchanterpurchase.keySet().contains(event.getRawSlot())) {
					final int cost = enchantercost.get(event.getRawSlot());
					item = enchanterpurchase.get(event.getRawSlot()).clone();
					final EnchanterPurchaseEvent e = new EnchanterPurchaseEvent(player, item, enchantercurrency, cost);
					List<String> me = null;
					boolean give = false;
					if(player.getGameMode().equals(GameMode.CREATIVE)) {
						give = true;
					} else if(enchantercurrency.equalsIgnoreCase("exp")) {
						final int totalxp = getTotalExperience(player);
						if(totalxp >= cost) {
							setTotalExperience(player, totalxp-cost);
							give = true;
						}
						me = config.getStringList("enchanter.messages." + (totalxp >= cost ? "xp-purchase" : "need-more-xp"));
					} else if(enchantercurrency.equalsIgnoreCase("cash")) {
						final boolean p = VaultAPI.economy.withdrawPlayer(player, cost).transactionSuccess();
						if(p) give = true;
						me = config.getStringList("enchanter.messages." + (p ? "cash-purchase" : "need-more-cash"));
					} else return;
					if(!player.getGameMode().equals(GameMode.CREATIVE)) {
						for(String s : me) {
							if(s.contains("{AMOUNT}")) s = s.replace("{AMOUNT}", formatInt(cost));
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
						}
					}
					if(give) {
						pluginmanager.callEvent(e);
						if(!e.isCancelled())
							giveItem(player, item);
					}
				}
				player.updateInventory();
			} else {
				/*
				 * Apply enchants
				 */
				final ItemStack ienchant = event.getCursor();
				final CustomEnchant enchant = CustomEnchant.valueOf(ienchant.getItemMeta().getDisplayName());
				final int level = getEnchantmentLevel(ienchant.getItemMeta().getDisplayName());
				final PlayerPreApplyCustomEnchantEvent ev = new PlayerPreApplyCustomEnchantEvent(player, enchant, getEnchantmentLevel(ienchant.getItemMeta().getDisplayName()), event.getCurrentItem());
				pluginmanager.callEvent(ev);
				if(!ev.isCancelled() && isOnCorrectItem(enchant, event.getCurrentItem())) {
					boolean apply = false;
					item = event.getCurrentItem().clone(); itemMeta = item.getItemMeta(); lore.clear();
					if(item.hasItemMeta() && itemMeta.hasLore()) {
						if(noMoreEnchantsAllowed != null && itemMeta.getLore().containsAll(noMoreEnchantsAllowed)) {
							ev.setCancelled(true);
							return;
						}
						lore.addAll(itemMeta.getLore());
					}
					CustomEnchantApplyResult result = null;
					if(enchant != null) {
						final int success = getRemainingInt(ienchant.getItemMeta().getLore().get(successSlot)), destroy = getRemainingInt(ienchant.getItemMeta().getLore().get(destroySlot));
						int prevlevel = -1, prevslot = -1, enchantsize = 0, haspermfor = 0, eoIncrement = 0;
						for(int i = 0; i <= 100; i++)
							if(player.hasPermission("RandomPackage.levelcap." + i))
								haspermfor = i;
						for(int z = 0; z < lore.size(); z++) {
							final CustomEnchant e = CustomEnchant.valueOf(lore.get(z));
							if(e != null) {
								enchantsize += 1;
								if(e.equals(enchant)) {
									prevslot = z;
									prevlevel = getEnchantmentLevel(lore.get(z));
									if(prevlevel == e.getMaxLevel()) return;
								}
							} else {
								final EnchantmentOrb eo = EnchantmentOrb.valueOf(lore.get(z));
								if(eo != null) eoIncrement = eo.getIncrement();
							}
						}
						if(haspermfor+eoIncrement <= enchantsize) {
							ev.setCancelled(true);
							return;
						} else {
							final String requires = enchant.getRequires();
							final CustomEnchant replaces = requires != null ? CustomEnchant.valueOf(requires.split(";")[0]) : null;
							final int requiredLvl = replaces != null ? Integer.parseInt(requires.split(";")[1]) : -1;
							final HashMap<CustomEnchant, Integer> enchants = replaces != null ? getEnchants(event.getCurrentItem()) : null;
							if(enchants != null && (!enchants.keySet().contains(replaces) || enchants.get(replaces) < requiredLvl)) return;
							//
							if(random.nextInt(100) <= success) {
								final String e = enchant.getRarity().getApplyColors() + enchant.getName() + " " + toRoman(level);
								if(lore.isEmpty()) {
									lore.add(e);
								} else if(prevlevel == -1 && prevslot == -1) {
									int r = -1; boolean d = false;
									final ArrayList<String> newlore = new ArrayList<>();
									for(String s : lore) {
										if(!d) r += 1;
										final CustomEnchant ce = CustomEnchant.valueOf(s);
										if(ce != null) {
											if(ce.equals(replaces)) d = true;
											newlore.add(s);
										}
									}
									newlore.add(e);
									for(String s : lore)
										if(!newlore.contains(s)) {
											newlore.add(s);
										}
									if(replaces != null) newlore.remove(r);
									lore = newlore;
								} else {
									lore.set(prevslot, enchant.getRarity().getApplyColors() + enchant.getName() + " " + toRoman(level > prevlevel ? level : prevlevel + 1));
								}
								result = lore.isEmpty() || prevlevel == -1 && prevslot == -1 ? CustomEnchantApplyResult.SUCCESS_APPLY : CustomEnchantApplyResult.SUCCESS_UPGRADE;
							} else if(random.nextInt(100) <= destroy) {
								result = lore.contains(WHITE_SCROLL) ? CustomEnchantApplyResult.DESTROY_WHITE_SCROLL : CustomEnchantApplyResult.DESTROY;
								if(lore.contains(WHITE_SCROLL)) lore.remove(WHITE_SCROLL);
								else                            item = new ItemStack(Material.AIR);
							} else {

							}
							apply = true;
							final PlayerApplyCustomEnchantEvent ce = new PlayerApplyCustomEnchantEvent(player, enchant, level, result);
							pluginmanager.callEvent(ce);
						}
					} else {
						if(whitescroll != null && event.getCursor().getItemMeta().equals(whitescroll.getItemMeta())
								&& event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR))
							if(!event.getCurrentItem().hasItemMeta() || event.getCurrentItem().hasItemMeta() && !event.getCurrentItem().getItemMeta().hasLore()
									|| event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().hasLore() && !event.getCurrentItem().getItemMeta().getLore().contains(WHITE_SCROLL)) {
								apply = true;
								lore.add(WHITE_SCROLL);
							}
					}
					if(apply) {
						if(!item.getType().equals(Material.AIR)) {
							if(lore.contains(WHITE_SCROLL)) { lore.remove(WHITE_SCROLL); lore.add(WHITE_SCROLL); }
							itemMeta.setLore(lore); lore.clear();
							item.setItemMeta(itemMeta);
						}
						event.setCancelled(true);
						event.setCurrentItem(item);
						if(event.getCursor().getAmount() == 1)
							event.setCursor(new ItemStack(Material.AIR));
						else
							event.getCursor().setAmount(event.getCursor().getAmount() - 1);
						player.updateInventory();
					}
				} else {
					ev.setCancelled(true);
				}
			}
		}
	}
	@EventHandler
	private void raritygemClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getCurrentItem() != null && event.getCursor() != null && event.getCurrentItem().hasItemMeta() && event.getCursor().hasItemMeta()) {
		    final ItemStack curs = event.getCursor(), curr = event.getCurrentItem();
		    final int cursorAmount = curs.getAmount();
			final RarityGem cursor = RarityGem.valueOf(curs), current = cursor != null ? RarityGem.valueOf(curr) : null;
			if(cursor == null || current == null) return;
			if(cursor.equals(current)) {
				final Player player = (Player) event.getWhoClicked();
				final int combinedTotal = getRemainingInt(curs.getItemMeta().getDisplayName()) + getRemainingInt(curr.getItemMeta().getDisplayName());
				event.setCancelled(true);
				item = cursor.getPhysicalItem().clone();
				itemMeta = item.getItemMeta();
				itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{SOULS}", ChatColor.translateAlternateColorCodes('&', cursor.getColors(combinedTotal)) + combinedTotal));
				item.setItemMeta(itemMeta);
				event.setCurrentItem(item);
				if(cursorAmount == 1) event.setCursor(new ItemStack(Material.AIR));
				else {
				    curs.setAmount(cursorAmount-1);
				    event.setCursor(curs);
                }
				player.updateInventory();
			}
		}
	}
	@EventHandler
	private void givedpClickEvent(InventoryClickEvent event) {
		if(event.isCancelled()) return;
		else if(event.getWhoClicked().getOpenInventory().getTopInventory().getType().equals(InventoryType.ANVIL)) {
			if(event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().hasDisplayName() && event.getCurrentItem().getItemMeta().hasLore() || event.getClick().equals(ClickType.NUMBER_KEY)) {
				CustomEnchant enchant = null;
				item = event.getClick().equals(ClickType.NUMBER_KEY) && event.getWhoClicked().getInventory().getItem(event.getHotbarButton()) != null ? event.getWhoClicked().getInventory().getItem(event.getHotbarButton()) : event.getCurrentItem();
				if(item.hasItemMeta() && item.getItemMeta().hasDisplayName())
					enchant = CustomEnchant.valueOf(item.getItemMeta().getDisplayName());
				if(enchant != null) {
					event.setCancelled(true);
					((Player) event.getWhoClicked()).updateInventory();
					event.getWhoClicked().closeInventory();
				}
			}
			return;
		} else if(event.getCurrentItem() == null || event.getCurrentItem().getType().equals(Material.AIR)) { return;
		} else if(event.getInventory().getType().equals(InventoryType.CRAFTING) || event.getInventory().getType().equals(InventoryType.PLAYER)) {
			final Player player = (Player) event.getWhoClicked();
			final ItemStack cursor = event.getCursor(), currentitem = event.getCurrentItem();
			boolean success = false;
			int enchantcount = -1;
			if(cursor.hasItemMeta() && cursor.getItemMeta().hasDisplayName() && cursor.getItemMeta().hasLore()) {
				item = currentitem; itemMeta = currentitem.getItemMeta(); lore.clear();
				CustomEnchant enchant = CustomEnchant.valueOf(currentitem);
				final RandomizationScroll randomizationscroll = RandomizationScroll.valueOf(cursor);
				if(enchant != null && randomizationscroll != null && randomizationscroll.getAppliesToRarities().contains(enchant.getRarity())) {
					for(String string : itemMeta.getLore()) {
						if(string.equals(SUCCESS.replace("{PERCENT}", "" + getRemainingInt(string))))        string = SUCCESS.replace("{PERCENT}", "" + random.nextInt(101));
						else if(string.equals(DESTROY.replace("{PERCENT}", "" + getRemainingInt(string))))   string = DESTROY.replace("{PERCENT}", "" + random.nextInt(101));
						lore.add(ChatColor.translateAlternateColorCodes('&', string));
					}
					itemMeta.setLore(lore); lore.clear();
					success = true;
				}
				HashMap<CustomEnchant, Integer> enchantmentsonitem = null;
				if(currentitem.hasItemMeta() && currentitem.getItemMeta().hasLore()) enchantmentsonitem = getEnchants(currentitem);
				if(cursor.getItemMeta().equals(whitescroll.getItemMeta())) {
					if(item.getType().name().endsWith("AXE") || item.getType().name().endsWith("SWORD") || item.getType().name().endsWith("SPADE")
							|| item.getType().equals(Material.BOW)
							|| item.getType().equals(Material.FISHING_ROD)
							|| item.getType().name().endsWith("HELMET") || item.getType().name().endsWith("CHESTPLATE") || item.getType().name().endsWith("LEGGINGS") || item.getType().name().endsWith("BOOTS")
							) {
						if(!item.hasItemMeta()
								|| item.hasItemMeta() && !itemMeta.hasLore()
								|| item.hasItemMeta() && itemMeta.hasLore() && !itemMeta.getLore().contains(WHITE_SCROLL)
								) {
							if(item.hasItemMeta() && itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
							lore.add(WHITE_SCROLL);
							itemMeta.setLore(lore); lore.clear();
							success = true;
						}
					}
				} else if(cursor.getItemMeta().equals(transmogscroll.getItemMeta())) {
					applyTransmogScroll(currentitem);
					//playSuccess((Player) event.getWhoClicked());
					event.setCancelled(true);
					event.setCurrentItem(currentitem);
					if(cursor.getAmount() == 1) event.setCursor(new ItemStack(Material.AIR));
					else                        cursor.setAmount(cursor.getAmount() - 1);
					player.updateInventory();
					return;
				} else {
					final BlackScroll bs = BlackScroll.valueOf(cursor);
					if(bs != null && item != null && item.hasItemMeta() && item.getItemMeta().hasLore() && !enchantmentsonitem.isEmpty()) {
						enchantcount = enchantmentsonitem.size();
						giveItem(player, applyBlackScroll(currentitem, cursor, bs));
						item = currentitem; itemMeta = item.getItemMeta();
						success = true;
					}
					final MagicDust dust = bs == null ? MagicDust.valueOf(cursor) : null;
					if(dust != null && enchant != null && dust.getAppliesTo().contains(enchant.getRarity())) {
						int percent = -1;
						for(int z = 0; z < dust.getItemStack().getItemMeta().getLore().size(); z++)
							if(dust.getItemStack().getItemMeta().getLore().get(z).contains("{PERCENT}"))
								percent = getRemainingInt(cursor.getItemMeta().getLore().get(z));
						if(percent == -1) return;
						for(String string : itemMeta.getLore()) {
							int r = getRemainingInt(string);
							if(string.equals(SUCCESS.replace("{PERCENT}", "" + r))) {
								if(r >= 100) return;
								else if(r + percent > 100) { r = 50; percent = 50; }
								string = SUCCESS.replace("{PERCENT}", "" + (r + percent));
							}
							lore.add(ChatColor.translateAlternateColorCodes('&', string));
						}
						itemMeta.setLore(lore); lore.clear();
						success = true;
					}
					final EnchantmentOrb orb = dust == null ? EnchantmentOrb.valueOf(cursor) : null;
					if(orb != null) {
						boolean did = false;
						final EnchantmentOrb o = EnchantmentOrb.getOrb(currentitem);
						for(String s : orb.getAppliesTo()) {
							if((o == null || !o.equals(orb) && o.getIncrement() < orb.getIncrement()) && currentitem.getType().name().toLowerCase().endsWith(s.toLowerCase())) {
								did = true;
								applyEnchantmentOrb(player, currentitem, cursor, orb);
							}
						}
						if(!did) return;
						success = true;
					}
					final SoulTracker soultracker = orb == null ? SoulTracker.valueOf(cursor) : null;
					if(soultracker != null) {
						applySoulTracker(player, currentitem, soultracker);
						success = true;
					}
				}
				if(success) {
					//playSuccess((Player) event.getWhoClicked());
					item.setItemMeta(itemMeta);
					if(itemMeta.hasDisplayName() && itemMeta.getDisplayName().contains(TRANSMOG.replace("{LORE_COUNT}", Integer.toString(enchantcount))))
						applyTransmogScroll(item);
					event.setCancelled(true);
					event.setCurrentItem(item);
					if(event.getCursor().getAmount() == 1) event.setCursor(new ItemStack(Material.AIR));
					else                                   event.getCursor().setAmount(event.getCursor().getAmount() - 1);
					player.updateInventory();
				}
			}
		}
	}
	@EventHandler
	private void inventoryCloseEvent(InventoryCloseEvent event) {
		final Player player = (Player) event.getPlayer();
		if(event.getInventory().getHolder() == event.getPlayer()) {
			final String title = event.getInventory().getTitle();
			if(title.equals(alchemist.getTitle())) {
				final boolean contains = alchemistAccept.contains(player);
				sendStringListMessage(player, config.getStringList("alchemist.messages." + (contains ? "exchange" : "close")));
				if(contains) {
					alchemistAccept.remove(player);
				} else {
					giveItem(player, event.getInventory().getItem(3));
					giveItem(player, event.getInventory().getItem(5));
				}
			} else if(title.equals(tinkerer.getTitle())) {
				final boolean contains = tinkererAccept.contains(player);
				sendStringListMessage(player, config.getStringList("tinkerer.messages." + (contains ? "accept" : "cancel") + "-trade"));
				if(contains) {
					tinkererAccept.remove(player);
					for(int i = 0; i < event.getInventory().getSize(); i++)
						if(event.getInventory().getItem(i) != null) if(i >= 5 && i <= 7 || i >= 14 && i <= 17 || i >= 23 && i <= 26 || i >= 32 && i <= 35 || i >= 41 && i <= 44 || i >= 50 && i <= 53) giveItem(player, event.getInventory().getItem(i));
				} else {
					for(int i = 0; i < event.getInventory().getSize(); i++)
						if(event.getInventory().getItem(i) != null) if(i >= 1 && i <= 3 || i >= 9 && i <= 12 || i >= 18 && i <= 21 || i >= 27 && i <= 30 || i >= 36 && i <= 39 || i >= 45 && i <= 48) giveItem(player, event.getInventory().getItem(i));
				}
			} else { return; }
			if(player.isOnline()) player.updateInventory();
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	private void customEnchantProcEvent(CustomEnchantProcEvent event) {
		if(event.getEvent() instanceof CustomEnchantProcEvent && !event.isCancelled() && !((CustomEnchantProcEvent) event.getEvent()).isCancelled()) {
			procPlayerArmor(event.getEvent(), event.getPlayer());
			procPlayerItem(event.getEvent(), event.getPlayer(), null);
		}
	}
	@EventHandler(priority = EventPriority.LOWEST)
	private void customEnchantProcEventCancel(CustomEnchantProcEvent event) {
		final Player player = event.getPlayer();
		if(stoppedAllEnchants.contains(player) || stoppedEnchants.keySet().contains(player) && stoppedEnchants.get(player).keySet().contains(event.getEnchant())) {
			event.setCancelled(true);
		}
	}
	public void procEnchant(Event event, CustomEnchant enchant, int level, ItemStack itemWithEnchant, Player P) {
		final CustomEnchantProcEvent e = new CustomEnchantProcEvent(event, enchant, level, itemWithEnchant, P);
		pluginmanager.callEvent(e);
		if(!e.isCancelled() && (!(e.getEvent() instanceof Cancellable) || e.getEvent() instanceof Cancellable && !((Cancellable) e.getEvent()).isCancelled())
				|| e.getEvent() instanceof PluginEnableEvent
				) {
			executeAttributes(e, e.getPlayer());
		}
	}

	public void executeAttributes(CustomEnchantProcEvent e, Player P) {
		final Event event = e.getEvent();
		final CustomEnchant enchant = e.getEnchant();
		for(String attr : enchant.getAttributes()) {
			final String A = attr.split(";")[0].toLowerCase();
			if(event instanceof PlayerArmorEvent && (A.equals("armorequip") && ((PlayerArmorEvent) event).getReason().name().contains("_EQUIP") || A.equals("armorunequip") && (((PlayerArmorEvent) event).getReason().name().contains("_UNEQUIP") || ((PlayerArmorEvent) event).getReason().name().contains("DROP")) || A.equals("armorpiecebreak") && ((PlayerArmorEvent) event).getReason().equals(ArmorEventReason.BREAK))
					|| event instanceof PvAnyEvent && (A.equals("pva") || A.equals("pvp") && ((PvAnyEvent) event).getVictim() instanceof Player || A.equals("pve") && !(((PvAnyEvent) event).getVictim() instanceof Player) || A.equals("arrowhit") && ((PvAnyEvent) event).getProjectile() != null && ((PvAnyEvent) event).getProjectile() instanceof Arrow && shotbows.keySet().contains(((PvAnyEvent) event).getProjectile().getUniqueId()))

					|| event instanceof isDamagedEvent && (A.equals("isdamaged") || A.equals("hitbyarrow") && ((isDamagedEvent) event).getDamager() instanceof Arrow || A.startsWith("damagedby(") && ((isDamagedEvent) event).getCause() != null && A.toUpperCase().contains(((isDamagedEvent) event).getCause().name()))

					|| event instanceof CustomEnchantEntityDamageByEntityEvent && A.startsWith("ceentityisdamaged")
					|| event instanceof CustomBossDamageByEntityEvent && A.startsWith("custombossisdamaged")

					|| event instanceof ArmorSetEquipEvent && A.equals("armorsetequip")
					|| event instanceof ArmorSetUnequipEvent && A.equals("armorsetunequip")

					|| event instanceof BlockPlaceEvent && A.equals("blockplace")
					|| event instanceof BlockBreakEvent && A.equals("blockbreak")

					|| event instanceof FoodLevelChangeEvent && A.equals("foodlevelgained") && ((FoodLevelChangeEvent) event).getEntity() instanceof Player && ((FoodLevelChangeEvent) event).getFoodLevel() > ((Player) ((FoodLevelChangeEvent) event).getEntity()).getFoodLevel()
					|| event instanceof FoodLevelChangeEvent && A.equals("foodlevellost") && ((FoodLevelChangeEvent) event).getEntity() instanceof Player && ((FoodLevelChangeEvent) event).getFoodLevel() < ((Player) ((FoodLevelChangeEvent) event).getEntity()).getFoodLevel()

					|| event instanceof PlayerItemDamageEvent && A.equals("isdurabilitydamaged")

					|| event instanceof PlayerInteractEvent && A.equals("playerinteract")

					|| event instanceof ProjectileHitEvent && A.equals("arrowland") && ((ProjectileHitEvent) event).getEntity() instanceof Arrow && getHitEntity((ProjectileHitEvent) event) == null
					|| event instanceof EntityShootBowEvent && A.equals("shootbow")

					|| event instanceof PlayerDeathEvent && (A.equals("playerdeath") || A.equals("killedplayer"))
					|| event instanceof EntityDeathEvent && A.equals("killedentity") && !(((EntityDeathEvent) event).getEntity() instanceof Player)

					|| event instanceof CustomEnchantProcEvent && A.equals("enchantproc")
					|| event instanceof CEAApplyPotionEffectEvent && A.equals("ceapplypotioneffect")

					|| event instanceof MobStackDepleteEvent && A.equals("mobstackdeplete")

					|| event instanceof PluginEnableEvent && A.startsWith("timer(")
					|| attr.toLowerCase().contains(";didproc;") && e.didProc()

					|| mcmmoIsEnabled && event instanceof com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent && (A.equals("mcmmoxpgained") || A.equals("mcmmoxpgained:" + ((com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent) event).getSkill().name().toLowerCase()))

					) {
				doAttribute(e, attr, enchant, P);
			}
		}
	}

	private void doAttribute(CustomEnchantProcEvent e, String attribute, CustomEnchant enchant, Player P) {
		final int level = e.getLevel();
		if(attribute.contains("level")) attribute = attribute.replace("level", Integer.toString(level));
		int b = -1;
		e.setCancelled(false);
		if(attribute.contains("random{")) {
			final String ee = attribute.split("random\\{")[1].split("}")[0];
			final int min = (int) oldevaluate(ee.split(":")[0]), max = (int) oldevaluate(ee.split(":")[1].split("}")[0]);
			int r = min + random.nextInt(max - min + 1);
			attribute = attribute.replace("random{" + ee + "}", Integer.toString(r));
		}
		for(String a : attribute.split(";")) {
			b++;
			if(a.toLowerCase().startsWith("didproc") && !e.didProc()) {
				return;
			} else if(a.toLowerCase().startsWith("chance=")) {
				HashMap<ItemStack, HashMap<CustomEnchant, Integer>> o = getEnchants(e.getPlayer());
				for(ItemStack q : o.keySet())
					for(CustomEnchant E : o.get(q).keySet())
						if(a.split("=")[1].contains(E.getName()))
							a = a.replace(E.getName(), Integer.toString((int) oldevaluate(E.getEnchantProcValue().replace("level", Integer.toString(o.get(q).get(E))))));
				final int chance = (int) oldevaluate(a.split("=")[1].replaceAll("\\p{L}", "0"));
				final boolean didproc = random.nextInt(100) <= chance;
				if(!didproc) { e.setProced(false); return; }
				e.setProced(true);
			} else if(!a.equals(attribute.split(";")[0]) && !a.toLowerCase().startsWith("chance=") && (!attribute.toLowerCase().contains("chance=") || e.didProc())) {
				if(!attribute.toLowerCase().contains("chance=")) e.setProced(true);
				executeAttribute(e, e.getEvent(), enchant, a, attribute, b, P);
				if(a.toLowerCase().startsWith("wait{")) return;
			}
		}
	}
	
	public void executeAttribute(CustomEnchantProcEvent ev, Event event, CustomEnchant enchant, String a, String attribute, int b, Player P) {
		if(event != null && a.toLowerCase().startsWith("cancel")) {
			if(event instanceof Cancellable) {
				((Cancellable) event).setCancelled(true);
			}
		} else {
			w(ev, event, enchant, getRecipients(event, a.contains("[") ? a.split("\\[")[1].split("]")[0] : a, P), a, attribute, b, P);
		}
	}
	public void w(CustomEnchantProcEvent ev, Event event, CustomEnchant enchant, List<LivingEntity> recipients, String a, String attribute, int b, Player P) {
	    try {
	        executeAttributes(ev, event, enchant, recipients, a, attribute, b, P);
        } catch (Exception e) {
	        System.out.print(" ");
	        System.out.print("[RandomPackage] Custom Enchant Exception caught. Below is the info that caused the error.");
	        System.out.print("[RandomPackage] Version: " + randompackage.getDescription().getVersion() + ". User: %%__USER__%%");
            System.out.print("[RandomPackage] CustomEnchantProcEvent = " + ev);
            System.out.print("[RandomPackage] Event = " + event);
	        System.out.print("[RandomPackage] Custom Enchant = " + (enchant != null ? enchant.getName() : "null"));
            System.out.print("[RandomPackage] recipients = " + recipients);
            System.out.print("[RandomPackage] a = " + a);
            System.out.print("[RandomPackage] attribute = " + attribute);
            System.out.print("[RandomPackage] b = " + b);
            System.out.print("[RandomPackage] P = " + P);
            System.out.print(" ");
            e.printStackTrace();
        }
	}

	private void executeAttributes(CustomEnchantProcEvent ev, Event event, CustomEnchant enchant, List<LivingEntity> recipients, String a, String attribute, int b, Player P) {
        if(ev != null && !ev.didProc()) return;
        final boolean isPVAny = event instanceof PvAnyEvent;
        final HashMap<String, LivingEntity> recipientss = getRecipients(event, P);
        for(String s : recipientss.keySet()) {
            if(a.contains(s + "X"))
                a = a.replace(s + "X", Integer.toString(recipientss.get(s).getLocation().getBlockX()));
            if(a.contains(s + "Y"))
                a = a.replace(s + "Y", Integer.toString(recipientss.get(s).getLocation().getBlockY()));
            if(a.contains(s + "Z"))
                a = a.replace(s + "Z", Integer.toString(recipientss.get(s).getLocation().getBlockZ()));
        }
        for(int i = 1; i <= 6; i++) {
            if(a.contains("maxHealthOf(") || a.contains("healthOf(")) {
                String value = a.contains("maxHealthOf(") ? "maxHealthOf" : "healthOf";
                value = value + "(" + a.split(value + "\\(")[1].split("\\)")[0] + ")";
                if(isPVAny) {
                    final PvAnyEvent e = (PvAnyEvent) event;
                    final LivingEntity damager = e.getDamager(), victim = e.getVictim();
                    if(value.contains("DAMAGER"))
                        a = a.replace(value, BigDecimal.valueOf(value.startsWith("max") ? damager.getMaxHealth() : damager.getHealth()).toPlainString());
                    if(value.contains("VICTIM"))
                        a = a.replace(value, victim != null ? BigDecimal.valueOf(value.startsWith("max") ? victim.getMaxHealth() : victim.getHealth()).toPlainString() : "0");
                }
            }
            if(a.contains("getXP(")) {
                String value = a.contains("getXP(") ? "getXP" : "";
                if(!value.equals("")) {
                    value = value + "(" + a.split(value + "\\(")[1].split("\\)")[0] + ")";
                    if(isPVAny) {
                        final PvAnyEvent E = (PvAnyEvent) event;
                        if(value.contains("DAMAGER"))
                            a = a.replace("DAMAGER", Integer.toString(getXP(E.getDamager())));
                        if(value.contains("VICTIM"))
                            a = a.replace("VICTIM", E != null && E.getVictim() instanceof Player ? Integer.toString(getXP(E.getVictim())) : "0");
                    }
                }
            }
        }
        if(isPVAny) {
            a = a.replace("dmg", Double.toString(((PvAnyEvent) event).getDamage()));
        } else if(event instanceof CustomBossDamageByEntityEvent) {
            a = a.replace("dmg", Double.toString(((CustomBossDamageByEntityEvent) event).getDamage()));
        } else if(event instanceof isDamagedEvent) {
            a = a.replace("dmg", Double.toString(((isDamagedEvent) event).getDamage()));
        } else if(event instanceof EntityDamageByEntityEvent) {
            a = a.replace("dmg", Double.toString(((EntityDamageByEntityEvent) event).getDamage()));
        }

        if(a.contains("random{")) {
            final String e = a.split("random\\{")[1].split("}")[0];
            final int min = (int) oldevaluate(e.split(":")[0]), max = (int) oldevaluate(e.split(":")[1].split("}")[0]);
            int r = min + random.nextInt(max - min + 1);
            a = a.replace("random{" + e + "}", Integer.toString(r));
        }
        if(a.contains("combo{")) {
            final String e = a.split("combo\\{")[1].split("}")[0], o = e.split(":")[0];
            final Player p = ev.getPlayer();
            final double combo = combos.keySet().contains(p) && combos.get(p).keySet().contains(o) ? combos.get(p).get(o) : 1.00;
            a = a.replace("combo{" + e + "}", Double.toString(combo));
        }
        if(a.contains("direction")) {
            final String type = "direction" + (a.contains("directionXOf") ? "X" : a.contains("directionYOf") ? "Y" : a.contains("directionZOf") ? "Z" : "") + "Of", r = a.split(type + "\\{")[1].split("}")[0];
            final LivingEntity recip = getRecipient(event, r);
            if(recip != null) {
                final Vector direc = recip.getLocation().getDirection();
                a = a.replace(type + "{" + r + "}", Double.toString(type.contains("X") ? direc.getX() : type.contains("Y") ? direc.getY() : type.contains("Z") ? direc.getZ() : 0.00));
            } else {
                Bukkit.broadcastMessage("[RandomPackage] recipient == null. Event=" + event.getEventName());
                return;
            }
        }
        if(a.contains("nearby{") || a.contains("nearbyAllies{") || a.contains("nearbyEnemies{")) {
            final boolean allies = a.contains("nearbyAllies{"), enemies = a.contains("nearbyEnemies{");
            final String e = a.split("nearby" + (allies ? "Allies" : enemies ? "Enemies" : "") + "\\{")[1].split("}")[0];
            final List<LivingEntity> r = new ArrayList<>();
            final LivingEntity who = event instanceof PluginEnableEvent ? P : getRecipient(event, e.split(":")[1]), k = getRecipient(event, e.split(":")[0]);
            if(who != null) {
                for(Entity en : who.getNearbyEntities(oldevaluate(e.split(":")[2]), oldevaluate(e.split(":")[3]), oldevaluate(e.split(":")[4]))) {
                    if(en instanceof LivingEntity && en instanceof Damageable && (k == null || k != null && !en.equals(k)))
                        if(!(en instanceof Player)
                                || who instanceof Player && en instanceof Player && (enemies && fapi.relationIsEnemyOrNull((Player) who, (Player) en) || allies && fapi.relationIsAlly((Player) who, (Player) en)))
                            r.add((LivingEntity) en);
                }
            }
            a = a.replace("nearby" + (allies ? "Allies" : enemies ? "Enemies" : "") + "{" + e + "}", r.toString().replace("\\p{Z}", ""));
            recipients = r;
        }
        if(a.contains("nearbySize{") || a.contains("nearbyAlliesSize{") || a.contains("nearbyEnemiesSize{")) {
            int size = 0;
            final boolean allies = a.contains("nearbyAlliesSize{"), enemies = a.contains("nearbyEnemiesSize{");
            final String e = a.split("nearby" + (allies ? "Allies" : enemies ? "Enemies" : "") + "Size\\{")[1].split("}")[0];
            final LivingEntity who = event instanceof PluginEnableEvent ? P : getRecipient(event, e.split(":")[1]), k = getRecipient(event, e.split(":")[0]);
            for(Entity en : who.getNearbyEntities(oldevaluate(e.split(":")[2]), oldevaluate(e.split(":")[3]), oldevaluate(e.split(":")[4]))) {
                if(en instanceof LivingEntity && en instanceof Damageable && (k == null || k != null && !en.equals(k)))
                    if(!allies && !(en instanceof Player)
                            || who instanceof Player && en instanceof Player && (enemies && fapi.relationIsEnemyOrNull((Player) who, (Player) en) || allies && fapi.relationIsAlly((Player) who, (Player) en)))
                        size += 1;
            }
            a = a.replace("nearby" + (allies ? "Allies" : enemies ? "Enemies" : "") + "Size{" + e + "}", Integer.toString(size));
        }

        if(a.toLowerCase().startsWith("addpotioneffect{")) {
            final PotionEffectType type = getPotionEffectType((a.contains("]") ? a.split("]")[1] : a.split("\\{")[1]).split(":")[0].toUpperCase());
            final PotionEffect pe = new PotionEffect(type, (int) oldevaluate(a.split(":")[2].split("}")[0]), (int) oldevaluate(a.split(":")[1]));
            for(LivingEntity l : recipients)
                addPotionEffect(event, ev != null ? ev.getPlayer() : null, l, pe, a.contains(":true") ? config.getStringList("messages.apply-potion-effect") : null, enchant, ev != null ? ev.getLevel() : -1);
        } else if(a.toLowerCase().startsWith("removepotioneffect{")) {
            final PotionEffectType type = getPotionEffectType((a.contains("]") ? a.split("]")[1] : a.split("\\{")[1]).split(":")[0].toUpperCase());
            for(LivingEntity l : recipients)
                removePotionEffect(l, getPotionEffect(l, type), a.contains(":true") ? config.getStringList("messages.remove-potion-effect") : null, enchant, ev != null ? ev.getLevel() : -1);
        } else if(a.toLowerCase().startsWith("sendmessage{")) {
            for(LivingEntity l : recipients)
                sendMessage(enchant, l, a.split("]")[1]);
        } else if(a.toLowerCase().startsWith("wait{")) {
            wait(ev, event, enchant, attribute, b, (int) oldevaluate(a.split("\\{")[1].split("}")[0]), P);
            return;
        } else if(a.toLowerCase().startsWith("damage{")) {
            a = a.toLowerCase();
            for(LivingEntity l : recipients)
                damage(P, l, oldevaluate(a.split("]")[1].split("}")[0].replace("h", "")), a.contains("h"));
        } else if(a.toLowerCase().startsWith("ignite{")) {
            a = a.toLowerCase();
            final int time = (int) oldevaluate(a.split("]")[1].split("}")[0]);
            if(a.split("\\[")[1].split("]")[0].contains("arrow")) {
                if(event instanceof EntityShootBowEvent)
                    (((EntityShootBowEvent) event).getProjectile()).setFireTicks(time);
            }
            for(LivingEntity l : recipients)
                if(l != null)
                    l.setFireTicks(time);
        } else if(a.toLowerCase().startsWith("heal{")) {
            a = a.toLowerCase();
            for(LivingEntity l : recipients)
                heal(l, oldevaluate(a.split("]")[1].split("}")[0].replace("h", "")));
        } else if(a.toLowerCase().startsWith("setdamage{")) {
            a = a.toLowerCase();
            final double dmg = oldevaluate(a.split("\\{")[1].split("}")[0]);
            if(isPVAny) {
                ((PvAnyEvent) event).setDamage(dmg);
            } else if(event instanceof EntityDamageEvent) {
                ((EntityDamageEvent) event).setDamage(dmg);
            }
        } else if(a.toLowerCase().startsWith("setdurability{")) {
            a = a.toLowerCase();
            for(LivingEntity l : recipients) {
                ItemStack p = null;
                double difference = -1;
                if(a.contains("mostdamaged")) {
                    for(ItemStack A : l.getEquipment().getArmorContents())
                        if(A != null && A.getType() != Material.AIR) {
                            double newdif = Double.parseDouble(Short.toString(A.getDurability())) / Double.parseDouble(Short.toString(A.getType().getMaxDurability()));
                            if(difference == -1 || newdif > difference) {
                                difference = newdif;
                                p = A;
                            }
                        }
                } else if(a.contains("helmet") && l.getEquipment().getHelmet() != null) p = l.getEquipment().getHelmet();
                else if(a.contains("chestplate") && l.getEquipment().getChestplate() != null) p = l.getEquipment().getChestplate();
                else if(a.contains("leggings") && l.getEquipment().getLeggings() != null) p = l.getEquipment().getLeggings();
                else if(a.contains("boots") && l.getEquipment().getBoots() != null) p = l.getEquipment().getBoots();
                else if(a.contains("all")) {
                    for(ItemStack q : l.getEquipment().getArmorContents())
                        q.setDurability((short) (oldevaluate(a.split(":")[1].split("}")[0].replace("durability", Short.toString(q.getDurability())))));
                } else if(a.contains("iteminhand")) p = getItemInHand(l);
                else if(a.contains("item")) p = ev.getItem();
                else return;
                if(p != null) {
                    a = a.replace("durability", Short.toString(p.getDurability()));
                    final double dura = oldevaluate(a.split(":")[1].split("}")[0]);
                    p.setDurability((short) (dura < 0 ? 0 : dura));
                }
            }
        } else if(a.toLowerCase().startsWith("setdroppedexp{")) {
            a = a.toLowerCase();
            int dropped = event instanceof BlockBreakEvent ? ((BlockBreakEvent) event).getExpToDrop() : event instanceof EntityDeathEvent ? ((EntityDeathEvent) event).getDroppedExp() : 0;
            final int xp = (int) oldevaluate(a.split("\\{")[1].split("}")[0].replace("droppedxp", Integer.toString(dropped)));
            if(event instanceof BlockBreakEvent)        ((BlockBreakEvent) event).setExpToDrop(xp);
            else if(event instanceof EntityDeathEvent)  ((EntityDeathEvent) event).setDroppedExp(xp);
        } else if(a.toLowerCase().startsWith("refillair{")) {
            a = a.toLowerCase();
            for(LivingEntity l : recipients)
                refillAir(l, Integer.parseInt(a.split("]")[1].split("}")[0]));
        } else if(event instanceof PlayerInteractEvent && a.toLowerCase().startsWith("breakhitblock")) {
            breakHitBlock((PlayerInteractEvent) event);
        } else if(a.toLowerCase().startsWith("sethealth{")) {
            for(LivingEntity l : recipients)
                setHealth(l, (int) oldevaluate(a.split("]")[1].split("}")[0]));
        } else if(a.toLowerCase().startsWith("smite{")) {
            final int amount = (int) oldevaluate(a.split("]")[1].split("}")[0]);
            if(a.toLowerCase().split("\\[")[1].split("]")[0].contains("arrowloc")) {
                if(event instanceof ProjectileHitEvent)
                    smite(((ProjectileHitEvent) event).getEntity().getLocation(), amount);
            }
            for(LivingEntity l : recipients)
                smite(l.getLocation(), amount);
        } else if(a.toLowerCase().startsWith("givedrops{")) {
            if(event instanceof BlockBreakEvent) {
                final BlockBreakEvent bb = (BlockBreakEvent) event;
                final Collection<ItemStack> drops = bb.getBlock().getDrops();
                for(LivingEntity l : recipients) {
                    if(l instanceof Player) {
                        for(ItemStack i : drops) {
                            giveItem((Player) l, i);
                        }
                    }
                }
                bb.setCancelled(true);
                bb.getBlock().setType(Material.AIR);
            }
        } else if(a.toLowerCase().startsWith("setxp{")) {
            final int value = (int) oldevaluate(a.toLowerCase().split("setxp\\{")[1].split("}")[0]);
            for(LivingEntity l : recipients)
                setXP(l, value);
        } else if(a.toLowerCase().startsWith("setvelocity{")) {
            String U = a.toLowerCase().split("]")[1];
            if(isPVAny) {
                final PvAnyEvent E = (PvAnyEvent) event;
                final Player d = E.getDamager();
                final Vector dv = d.getVelocity();
                final LivingEntity victim = E.getVictim();
                U = U.replace("velocityxof(damager)", Double.toString(dv.getX())).replace("velocityyof(damager)", Double.toString(dv.getY())).replace("velocityzof(damager)", Double.toString(dv.getZ()));
                U = U.replace("velocityof(damager)", dv.getX() + ":" + dv.getY() + ":" + dv.getZ());
                if(victim != null) {
                    final Vector v = victim.getVelocity();
                    U = U.replace("velocityxof(victim)", Double.toString(v.getX())).replace("velocityyof(victim)", Double.toString(v.getY())).replace("velocityzof(victim)", Double.toString(v.getZ()));
                    U = U.replace("velocityof(victim)", v.getX() + ":" + v.getY() + ":" + v.getZ());
                }
            }
            final String[] u = U.split(":");
            final Vector knockback = new Vector(oldevaluate(u[0]), oldevaluate(u[1]), oldevaluate(u[2].split("}")[0]));
            for(LivingEntity l : recipients)
                setVelocity(l, knockback);
        } else if(a.toLowerCase().startsWith("healhunger{")) {
            final int h = (int) oldevaluate(a.split("]")[1].split("}")[0]);
            for(LivingEntity l : recipients)
                healHunger(l, h);
        } else if(a.toLowerCase().startsWith("freeze{")) {
            final int time = (int) oldevaluate(a.split("]")[1].split("}")[0]);
            for(LivingEntity l : recipients)
                freeze(l, time);
        } else if(a.toLowerCase().startsWith("procenchants{")) {
            for(LivingEntity l : recipients)
                if(l instanceof Player) {
                    HashMap<ItemStack, HashMap<CustomEnchant, Integer>> enchants = getEnchants((Player) l);
                    for(ItemStack is : enchants.keySet())
                        for(CustomEnchant ce : enchants.get(is).keySet())
                            if(!ce.getAttributes().toString().toLowerCase().contains("procenchants{"))
                                procEnchant(event, ce, enchants.get(is).get(ce), is, P);
                }
        } else if(a.startsWith("dropItem{")) {
            String y = a.split("dropItem\\{")[1].split("]")[1];
            if(event instanceof PlayerInteractEvent) y = y.replace("player", ((PlayerInteractEvent) event).getPlayer().getName());
            for(LivingEntity l : recipients)
                dropItem(l, d(null, y, 0));
        } else if(a.toLowerCase().startsWith("setgainedxp{") && mcmmoIsEnabled) {
            a = a.toLowerCase();
            if(event instanceof com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent) {
                final com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent M = (com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent) event;
                final int xp = (int) oldevaluate(a.split("setgainedxp\\{")[1].split("}")[0].replace("xp", Integer.toString(M.getXpGained())));
                M.setRawXpGained(xp);
                M.setXpGained(xp);
            }
        } else if(a.toLowerCase().startsWith("playsound{")) {
            a = a.toLowerCase().split("]")[1];
            final String sound = a.split(":")[0];
            final int pitch = Integer.parseInt(a.split(":")[1]), volume = Integer.parseInt(a.split(":")[2]), playtimes = Integer.parseInt(a.split(":")[3]);
            final boolean globalsound = a.toLowerCase().endsWith(":true");
            for(LivingEntity l : recipients)
                playSound(sound, pitch, volume, l, playtimes, globalsound);
        } else if(a.toLowerCase().startsWith("spawnentity{")) {
            final String s = a.split("]")[1].split("}")[0];
            final CustomEnchantEntity e = CustomEnchantEntity.valueOf(s.split(":")[0]);
            if(e != null)
                for(LivingEntity l : recipients)
                    e.spawn(l, getRecipient(event, s.split(":")[2]), event);
        } else if(a.toLowerCase().startsWith("stopenchant{")) {
            final String J = a.split("]")[1].split("}")[0];
            final int seconds = Integer.parseInt(J.split(":")[1]);
            for(LivingEntity l : recipients) {
                if(l instanceof Player) {
                    final Player p = (Player) l;
                    if(J.toLowerCase().startsWith("all")) {
                        stoppedAllEnchants.add(p);
                        scheduler.scheduleSyncDelayedTask(randompackage, () -> stoppedAllEnchants.remove(p), 20*seconds);
                    } else {
                        final CustomEnchant ce = CustomEnchant.valueOf(J.split(":")[0]);
                        if(ce != null) {
                            if(!stoppedEnchants.keySet().contains(p)) stoppedEnchants.put(p, new HashMap<>());
                            if(stoppedEnchants.get(p).keySet().contains(ce)) {
                                scheduler.cancelTask(stoppedEnchants.get(p).get(ce));
                            }
                            int task = scheduler.scheduleSyncDelayedTask(randompackage, () -> stoppedEnchants.get(p).remove(ce), 20*seconds);
                            stoppedEnchants.get(p).put(ce, task);
                        }
                    }
                }
            }
        } else if(a.toLowerCase().startsWith("breakblocks{")) {
            a = a.toLowerCase();
            final int x1 = (int) oldevaluate(a.split("]")[1].split(":")[0]), y1 = (int) oldevaluate(a.split("]")[1].split(":")[1]), z1 = (int) oldevaluate(a.split("]")[1].split(":")[2]),
                    x2 = (int) oldevaluate(a.split("]")[1].split(":")[3]), y2 = (int) oldevaluate(a.split("]")[1].split(":")[4]), z2 = (int) oldevaluate(a.split("]")[1].split(":")[5].split("}")[0]);
            for(LivingEntity le : recipients)
                if(event instanceof BlockBreakEvent)
                    breakBlocks(getItemInHand(le).getType(), ((BlockBreakEvent) event).getBlock(), x1, y1, z1, x2, y2, z2);
        } else if(a.toLowerCase().startsWith("remove{")) {
            for(LivingEntity l : recipients)
                if(!(l instanceof Player))
                    l.remove();
        } else if(a.toLowerCase().startsWith("replaceblock{")) {
            final String args = a.toLowerCase().split("\\{")[1].split("}")[0];
            final World w = ev.getPlayer().getWorld();
            final int x = (int) oldevaluate(args.split(":")[0]), y = (int) oldevaluate(args.split(":")[1]), z = (int) oldevaluate(args.split(":")[2]);
            final Location l = new Location(w, x, y, z);
            final Material type = Material.valueOf(args.split(":")[3].toUpperCase());
            final Byte data = Byte.parseByte(args.split(":")[4]);
            final int ticks = Integer.parseInt(args.split(":")[5]);
            setTemporaryBlock(l, type, data, ticks);
        } else if(a.toLowerCase().startsWith("stealxp{")) {
            final String arg = a.split("\\{")[1].split("}")[0];
            final LivingEntity receiver = getRecipient(event, arg.split(":")[0]), target = getRecipient(event, arg.split(":")[1]);
            final int amount = Integer.parseInt(arg.split(":")[2]);
            if(receiver != null && receiver instanceof Player && target != null && target instanceof Player) {

            }
        } else if(a.toLowerCase().startsWith("depleteraritygem{")) {
            final Player player = ev.getPlayer();
            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            if (!pdata.hasActiveRarityGem(RarityGem.valueOf(a.split("\\{")[1].split(":")[0]))) {
                ev.setProced(false);
                return;
            }
            final RarityGem gem = RarityGem.valueOf(a.split("\\{")[1].split(":")[0]);
            final ItemStack g = getRarityGem(gem, player);
            if (g != null) {
                itemMeta = g.getItemMeta();
                final int amount = getRemainingInt(itemMeta.getDisplayName());
                final String fn = fapi.getFaction(player);
                int depleteAmount = Integer.parseInt(a.split(":")[1].split("}")[0]);
                double f = fapi.getDecreaseRarityGemPercent(fn, gem);
                depleteAmount -= depleteAmount * f;
                if (amount - depleteAmount <= 0) {
                    depleteAmount = amount;
                    pdata.toggleRarityGem(ev, gem);
                }
                itemMeta = g.getItemMeta();
                itemMeta.setDisplayName(gem.getPhysicalItem().getItemMeta().getDisplayName().replace("{SOULS}", Integer.toString(amount - depleteAmount)));
                g.setItemMeta(itemMeta);
                ev.getPlayer().updateInventory();
            }
        } else if(a.toLowerCase().startsWith("depletestacksize{") && event instanceof MobStackDepleteEvent) {
            final int amount = Integer.parseInt(a.split("\\{")[1].split("}")[0]);
            ((MobStackDepleteEvent) event).setAmount(amount);
        } else if(a.toLowerCase().startsWith("createcombo{")) {
            final String path = a.split("\\{")[1].split("}")[0];
            final CustomEnchant n = CustomEnchant.valueOf(path.split(":")[0]);
            final Player p = ev.getPlayer();
            if(!combos.keySet().contains(p)) combos.put(p, new HashMap<>());
            if(!combos.get(p).keySet().contains(n))
                combos.get(p).put(n, Double.parseDouble(path.split(":")[1]));
        } else if(a.toLowerCase().startsWith("addcombo{")) {
            final String path = a.split("\\{")[1].split("}")[0];
            final CustomEnchant n = CustomEnchant.valueOf(path.split(":")[0]);
            final Player p = ev.getPlayer();
            if(!combos.keySet().contains(p)) combos.put(p, new HashMap<>());
            combos.get(p).put(n, (combos.get(p).keySet().contains(n) ? combos.get(p).get(n) : 0.00)+Double.parseDouble(path.split(":")[1]));
        } else if(a.toLowerCase().startsWith("depletecombo{")) {
            final String path = a.split("\\{")[1].split("}")[0];
            final CustomEnchant n = CustomEnchant.valueOf(path.split(":")[0]);
            final Player p = ev.getPlayer();
            if(combos.keySet().contains(p) && combos.get(p).keySet().contains(n)) {
                final double d = combos.get(p).get(n), l = Double.parseDouble(path.split(":")[1]);
                combos.get(p).put(n, d-l < 0.00 ? 0.00 : round(d-l, 2));
            }
        } else if(a.toLowerCase().startsWith("stopcombo{")) {
            final CustomEnchant n = CustomEnchant.valueOf(a.split("\\{")[1].split("}")[0]);
            final Player p = ev.getPlayer();
            if(combos.keySet().contains(p) && combos.get(p).keySet().contains(n)) {
                combos.get(p).remove(n);
            }
        } else if(a.toLowerCase().startsWith("explode{")) {
            final Location l = getRecipientLoc(event, a.split("\\[")[1].split("]")[0]);
            if(l != null)
                explode(l, Float.parseFloat(a.split("]")[1].split(":")[0]), Boolean.parseBoolean(a.split(":")[1]), Boolean.parseBoolean(a.split(":")[2].split("}")[0]));
        } else if(a.toLowerCase().startsWith("if{")) {
            doIf(ev, event, enchant, ev.getLevel(), a, attribute, b, a.split("\\{")[1], P);
        }
    }


	public ItemStack getRarityGem(RarityGem gem, Player player) {
		for(int i = 0; i < player.getInventory().getSize(); i++) {
			final ItemStack a = player.getInventory().getItem(i);
			if(a != null && a.hasItemMeta() && a.getItemMeta().hasLore() && a.getItemMeta().getLore().equals(gem.getPhysicalItem().getItemMeta().getLore())) {
				return a;
			}
		}
		return null;
	}
	
	private void doIf(CustomEnchantProcEvent ev, Event event, CustomEnchant enchant, int level, String a, String attribute, int b, String input, Player P) {
		final ArrayList<Boolean> ifs = new ArrayList<>();
		for(LivingEntity l : getRecipients(event, input, P)) {
			for(String s : (a.split("->")[0].contains("]") ? a.split("->")[0].split("]")[1] : a.split("->")[0].split("\\{")[1]).split("&&"))
				ifs.add(doVariable(ev, event, enchant, l, s));
		}
		if(!ifs.contains(false)) {
			for(String q : (a.split("->")[1].contains("-<") ? a.split("->")[1].split("-<")[0] : a.split("->")[1]).split("&&")) executeAttribute(ev, event, enchant, q, attribute, b, P);
		} else if(a.contains("-<")) {
			for(String q : a.split("-<")[1].split("&&")) executeAttribute(ev, event, enchant, q, attribute, b, P);
		}
	}
	private boolean doVariable(CustomEnchantProcEvent e, Event event, CustomEnchant enchant, LivingEntity entity, String input) {
		if(input.startsWith("isHolding("))      return isHolding(entity, input.split("\\(")[1].split("\\)")[0]);
		else if(input.startsWith("isBlocking")) return isBlocking(entity);
		else if(input.startsWith("healthIs<=:")) return healthIsLessThanOrEqualTo(entity, oldevaluate(input.split(":")[1].split(":")[0]));
		else if(input.startsWith("healthIs>=:")) return healthIsGreaterThanOrEqualTo(entity, oldevaluate(input.split(":")[1].split(":")[0]));
		else if(input.startsWith("isUnderwater")) return entity.getRemainingAir() < entity.getMaximumAir();
		else if(input.startsWith("hitBlock(")) return event instanceof PlayerInteractEvent && hitBlock((PlayerInteractEvent) event, input.split("hitBlock\\(")[1].split("\\)")[0].toUpperCase());
		else if(input.startsWith("canBreakHitBlock")) return event instanceof PlayerInteractEvent && ((PlayerInteractEvent) event).getClickedBlock() != null && fapi.canBreakBlock(((PlayerInteractEvent) event).getPlayer(), ((PlayerInteractEvent) event).getClickedBlock().getLocation());
		else if(input.startsWith("isHeadshot")) {
			final PvAnyEvent eve = event instanceof PvAnyEvent ? (PvAnyEvent) event : null;
			final Projectile p = eve != null ? eve.getProjectile() : null;
			return eve != null && p instanceof Arrow && p.getLocation().getY() > eve.getVictim().getEyeLocation().getY();
		} else if(input.startsWith("isSneaking")) return entity instanceof Player && ((Player) entity).isSneaking();
		else if(input.startsWith("didproc")) return e.didProc();
		else if(input.startsWith("didntproc")) return !e.didProc();
		else if(input.toLowerCase().startsWith("fromspawner")) return spawnedFromSpawner.contains(entity.getUniqueId());
		else if(input.startsWith("enchantIs(")) {
			if(enchant != null) {
				final String inpu = input.split("enchantIs\\(")[1].split("\\\\")[0];
				for(String s : inpu.split("\\|\\|")) if(s.equals(enchant.getName())) return true;
			}
			return false;
		} else if(input.equals("hitCEEntity")) {
			if(event instanceof PvAnyEvent) {
				final LivingEntity victim = ((PvAnyEvent) event).getVictim();
				return victim != null && CustomEnchantEntity.valueOf(victim.getUniqueId()) != null;
			}
		} else if(input.toLowerCase().startsWith("eval{") && event instanceof CEAApplyPotionEffectEvent) {
			final CEAApplyPotionEffectEvent A = (CEAApplyPotionEffectEvent) event;
			final int enchantlevel = A.getEnchantLevel(), potionlevel = A.getPotionEffect().getAmplifier() + 1;
			String f = input.toLowerCase().split("eval\\{")[1].split("}")[0].replace("level", Integer.toString(enchantlevel)).replace("potionlevel", Integer.toString(potionlevel)),
					s = input.toLowerCase().split("eval\\{")[2].split("}")[0].replace("level", Integer.toString(enchantlevel)).replace("potionlevel", Integer.toString(potionlevel));
			
		} else if(input.toLowerCase().startsWith("distancebetween(")) {
			final List<LivingEntity> recipients = getRecipients(event, input.split("distanceBetween\\(")[1].split("\\)")[0], null);
			if(recipients.size() == 2)
				return input.split("distanceBetween\\(")[1].split("\\)")[1].startsWith("<=") ? distanceBetween(recipients.get(0), recipients.get(1)) <= oldevaluate(input.split("\\)<=")[1]) : distanceBetween(recipients.get(0), recipients.get(1)) >= oldevaluate(input.split("\\)>=")[1]);
			return false;
		} else if(input.toLowerCase().startsWith("isfacing(")) {
			final String facing = facing(entity).toLowerCase();
			for(String s : input.toLowerCase().split("isfacing\\(")[1].split("\\)")[0].split("\\|\\|")) {
				if(facing.toLowerCase().startsWith(s.toLowerCase())) return true;
			}
			return false;
		}
		return false;
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	private void entityTargetLivingEntityEvent(EntityTargetLivingEntityEvent event) {
		if(!event.isCancelled() && event.getEntity() instanceof LivingEntity && event.getTarget() instanceof Player) {
			final RPPlayerData pdata = RPPlayerData.get(event.getTarget().getUniqueId());
			final CustomEnchantEntity entity = CustomEnchantEntity.valueOf(event.getEntity().getUniqueId());
			if(entity != null && !entity.canTargetSummoner() && pdata.customEnchantEntities.contains(event.getEntity().getUniqueId())) event.setCancelled(true);
		}
	}
	public void addPotionEffect(Event event, Player player, LivingEntity entity, PotionEffect potioneffect, List<String> message, CustomEnchant enchant, int level) {
	    if(entity != null) {
            final CEAApplyPotionEffectEvent e = new CEAApplyPotionEffectEvent(event, player, entity, enchant, level, potioneffect);
            pluginmanager.callEvent(e);
            if(!e.isCancelled()) {
                entity.addPotionEffect(potioneffect);
                dopotioneffect(entity, potioneffect, message, enchant, level);
            } else if(e != null && entity instanceof Player && e.isCancelled()) {
                procPlayerArmor(e, (Player) entity);
                procPlayerItem(e, (Player) entity, null);
            }
        }
	}
	private void removePotionEffect(LivingEntity entity, PotionEffect potioneffect, List<String> message, CustomEnchant enchant, int level) {
		if(potioneffect != null && entity != null && entity.hasPotionEffect(potioneffect.getType())) {
			entity.removePotionEffect(potioneffect.getType());
			dopotioneffect(entity, potioneffect, message, enchant, level);
		}
	}
	private void dopotioneffect(LivingEntity entity, PotionEffect potioneffect, List<String> message, CustomEnchant enchant, int level) {
		if(message != null) {
			for(String s : message) {
				if(s.contains("{ENCHANT}")) s = s.replace("{ENCHANT}", enchant.getName() + " " + toRoman(level));
				if(s.contains("{POTION_EFFECT}")) s = s.replace("{POTION_EFFECT}", potioneffect.getType().getName() + " " + toRoman(potioneffect.getAmplifier() + 1));
				entity.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
			}
		}
	}
	private void damage(LivingEntity damager, LivingEntity entity, double damage, boolean heartDmg) {
		if(!heartDmg && entity.getHealth() - damage <= 0.00 || heartDmg && (entity.getHealth() - ((int) damage)) <= 0.00) {
            damage = entity.getHealth();
        }
        entity.damage(heartDmg ? ((int) damage) : damage, damager);
	}
	private void explode(Location l, float power, boolean setFire, boolean breakBlocks) {
		l.getWorld().createExplosion(l.getX(), l.getY(), l.getZ(), power, setFire, breakBlocks);
	}
	private void heal(LivingEntity entity, double by) {
		if(entity.getHealth() != entity.getMaxHealth()) {
			entity.setHealth(entity.getHealth() + by > entity.getMaxHealth() ? entity.getMaxHealth() : entity.getHealth() + by);
		}
	}
	private void healHunger(LivingEntity entity, int by) {
		final Player player = entity instanceof Player ? (Player) entity : null;
		if(player != null && player.getFoodLevel() + by <= 20) {
			player.setFoodLevel(player.getFoodLevel() + by);
		}
	}
	private void setTemporaryBlock(Location l, Material m, byte data, int ticks) {
	    final World w = l.getWorld();
		final Block prev = w.getBlockAt(l);
	    final Material prevm = prev.getType();
	    final byte prevd = prev.getState().getRawData();
	    if(!temporayblocks.keySet().contains(l)) {
            temporayblocks.put(l, new HashMap<>());
            temporayblocks.get(l).put(new ItemStack(prevm, 1, prevd), new HashMap<>());
            w.getBlockAt(l).setType(m);
            w.getBlockAt(l).getState().setRawData(data);
            final Block newb = w.getBlockAt(l);
            temporayblocks.get(l).get(prev).put(newb, ticks);
            scheduler.scheduleSyncDelayedTask(randompackage, () -> {
                w.getBlockAt(l).setType(prevm);
                w.getBlockAt(l).getState().setRawData(prevd);
                temporayblocks.remove(l);
            }, ticks);
        }
    }
	private void setHealth(LivingEntity entity, double newHealth) { if(newHealth < 0.00) entity.setHealth(0.00); else entity.setHealth(newHealth); }
	private boolean isHolding(LivingEntity entity, String input) {
	    final ItemStack i = getItemInHand(entity);
		return i != null && i.getType().name().endsWith(input.toUpperCase());
	}
	private boolean isBlocking(LivingEntity entity) { return entity instanceof Player && ((Player) entity).isBlocking(); }
	private boolean healthIsLessThanOrEqualTo(LivingEntity entity, double target) { return entity.getHealth() <= target; }
	private boolean healthIsGreaterThanOrEqualTo(LivingEntity entity, double target) { return entity.getHealth() >= target; }
	private boolean hitBlock(PlayerInteractEvent event, String block) {
		return event.getClickedBlock() != null && event.getClickedBlock().getType().name().equals(block.toUpperCase());
	}
	private void breakHitBlock(PlayerInteractEvent event) { event.getClickedBlock().breakNaturally(); }
	private void refillAir(LivingEntity entity, int addedAir) {
		entity.setRemainingAir(entity.getRemainingAir() + addedAir > entity.getMaximumAir() ? entity.getMaximumAir() : entity.getRemainingAir() + addedAir);
	}
	private int getXP(LivingEntity entity) { return entity instanceof Player ? getTotalExperience((Player) entity) : 0; }
	private void setXP(LivingEntity entity, int value) {
		if(entity instanceof Player) {
			setTotalExperience((Player) entity, value);
		}
	}
	private void dropItem(LivingEntity entity, ItemStack is) { entity.getWorld().dropItem(entity.getLocation(), is); }
	private void particle(String particle, Location location, boolean global) {
		
	}
	private void breakBlocks(Material usedItem, Block b, int x1, int y1, int z1, int x2, int y2, int z2) {
		if(usedItem != null && b != null) {
			final int B1 = b.getLocation().getBlockX(), B2 = b.getLocation().getBlockY(), B3 = b.getLocation().getBlockZ();
			final int X1 = x1 > x2 ? x2 : x1, X2 = x2 > x1 ? x2 : x1, Y1 = y1 > y2 ? y2 : y1, Y2 = y2 > y1 ? y2 : y1, Z1 = z1 > z2 ? z2 : z1, Z2 = z2 > z1 ? z2 : z1;
			for(int x = B1 + X1; x <= B1 + X2; x++) {
				for(int y = B2 + Y1; y <= B2 + Y2; y++) {
					for(int z = B3 + Z1; z <= B3 + Z2; z++) {
						Block block = b.getWorld().getBlockAt(new Location(b.getWorld(), x, y, z));
						if(canBeBroken(usedItem, block)) block.breakNaturally();
					}
				}
			}
		}
	}
	private boolean canBeBroken(Material usedItem, Block block) {
		for(String s : blockbreakblacklist.get("global")) {
			if(block.getType().name().toLowerCase().endsWith(s.toLowerCase())) return false;
		}
		final String i = usedItem.name().toLowerCase();
		for(String s : blockbreakblacklist.keySet()) {
			final String S = s.toLowerCase();
			if(!s.equals("global") && i.endsWith(S)) {
				for(String q : blockbreakblacklist.get(s)) {
					if(block.getType().name().toLowerCase().endsWith(q.toLowerCase())) return false;
				}
			}
		}
		return true;
	}
	/*
	 *  Code is from "Adrian Sohn" at
	 *  https://stackoverflow.com/questions/35831619/get-the-direction-a-player-is-looking
	 */
	private String facing(Entity entity) {
		float yaw = entity.getLocation().getYaw();
	    if (yaw < 0) yaw += 360;
	    return yaw >= 314 || yaw < 45 ? "SOUTH" : yaw < 135 ? "WEST" : yaw < 225 ? "NORTH" : yaw < 315 ? "EAST" : "NORTH";
	}
	private double distanceBetween(Entity e1, Entity e2) {
		return e1.getLocation().distance(e2.getLocation());
	}
	private void sendMessage(CustomEnchant enchant, LivingEntity entity, String message) {
		if(message.contains("\\n")) for(String s : message.split("\\\\n")) entity.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("}", "").replace("%ENCHANT%", enchant.getName())));
		else                                                               entity.sendMessage(ChatColor.translateAlternateColorCodes('&', message.replace("}", "").replace("%ENCHANT%", enchant.getName())));
	}
	private void smite(Location loc, int times) {
		for(int i = 1; i <= times; i++)
			loc.getWorld().strikeLightning(loc);
	}
	private void setVelocity(LivingEntity entity, Vector vel) {
		entity.setVelocity(vel);
	}
	private void stopEnchant(Player player, String input, int ticks) {
		final HashMap<ItemStack, HashMap<CustomEnchant, Integer>> enchants = getEnchants(player);
		if(input.toLowerCase().equals("all")) {
			
		}
	}
	private void freeze(LivingEntity entity, int ticks) {
		if(entity instanceof Player) {
			final Player player = (Player) entity;
			final float walkspeed = player.getWalkSpeed();
			frozen.add(player);
			player.setWalkSpeed(0);
			scheduler.scheduleSyncDelayedTask(randompackage, new Runnable() {
				public void run() {
					player.setWalkSpeed(walkspeed);
					frozen.remove(player);
				}
			}, ticks);
		}
	}
	private void wait(CustomEnchantProcEvent ev, Event event, CustomEnchant enchant, String attribute, int number, int ticks, Player P) {
		String t = "";
		for(int i = number+1; i < attribute.split(";").length; i++)
			t = t + attribute.split(";")[i] + ";";
		final String w = t;
		scheduler.scheduleSyncDelayedTask(randompackage, new Runnable() {
			public void run() {
				int b = number;
				for(String s : w.split(";")) {
					b += 1;
					executeAttribute(ev, event, enchant, s, attribute, b, P);
				}
			}
		}, ticks);
	}

	public HashMap<String, LivingEntity> getRecipients(Event event, Player p) {
		final HashMap<String, LivingEntity> recipients = new HashMap<>();
		if(event instanceof CustomEnchantEntityDamageByEntityEvent) {
			final CustomEnchantEntityDamageByEntityEvent e = (CustomEnchantEntityDamageByEntityEvent) event;
			recipients.put("OWNER", e.getCustomEnchantEntity().getSummoner());
			if(e.getDamager() instanceof LivingEntity) recipients.put("DAMAGER", (LivingEntity) e.getDamager());
		} else if(event instanceof ArmorSetEquipEvent) {
			recipients.put("PLAYER", ((ArmorSetEquipEvent) event).getPlayer());
		} else if(event instanceof ArmorSetUnequipEvent) {
			recipients.put("PLAYER", ((ArmorSetUnequipEvent) event).getPlayer());
		} else if(event instanceof PlayerArmorEvent) {
			recipients.put("PLAYER", ((PlayerArmorEvent) event).getPlayer());
		} else if(event instanceof PlayerInteractEvent) {
			recipients.put("PLAYER", ((PlayerInteractEvent) event).getPlayer());
		} else if(event instanceof BlockBreakEvent) {
			recipients.put("PLAYER", ((BlockBreakEvent) event).getPlayer());
		} else if(event instanceof PlayerItemDamageEvent) {
			recipients.put("PLAYER", ((PlayerItemDamageEvent) event).getPlayer());
		} else if(event instanceof FoodLevelChangeEvent) {
			recipients.put("PLAYER", ((FoodLevelChangeEvent) event).getEntity());
		} else if(event instanceof EntityDeathEvent) {
			final EntityDeathEvent e = (EntityDeathEvent) event;
			recipients.put("VICTIM", e.getEntity());
			recipients.put("DAMAGER", e.getEntity().getKiller());
		} else if(event instanceof CEAApplyPotionEffectEvent) {
			final CEAApplyPotionEffectEvent e = (CEAApplyPotionEffectEvent) event;
			recipients.put("VICTIM", e.getAppliedTo());
			recipients.put("DAMAGER", e.getPlayer());
		} else if(event instanceof PvAnyEvent) {
			final PvAnyEvent pva = (PvAnyEvent) event;
			recipients.put("DAMAGER", pva.getDamager());
			final LivingEntity v = pva.getVictim();
			if(v != null) {
				if(v instanceof Arrow && ((Arrow) v).getShooter() instanceof Player) recipients.put("SHOOTER", (Player) ((Arrow) v).getShooter());
				recipients.put("VICTIM", v);
			}
		} else if(event instanceof EntityDamageEvent) {
			recipients.put("VICTIM", (LivingEntity) ((EntityDamageEvent) event).getEntity());
		} else if(event instanceof PluginEnableEvent) {
			recipients.put("PLAYER", p);
		}
		return recipients;
	}
	public List<LivingEntity> getRecipients(Event event, String input, Player p) {
		ArrayList<LivingEntity> recipients = new ArrayList<>();
		if(event instanceof CustomEnchantEntityDamageByEntityEvent) {
			final CustomEnchantEntityDamageByEntityEvent e = (CustomEnchantEntityDamageByEntityEvent) event;
			if(input.toLowerCase().contains("owner")) recipients.add(e.getCustomEnchantEntity().getSummoner());
			if(input.toLowerCase().contains("damager") && e.getDamager() instanceof LivingEntity) recipients.add((LivingEntity) e.getDamager());
		} else if(event instanceof ArmorSetEquipEvent) {
			recipients.add(((ArmorSetEquipEvent) event).getPlayer());
		} else if(event instanceof ArmorSetUnequipEvent) {
			recipients.add(((ArmorSetUnequipEvent) event).getPlayer());
		} else if(event instanceof PlayerArmorEvent) {
			recipients.add(((PlayerArmorEvent) event).getPlayer());
		} else if(event instanceof MobStackDepleteEvent) {
			final MobStackDepleteEvent e = (MobStackDepleteEvent) event;
			final Entity k = e.getKiller();
			if(k instanceof LivingEntity)
				recipients.add((LivingEntity) k);
		} else if(event instanceof PlayerInteractEvent) {
			recipients.add(((PlayerInteractEvent) event).getPlayer());
		} else if(event instanceof BlockBreakEvent) {
			recipients.add(((BlockBreakEvent) event).getPlayer());
		} else if(event instanceof PlayerItemDamageEvent) {
			recipients.add(((PlayerItemDamageEvent) event).getPlayer());
		} else if(event instanceof FoodLevelChangeEvent) {
			recipients.add(((FoodLevelChangeEvent) event).getEntity());
		} else if(event instanceof EntityDeathEvent) {
			final EntityDeathEvent e = (EntityDeathEvent) event;
			if(input.toLowerCase().contains("victim")) recipients.add(e.getEntity());
			if(input.toLowerCase().contains("damager")) recipients.add(e.getEntity().getKiller());
		} else if(event instanceof CEAApplyPotionEffectEvent) {
			final CEAApplyPotionEffectEvent e = (CEAApplyPotionEffectEvent) event;
			if(input.toLowerCase().contains("victim")) recipients.add(e.getAppliedTo());
			if(input.toLowerCase().contains("damager")) recipients.add(e.getPlayer());
		} else if(event instanceof ProjectileHitEvent) {
			final ProjectileHitEvent e = (ProjectileHitEvent) event;
			if(input.toLowerCase().contains("shooter") && e.getEntity().getShooter() instanceof LivingEntity) recipients.add((LivingEntity) e.getEntity().getShooter());
			final LivingEntity hit = getHitEntity(e);
			if(input.toLowerCase().contains("victim") && hit != null) recipients.add(hit);
		} else if(event instanceof PvAnyEvent) {
			final PvAnyEvent e = (PvAnyEvent) event;
			if(input.toLowerCase().contains("damager")) recipients.add(e.getDamager());
			if(input.toLowerCase().contains("victim") && e.getVictim() != null) recipients.add(e.getVictim());
			if(input.toLowerCase().contains("shooter") && e.getProjectile() != null && e.getProjectile().getShooter() instanceof LivingEntity) recipients.add((LivingEntity) e.getProjectile().getShooter());
		} else if(event instanceof isDamagedEvent) {
		    final isDamagedEvent is = (isDamagedEvent) event;
		    if(input.toLowerCase().contains("victim")) recipients.add(is.getVictim());
		    if(input.toLowerCase().contains("damager")) recipients.add(is.getDamager());
		} else if(event instanceof EntityDamageEvent && !(event instanceof EntityDamageByEntityEvent)) {
            recipients.add((LivingEntity) ((EntityDamageEvent) event).getEntity());
        } else if(event instanceof MaskEquipEvent || event instanceof MaskUnequipEvent) {
		    final boolean e = event instanceof MaskEquipEvent;
		    recipients.add(e ? ((MaskEquipEvent) event).player : ((MaskUnequipEvent) event).player);
		} else if(event instanceof PluginEnableEvent) {
			recipients.add(p);
		}
		return recipients;
	}
	private Location getRecipientLoc(Event event, String input) {
		if(event instanceof PlayerArmorEvent) {
			return ((PlayerArmorEvent) event).getPlayer().getLocation();
		} else if(event instanceof PlayerInteractEvent) {
			return ((PlayerInteractEvent) event).getPlayer().getLocation();
		} else if(event instanceof ProjectileHitEvent) {
			final ProjectileHitEvent e = (ProjectileHitEvent) event;
			if(input.toLowerCase().contains("arrow") && e.getEntity() instanceof Arrow) return e.getEntity().getLocation();
			if(input.toLowerCase().contains("shooter") && e.getEntity().getShooter() instanceof LivingEntity) return ((LivingEntity) e.getEntity().getShooter()).getLocation();
		} else if(event instanceof PvAnyEvent) {
			final PvAnyEvent e = (PvAnyEvent) event;
			if(input.contains("damager")) return e.getProjectile() != null ? e.getProjectile().getLocation() : e.getDamager().getLocation();
			if(input.contains("victim") && e.getVictim() != null) return e.getVictim().getLocation();
			if(input.contains("shooter") && e.getProjectile() != null) return ((LivingEntity) e.getProjectile().getShooter()).getLocation();
		}
		return null;
	}
	private LivingEntity getRecipient(Event event, String input) {
		if(event instanceof PlayerArmorEvent) {
			return ((PlayerArmorEvent) event).getPlayer();
		} else if(event instanceof PlayerInteractEvent) {
			return ((PlayerInteractEvent) event).getPlayer();
		} else if(event instanceof ProjectileHitEvent) {
			final ProjectileHitEvent e = (ProjectileHitEvent) event;
			if(input.toLowerCase().contains("arrow") && e.getEntity() instanceof Arrow) return (LivingEntity) e.getEntity();
			if(input.toLowerCase().contains("shooter") && e.getEntity().getShooter() != null && e.getEntity().getShooter() instanceof LivingEntity) return (LivingEntity) e.getEntity().getShooter();
		} else if(event instanceof PvAnyEvent) {
			final PvAnyEvent e = (PvAnyEvent) event;
			if(input.contains("damager")) return e.getProjectile() != null ? (LivingEntity) e.getProjectile() : e.getDamager();
			if(input.contains("victim") && e.getVictim() != null) return e.getVictim();
			if(input.contains("shooter") && e.getProjectile() != null) return (LivingEntity) e.getProjectile().getShooter();
		}
		return null;
	}
	public ItemStack applyBlackScroll(ItemStack is, ItemStack blackscroll, BlackScroll bs) {
		item = null;
		final HashMap<CustomEnchant, Integer> enchants = getEnchants(is);
		if(is != null && getEnchants(is).keySet().size() > 0) {
			CustomEnchant enchant = (CustomEnchant) enchants.keySet().toArray()[random.nextInt(enchants.keySet().size())];
			for(int f = 1; f <= 5; f++) {
				if(bs.getAppliesTo().contains(enchant.getRarity())) {
					int successP = -1, enchantlevel = enchants.get(enchant);
					for(String string : blackscroll.getItemMeta().getLore()) if(getRemainingInt(string) != -1) successP = getRemainingInt(string);
					itemMeta = is.getItemMeta(); lore.clear(); lore.addAll(itemMeta.getLore());
					int enchantslot = -1;
					for(int i = 0; i < lore.size(); i++) if(lore.get(i).equals(enchant.getRarity().getApplyColors() + enchant.getName() + " " + toRoman(enchantlevel))) enchantslot = i;
					if(enchantslot == -1) return null;
					lore.remove(enchantslot);
					itemMeta.setLore(lore); lore.clear();
					is.setItemMeta(itemMeta);
					return getRevealedItem(enchant, enchantlevel, successP, 100, true, true).clone();
				} else enchant = (CustomEnchant) enchants.keySet().toArray()[random.nextInt(enchants.keySet().size())];
			}
		}
		return item;
	}
	public void applySoulTracker(Player player, ItemStack is, SoulTracker soultracker) {
		if(is != null) {
			itemMeta = is.getItemMeta(); lore.clear();
			if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
			boolean did = false;
			for(String s : soultracker.getAppliesTo()) {
				if(!did && is.getType().name().toLowerCase().endsWith(s.toLowerCase())) {
					if(!lore.isEmpty()) {
						for(int i = 0; i < lore.size(); i++) {
							final String targetLore = lore.get(i);
							for(SoulTracker st : SoulTracker.trackers) {
								if(!did && targetLore.startsWith(st.getAppliedLore().replace("{SOULS}", ""))) {
									did = true;
									lore.set(i, soultracker.getAppliedLore().replace("{SOULS}", "0"));
								}
							}
						}
						if(!did) {
						    did = true;
						    lore.add(soultracker.getAppliedLore().replace("{SOULS}", "0"));
                        }
					} else {
						lore.add(soultracker.getAppliedLore().replace("{SOULS}", "0"));
						did = true;
					}
				}
			}
			if(did) {
				for(String string : soultracker.getApplyMessage()) {
					if(string.contains("{ITEM}")) string = string.replace("{ITEM}", is.getType().name());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
				}
				itemMeta.setLore(lore); lore.clear();
				is.setItemMeta(itemMeta);
			}
		}
	}
	public boolean applyTransmogScroll(ItemStack is) {
		final String currentitem = is != null ? is.getType().name() : null;
		if(currentitem != null && (currentitem.endsWith("AXE") || currentitem.endsWith("SWORD") || currentitem.endsWith("SPADE") || currentitem.equals("BOW") || currentitem.endsWith("HELMET") || currentitem.endsWith("CHESTPLATE") || currentitem.endsWith("LEGGINGS") || currentitem.endsWith("BOOTS"))) {
			final String TRANSMOG = ChatColor.translateAlternateColorCodes('&', config.getString("items.transmog-scroll.apply"));
			HashMap<CustomEnchant, Integer> enchants = getEnchants(is);
			final int size = enchants.keySet().size(), prevsize = enchants.keySet().size();
			itemMeta = is.getItemMeta(); lore.clear();
			if(itemMeta.hasLore()) {
				for(EnchantRarity r : CustomEnchant.format) {
					for(String s : itemMeta.getLore()) {
						final CustomEnchant enchant = CustomEnchant.valueOf(s);
						if(enchant != null && enchant.getRarity().equals(r)) lore.add(s);
					}
				}
				String soultracker = null;
				for(String s : itemMeta.getLore())
					for(SoulTracker st : SoulTracker.trackers)
						if(s.startsWith(st.getAppliedLore().replace("{SOULS}", "")))
							soultracker = s;
				if(lore.contains(WHITE_SCROLL)) { lore.remove(WHITE_SCROLL); lore.add(WHITE_SCROLL); }
				if(soultracker != null) { lore.remove(soultracker); lore.add(soultracker); }
				for(String s : itemMeta.getLore())
					if(!lore.contains(s)) lore.add(s);
			}
			itemMeta.setLore(lore); lore.clear();
			//
			String name;
			if(itemMeta.hasDisplayName()) {
				name = itemMeta.getDisplayName();
				if(name.contains(TRANSMOG.replace("{LORE_COUNT}", "" + prevsize))) name = name.replace(TRANSMOG.replace("{LORE_COUNT}", "" + prevsize), TRANSMOG.replace("{LORE_COUNT}", "" + size));
			} else name = is.getType().name();
			if(name.equals(is.getType().name())) name = toMaterial(is.getType().name(), false);
			name = name.replace("{ENCHANT_SIZE}", TRANSMOG.replace("{LORE_COUNT}", "" + size));
			if(!name.contains(TRANSMOG.replace("{LORE_COUNT}", Integer.toString(size)))) name = name + " " + TRANSMOG.replace("{LORE_COUNT}", Integer.toString(size));
			ChatColor color = ChatColor.RESET;
			if(itemMeta.hasEnchants()) color = ChatColor.AQUA;
			itemMeta.setDisplayName(color + name);
			is.setItemMeta(itemMeta);
			return true;
		}
		return false;
	}
	private void applyEnchantmentOrb(Player player, ItemStack is, ItemStack enchantmentorb, EnchantmentOrb orb) {
		EnchantmentOrb prevOrb = null;
		final int percent = getRemainingInt(enchantmentorb.getItemMeta().getLore().get(orb.getPercentLoreSlot()));
		item = is; itemMeta = item.getItemMeta(); lore.clear();
		if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
		for(String s : lore) {
			EnchantmentOrb q = EnchantmentOrb.valueOf(s);
			if(q != null) prevOrb = q;
		}
		if(random.nextInt(100) <= percent) {
			if(prevOrb == null) {
				lore.add(orb.getApplyLore());
			} else {
				final String prev = prevOrb.getApplyLore();
				boolean did = false;
				for(int i = 0; i < lore.size(); i++) {
					if(!did && lore.get(i).equals(prev)) {
						did = true;
						lore.set(i, orb.getApplyLore());
					}
				}
				if(!did) return;
			}
			//playSuccess(player);
		} else {
			//playDestroy(player);
			return;
		}
		itemMeta.setLore(lore); lore.clear();
		item.setItemMeta(itemMeta);
		player.updateInventory();
	}
	public void applyWhiteScroll(Player player, ItemStack is) {
		if(is != null) {
			if(is.hasItemMeta() && is.getItemMeta().hasLore() && is.getItemMeta().getLore().contains(WHITE_SCROLL)) return;
			itemMeta = is.getItemMeta(); lore.clear();
			if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
			lore.add(WHITE_SCROLL);
			itemMeta.setLore(lore); lore.clear();
			is.setItemMeta(itemMeta);
			player.updateInventory();
		}
	}
	public void splitsouls(Player player, int amount) {
		item = getItemInHand(player);
		RarityGem g = RarityGem.valueOf(item);
        int collectedsouls = 0, gems = 0;
        List<String> split = null;
		SoulTracker appliedst = null;
		if(g != null) {
            split = g.getSplitMessage();
            collectedsouls = getRemainingInt(item.getItemMeta().getDisplayName());
        } else if(item == null || !item.hasItemMeta() || !item.getItemMeta().hasLore()) {
			sendStringListMessage(player, config.getStringList("messages.need-item-with-soul-tracker"));
		} else {
			itemMeta = item.getItemMeta();
			lore.clear();
			if(itemMeta.hasLore())
				lore.addAll(itemMeta.getLore());
			boolean did = false;
			int applied = -1, totalsouls = -1;
            for(SoulTracker st : SoulTracker.trackers) {
                int i = -1;
                if(!did) {
					for(String s : lore) {
						i += 1;
						final String a = st.getAppliedLore();
						if(s.startsWith(a.replace("{SOULS}", ""))) {
							appliedst = st;
							applied = i;
							split = st.getSplitMessage();
							collectedsouls = getRemainingInt(s);
							totalsouls = collectedsouls;
							if(amount == -1) {
								amount = collectedsouls;
							} else if(collectedsouls <= 0) {
								sendStringListMessage(player, config.getStringList("messages.need-to-collect-souls"));
								return;
							} else {
								collectedsouls = amount;
							}
							if(amount == 0)  {
								sendStringListMessage(player, config.getStringList("messages.need-to-collect-souls"));
								return;
							}
							gems = (int) (collectedsouls * st.getSplitMultiplier());
							did = true;
						}
					}
				}
            }
            if(did) {
				lore.set(applied, appliedst.getAppliedLore().replace("{SOULS}", Integer.toString(totalsouls - amount)));
				itemMeta.setLore(lore); lore.clear();
				item.setItemMeta(itemMeta);
				player.updateInventory();
			}
		}
		if(split != null) {
			if(g == null) {
				g = appliedst.getConvertsTo();
			}
            item = g.getPhysicalItem().clone(); itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(item.getItemMeta().getDisplayName().replace("{SOULS}", ChatColor.translateAlternateColorCodes('&', g.getColors(gems)) + gems));
            if(gems != 0) item.setAmount(1);
            item.setItemMeta(itemMeta);
            giveItem(player, item);
            for(String string : split) {
                if(string.contains("{SOULS}")) string = string.replace("{SOULS}", Integer.toString(collectedsouls));
                if(string.contains("{GEMS}")) string = string.replace("{GEMS}", Integer.toString(gems));
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
            }
            player.updateInventory();
        }
	}
	/*
	 * This code is from "batman" at
	 * https://stackoverflow.com/questions/9073150/converting-roman-numerals-to-decimal
	 */
	private int fromRoman(String num) {
		num = ChatColor.stripColor(num.toUpperCase());
		int intNum = 0, prev = 0;
		for(int i = num.length() - 1; i >= 0; i--) {
			final String character = num.substring(i, i + 1);
			int temp = RomanNumeralValues.valueOf(character).asInt();
			if(temp < prev) intNum -= temp;
			else            intNum += temp;
			prev = temp;
		}
		return intNum;
	}   
}

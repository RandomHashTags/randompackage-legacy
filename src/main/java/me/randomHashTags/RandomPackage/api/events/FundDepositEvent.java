package me.randomHashTags.RandomPackage.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.api.Fund;

public class FundDepositEvent extends Event implements Cancellable {
	private final Fund fund = Fund.getFund();
	private final Player player;
	private final double amount;
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled;
	public FundDepositEvent(Player player, double amount) {
		cancelled = false;
		this.player = player;
		this.amount = amount;
	}
	public Player getPlayer() { return player; }
	public double getAmount() { return amount; }
	public double getServerFunds() { return fund.total; }
	public double getNewServerFunds() { return fund.total + amount; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}

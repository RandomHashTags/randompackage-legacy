package me.randomHashTags.RandomPackage.api.events.customboss;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent;

public class CustomMinionDeathEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private final LivingEntity minion;
    private final EntityDamageEvent damagecause;
    public CustomMinionDeathEvent(LivingEntity minion, EntityDamageEvent damagecause) {
        this.minion = minion;
        this.damagecause = damagecause;
    }
    public LivingEntity getMinion() { return minion; }
    public EntityDamageEvent getLastDamageCause() { return damagecause; }
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}

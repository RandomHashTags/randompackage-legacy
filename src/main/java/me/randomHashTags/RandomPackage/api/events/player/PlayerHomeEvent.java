package me.randomHashTags.RandomPackage.api.events.player;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class PlayerHomeEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled;
	private final boolean delete;
	private final Player player;
	private final String name;
	private final Location loc;
	private final ItemStack icon;
	public PlayerHomeEvent(Player player, String name, Location loc, ItemStack icon, boolean delete) {
		this.player = player;
		this.name = name;
		this.loc = loc;
		this.icon = icon;
		this.delete = delete;
		cancelled = false;
	}
	public Player getPlayer() { return player; }
	public String getHomeName() { return name; }
	public Location getHomeLocation() { return loc; }
	public ItemStack getHomeIcon() { return icon; }
	public boolean isDeleted() { return delete; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
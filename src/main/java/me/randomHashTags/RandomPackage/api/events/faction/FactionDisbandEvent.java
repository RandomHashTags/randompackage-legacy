package me.randomHashTags.RandomPackage.api.events.faction;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class FactionDisbandEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private final Player disbander;
    private final String factionName;
    public FactionDisbandEvent(Player disbander, String factionName) {
        this.disbander = disbander;
        this.factionName = factionName;
    }
    public Player getDisbander() { return disbander; }
    public String getFaction() { return factionName; }

    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}

package me.randomHashTags.RandomPackage.api.events;

import org.bukkit.OfflinePlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.coinflips.CoinFlipChallenge;

public class CoinFlipChallengeEndEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private final CoinFlipChallenge match;
	private final OfflinePlayer loser, winner;
	private final double wonCash;
	public CoinFlipChallengeEndEvent(CoinFlipChallenge match, OfflinePlayer winner, OfflinePlayer loser, double wonCash) {
		this.match = match;
		this.winner = winner;
		this.loser = loser;
		this.wonCash = wonCash;
	}
	public CoinFlipChallenge getMatch() { return match; }
	public OfflinePlayer getWinner() { return winner; }
	public OfflinePlayer getLoser() { return loser; }
	public double getWonCash() { return wonCash; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}

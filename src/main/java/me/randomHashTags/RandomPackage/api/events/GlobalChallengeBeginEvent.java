package me.randomHashTags.RandomPackage.api.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.randomHashTags.RandomPackage.utils.classes.globalchallenges.GlobalChallenge;

public class GlobalChallengeBeginEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	final GlobalChallenge challenge;
	public GlobalChallengeBeginEvent(GlobalChallenge challenge) {
		this.challenge = challenge;
	}
	public GlobalChallenge getChallenge() { return challenge; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
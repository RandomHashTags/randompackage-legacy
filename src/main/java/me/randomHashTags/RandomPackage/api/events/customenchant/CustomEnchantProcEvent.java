package me.randomHashTags.RandomPackage.api.events.customenchant;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;

public class CustomEnchantProcEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final Event event;
	private final CustomEnchant enchant;
	private final int level;
	private final ItemStack itemWithEnchant;
	private boolean cancelled, proced;
	private final Player P;
	public CustomEnchantProcEvent(Event event, CustomEnchant enchant, int level, ItemStack itemWithEnchant, Player P) {
		this.event = event;
		this.enchant = enchant;
		this.level = level;
		this.itemWithEnchant = itemWithEnchant;
		this.P = P;
		cancelled = false;
		proced = false;
	}
	
	public Event getEvent() { return event; }
	public CustomEnchant getEnchant() { return enchant; }
	public int getLevel() { return level; }
	public ItemStack getItem() { return itemWithEnchant; }
	public boolean didProc() { return proced; }
	public void setProced(boolean proced) { this.proced = proced; }
	public Player getPlayer() { return P; }
	
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public boolean isCancelled() { return cancelled; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}

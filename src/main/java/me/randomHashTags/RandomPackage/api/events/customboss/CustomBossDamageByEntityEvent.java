package me.randomHashTags.RandomPackage.api.events.customboss;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CustomBossDamageByEntityEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final LivingEntity boss;
	private final Entity damager;
	private double initialdamage;
	private boolean cancelled;
	public CustomBossDamageByEntityEvent(LivingEntity boss, Entity damager, double initialdamage) {
		this.boss = boss;
		this.damager = damager;
		this.initialdamage = initialdamage;
	}
	public LivingEntity getBoss() { return boss; }
	public Entity getDamager() { return damager; }
	public double getDamage() { return initialdamage; }
	public void setDamage(double damage) { this.initialdamage = damage; }
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}

package me.randomHashTags.RandomPackage.api.events.customenchant;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent;

public class isDamagedEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private final Player victim;
    private LivingEntity damager;
    private double damage;
    private EntityDamageEvent.DamageCause cause;
    public isDamagedEvent(Player victim, LivingEntity damager, double damage) {
        this.damager = damager;
        this.victim = victim;
        this.damage = damage;
    }
    public isDamagedEvent(Player victim, EntityDamageEvent.DamageCause cause) {
        this.victim = victim;
        this.cause = cause;
    }
    public Player getVictim() { return victim; }
    public LivingEntity getDamager() { return damager; }
    public double getDamage() { return damage; }
    public void setDamage(double damage) { this.damage = damage; }
    public EntityDamageEvent.DamageCause getCause() { return cause; }

    public void setCancelled(boolean cancel) { cancelled = cancel; }
    public boolean isCancelled() { return cancelled; }
    public HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}

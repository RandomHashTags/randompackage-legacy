package me.randomHashTags.RandomPackage.api.events.customenchant;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.potion.PotionEffectType;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;

public class CEARemovePotionEffectEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final Event event;
	private final Player player;
	private final CustomEnchant procedenchant;
	private final int enchantlevel;
	private final PotionEffectType potioneffecttype;
	private boolean cancelled;
	public CEARemovePotionEffectEvent(Event event, Player player, CustomEnchant procedenchant, int enchantlevel, PotionEffectType potioneffecttype) {
		this.event = event;
		this.player = player;
		this.procedenchant = procedenchant;
		this.enchantlevel = enchantlevel;
		this.potioneffecttype = potioneffecttype;
		cancelled = false;
	}
	public Event getEvent() { return event; }
	public Player getPlayer() { return player; }
	public CustomEnchant getProcedEnchant() { return procedenchant; }
	public int getProcedEnchantLevel() { return enchantlevel; }
	public PotionEffectType getRemovedPotionEffect() { return potioneffecttype; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public boolean isCancelled() { return cancelled; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
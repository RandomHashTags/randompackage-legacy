package me.randomHashTags.RandomPackage.api.events.auctionhouse;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class PlayerAuctionBiddableItemEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled;
	public final Player player;
	public final long auctionedtime;
	public final List<ItemStack> items;
	public final double startingbid, bidincrement, buynow;
	public PlayerAuctionBiddableItemEvent(Player player, long auctionedtime, List<ItemStack> items, double startingbid, double bidincrement, double buynow) {
		this.player = player;
		this.auctionedtime = auctionedtime;
		this.items = items;
		this.startingbid = startingbid;
		this.bidincrement = bidincrement;
		this.buynow = buynow;
		cancelled = false;
	}
	public boolean isCancelled() { return cancelled; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}
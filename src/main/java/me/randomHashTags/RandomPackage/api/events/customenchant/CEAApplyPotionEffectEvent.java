package me.randomHashTags.RandomPackage.api.events.customenchant;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.potion.PotionEffect;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;

public class CEAApplyPotionEffectEvent extends Event implements Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private final Event event;
	private final Player player;
	private final LivingEntity appliedto;
	private final CustomEnchant enchant;
	private final PotionEffect potioneffect;
	private boolean cancelled;
	private final int enchantlevel;
	public CEAApplyPotionEffectEvent(Event event, Player player, LivingEntity appliedto, CustomEnchant enchant, int enchantlevel, PotionEffect potioneffect) {
		this.event = event;
		this.player = player;
		this.appliedto = appliedto;
		this.enchant = enchant;
		this.enchantlevel = enchantlevel;
		this.potioneffect = potioneffect;
		cancelled = false;
	}
	public Event getEvent() { return event; }
	public Player getPlayer() { return player; }
	public LivingEntity getAppliedTo() { return appliedto; }
	public CustomEnchant getEnchant() { return enchant; }
	public int getEnchantLevel() { return enchantlevel; }
	public PotionEffect getPotionEffect() { return potioneffect; }
	public void setCancelled(boolean cancel) { cancelled = cancel; }
	public boolean isCancelled() { return cancelled; }
	public HandlerList getHandlers() { return handlers; }
	public static HandlerList getHandlerList() { return handlers; }
}

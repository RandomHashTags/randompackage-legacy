package me.randomHashTags.RandomPackage.api;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.api.events.player.PlayerTeleportDelayEvent;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.HashMap;
import java.util.UUID;

public class XPEvents extends RandomPackageAPI implements CommandExecutor, Listener {
    public boolean isEnabled = false;
    private static XPEvents instance;
    public static final XPEvents getXPEvents() {
        if(instance == null) instance = new XPEvents();
        return instance;
    }

    private HashMap<Player, String> delayed = new HashMap<>();

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if(!(sender instanceof Player)) return true;
        final Player player = (Player) sender;
        if(hasPermission(sender, "RandomPackage.xpbottle", true)) {
            if(args.length == 0) {
                sendStringListMessage(player, randompackage.getConfig().getStringList("xpbottle.argument-0"));
            } else {
                final String a = args[0];
                if(parseInt(a) == null) {
                    for(String s : randompackage.getConfig().getStringList("xpbottle.invalid-amount")) {
                        if(s.contains("{AMOUNT}")) s = s.replace("{AMOUNT}", a);
                        player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
                    }
                } else {
                    int amount = parseInt(a);
                    if(amount <= 0) sendStringListMessage(sender, randompackage.getConfig().getStringList("xpbottle.withdraw-at-least"));
                    else if(amount > getTotalExperience(player)) sendStringListMessage(player, randompackage.getConfig().getStringList("xpbottle.not-enough-to-bottle"));
                    else                                         xpbottle(player, amount);
                }
            }
        }
        return true;
    }

    public void enable() {
        if(isEnabled) return;
        pluginmanager.registerEvents(this, randompackage);
        isEnabled = true;
    }
    public void disable() {
        if(!isEnabled) return;
        isEnabled = false;
        delayed.clear();
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    private void playerMoveEvent(PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final UUID u = player.getUniqueId();
        final PlayerTeleportDelayEvent tel = PlayerTeleportDelayEvent.teleporting.keySet().contains(u) ? PlayerTeleportDelayEvent.teleporting.get(u) : null;
        if(tel != null) {
            final Location pl = player.getLocation();
            Location L = tel.from;
            if(L.getBlockX() == pl.getBlockX()
                    && L.getBlockY() == pl.getBlockY()
                    && L.getBlockZ() == pl.getBlockZ()) {
                return;
            } else {
                final HashMap<UUID, PlayerTeleportDelayEvent> events = PlayerTeleportDelayEvent.teleporting;
                for(String s : randompackage.getConfig().getStringList("xpbottle.teleport-cancelled"))
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
                tel.setCancelled(true);
                scheduler.cancelTask(events.get(u).task);
                events.remove(u);
            }
        }
    }
    @EventHandler
    private void playerQuitEvent(PlayerQuitEvent event) {
        final UUID u = event.getPlayer().getUniqueId();
        final PlayerTeleportDelayEvent tel = PlayerTeleportDelayEvent.teleporting.keySet().contains(u) ? PlayerTeleportDelayEvent.teleporting.get(u) : null;
        if(tel != null) tel.setCancelled(true);
    }
    @EventHandler(priority = EventPriority.HIGHEST)
    private void playerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        if(!event.isCancelled()) {
            for(String s : randompackage.getConfig().getStringList("xpbottle.delayed-commands")) {
                if(event.getMessage().toLowerCase().startsWith(s.toLowerCase())) {
                    delayed.put(event.getPlayer(), s);
                    return;
                }
            }
            scheduler.scheduleSyncDelayedTask(randompackage, () -> delayed.remove(event.getPlayer()), 1);
        }
    }
    @EventHandler(priority = EventPriority.HIGHEST)
    private void playerTeleportEvent(PlayerTeleportEvent event) {
        final Player player = event.getPlayer();
        if(randompackage.getConfig().getStringList("xpbottle.teleport-causes").contains(event.getCause().name()) && delayed.keySet().contains(player)) {
            delayed.remove(player);
            double delay = randompackage.getConfig().getDouble("xpbottle.teleportation-default-delay");
            if(hasPermission(event.getPlayer(), "RandomPackage.xpbottle.bypass-delay", false) || delay <= 0) return;
            final UUID u = player.getUniqueId();
            final RPPlayerData pdata = RPPlayerData.get(u);
            if(pdata.isXPExhausted()) {
                int sec = (int) ((pdata.xpExhaustionExpiration - System.currentTimeMillis()) / 1000);
                int min = sec / 60;
                sec -= min * 60;
                final int ss = sec;
                for(String s : randompackage.getConfig().getStringList("xpbottle.cannot-teleport")) {
                    if(s.contains("{TIME}")) s = s.replace("{TIME}", min + "m " + ss + "s");
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
                }
            } else {
                final HashMap<UUID, PlayerTeleportDelayEvent> events = PlayerTeleportDelayEvent.teleporting;
                final double mindelay = randompackage.getConfig().getDouble("xpbottle.teleportation-min-delay");
                delay -= getTotalExperience(player) / randompackage.getConfig().getDouble("xpbottle.teleportation-variable");
                delay = round(delay, 3);
                if(delay < mindelay) delay = mindelay;
                final double uu = delay*fapi.getTeleportDelayMultiplier(fapi.getFaction(player));
                if(events.keySet().contains(u)) {
                    events.get(u).setCancelled(true);
                    scheduler.cancelTask(events.get(u).task);
                    events.remove(u);
                }
                final PlayerTeleportDelayEvent e = new PlayerTeleportDelayEvent(event.getPlayer(), uu, event.getFrom(), event.getTo());
                pluginmanager.callEvent(e);
                if(!e.isCancelled()) {
                    for(String s : randompackage.getConfig().getStringList("xpbottle.pending-teleport")) {
                        if(s.contains("{SECS}")) s = s.replace("{SECS}", roundDoubleString(e.getDelay(), 3));
                        player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
                    }
                } else {
                    scheduler.cancelTask(events.get(u).task);
                    events.remove(u);
                }
            }
            event.setCancelled(true);
        }
    }


    public void xpbottle(Player player, int amount) {
        final int minbottle = randompackage.getConfig().getInt("xpbottle.min-bottle");
        final FileConfiguration config = randompackage.getConfig();
        final HashMap<String, String> replacements = new HashMap<>();
        replacements.put("{MIN}", Integer.toString(minbottle));
        replacements.put("{AMOUNT}", Integer.toString(amount));
        if(amount < minbottle) {
            sendStringListMessage(player, config.getStringList("xpbottle.withdraw-at-least"), replacements);
        } else {
            final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
            if(pdata.isXPExhausted() && !hasPermission(player, "RandomPackage.xpbottle.bypass-exhaustion", false)) {
                int sec = (int) ((pdata.xpExhaustionExpiration - System.currentTimeMillis()) / 1000);
                int min = sec / 60;
                sec -= min * 60;
                replacements.put("{TIME}", min + "m " + sec + "s");
                sendStringListMessage(player, config.getStringList("xpbottle.cannot-xpbottle"), replacements);
            } else {
                sendStringListMessage(player, config.getStringList("xpbottle.withdraw"), replacements);
                giveItem(player, givedpitem.getXpbottle(amount, player.getName()));

                int xp = Math.round(getTotalExperience(player));
                setTotalExperience(player, xp-amount);
                playSound(sounds, "xpbottle.withdraw", player, player.getLocation(), false);
                final int exh = randompackage.getConfig().getInt("xpbottle.exp-exhaustion");
                if(exh != -1) {
                    pdata.xpExhaustionExpiration = System.currentTimeMillis() + (exh * 1000 * 60);
                    replacements.put("{MIN}", Integer.toString(exh));
                    sendStringListMessage(player, config.getStringList("xpbottle.afflict"), replacements);
                }
            }
        }
    }
}

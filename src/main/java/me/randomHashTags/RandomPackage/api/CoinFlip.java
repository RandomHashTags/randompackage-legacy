package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.coinflips.CoinFlipChallenge;
import me.randomHashTags.RandomPackage.utils.classes.coinflips.CoinFlipMatch;
import me.randomHashTags.RandomPackage.api.events.CoinFlipChallengeEndEvent;
import me.randomHashTags.RandomPackage.utils.supported.VaultAPI;
import net.milkbowl.vault.economy.Economy;

public class CoinFlip extends RandomPackageAPI implements Listener {
	
	public boolean isEnabled = false;
	private static CoinFlip instance;
	public static final CoinFlip getCoinFlip() {
		if(instance == null) instance = new CoinFlip();
		return instance;
	}
	private static HashMap<Player, Double> creating = new HashMap<>();
	private static HashMap<Integer, String> display = new HashMap<>();
	private static HashMap<Player, CoinFlipMatch> challenging = new HashMap<>();
	private static HashMap<Inventory, List<Integer>> schedulers = new HashMap<>();

	public int minBet = 0, countdownStart = 0, expiresIn = 0;
	public double taxRate = 0.00;
	public FileConfiguration config;
	private Inventory coinflip, challenge, choose;
	private ItemStack countdown;
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(player != null) {
			if(args.length == 0) viewCoinFlips(player);
			else if(args[0].equals("stats") && hasPermission(player, "RandomPackage.coinflip.stats", true)) viewStats(player, RPPlayerData.get(player.getUniqueId()));
			else if(args[0].equals("help") && hasPermission(player, "RandomPackage.coinflip.help", true)) viewHelp(player);
			else if(args[0].equals("cancel") && hasPermission(player, "RandomPackage.coinflip.cancel", true)) cancel(player);
			else if(args[0].equals("toggle") && hasPermission(player, "RandomPackage.coinflip.toggle", true)) toggleNotifications(RPPlayerData.get(player.getUniqueId()));
			else if(getRemainingInt(args[0]) >= 0 && hasPermission(player, "RandomPackage.coinflip.create", true)) {
				pickToCreate(player, getRemainingInt(args[0]));
			}
		}
		return true;
	}

	public void enable() {
		final long started = System.currentTimeMillis();
	    if(isEnabled) return;
		save("Features", "coinflip.yml");
	    pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "coinflip.yml"));
        countdown = d(config, "challenge.countdown", 0);
        minBet = config.getInt("minimum-bet");
        countdownStart = config.getInt("challenge.countdown-start");
        taxRate = config.getDouble("tax-rate");
        expiresIn = config.getInt("expires-in");
        coinflip = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', config.getString("gui.title")));
        for(int i = 0; i < coinflip.getSize(); i++)
            if(config.get("gui." + i + ".item") != null)
                coinflip.setItem(i, d(config, "gui." + i, 0));
        challenge = Bukkit.createInventory(null, config.getInt("challenge.size"), ChatColor.translateAlternateColorCodes('&', config.getString("challenge.title")));
        choose = Bukkit.createInventory(null, config.getInt("choose.size"), ChatColor.translateAlternateColorCodes('&', config.getString("choose.title")));
        for(int i = 0; i < choose.getSize(); i++) {
            if(config.get("choose." + i) != null) {
                display.put(i, ChatColor.translateAlternateColorCodes('&', config.getString("choose." + i + ".display")));
                item = d(config, "choose." + i, 0); itemMeta = item.getItemMeta(); lore.clear();
                if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
                for(String s : config.getStringList("choose.added-lore")) lore.add(ChatColor.translateAlternateColorCodes('&', s));
                itemMeta.setLore(lore); lore.clear();
                item.setItemMeta(itemMeta);
                choose.setItem(i, item);
            }
        }
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded CoinFlip &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disable() {
	    if(!isEnabled) return;
	    isEnabled = false;
        display.clear();
        for(Inventory i : schedulers.keySet()) for(int s : schedulers.get(i)) Bukkit.getScheduler().cancelTask(schedulers.get(i).get(s));
        for(Player p : creating.keySet()) p.closeInventory();
        for(Player p : challenging.keySet()) p.closeInventory();
        for(CoinFlipChallenge c : CoinFlipChallenge.challenges) {
            final OfflinePlayer ch = c.getChallenger(), cr = c.getMatch().getCreator();
            if(ch.isOnline()) ch.getPlayer().closeInventory();
            if(cr.isOnline()) cr.getPlayer().closeInventory();
            c.delete();
        }
		HandlerList.unregisterAll(this);
    }
	
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		final ItemStack cu = event.getCurrentItem();
		if(!event.isCancelled() && cu != null && !cu.getType().equals(Material.AIR)) {
			final Player player = (Player) event.getWhoClicked();
			final Inventory top = player.getOpenInventory().getTopInventory();
			final int r = event.getRawSlot();
			if(top.getHolder() == player) {
				final String t = top.getTitle();
				if(t.equals(coinflip.getTitle()) || t.equals(choose.getTitle())) {
					event.setCancelled(true);
					player.updateInventory();
					if(r >= top.getSize()) return;
					if(t.equals(coinflip.getTitle())) {
						final CoinFlipMatch c = CoinFlipMatch.valueOf(r);
						if(c != null) {
							pickToChallenge(player, c);
						}
					} else if(challenging.keySet().contains(player)) {
						challengeAccepted(challenging.get(player), player, r, cu);
					} else {
						tryToCreateMatch(r, player, creating.get(player), cu);
						player.closeInventory();
					}
				}
			} else {
				final CoinFlipChallenge c = CoinFlipChallenge.valueOf(player);
				if(c != null) {
					event.setCancelled(true);
					player.updateInventory();
				}
			}
		}
	}
	private ItemStack getPlayerDisplay(OfflinePlayer player) {
		final String q = config.getString("challenge.players.item").toLowerCase().replace("%player%", player.getName());
		item = d(null, q, 0); itemMeta = item.getItemMeta(); lore.clear();
		if(config.get("challenge.players.name") != null) itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', config.getString("challenge.players.name").replace("{FACTION}", fapi.getFaction(player)).replace("%player%", player.getName())));
		if(config.get("challenge.players.lore") != null) {
			final String b = formatDouble(VaultAPI.economy.getBalance(player));
			for(String s : config.getStringList("challenge.players.lore"))
				lore.add(ChatColor.translateAlternateColorCodes('&', s.replace("{BALANCE}", b).replace("%player%", player.getName())));
		}
		itemMeta.setLore(lore); lore.clear();
		item.setItemMeta(itemMeta);
		return item;
	}
	private void remove(CoinFlipMatch c) {
		int slot = c.getSlot();
		if(slot != -1) {
			coinflip.setItem(c.getSlot(), new ItemStack(Material.AIR));
			for(int i = 0; i < coinflip.getSize(); i++)
				if(i+1 < coinflip.getSize() && CoinFlipMatch.valueOf(i+1) != null)
					coinflip.setItem(i, coinflip.getItem(i+1));
		}
	}
	private void challengeAccepted(CoinFlipMatch match, Player challenger, int rawslot, ItemStack chosen) {
		VaultAPI.economy.withdrawPlayer(challenger, match.getWager());
		remove(match);
		for(Player p : challenging.keySet()) {
			if(challenging.get(p).equals(match)) {
				p.closeInventory();
				challenging.remove(p);
			}
		}
		match.delete();
		final CoinFlipChallenge c = new CoinFlipChallenge(match, challenger, chosen, ChatColor.translateAlternateColorCodes('&', config.getString("choose." + rawslot + ".display")));
		final Player creator = match.getCreator().isOnline() ? match.getCreator().getPlayer() : null;
		final Inventory inv = Bukkit.createInventory(null, challenge.getSize(), challenge.getTitle().replace("{WAGER}", formatDouble(match.getWager())));
		int slot = 4;
		for(int i = 0; i < inv.getSize(); i++) {
			if(config.get("challenge." + i) != null && config.getString("challenge." + i) != null) {
				final String a = config.getString("challenge." + i).toUpperCase();
				if(a.equals("{PLAYER1}") || a.equals("{PLAYER2}")) {
					item = getPlayerDisplay(a.equals("{PLAYER1}") ? match.getCreator() : challenger);
				} else if(a.equals("{PLAYER1_CHOICE}") || a.equals("{PLAYER2_CHOICE}")) {
					item = d(config, "choose." + (a.contains("1") ? match.getConfigSlot() : rawslot) + ".challenge", 0); itemMeta = item.getItemMeta(); lore.clear();
					if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{PLAYER}", (a.contains("1") ? match.getCreator() : challenger).getName()));
					item.setItemMeta(itemMeta);
				} else if(a.equals("{COUNTDOWN}")) {
					slot = i;
					item = getCooldown(countdownStart);
				} else item = null;
				if(item != null) inv.setItem(i, item);
			}
		}
		if(creator != null) {
			for(String s : config.getStringList("messages.accepted")) creator.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{PLAYER}", challenger.getName())));
			creator.openInventory(inv);
			creator.updateInventory();
		}
		for(String s : config.getStringList("messages.accepted-bet")) challenger.sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{WAGER}", formatDouble(match.getWager()))));
		challenger.openInventory(inv);
		challenger.updateInventory();
		startCountdown(c, inv, slot);
	}
	private ItemStack getCooldown(int sec) {
		item = countdown.clone(); itemMeta = item.getItemMeta(); lore.clear();
		if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{COUNTDOWN}", Integer.toString(sec)));
		if(itemMeta.hasLore()) for(String s : itemMeta.getLore()) lore.add(s.replace("{COUNTDOWN}", Integer.toString(sec)));
		itemMeta.setLore(lore); lore.clear();
		item.setItemMeta(itemMeta);
		return item;
	}
	private void startCountdown(CoinFlipChallenge cf, Inventory inv, int slot) {
		final CoinFlipMatch match = cf.getMatch();
		schedulers.put(inv, new ArrayList<>());
		int c = countdownStart;
		final List<String> rolling = config.getStringList("challenge.rolling.lore");
		for(int i = 1; i <= countdownStart; i++) {
			c -= 1;
			final int d = c;
			int k = Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> {
				inv.setItem(slot, getCooldown(d));
				for(HumanEntity a : inv.getViewers()) if(a instanceof Player) ((Player) a).updateInventory();

				if(d == 0) {
					int e = 0;
					final int q = 60, r = random.nextInt(2);
					for(int x = 1; x <= q+r; x++) {
						e = e == 1 ? 0 : 1;
						final int X = x, f = e;
						int j = Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> {
							item = f == 0 ? match.getChosen().clone() : cf.getChallengerChosen().clone(); itemMeta = item.getItemMeta(); lore.clear();
							for(String s : rolling) lore.add(ChatColor.translateAlternateColorCodes('&', s));
							itemMeta.setLore(lore); lore.clear();
							item.setItemMeta(itemMeta);
							inv.setItem(slot, item);
							for(HumanEntity a : inv.getViewers()) if(a instanceof Player) ((Player) a).updateInventory();

							if(X == q+r) {
								stopSchedulers(inv);
								final OfflinePlayer winner = X==60 ? match.getCreator() : cf.getChallenger(), loser = X==60 ? cf.getChallenger() : match.getCreator();
								final RPPlayerData pw = RPPlayerData.get(winner.getUniqueId()), pl = RPPlayerData.get(loser.getUniqueId());
								final String display = X==60 ? match.getDisplay() : cf.getChallengerDisplay();
								final double won = match.getWager()*2, tax = match.getTax()*2, m = won-tax;
								final CoinFlipChallengeEndEvent ev = new CoinFlipChallengeEndEvent(cf, winner, loser, m);
								pluginmanager.callEvent(ev);
								eco.depositPlayer(winner, m);
								pw.addCoinflipWin(won/2);
								pl.addCoinflipLoss(won/2);
								for(String s : config.getStringList("messages.won")) {
									s = ChatColor.translateAlternateColorCodes('&', s.replace("{DISPLAY}", display).replace("{WINNER}", winner.getName()));
									if(winner.isOnline()) winner.getPlayer().sendMessage(s);
									if(loser.isOnline()) loser.getPlayer().sendMessage(s);
								}
								if(winner.isOnline())
									for(String s : config.getStringList("messages.won-bet")) winner.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', s.replace("{WAGER}", formatDouble(m))));
								for(String o : config.getStringList("messages.broadcast-winner")) {
									o = o.replace("{WINNER_F}", fapi.getFaction(winner)).replace("{LOSER_F}", fapi.getFaction(loser));
									o = o.replace("{WINNER_TOTAL_W}", Integer.toString(pw.coinflipWins)).replace("{WINNER_TOTAL_L}", Integer.toString(pw.coinflipLosses));
									o = o.replace("{LOSER_TOTAL_W}", Integer.toString(pl.coinflipWins)).replace("{LOSER_TOTAL_L}", Integer.toString(pl.coinflipLosses));
									o = o.replace("{WINNER}", winner.getName()).replace("{LOSER}", loser.getName()).replace("{WAGER}", formatDouble(won));
									for(Player p : Bukkit.getOnlinePlayers())
										if(RPPlayerData.get(p.getUniqueId()).receivesCoinflipNotifications)
											p.sendMessage(ChatColor.translateAlternateColorCodes('&', o));
								}
								//Bukkit.broadcastMessage("winner = " + (X==60 ? "left" : "right"));
								schedulers.remove(inv);
							}
						}, 2*x);
						schedulers.get(inv).add(j);
					}

				}
			}, 20*i);
			schedulers.get(inv).add(k);
		}
		
	}
	public void stopSchedulers(Inventory inv) {
		if(schedulers.keySet().contains(inv)) {
			for(int i : schedulers.get(inv)) Bukkit.getScheduler().cancelTask(i);
			schedulers.remove(inv);
		}
	}
	public void viewCoinFlips(Player player) {
		if(hasPermission(player, "RandomPackage.coinflip", true)) {
			int size = CoinFlipMatch.matches.size();
			size = size == 9 || size == 18 || size == 27 || size == 37 || size == 45 || size == 54 ? size : ((size+9)/9)*9;
			player.openInventory(Bukkit.createInventory(player, size, coinflip.getTitle()));
			for(int i = 0; i < size; i++) if(coinflip.getItem(i) != null) player.getOpenInventory().getTopInventory().setItem(i, coinflip.getItem(i));
			final List<String> can = config.getStringList("gui.wager.can-afford"), cannot = config.getStringList("gui.wager.cannot-afford");
			for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
				final CoinFlipMatch c = CoinFlipMatch.valueOf(i);
				if(c != null) {
					final String display = ChatColor.translateAlternateColorCodes('&', config.getString("choose." + c.getConfigSlot() + ".display"));
					final List<String> a = c.getWager() > VaultAPI.economy.getBalance(player) ? cannot : can;
					final RPPlayerData pdata = RPPlayerData.get(c.getCreator().getUniqueId());
					final double cfL = pdata.coinflipLosses;
					final double totalW = pdata.coinflipWins, total = totalW+(cfL != 0.00 ? cfL : 1);
					final String wlr = roundDoubleString(totalW/total, 2);
					item = player.getOpenInventory().getTopInventory().getItem(i);
					if(item != null) {
						itemMeta = item.getItemMeta(); lore.clear();
						if(itemMeta.hasLore()) {
							for(String s : itemMeta.getLore())
								if(s.equals("{AFFORD}")) {
									for(String S : a) lore.add(ChatColor.translateAlternateColorCodes('&', S));
								} else {
									lore.add(s.replace("{WIN_LOSS_RATIO}", wlr).replace("{DISPLAY}", display));
								}
						}
						itemMeta.setLore(lore); lore.clear();
						item.setItemMeta(itemMeta);
					}
				}
			}
			player.updateInventory();
		}
	}
	public void toggleNotifications(RPPlayerData player) {
		player.receivesCoinflipNotifications = !player.receivesCoinflipNotifications;
		if(player.getOfflinePlayer().isOnline())
			sendStringListMessage(player.getOfflinePlayer().getPlayer(), config.getStringList("messages.toggle-" + (player.receivesCoinflipNotifications ? "on" : "off")));
	}
	public void cancel(Player player) {
		final CoinFlipMatch c = CoinFlipMatch.valueOf(player);
		if(c != null) {
			sendStringListMessage(player, config.getStringList("messages.cancel"));
			VaultAPI.economy.depositPlayer(player, c.getWager());
			coinflip.setItem(c.getSlot(), new ItemStack(Material.AIR));
			remove(c);
			c.delete();
		}
	}
	public void pickToCreate(Player player, double wager) {
		final Economy e = VaultAPI.economy;
		if(CoinFlipMatch.valueOf(player) != null) {
			sendStringListMessage(player, config.getStringList("messages.already-in-a-match"));
		} else if(e.getBalance(player) < wager) {
			sendStringListMessage(player, config.getStringList("messages.not-enough-money-to-bet"));
		} else if(wager < minBet) {
			sendStringListMessage(player, config.getStringList("messages.bet-less-than-min"));
		} else {
			creating.put(player, wager);
			player.openInventory(Bukkit.createInventory(player, choose.getSize(), choose.getTitle()));
			player.getOpenInventory().getTopInventory().setContents(choose.getContents());
			for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
				item = player.getOpenInventory().getTopInventory().getItem(i);
				if(item != null) {
					itemMeta = item.getItemMeta(); lore.clear();
					for(String s : itemMeta.getLore()) lore.add(s.replace("{WAGER}", formatDouble(wager)));
					itemMeta.setLore(lore); lore.clear();
					item.setItemMeta(itemMeta);
				}
			}
			player.updateInventory();
		}
	}
	public void pickToChallenge(Player player, CoinFlipMatch match) {
		final UUID c = match.getCreator().getUniqueId();
		if(c.equals(player.getUniqueId()))
			sendStringListMessage(player, config.getStringList("messages.already-in-a-match"));
		else if(match.getWager() > VaultAPI.economy.getBalance(player))
			sendStringListMessage(player, config.getStringList("messages.not-enough-money-to-accept"));
		else {
			player.openInventory(Bukkit.createInventory(player, choose.getSize(), choose.getTitle()));
			player.getOpenInventory().getTopInventory().setContents(choose.getContents());
			challenging.put(player, match);
			final ItemStack chosen = match.getChosen();
			final String wager = formatDouble(match.getWager());
			for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
				item = player.getOpenInventory().getTopInventory().getItem(i);
				if(item != null) {
					itemMeta = item.getItemMeta(); lore.clear();
					if(itemMeta.hasLore()) for(String s : itemMeta.getLore()) lore.add(s.replace("{WAGER}", wager));
					itemMeta.setLore(lore); lore.clear();
					item.setItemMeta(itemMeta);
					if(!item.getItemMeta().equals(chosen.getItemMeta())) player.getOpenInventory().getTopInventory().setItem(i, item);
					else player.getOpenInventory().getTopInventory().setItem(i, new ItemStack(Material.AIR));
				}
			}
			player.updateInventory();
		}
	}
	@EventHandler
	private void inventoryCloseEvent(InventoryCloseEvent event) {
		final Player player = (Player) event.getPlayer();
		if(creating.keySet().contains(player)) creating.remove(player);
		if(challenging.keySet().contains(player)) challenging.remove(player);
		final CoinFlipChallenge c = CoinFlipChallenge.valueOf(player);
		if(c != null && event.getInventory().getViewers().size() == 0) {
			final CoinFlipMatch cfm = c.getMatch();
			final OfflinePlayer winner = random.nextInt(2) == 0 ? c.getChallenger() : cfm.getCreator(), loser = winner == c.getChallenger() ? cfm.getCreator() : c.getChallenger();
			final CoinFlipChallengeEndEvent e = new CoinFlipChallengeEndEvent(c, winner, loser, (cfm.getWager()*2)-(cfm.getTax()*2));
			pluginmanager.callEvent(e);
			c.delete();
		}
	}
	public void tryToCreateMatch(int rawslot, Player player, double wager, ItemStack chosen) {
		if(wager < minBet) {
			sendStringListMessage(player, config.getStringList("messages.bet-less-than-min"));
		} else {
			for(String s : config.getStringList("messages.joinQueue")) {
				if(s.contains("{WAGER}")) s = s.replace("{WAGER}", formatDouble(wager));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
			}
			eco.withdrawPlayer(player, wager);
			final long time = System.currentTimeMillis();
			final CoinFlipMatch c = new CoinFlipMatch(rawslot, time, player, wager, wager*taxRate, chosen, ChatColor.translateAlternateColorCodes('&', config.getString("choose." + rawslot + ".display")), time+(expiresIn*1000));
			addCoinFlipMatch(c);
		}
	}
	public void addCoinFlipMatch(CoinFlipMatch match) {
		final OfflinePlayer creator = match.getCreator();
		final RPPlayerData pdata = RPPlayerData.get(creator.getUniqueId());
		item = d(config, "gui.wager", 0); itemMeta = item.getItemMeta(); lore.clear();
		if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("%player%", creator.getName()).replace("{FACTION}", fapi.getFaction(creator)));
		final double wager = match.getWager(), tax = match.getTax();
		final int recentW = pdata.getCoinflipRecentWins(), recentL = pdata.getCoinflipRecentLosses(), totalW = pdata.coinflipWins, totalL = pdata.coinflipLosses;
		if(itemMeta.hasLore()) {
			for(String s : itemMeta.getLore()) {
				s = s.replace("{WAGER}", formatDouble(wager)).replace("{TAX}", formatDouble(tax)).replace("{RECENT_W}", formatInt(recentW)).replace("{RECENT_L}", formatInt(recentL)).replace("{TOTAL_W}", formatInt(totalW)).replace("{TOTAL_L}", formatInt(totalL));
				lore.add(s);
			}
		}
		itemMeta.setLore(lore); lore.clear();
		item.setItemMeta(itemMeta);
		coinflip.addItem(item);
	}
	public void viewStats(Player player, RPPlayerData target) {
		for(String s : config.getStringList("messages.stats")) {
			s = s.replace("{RECENT_WINS}", formatInt(target.getCoinflipRecentWins())).replace("{RECENT_WINS_$}", formatDouble(target.getCoinflipRecentWonCash()));
			s = s.replace("{RECENT_LOSSES}", formatInt(target.getCoinflipRecentLosses())).replace("{RECENT_LOSSES_$}", formatDouble(target.getCoinflipRecentLostCash()));
			s = s.replace("{TOTAL_WINS}", formatInt(target.coinflipWins)).replace("{TOTAL_WINS_$}", formatDouble(target.coinflipWonCash));
			s = s.replace("{TOTAL_LOSSES}", formatInt(target.coinflipLosses)).replace("{TOTAL_LOSSES_$}", formatDouble(target.coinflipLostCash));
			s = s.replace("{TAXES_PAID}", formatDouble(target.coinflipTaxesPaid));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
		}
	}
	public void viewHelp(CommandSender sender) {
		for(String s : config.getStringList("messages.help")) sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
	}
}

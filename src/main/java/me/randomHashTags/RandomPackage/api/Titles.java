package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;

public class Titles extends RandomPackageAPI {
	
	public boolean isEnabled = false;
	private static Titles instance;
	public static final Titles getTitles() {
		if(instance == null) instance = new Titles();
		return instance;
	}
	public YamlConfiguration config;
	
	public ItemStack interactableItem;
	private ItemStack nextpage, background, active, inactive;
	private String selftitle, tabformat;
	public List<String> titles = new ArrayList<>();
	
	private HashMap<Player, Integer> pages = new HashMap<>();
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(player != null) {
			final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
			if(args.length == 0)
				viewTitles(player, pdata.activeTitle, pdata.ownedTitles, 1);
		}
		return true;
	}

	public void enable() {
		final long started = System.currentTimeMillis();
		if(isEnabled) return;
		pluginmanager.registerEvents(this, randompackage);
		save("Features", "titles.yml");
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "titles.yml"));
		interactableItem = d(config, "interactable-item", 0);
		nextpage = d(config, "gui.next-page", 0);
		background = d(config, "gui.background", 0);
		active = d(config, "gui.active-title", 0);
		inactive = d(config, "gui.inactive-title", 0);
		for(String s : config.getStringList("titles")) titles.add(s);
		selftitle = ChatColor.translateAlternateColorCodes('&', config.getString("gui.self-title"));
		//othertitle = ChatColor.translateAlternateColorCodes('&', config.getString("gui.other-title"));
		tabformat = ChatColor.translateAlternateColorCodes('&', config.getString("tab.format"));
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + titles.size() + " titles &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		titles.clear();
		for(Player p : pages.keySet()) p.closeInventory();
		HandlerList.unregisterAll(this);
	}

	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		final String t = isATitle(event.getItem());
		if(t != null) {
			event.setCancelled(true);
			event.getPlayer().updateInventory();
			final RPPlayerData pdata = RPPlayerData.get(event.getPlayer().getUniqueId());
			final boolean has = pdata.ownedTitles.contains(t);
			final List<String> m = config.getStringList("messages." + (has ? "already-own" : "redeem"));
			for(String s : m) {
				s = s.replace("{TITLE}", ChatColor.translateAlternateColorCodes('&', t));
				event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', s));
			}
			if(!has) {
				pdata.addOwnedTitle(t);
				removeItem(event.getPlayer(), event.getItem(), 1);
			}
		}
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked() && !event.isCancelled() && event.getCurrentItem() != null) {
			final String t = event.getWhoClicked().getOpenInventory().getTopInventory().getTitle();
			if(t.equals(selftitle)) {
				event.setCancelled(true);
				((Player) event.getWhoClicked()).updateInventory();
				if(event.getRawSlot() >= event.getWhoClicked().getOpenInventory().getTopInventory().getSize()) return;
				final Player player = (Player) event.getWhoClicked();
				final RPPlayerData pdata = RPPlayerData.get(player.getUniqueId());
				final String a = pdata.activeTitle;
				final List<String> titles = pdata.ownedTitles;
				final int page = pages.keySet().contains(player) ? pages.get(player) : 0, Z = (page*53)+event.getRawSlot();
				if(event.getCurrentItem().equals(nextpage)) {
					player.closeInventory();
					viewTitles(player, a, titles, page+2);
				} else if(titles.size() > Z) {
					final String q = (a != null && a.equals(titles.get(Z)) ? "un" : "") + "equip";
					final List<String> s = config.getStringList("messages." + q);
					for(String h : s) player.sendMessage(ChatColor.translateAlternateColorCodes('&', h.replace("{TITLE}", titles.get(Z))));
					pdata.activeTitle = q.equals("unequip") ? "null" : titles.get(Z);
					update(player, a, titles, event.getRawSlot(), Z);
				}
			}
		}
	}
	@EventHandler
	private void inventoryCloseEvent(InventoryCloseEvent event) {
		final Player player = (Player) event.getPlayer();
		if(pages.keySet().contains(player)) pages.remove(player);
	}
	public String isATitle(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore()) {
			for(String s : titles) {
				item = interactableItem.clone(); itemMeta = item.getItemMeta(); lore.clear();
				itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{TITLE}", ChatColor.translateAlternateColorCodes('&', s)));
				for(String l : itemMeta.getLore()) lore.add(ChatColor.translateAlternateColorCodes('&', l.replace("{TITLE}", s)));
				itemMeta.setLore(lore); lore.clear();
				item.setItemMeta(itemMeta);
				if(is.getItemMeta().equals(item.getItemMeta())) return s;
			}
		}
		return null;
	}
	private void viewTitles(Player player, String activetitle, List<String> titles, int page) {
		if(hasPermission(player, "RandomPackage.titles", true)) {
			page = page-1;
			pages.put(player, page);
			int size = titles.size()-(53*page);
			if(size <= 0 || size == 1 && titles.get(0).equals("")) {
				sendStringListMessage(player, config.getStringList("messages.no-unlocked-titles"));
			} else {
				size = size == 9 || size == 18 || size == 27 || size == 36 || size == 45 || size == 54 ? size : ((size+9)/9)*9;
				size = size > 54 ? 54 : size;
				player.openInventory(Bukkit.createInventory(player, size, selftitle));
				for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
					final int x = i+(53*page);
					final String tt = titles.size() > x ? titles.get(x) : null;
					if(tt != null && !tt.equals("")) {
						item = i == 53 && titles.size() > (54*page) ? nextpage : tt != null ? activetitle != null && activetitle.equals(tt) ? active.clone() : inactive.clone() : background.clone();
						itemMeta = item.getItemMeta(); lore.clear();
						if(tt != null) {
							if(!item.equals(nextpage)) {
								if(itemMeta.hasDisplayName()) itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{TITLE}", ChatColor.translateAlternateColorCodes('&', tt)));
								if(itemMeta.hasLore()) for(String s : itemMeta.getLore()) lore.add(ChatColor.translateAlternateColorCodes('&', s.replace("{TITLE}", tt)));
							}
						}
						itemMeta.setLore(lore); lore.clear();
						item.setItemMeta(itemMeta);
						player.getOpenInventory().getTopInventory().setItem(i, item);
					} else {
						player.getOpenInventory().getTopInventory().setItem(i, background.clone());
					}
				}
				player.updateInventory();
			}
		}
	}
	private void update(Player player, String activetitle, List<String> titles, int rawslot, int Z) {
		int prevactive = 0;
		ItemStack e = null;
		if(activetitle != null) {
			e = active.clone(); itemMeta = e.getItemMeta(); lore.clear();
			itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{TITLE}", ChatColor.translateAlternateColorCodes('&', activetitle)));
			for(String s : itemMeta.getLore()) lore.add(ChatColor.translateAlternateColorCodes('&', s.replace("{TITLE}", activetitle)));
			itemMeta.setLore(lore); lore.clear();
			e.setItemMeta(itemMeta);
		}
		for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) if(player.getOpenInventory().getTopInventory().getItem(i).equals(e)) prevactive = i;
		for(int i = 1; i <= (this.titles.get(Z).equals(activetitle) ? 1 : 2); i++) {
			item = i == 1 ? inactive.clone() : active.clone(); itemMeta = item.getItemMeta(); lore.clear();
			itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{TITLE}", ChatColor.translateAlternateColorCodes('&', titles.get(i == 1 ? prevactive : Z))));
			for(String s : itemMeta.getLore()) lore.add(ChatColor.translateAlternateColorCodes('&', s.replace("{TITLE}", titles.get(i == 1 ? prevactive : rawslot))));
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
			player.getOpenInventory().getTopInventory().setItem(i == 1 ? prevactive : rawslot, item);
		}
		player.updateInventory();
	}
	public int getTitle(String title) {
		int i = 0;
		for(String s : titles) {
			if(s.equals(title)) return i;
			i++;
		}
		return -1;
	}
}

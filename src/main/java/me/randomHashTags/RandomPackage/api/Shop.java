package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.List;

import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.Potion;

import me.randomHashTags.RandomPackage.RandomPackageAPI;

@SuppressWarnings({"deprecation"})
public class Shop extends RandomPackageAPI implements CommandExecutor, Listener {
	
	public boolean isEnabled = false;
	private static Shop instance;
	public static final Shop getShop() {
	    if(instance == null) instance = new Shop();
	    return instance;
	}
	
	private Inventory inv;
	private Inventory[] categories = new Inventory[54];
	private FileConfiguration config;
	private ItemStack back;

	public void enable() {
	    final long started = System.currentTimeMillis();
	    if(isEnabled) return;
	    save("Features", "shop.yml");
	    pluginmanager.registerEvents(this, randompackage);
	    isEnabled = true;
        config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "shop.yml"));
        back = d(config, "items.back-to-categories", 0);
        inv = Bukkit.createInventory(null, config.getInt("menu.size"), ChatColor.translateAlternateColorCodes('&', config.getString("menu.title")));
        for(int z = 0; z < inv.getSize(); z++) {
            if(config.get("menu." + z + ".item") != null) {
                item = d(config, "menu." + z, 0);
                itemMeta = item.getItemMeta();
                itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_POTION_EFFECTS);
                item.setItemMeta(itemMeta);
                inv.setItem(z, item);
            }
        }
        int loaded = 0;
        categories = new Inventory[54];
        for(int i = 0; i < 54; i++) {
            if(config.get(i + ".title") != null) {
                loaded += 1;
                Inventory inv = Bukkit.createInventory(null, config.getInt(i + ".size"), ChatColor.translateAlternateColorCodes('&', config.getString(i + ".title")));
                for(int z = 0; z < inv.getSize(); z++) {
                    if(config.get(i + "." + z + ".item") != null) {
                        item = config.getString(i + "." + z + ".item").equalsIgnoreCase("back") ? back : d(config, i + "." + z, 0);
                        inv.setItem(z, item);
                    }
                }
                categories[i] = inv;
            }
        }
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " shop categories &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    public void disable() {
	    if(!isEnabled) return;
	    isEnabled = false;
        HandlerList.unregisterAll(this);
    }

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(player != null) view(player);
		return true;
	}
	public double getDiscount(Player player) {
	    if(player.hasPermission("RandomPackage.shop.discount.cancel")) return 0.00;
	    double d = 0;
        for(int k = 1; k <= 100; k++)
            if(player.hasPermission("RandomPackage.shop.discount." + k))
                d = k;
            return d/100;
    }
	@EventHandler(priority = EventPriority.HIGHEST)
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getWhoClicked().getOpenInventory().getTopInventory().getHolder() == event.getWhoClicked()) {
			final Player player = (Player) event.getWhoClicked();
			final String t = player.getOpenInventory().getTopInventory().getTitle();
			final int shop = getShopInventory(t);
			if(t.equals(inv.getTitle()) || shop != -1) {
                event.setCancelled(true);
                player.updateInventory();
                final int r = event.getRawSlot();
                final String c = event.getClick().name();
                if(r < 0 || r >= player.getOpenInventory().getTopInventory().getSize() || !c.contains("LEFT") && !c.contains("RIGHT") || event.getCurrentItem() == null) return;
                if(shop == -1) {
                    viewCategory(player, r);
                } else {
                    if(event.getCurrentItem().equals(back)) {
                        view(player);
                        return;
                    }
                    final double discount = getDiscount(player);
                    final String prices = config.getString(shop + "." + event.getRawSlot() + ".prices");
                    if(prices == null) {
                        return;
                    } else if(eco == null) {
                        Bukkit.broadcastMessage("[RandomPackage] Economy plugin required to use the /shop!");
                        player.closeInventory();
                    } else if(c.endsWith("LEFT") && Double.parseDouble(prices.split(";")[0]) != 0.00) {
                        double cost = Double.parseDouble(prices.split(";")[0]), dis = discount*cost;
                        cost -= dis;
                        int amountPurchased = c.equals("LEFT") ? config.getInt("click-options.left") : config.getInt("click-options.shift-left");

                        if(amountPurchased > event.getCurrentItem().getMaxStackSize()) amountPurchased = event.getCurrentItem().getMaxStackSize();
                        if(config.get(shop + "." + event.getRawSlot() + ".custom") != null) {
                            item = d(config, shop + "." + event.getRawSlot() + ".custom", 0).clone();
                            item.setAmount(amountPurchased);
                        } else {
                            item = new ItemStack(event.getCurrentItem().getType(), amountPurchased, event.getCurrentItem().getData().getData());
                            if(item.getType().name().contains("POTION")) {
                                PotionMeta meta = (PotionMeta) event.getCurrentItem().getItemMeta(); lore.clear(); meta.setLore(lore); meta.setDisplayName(null);
                                if(v.contains("1.8")) item = Potion.fromItemStack(event.getCurrentItem()).toItemStack(amountPurchased);
                                if(item.getType().name().contains("SPLASH")) item.setType(Material.getMaterial("SPLASH_POTION"));
                                item.setItemMeta(meta);
                            }
                        }
                        boolean purchased = false;
                        if(eco.withdrawPlayer((Player) event.getWhoClicked(), cost * amountPurchased).transactionSuccess()) {
                            purchased = true;
                            if(item.getType().equals(UMaterial.valueOf("SPAWNER").getMaterial())) giveSpawner(player, shop + "." + event.getRawSlot() + ".item", config, amountPurchased);
                            else                                  giveItem((Player) event.getWhoClicked(), item);
                        }
                        playSound(sounds, "shop." + (purchased ? "buy" : "not-enough-balance"), player, player.getLocation(), false);
                        for(String string : config.getStringList("messages.purchase" + (purchased ? "" : "-incomplete")) ) {
                            if(string.contains("{PRICE}")) string = string.replace("{PRICE}", formatDouble((purchased ? amountPurchased : 1) * cost));
                            if(string.contains("{AMOUNT}")) string = string.replace("{AMOUNT}", "" + amountPurchased);
                            if(string.contains("{ITEM}")) string = string.replace("{ITEM}", item.getType().name());
                            event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', string));
                        }
                    } else if(event.getClick().name().endsWith("RIGHT") && Double.parseDouble(prices.split(";")[1]) != 0.00) {
                        item = new ItemStack(event.getCurrentItem().getType(), 1, event.getCurrentItem().getData().getData());
                        String p = "messages.sell" + (!event.getWhoClicked().getInventory().containsAtLeast(item, 1) ? "-incomplete" : "");
                        final double price = Double.parseDouble(prices.split(";")[1]);
                        int amountSold = c.equals("RIGHT") ? config.getInt("click-options.right") : config.getInt("click-options.shift-right");

                        if(!event.getWhoClicked().getInventory().containsAtLeast(item, 1)) {
                            playSound(sounds, "shop.not-enough-to-sell", player, player.getLocation(), false);
                        } else {
                            final int has = getTotalAmount(player.getInventory(), item.getType(), item.getData().getData());
                            amountSold = amountSold > has ? has : amountSold;
                            if(event.getWhoClicked().getInventory().containsAtLeast(item, amountSold)) {
                                eco.depositPlayer(player, price * amountSold);
                                removeItem(player, item, amountSold);
                            } else {
                                p = "messages.sell-incomplete";
                            }
                        }
                        final String n = UMaterial.matchUMaterial(item.getType().name(), item.getData().getData()).name(), pr = formatDouble(price), amts = formatInt(amountSold), ttl = formatDouble((amountSold*price));
                        for(String string : config.getStringList(p)) {
                            if(string.contains("{TOTAL_REVENUE}")) string = string.replace("{TOTAL_REVENUE}", ttl);
                            if(string.contains("{AMOUNT}")) string = string.replace("{AMOUNT}", amts);
                            if(string.contains("{PRICE}")) string = string.replace("{PRICE}", pr);
                            if(string.contains("{ITEM}")) string = string.replace("{ITEM}", n);
                            event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', string));
                        }
                    }
                    return;
                }
            }
        }
	}
	public int getShopInventory(String title) {
        for(int i = 0; i < 54; i++) {
            final Inventory ii = categories[i];
            if(ii != null && title.equals(ii.getTitle())) return i;
        }
        return -1;
    }
	public void view(Player player) {
	    if(hasPermission(player, "RandomPackage.shop", true)) {
            player.closeInventory();
            scheduler.scheduleSyncDelayedTask(randompackage, () -> {
                player.openInventory(Bukkit.createInventory(player, inv.getSize(), inv.getTitle()));
                player.getOpenInventory().getTopInventory().setContents(inv.getContents());
                player.updateInventory();
            }, 0);
        }
	}
	public void viewCategory(Player player, int rawslot) {
		if(categories[rawslot] != null) {
			final String perm = config.get("menu." + rawslot + ".perm") != null ? config.getString("menu." + rawslot + ".perm") : null;
			if(perm == null || perm != null && perm.equalsIgnoreCase("none") || perm != null & player.hasPermission(perm)) {
			    player.closeInventory();
				player.openInventory(Bukkit.createInventory(player, categories[rawslot].getSize(), categories[rawslot].getTitle()));
				player.getOpenInventory().getTopInventory().setContents(categories[rawslot].getContents());
				final double discount = getDiscount(player);
				final int LC = config.getInt("click-options.left"), RC = config.getInt("click-options.right"), SLC = config.getInt("click-options.shift-left"), SRC = config.getInt("click-options.shift-right");
				final List<String> buylore = config.getStringList("lores.purchase"), selllore = config.getStringList("lores.sell");
				for(int i = 0; i < player.getOpenInventory().getTopInventory().getSize(); i++) {
				    item = player.getOpenInventory().getTopInventory().getItem(i);
				    if(item != null && !item.equals(back)) {
				        item = item.clone();
                        itemMeta = item.getItemMeta(); lore.clear();
                        if(itemMeta.hasLore()) lore.addAll(itemMeta.getLore());
                        boolean buy = false, sell = false;
                        final String prices = config.getString(rawslot + "." + i + ".prices");
                        if(prices != null) {
                            double buyp = Double.parseDouble(prices.split(";")[0]), sellp = Double.parseDouble(prices.split(";")[1]);
                            buyp -= buyp*discount;
                            if(buyp >= 0.00) {
                                final String b = formatDouble(round(buyp, 2));
                                buy = true;
                                for(String string : buylore) {
                                    if(string.contains("{BUY}")) string = string.replace("{BUY}", b);
                                    lore.add(ChatColor.translateAlternateColorCodes('&', string));
                                }
                            }
                            if(sellp != 0.00) {
                                sell = true;
                                for(String string : selllore) {
                                    if(string.contains("{SELL}")) string = string.replace("{SELL}", formatDouble(sellp));
                                    lore.add(ChatColor.translateAlternateColorCodes('&', string));
                                }
                            }
                        }
                        int leftclick = LC, rightclick = RC, shiftleftclick = SLC, shiftrightclick = SRC;
                        for(int d = 1; d <= 2; d++) {
                            if(d == 1 && buy && shiftleftclick > item.getMaxStackSize())        shiftleftclick = item.getMaxStackSize();
                            else if(d == 2 && sell && shiftrightclick > item.getMaxStackSize()) shiftrightclick = item.getMaxStackSize();
                            final String q = d == 1 ? "LEFT" : "RIGHT";
                            if(d == 1 && buy || d == 2 && sell)
                                for(String string : config.getStringList("lores." + q.toLowerCase() + "-clicks")) {
                                    if(string.contains("{" + q + "_CLICK}")) string = string.replace("{" + q + "_CLICK}", "" + (d == 1 ? leftclick : rightclick));
                                    if(string.contains("{SHIFT_" + q + "_CLICK}")) string = string.replace("{SHIFT_" + q + "_CLICK}", "" + (d == 1 ? shiftleftclick : shiftrightclick));
                                    lore.add(ChatColor.translateAlternateColorCodes('&', string));
                                }
                        }
                        itemMeta.setLore(lore); lore.clear();
                        item.setItemMeta(itemMeta);
				        player.getOpenInventory().getTopInventory().setItem(i, item);
                    }
                }
				player.updateInventory();
			} else
				sendStringListMessage(player, config.getStringList("messages.no-permission"));
		}
	}
}

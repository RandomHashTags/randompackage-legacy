package me.randomHashTags.RandomPackage.api;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RandomPackageAPI;

public class Trade extends RandomPackageAPI implements Listener, CommandExecutor {
	public boolean isEnabled = false;
	private static Trade instance;
	public static final Trade getTrade() {
		if(instance == null) instance = new Trade();
		return instance;
	}

	public YamlConfiguration config;
	private int radius = 0, countdown = 0;
	private String title = null;
	private Inventory q = null;
	private ItemStack divider, accept, accepting;
	
	private ArrayList<Player> accepttrade = new ArrayList<>();
	// <Sender>,<Receiver>
	private HashMap<UUID, UUID> trades = new HashMap<>();
	private HashMap<Player, Player> tradingWith = new HashMap<>();


	public void enable() {
		final long started = System.currentTimeMillis();
		if(isEnabled) return;
		save("Features", "trade.yml");
		pluginmanager.registerEvents(this, randompackage);
		isEnabled = true;
		config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, "trade.yml"));
		divider = d(config, "gui.divider", 0);
		accept = d(config, "gui.accept", 0);
		accepting = d(config, "gui.accepting", 0);
		countdown = config.getInt("gui.countdown-start");
		radius = config.getInt("radius");
		title = ChatColor.translateAlternateColorCodes('&', config.getString("gui.title"));
		q = Bukkit.createInventory(null, 54, title);
		accept.setAmount(countdown); accepting.setAmount(countdown);
		q.setItem(0, accept); q.setItem(8, accept);
		for(int i = 4; i < 54; i += 9) q.setItem(i, divider);
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded Trade &e(took " + (System.currentTimeMillis()-started) + "ms)"));
	}
	public void disable() {
		if(!isEnabled) return;
		accepttrade.clear();
		trades.clear();
		for(Player player : tradingWith.keySet()) player.closeInventory();
		tradingWith.clear();
		isEnabled = false;
		HandlerList.unregisterAll(this);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(args.length == 0)
			sendStringListMessage(player, config.getStringList("messages.commands"));
		else if(args.length == 1) {
			sendRequest(player, args[0]);
		}
		return true;
	}
	
	
	public void sendRequest(Player sender, String receiver) {
		if(hasPermission(sender, "RandomPackage.trade", true)) {
			if(receiver == null || Bukkit.getPlayer(receiver) == null || Bukkit.getPlayer(receiver) != null && !Bukkit.getPlayer(receiver).isOnline()
					|| radius != -1 && sender.getWorld() != Bukkit.getPlayer(receiver).getWorld()
					|| radius != -1 && radius != -2 && sender.getLocation().distance(Bukkit.getPlayer(receiver).getLocation()) > radius
			)
				sendStringListMessage(sender, config.getStringList("messages.not-within-range"), receiver);
			else if(sender == Bukkit.getPlayer(receiver))
				sendStringListMessage(sender, config.getStringList("messages.send-self"));
			else {
				final UUID s = sender.getUniqueId();
				final Player target = Bukkit.getPlayer(receiver);
				if(trades.keySet().contains(target.getUniqueId()) && trades.get(target.getUniqueId()).equals(s)) {
					acceptRequest(target, sender);
				} else {
					sendStringListMessage(sender, config.getStringList("messages.send-request"), target.getName());
					sendStringListMessage(target, config.getStringList("messages.receive-request"), sender.getName());
					trades.put(s, target.getUniqueId());
					scheduler.scheduleSyncDelayedTask(randompackage, () -> {
						if(trades.keySet().contains(s) && trades.get(s).equals(target.getUniqueId()))
							trades.remove(s);
					}, 20 * 10);
				}
			}
		}
	}
	public void acceptRequest(Player accepter, Player requester) {
		trades.remove(requester.getUniqueId());
		tradingWith.put(accepter, requester);
		tradingWith.put(requester, accepter);
		Inventory inv1 = Bukkit.createInventory(requester, 54, title.replace("{PLAYER}", requester.getName())), inv2 = Bukkit.createInventory(accepter, 54, title.replace("{PLAYER}", accepter.getName()));
		inv1.setContents(q.getContents()); inv2.setContents(q.getContents());
		accepter.openInventory(inv1);
		requester.openInventory(inv2);
	}
	@EventHandler
	private void inventoryCloseEvent(InventoryCloseEvent event) {
		Player player = (Player) event.getPlayer(), other = null;
		if(tradingWith.keySet().contains(player)) {
			other = (Player) event.getInventory().getHolder();
		} else return;
		sendStringListMessage(player, config.getStringList("messages.cancelled"));
		sendStringListMessage(other, config.getStringList("messages.cancelled"));
		final Player o = other;
		tradingWith.keySet().remove(player);
		tradingWith.keySet().remove(other);
		giveItems(player, true);
		giveItems(other, true);
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> o.closeInventory(), 1);
		stopCountdown(player);
	}
	@EventHandler
	private void inventoryClickEvent(InventoryClickEvent event) {
		if(!event.isCancelled() && event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) {
			boolean d = false;
			if(tradingWith.keySet().contains(event.getWhoClicked())) {
				d = true;
			} else {
				for(int i = 0; i < tradingWith.size(); i++)
					if(tradingWith.get(i) != null && tradingWith.get(i).equals(event.getWhoClicked()))
						d = true;
			}
			if(d) {
				final Player player = (Player) event.getWhoClicked();
				event.setCancelled(true);
				player.updateInventory();
				if(event.getCurrentItem().equals(divider)
						|| event.getCurrentItem().getItemMeta().equals(accept.getItemMeta())
						|| event.getCurrentItem().getItemMeta().equals(accepting.getItemMeta())) {
					if(event.getRawSlot() == 0)
						acceptTrade(player);
					return;
				}
				if(event.getRawSlot() >= event.getWhoClicked().getOpenInventory().getTopInventory().getSize()) {
					if(accepttrade.contains(player)) stopCountdown(player);
					putItem(player, event.getCurrentItem());
					event.setCurrentItem(new ItemStack(Material.AIR));
				} else {
					boolean e = false;
					for(int i = 0; i < yours.split(",").length; i++)
						if(Integer.parseInt(yours.split(",")[i]) == event.getRawSlot())
							e = true;
					if(e) takeBackItem(player, event.getRawSlot());
				}
			}
		}
	}
	private final String yours = "1,2,3,"
							+ "9,10,11,12,"
							+ "18,19,20,21,"
							+ "27,28,29,30,"
							+ "36,37,38,39,"
							+ "45,46,47,48",
				others = "5,6,7,"
							+ "14,15,16,17,"
							+ "23,24,25,26,"
							+ "32,33,34,35,"
							+ "41,42,43,44,"
							+ "50,51,52,53";
	private void giveItems(Player player, boolean self) {
		String type = yours;
		if(!self) type = others;
		for(int i = 0; i < type.split("\\,").length; i++) {
			final int f = Integer.parseInt(type.split("\\,")[i]);
			if(player.getOpenInventory().getTopInventory().getItem(f) != null)
				giveItem(player, player.getOpenInventory().getTopInventory().getItem(f));
		}
	}
	private void acceptTrade(Player player) {
		final Player other = (Player) player.getOpenInventory().getTopInventory().getHolder();
		item = new ItemStack(Material.APPLE, 1); itemMeta = item.getItemMeta(); item.setItemMeta(itemMeta);
		if(accepttrade.contains(player)) {
			accepttrade.remove(player);
			item = accept.clone();
			stopCountdown(player);
		} else {
			accepttrade.add(player);
			item = accepting.clone();
		}
		player.getOpenInventory().getTopInventory().setItem(0, item);
		other.getOpenInventory().getTopInventory().setItem(8, item);
		if(player.getOpenInventory().getTopInventory().getItem(0).getItemMeta().equals(accepting.getItemMeta())
				&& player.getOpenInventory().getTopInventory().getItem(8).getItemMeta().equals(accepting.getItemMeta())) {
			beginCountdown(player);
		}
	}
	private void putItem(Player player, ItemStack is) {
		Player other = (Player) player.getOpenInventory().getTopInventory().getHolder();
		int empty = -1;
		for(int i = 0; i < yours.split("\\,").length; i++)
			if(empty == -1 && player.getOpenInventory().getTopInventory().getItem(Integer.parseInt(yours.split("\\,")[i])) == null)
				empty = Integer.parseInt(yours.split("\\,")[i]);
		if(empty < 0) return;
		player.getOpenInventory().getTopInventory().setItem(empty, is);
		other.getOpenInventory().getTopInventory().setItem(empty + (empty < 4 ? 4 : 5), is);
		player.updateInventory();
		other.updateInventory();
	}
	private void takeBackItem(Player player, int rawslot) {
		Player other = (Player) player.getOpenInventory().getTopInventory().getHolder();
		giveItem(player, player.getOpenInventory().getTopInventory().getItem(rawslot));
		player.getOpenInventory().getTopInventory().setItem(rawslot, new ItemStack(Material.AIR));
		other.getOpenInventory().getTopInventory().setItem(rawslot + (rawslot < 4 ? 4 : 5), new ItemStack(Material.AIR));
		player.updateInventory();
		other.updateInventory();
	}
	private HashMap<Player, ArrayList<Integer>> schedulers = new HashMap<>();
	private void stopCountdown(Player player) {
		Player other = (Player) player.getOpenInventory().getTopInventory().getHolder();
		if(schedulers.keySet().contains(player) && schedulers.keySet().contains(other)) {
			for(int i : schedulers.get(player)) Bukkit.getScheduler().cancelTask(i);
			for(int i : schedulers.get(other)) Bukkit.getScheduler().cancelTask(i);
			schedulers.keySet().remove(player);
			schedulers.keySet().remove(other);
		}
		if(tradingWith.keySet().contains(player) && tradingWith.keySet().contains(other)) {
			player.getOpenInventory().getTopInventory().setItem(0, accept.clone());
			player.getOpenInventory().getTopInventory().setItem(8, accept.clone());
			other.getOpenInventory().getTopInventory().setItem(0, accept.clone());
			other.getOpenInventory().getTopInventory().setItem(8, accept.clone());
		}
	}
	private void beginCountdown(Player player) {
		Player other = (Player) player.getOpenInventory().getTopInventory().getHolder();
		schedulers.put(player, new ArrayList<>());
		schedulers.put(other, new ArrayList<>());
		for(int i = 1; i <= countdown; i++) {
			int q = Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> {
                if(player.getOpenInventory().getTopInventory().getItem(0).getAmount() == 1 && other.getOpenInventory().getTopInventory().getItem(0).getAmount() == 1) {
                    giveItems(player, false);
                    giveItems(other, false);
                    tradingWith.keySet().remove(player);
                    tradingWith.keySet().remove(other);
                    player.closeInventory();
                    other.closeInventory();
                    sendStringListMessage(player, config.getStringList("messages.accepted"));
                    sendStringListMessage(other, config.getStringList("messages.accepted"));
                    stopCountdown(player);
                } else {
                    player.getOpenInventory().getTopInventory().getItem(0).setAmount(player.getOpenInventory().getTopInventory().getItem(0).getAmount() - 1);
                    player.getOpenInventory().getTopInventory().getItem(8).setAmount(player.getOpenInventory().getTopInventory().getItem(8).getAmount() - 1);
                    other.getOpenInventory().getTopInventory().getItem(0).setAmount(other.getOpenInventory().getTopInventory().getItem(0).getAmount() - 1);
                    other.getOpenInventory().getTopInventory().getItem(8).setAmount(other.getOpenInventory().getTopInventory().getItem(8).getAmount() - 1);
                }
			}, i * 20);
			schedulers.get(player).add(q);
			schedulers.get(other).add(q);
		}
	}

	private void sendStringListMessage(Player player, List<String> message, String extra) {
		if(message != null && !message.isEmpty()) {
			if(message.size() >= 1 && !message.get(0).equals("")) {
				for(String string : message) {
					if(string.contains("{TARGET}")) string = string.replace("{TARGET}", "" + extra);
					if(string.contains("{SENDER}")) string = string.replace("{SENDER}", "" + extra);
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
				}
			}
		}
	}
}

package me.randomHashTags.RandomPackage.utils.classes.kits;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.randomHashTags.RandomPackage.api.Kits;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.universal.UVersion;

public class FallenHero extends UVersion {
	private static final RandomPackageAPI api = RandomPackageAPI.getAPI();
	public static final List<FallenHero> heroes = new ArrayList<>();
	private static final HashMap<UUID, FallenHero> livingFH = new HashMap<>();
	private static final Kits kits = Kits.getKits();

	public final Object globalkitORevolutionkit;
	private final ItemStack spawnitem, gem;
	private final String path, type, spawnloc;
	private final int gemDropChance;
	public FallenHero(String path, String spawnloc, Object globalkitORevolutionkit, String type, ItemStack spawnitem, int gemDropChance, ItemStack gem) {
		this.path = path;
		this.spawnloc = spawnloc;
		this.globalkitORevolutionkit = globalkitORevolutionkit;
		this.type = type;
		this.spawnitem = spawnitem;
		this.gemDropChance = gemDropChance;
		this.gem = gem;
		heroes.add(this);
	}
	public String getPath() { return path; }
	public String getSpawnLocation() { return spawnloc; }
	public GlobalKit getGlobalKit() { return globalkitORevolutionkit instanceof GlobalKit ? (GlobalKit) globalkitORevolutionkit : null; }
	public EvolutionKit getEvolutionKit() { return globalkitORevolutionkit instanceof EvolutionKit ? (EvolutionKit) globalkitORevolutionkit : null; }
	public String getType() { return type; }
	public ItemStack getSpawnItem() { return spawnitem.clone(); }
	public int getGemDropChance() { return gemDropChance; }
	public ItemStack getGem() { return gem.clone(); }
	public void kill(LivingEntity fallenhero, LivingEntity killer) {
		livingFH.remove(fallenhero.getUniqueId());
		final Location l = fallenhero.getLocation();
		final GlobalKit gkit = getGlobalKit();
		final EvolutionKit vkit = getEvolutionKit();
		final String n = gkit != null ? gkit.path : vkit.path;
		final YamlConfiguration yml = gkit != null ? kits.gkits : kits.vkits;

		if(random.nextInt(100) <= gemDropChance) {
			for(String s : yml.getStringList("messages.receive-kit")) {
				if(s.contains("{PLAYER}")) s = s.replace("{PLAYER}", killer.getName());
				if(s.contains("{NAME}")) s = s.replace("{NAME}", getFallenHeroName());
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', s));
			}
			fallenhero.getWorld().dropItem(fallenhero.getLocation(), gem);
		} else {
			final World w = fallenhero.getWorld();
			final List<KitItem> items = new ArrayList<>(gkit != null ? gkit.items : vkit.items);
			w.dropItem(l, api.d(yml, "kits." + n + ".items." + items.get(random.nextInt(items.size())).path, random.nextInt(gkit != null ? gkit.maxTier : vkit.maxLevel)));
		}
	}
	public String getFallenHeroName() {
		final GlobalKit gkit = getGlobalKit();
		final FileConfiguration config = gkit != null ? kits.gkits : kits.vkits;
		String n = gkit != null ? (gkit.isHeroic ? "" : "") + gkit.getDisplayedItem().getItemMeta().getDisplayName() : getEvolutionKit().getDisplayedItem().getItemMeta().getDisplayName();
		if(gkit != null && gkit.isHeroic)
			n = ChatColor.translateAlternateColorCodes('&', config.getString("items.heroic.prefix").replace("{NAME}", n));
		return n;
	}
	public void spawn(LivingEntity summoner, Location loc) {
		if(globalkitORevolutionkit == null) return;
		final GlobalKit gkit = globalkitORevolutionkit instanceof GlobalKit ? (GlobalKit) globalkitORevolutionkit : null;
		final EvolutionKit vkit = globalkitORevolutionkit instanceof EvolutionKit ? (EvolutionKit) globalkitORevolutionkit : null;
		if(gkit == null && vkit == null) return;
		final String N = gkit != null ? gkit.path : vkit.path, fh = gkit != null ? gkit.fallenhero : vkit.getFallenHero();
		final FileConfiguration config = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features" + File.separator, (gkit != null ? "global" : "evolution") + " kits.yml"));
		final LivingEntity l = getEntity(type, loc, true);
		final String n = getFallenHeroName();
		l.setCustomName(n);
		l.setCustomNameVisible(true);
		for(String s : config.getStringList("fallen-heroes." + fh + ".potion-effects")) {
			
		}
		for(int i = 1; i <= 4; i++) {
			final String s = i == 1 ? "helmet" : i == 2 ? "chestplate" : i == 3 ? "leggings" : i == 4 ? "boots" : "null";
			boolean did = false;
			for(int z = 0; z <= 100; z++) {
				if(config.get("kits." + N + "." + z + ".item") != null && !did) {
					ItemStack h = null;
					if(config.getString("fallen-heroes." + path + ".equipment." + s + ".item").toUpperCase().equals("{" + s.toUpperCase() + "}")) {
						if(config.getString("kits." + N + "." + z + ".item").toLowerCase().split(":")[0].contains(path.toLowerCase())) {
							h = api.d(config, "kits." + N + "." + z, 0);
						}
					}
					if(h != null) {
						did = true;
						giveItem((Player) summoner, h);
						if(i == 1) l.getEquipment().setHelmet(h);
						else if(i == 2) l.getEquipment().setChestplate(h);
						else if(i == 3) l.getEquipment().setLeggings(h);
						else if(i == 4) l.getEquipment().setBoots(h);
					}
				}
			}
		}
		if(l instanceof Creature) ((Creature) l).setTarget(summoner);
		livingFH.put(l.getUniqueId(), this);
		for(String s : config.getStringList("messages.summon")) {
			if(s.contains("{NAME}")) s = s.replace("{NAME}", n);
			summoner.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
		}
	}
	public static FallenHero valueOf(ItemStack spawnitem) {
		if(spawnitem != null && spawnitem.hasItemMeta())
			for(FallenHero h : heroes)
				if(h.getSpawnItem().getItemMeta().equals(spawnitem.getItemMeta())) return h;
		return null;
	}
	public static FallenHero valueOF(ItemStack gem) {
		if(gem != null && gem.hasItemMeta())
			for(FallenHero h : heroes)
				if(h.getGem().getItemMeta().equals(gem.getItemMeta())) return h;
		return null;
	}
	public static FallenHero valueOf(UUID uuid) {
		for(UUID u : livingFH.keySet()) if(u == uuid) return livingFH.get(uuid);
		return null;
	}
	public static FallenHero valueOf(GlobalKit gkit) {
		for(FallenHero f : heroes) if(f.getGlobalKit() == gkit) return f;
		return null;
	}
	public static FallenHero valueOf(EvolutionKit vkit) {
		for(FallenHero f : heroes) if(f.getEvolutionKit() == vkit) return f;
		return null;
	}
}

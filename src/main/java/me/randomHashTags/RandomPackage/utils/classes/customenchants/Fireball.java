package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

public class Fireball {
	public static final List<Fireball> fireballs = new ArrayList<>();
	
	private final int number;
	private final ItemStack is;
	private final List<EnchantRarity> exchangeablerarities;
	private final List<MagicDust> revealsdust;
	public Fireball(int number, ItemStack is, List<EnchantRarity> exchangeablerarities, List<MagicDust> revealsdust) {
		this.number = number;
		this.is = is;
		this.exchangeablerarities = exchangeablerarities;
		this.revealsdust = revealsdust;
		fireballs.add(this);
	}
	public int getNumberInConfig() { return number; }
	public ItemStack getItemStack() { return is.clone(); }
	public List<EnchantRarity> getExchangeableRarities() { return exchangeablerarities; }
	public List<MagicDust> getRevealsDust() { return revealsdust; }
	
	public static Fireball valueOf(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore()) {
			for(Fireball f : fireballs) if(is.getItemMeta().equals(f.getItemStack().getItemMeta())) return f;
		}
		return null;
	}
	public static Fireball valueOf(int number) {
		for(Fireball f : fireballs) if(f.getNumberInConfig() == number) return f;
		return null;
	}
	public static Fireball valueOf(List<EnchantRarity> appliestorarities) {
		for(Fireball f : fireballs) if(f.getExchangeableRarities().equals(appliestorarities)) return f;
		return null;
	}
}

package me.randomHashTags.RandomPackage.utils.classes.kits;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

public class GlobalKit {
	public static final List<GlobalKit> kits = new ArrayList<>();
	public final String path, fallenhero;
	public final int slot, maxTier, cooldown;
	private final ItemStack gui;
	public final boolean isHeroic;
	public List<KitItem> items;
	public GlobalKit(String path, int slot, int maxTier, int cooldown, ItemStack gui, boolean isHeroic, String fallenhero) {
		this.path = path;
		this.slot = slot;
		this.maxTier = maxTier;
		this.cooldown = cooldown;
		this.gui = gui;
		this.isHeroic = isHeroic;
		this.fallenhero = fallenhero;
		kits.add(this);
	}
	public GlobalKit(String path, int slot, int maxTier, int cooldown, ItemStack gui, boolean isHeroic, List<KitItem> items, String fallenhero) {
		this.path = path;
		this.slot = slot;
		this.maxTier = maxTier;
		this.cooldown = cooldown;
		this.gui = gui;
		this.isHeroic = isHeroic;
		this.items = items;
		this.fallenhero = fallenhero;
		kits.add(this);
	}
	public ItemStack getDisplayedItem() { return gui.clone(); }

	public static GlobalKit valueOf(int slot) {
		for(GlobalKit k : kits) if(k.slot == slot) return k;
		return null;
	}
	public static GlobalKit valueOf(String path) {
		for(GlobalKit k : kits) if(k.path.equals(path)) return k;
		return null;
	}
}

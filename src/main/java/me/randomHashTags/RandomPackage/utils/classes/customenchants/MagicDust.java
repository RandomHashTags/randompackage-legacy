package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

public class MagicDust {
	private final ItemStack is;
	private final int number, chance, minpercent, maxpercent, upgradesto, upgradecost;
	private final List<EnchantRarity> appliesto;
	public static List<MagicDust> dust = new ArrayList<MagicDust>();
	public MagicDust(int number, ItemStack is, int chance, int minpercent, int maxpercent, List<EnchantRarity> appliesto, int upgradesto, int upgradecost) {
		this.number = number;
		this.is = is;
		this.chance = chance;
		this.minpercent = minpercent;
		this.maxpercent = maxpercent;
		this.appliesto = appliesto;
		this.upgradesto = upgradesto;
		this.upgradecost = upgradecost;
		dust.add(this);
	}
	public int getNumberInConfig() { return number; }
	public ItemStack getItemStack() { return is.clone(); }
	public int getChance() { return chance; }
	public int getMinPercent() { return minpercent; }
	public int getMaxPercent() { return maxpercent; }
	public List<EnchantRarity> getAppliesTo() { return appliesto; }
	public int getUpgradesToNumber() { return upgradesto; }
	public int getUpgradeCost() { return upgradecost; }
	
	public static MagicDust valueOf(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore()) {
			for(MagicDust dust : dust) {
				ItemStack i = dust.getItemStack();
				if(i.getType().equals(is.getType()) && is.getItemMeta().getDisplayName().equals(i.getItemMeta().getDisplayName())) return dust;
			}
		}
		return null;
	}
	public static MagicDust valueOf(int number) {
		for(MagicDust d : dust)
			if(d.getNumberInConfig() == number) return d;
		return null;
	}
}

package me.randomHashTags.RandomPackage.utils.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.randomHashTags.RandomPackage.RandomPackageAPI;

public class MonthlyCrate {
	private static final RandomPackageAPI api = RandomPackageAPI.getAPI();
	public static final List<MonthlyCrate> monthlycrates = new ArrayList<>();
	public static final HashMap<UUID, List<String>> revealing = new HashMap<>();
	private static final Random random = new Random();
	public final String path;
	private final ItemStack is;
	public final Inventory regularInv, bonusInv;
	public final List<String> rewards, bonusRewards, redeemFormat, redeemBonusFormat;
	public final List<Integer> rewardSlots, bonusRewardSlots;
	private final boolean canDupeRewards;
	
	public MonthlyCrate(String path, ItemStack is, Inventory regularInv, Inventory bonusInv, List<String> rewards, List<String> bonusRewards, List<String> redeemFormat, List<String> redeemBonusFormat, boolean canDupeRewards) {
		this.path = path;
		this.is = is;
		this.regularInv = regularInv;
		this.bonusInv = bonusInv;
		this.rewards = rewards;
		this.bonusRewards = bonusRewards;
		this.canDupeRewards = canDupeRewards;
		final ArrayList<Integer> rewardslots = new ArrayList<>(), bonusrewardslots = new ArrayList<>();
		for(int i = 0; i < redeemFormat.size(); i++) {
			for(int o = 0; o < redeemFormat.get(i).length(); o++) {
				final int slot = (i*9)+o;
				final String a = redeemFormat.get(i).substring(o, o+1);
				if(a.equals("-"))
					rewardslots.add(slot);
				else if(a.equals("+"))
					bonusrewardslots.add(slot);
			}
		}
		this.rewardSlots = rewardslots;
		this.bonusRewardSlots = bonusrewardslots;
		this.redeemFormat = redeemFormat;
		this.redeemBonusFormat = redeemBonusFormat;
		monthlycrates.add(this);
	}

	public ItemStack getItemStack() { return is.clone(); }
	public ItemStack getRandomReward(Player player, boolean inMonthlyCrate, boolean bonus) {
		final UUID u = player.getUniqueId();
		List<String> availableRewards = new ArrayList<>();
		availableRewards.addAll(bonus ? bonusRewards : rewards);
		if(availableRewards.size() == 0) return new ItemStack(Material.AIR);
		if(inMonthlyCrate && !canDupeRewards) {
			if(!revealing.keySet().contains(u)) revealing.put(u, new ArrayList<>());
			for(String i : revealing.get(u)) availableRewards.remove(i);
		}
		final int n = random.nextInt(availableRewards.size());
		String reward = availableRewards.get(n);
		if(inMonthlyCrate && !canDupeRewards) revealing.get(u).add(reward);
		if(reward.contains("||")) reward = reward.split("\\|\\|")[random.nextInt(reward.split("\\|\\|").length)];
		return api.d(null, reward, 0);
	}
	public static MonthlyCrate valueOf(int slot) {
		if(slot < monthlycrates.size()) return monthlycrates.get(slot);
		return null;
	}
	public static MonthlyCrate valueOf(ItemStack is) {
		for(MonthlyCrate mc : monthlycrates)
			if(is.getItemMeta().equals(mc.getItemStack().getItemMeta()))
				return mc;
		return null;
	}
	public static MonthlyCrate valueOf(String unlockedBy, ItemStack is) {
		for(MonthlyCrate mc : monthlycrates) {
			final ItemMeta m = mc.getItemStack().getItemMeta();
			List<String> lore = new ArrayList<>();
			if(m.hasLore()) {
				for(String s : m.getLore()) lore.add(s.replace("{UNLOCKED_BY}", unlockedBy));
				m.setLore(lore);
			}
			if(is.getItemMeta().equals(m)) return mc;
		}
		return valueOf(is);
	}
	public static MonthlyCrate valueOf(String titleORname) {
		for(MonthlyCrate mc : monthlycrates)
			if(mc.path.equals(titleORname) || mc.regularInv.getTitle().equals(titleORname) || mc.bonusInv.getTitle().equals(titleORname)) return mc;
		return null;
	}
}

package me.randomHashTags.RandomPackage.utils.classes.duels;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QueuedDuel {
    private static final Random random = new Random();
    public static final List<QueuedDuel> unranked = new ArrayList<>(), ranked = new ArrayList<>();
    public final Player player;
    public final DuelSettings settings;
    public final DuelType type;
    public QueuedDuel(Player player, DuelSettings settings, DuelType type) {
        this.player = player;
        this.settings = settings;
        this.type = type;
        if(type.equals(DuelType.RANKED)) ranked.add(this);
        else if(type.equals(DuelType.UNRAKNED)) unranked.add(this);
        tryStartDuel(type);
    }
    private void tryStartDuel(DuelType type) {
        final List<QueuedDuel> queue = type.equals(DuelType.RANKED) ? ranked : unranked;
        if(queue.size() == 2) {
            queue.remove(this);
            startDuel(queue.get(random.nextInt(queue.size())));
        }
    }
    private void startDuel(QueuedDuel opponent) {
        opponent.delete();
    }
    public void delete() {
        ranked.remove(this);
        unranked.remove(this);
    }
    public static QueuedDuel valueOf(Player player) {
        for(QueuedDuel q : unranked)
            if(q.player.equals(player))
                return q;
        for(QueuedDuel q : ranked)
            if(q.player.equals(player))
                return q;
        return null;
    }
}

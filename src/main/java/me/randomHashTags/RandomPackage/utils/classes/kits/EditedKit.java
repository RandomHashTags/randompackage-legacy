package me.randomHashTags.RandomPackage.utils.classes.kits;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class EditedKit {

    public static final List<EditedKit> editing = new ArrayList<>();

    public Player player;
    public List<KitItem> original, edited;
    public int selected = -1;
    public EditedKit(Player player, List<KitItem> original, List<KitItem> edited) {
        this.player = player;
        this.original = original;
        this.edited = edited;
        editing.add(this);
    }
    public void delete() {
        editing.remove(this);
    }

    public static EditedKit valueOf(Player player) {
        for(EditedKit e : editing)
            if(e.player.equals(player))
                return e;
        return null;
    }
}

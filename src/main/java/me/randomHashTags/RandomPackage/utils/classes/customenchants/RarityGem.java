package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class RarityGem {
	public static final List<RarityGem> gems = new ArrayList<>();
	private final String path;
	private final int timeBetweenSameKills;
	private final ItemStack physicalItem;
	private final List<EnchantRarity> worksFor;
	private final List<String> splitMessage, toggleon, toggleoffInteract, toggleoffDropped, toggleoffMoved, toggleoffRanOut;
	private final HashMap<Integer, String> colors;
	public RarityGem(String path, ItemStack physicalItem, List<EnchantRarity> worksFor, List<String> splitMessage, int timeBetweenSameKills, HashMap<Integer, String> colors, List<String> toggleon, List<String> toggleoffInteract, List<String> toggleoffDropped, List<String> toggleoffMoved, List<String> toggleoffRanOut) {
		this.path = path;
		this.physicalItem = physicalItem;
		this.worksFor = worksFor;
		this.splitMessage = splitMessage;
		this.timeBetweenSameKills = timeBetweenSameKills;
		this.colors = colors;
		this.toggleon = toggleon;
		this.toggleoffInteract = toggleoffInteract;
		this.toggleoffDropped = toggleoffDropped;
		this.toggleoffMoved = toggleoffMoved;
		this.toggleoffRanOut = toggleoffRanOut;
		gems.add(this);
	}
	public String getPath() { return path; }
	public ItemStack getPhysicalItem() { return physicalItem.clone(); }
	public ItemStack getPhysicalItem(int souls) {
		final ItemStack item = physicalItem.clone();
		final ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(itemMeta.getDisplayName().replace("{SOULS}", getColors(this, souls) + Integer.toString(souls)));
		item.setItemMeta(itemMeta);
		return item;
	}
	public List<EnchantRarity> getWorksFor() { return worksFor; }
	public List<String> getSplitMessage() { return splitMessage; }
	public int getTimeBetweenSameKills() { return timeBetweenSameKills; }
	public HashMap<Integer, String> getColors() { return colors; }
	public List<String> getToggleOnMsg() { return toggleon; }
	public List<String> getToggleOffInteractMsg() { return toggleoffInteract; }
	public List<String> getToggleOffDroppedMsg() { return toggleoffDropped; }
	public List<String> getToggleOffMovedMsg() { return toggleoffMoved; }
	public List<String> getToggleOffRanOutMsg() { return toggleoffRanOut; }
	
	public static RarityGem valueOf(String path) {
		for(RarityGem g : gems)
			if(g.getPath().equals(path))
				return g;
		return null;
	}
	public static RarityGem valueOf(ItemStack item) {
		if(item != null && item.hasItemMeta() && item.getItemMeta().hasLore())
			for(RarityGem g : gems)
				if(g.getPhysicalItem().getItemMeta().getLore().equals(item.getItemMeta().getLore()))
					return g;
		return null;
	}
	public String getColors(int soulsCollected) {
		final HashMap<Integer, String> colors = getColors();
		if(soulsCollected < 100) return colors.get(0);
		int last = -1;
		for(int i = 100; i <= 1000000; i += 100)
			if(soulsCollected >= i && soulsCollected < i + 100) {
				final String c = colors.get(i);
				if(c != null) last += 1;
				return c != null ? c : colors.get(last);
			}
		return colors.get(-1);
	}
	public static String getColors(RarityGem gem, int soulsCollected) {
		return gem.getColors(soulsCollected);
	}
}
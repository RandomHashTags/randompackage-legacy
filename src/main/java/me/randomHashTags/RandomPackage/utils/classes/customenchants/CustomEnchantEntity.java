package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.api.CustomEnchants;

@SuppressWarnings({"unchecked"})
public class CustomEnchantEntity extends CustomEnchants {
	public static final HashMap<String, CustomEnchantEntity> entities = new HashMap<>();
	public static final HashMap<UUID, List<LivingEntity>> getLivingEntities = new HashMap<>();
	private static final HashMap<UUID, String> livingEntityTypes = new HashMap<>();
	private final EntityType type;
	private final String name, customname;
	private final List<String> attributes;
	private final boolean canTargetSummoner, dropsItemsUponDeath;
	private LivingEntity summoner;
	public CustomEnchantEntity(EntityType type, String name, String customname, List<String> attributes, boolean canTargetSummoner, boolean dropsItemsUponDeath) {
		this.type = type;
		this.name = ChatColor.translateAlternateColorCodes('&', name);
		this.customname = ChatColor.translateAlternateColorCodes('&', customname);
		this.attributes = attributes;
		this.canTargetSummoner = canTargetSummoner;
		this.dropsItemsUponDeath = dropsItemsUponDeath;
		this.summoner = null;
		entities.put(name, this);
	}
	public EntityType getEntityType() { return type; }
	public LivingEntity getSummoner() { return summoner; }
	public String getName() { return name; }
	public String getCustomName() { return customname; }
	public List<String> getAttributes() { return attributes; }
	public boolean canTargetSummoner() { return canTargetSummoner; }
	public boolean dropsItemsUponDeath() { return dropsItemsUponDeath; }
	public void spawn(LivingEntity summoner, LivingEntity target, Event event) {
		final LivingEntity le = getEntity(type.name(), summoner.getLocation(), true);
		final UUID summonerID = summoner.getUniqueId();
		le.setCustomName(customname.replace("{PLAYER}", summoner.getName()));
		le.setCanPickupItems(false);
		le.setCustomNameVisible(true);
		if(le instanceof Creature && target != null) ((Creature) le).setTarget(target);
		if(summoner instanceof Player) {
			this.summoner = summoner;
			final RPPlayerData pdata = RPPlayerData.get(summoner.getUniqueId());
			pdata.addCustomEnchantEntity(le.getUniqueId());
		}
		for(String s : getAttributes()) {
			int b = -1;
			for(String attr : s.split(";")) {
				b += 1;
				w(null, event, null, Arrays.asList(le), attr, s, b, summoner instanceof Player ? (Player) summoner : null);
				if(attr.toLowerCase().startsWith("despawn{"))
					Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, new Runnable() {
						public void run() {
							le.remove();
						}
					}, (int) oldevaluate(attr.split("\\{")[1].split("}")[0]));
			}
		}
		if(!getLivingEntities.keySet().contains(summonerID)) getLivingEntities.put(summonerID, new ArrayList<>());
		getLivingEntities.get(summonerID).add(le);
		livingEntityTypes.put(le.getUniqueId(), name);
	}
	public void setNewSummoner(LivingEntity le, Player newsummoner) {
		final UUID b = le.getUniqueId(), u = newsummoner.getUniqueId();
		getLivingEntities.remove(b);
		livingEntityTypes.remove(b);
		if(!getLivingEntities.keySet().contains(u)) getLivingEntities.put(u, new ArrayList<>());
		getLivingEntities.get(u).add(le);
		livingEntityTypes.put(le.getUniqueId(), name);
	}
	public static CustomEnchantEntity valueOf(String name) {
		for(CustomEnchantEntity e : entities.values()) if(e.getName().equals(name)) return e;
		return null;
	}
	public static CustomEnchantEntity valueOf(UUID uuid) {
		return livingEntityTypes.keySet().contains(uuid) ? entities.get(livingEntityTypes.get(uuid)) : null;
	}
	public static UUID getSummoner(LivingEntity entity) {
		for(int i = 0; i < getLivingEntities.values().size(); i++) {
			List<LivingEntity> l = (List<LivingEntity>) getLivingEntities.values().toArray()[i];
			if(l.contains(entity)) return ((UUID) getLivingEntities.keySet().toArray()[i]);
		}
		return null;
	}
}

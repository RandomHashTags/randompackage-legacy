package me.randomHashTags.RandomPackage.utils.classes.conquests;

import me.randomHashTags.RandomPackage.RandomPackage;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.api.events.conquest.ConquestDamageEvent;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.*;

public class LivingConquestChest {
    private static final PluginManager pluginmanager = Bukkit.getPluginManager();
    public static final List<LivingConquestChest> living = new ArrayList<>();
    private static final RandomPackageAPI api = RandomPackageAPI.getAPI();

    private final Random random = new Random();
    private final BukkitScheduler scheduler = Bukkit.getScheduler();
    public final Location location;
    private final int x, y, z;
    private int announceTask, despawnTask, minutes;
    public final ConquestChest type;
    public final long spawnedTime;
    public long damageDelayExpire;
    public int health;
    public String conquerer;
    public LivingConquestChest(Location location, ConquestChest type, long spawnedTime, boolean sendMessage, boolean spawnBosses) {
        this.health = type.spawnedHealth;
        this.location = location;
        this.x = location.getBlockX();
        this.y = location.getBlockY();
        this.z = location.getBlockZ();
        this.type = type;
        this.spawnedTime = spawnedTime;
        q(spawnBosses);
        if(sendMessage) send(type.spawnMsg);
    }
    public LivingConquestChest(Location location, ConquestChest type, int health, long spawnedTime, boolean sendMessage, boolean spawnBosses) {
        this.health = health;
        this.location = location;
        this.x = location.getBlockX();
        this.y = location.getBlockY();
        this.z = location.getBlockZ();
        this.type = type;
        this.spawnedTime = spawnedTime;
        q(spawnBosses);
        if(sendMessage) send(type.spawnMsg);
    }
    private void q(boolean spawnBosses) {
        final World w = location.getWorld();
        final Block b = w.getBlockAt(location);
        b.setType(type.placedBlock.getType());
        b.getState().update();
        living.add(this);
        final int a = type.announceIntervalAfterSpawned, repeat = a*60*20;
        announceTask = scheduler.scheduleSyncRepeatingTask(RandomPackage.getPlugin, () -> {
            minutes += a;
            send(type.stillAliveMsg);
        }, repeat, repeat);
        despawnTask = scheduler.scheduleSyncDelayedTask(RandomPackage.getPlugin, () -> {
            delete(false);
        }, type.despawnDelay*20*60);
        if(spawnBosses) {
            for(ConquestMob c : type.spawnedBosses.keySet()) {
                final String target = type.spawnedBosses.get(c);
                final int min = target.contains("-") ? Integer.parseInt(target.split("-")[0]) : Integer.parseInt(target), max = target.contains("-") ? Integer.parseInt(target.split("-")[1]) : 0, amount = target.contains("-") ? min+random.nextInt(max-min+1) : min;
                for(int i = 1; i <= amount; i++)
                    c.spawn(location);
            }
        }
    }
    private void send(List<String> msg) {
        for(String s : msg) {
            s = ChatColor.translateAlternateColorCodes('&', s.replace("{MIN}", Integer.toString(minutes)).replace("{X}", Integer.toString(x)).replace("{Y}", Integer.toString(y)).replace("{Z}", Integer.toString(z)).replace("{HP}", Integer.toString(health)).replace("{MAX_HP}", Integer.toString(type.maxHealth)));
            Bukkit.broadcastMessage(s);
        }
    }
    public void damage(Player player, Location l, double damage, boolean callEvent) {
        final long t = System.currentTimeMillis();
        final double d = type.dmgDelay;
        if(d <= 0 || t >= damageDelayExpire) {
            if(d > 0) damageDelayExpire = (long) (t+(d/20*1000));
            ConquestDamageEvent cde;
            if(callEvent) {
                cde = new ConquestDamageEvent(player, this, type.dmgPerHit);
                pluginmanager.callEvent(cde);
                if(cde.isCancelled()) return;
            }
            if(living.contains(this)) {
                health -= damage;
                final int r = type.healthMsgRadius;
                final HashMap<String, String> replacements = new HashMap<>();
                replacements.put("{PLAYER}", player.getName());
                replacements.put("{X}", Integer.toString(x));
                replacements.put("{Y}", Integer.toString(y));
                replacements.put("{Z}", Integer.toString(z));
                replacements.put("{HP}", Integer.toString(health));
                replacements.put("{MAX_HP}", Integer.toString(type.maxHealth));
                final Collection<Entity> nearby = location.getWorld().getNearbyEntities(location, r, r, r);
                final List<String> msg = health <= 0.00 ? type.unlockedMsg : type.healthMsg;
                if(health <= 0.00) {
                    conquerer = player.getName();
                    delete(true);
                }
                for(Entity e : nearby)
                    if(e instanceof Player)
                        api.sendStringListMessage(e, msg, replacements);
            }
        }
    }
    public void delete(boolean dropsRewards) {
        scheduler.cancelTask(announceTask);
        scheduler.cancelTask(despawnTask);
        final World w = location.getWorld();
        living.remove(this);
        w.getBlockAt(location).setType(Material.AIR);
        if(dropsRewards)
            for(ItemStack is : getRandomRewards())
                w.dropItem(location, is);
    }
    public List<ItemStack> getRandomRewards() {
        final Random random = new Random();
        final List<ItemStack> r = new ArrayList<>();
        final List<String> rewards = new ArrayList<>(type.rewards);
        final String rs = type.rewardSize;
        final int min = rs.contains("-") ? Integer.parseInt(rs.split("-")[0]) : 0, amount = !rs.contains("-") ? Integer.parseInt(rs) : min + random.nextInt(Integer.parseInt(rs.split("-")[1])-min+1);
        for(int i = 1; i <= amount; i++) {
            final String reward = rewards.get(random.nextInt(rewards.size()));
            r.add(api.d(null, reward, 0));
            rewards.remove(reward);
        }
        return r;
    }
    public static LivingConquestChest valueOf(Location l) {
        for(LivingConquestChest c : living)
            if(c.location.equals(l))
                return c;
        return null;
    }
    public static LivingConquestChest valueOf(Chunk chunk) {
        for(LivingConquestChest c : living)
            if(c.location.getChunk().equals(chunk))
                return c;
        return null;
    }
}

package me.randomHashTags.RandomPackage.utils.classes;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class AuctionedBiddableItem {
	
	public static TreeMap<Long, List<AuctionedBiddableItem>> auctionedbids = new TreeMap<>();
	public static HashMap<UUID, List<AuctionedBiddableItem>> playersWatchlist = new HashMap<>();
	
	public final Player player;
	public final List<ItemStack> items;
	public final double startingprice, bidincrement, buynow;
	public double currentbid;
	public Player bidder;
	public final int duration;
	public final long auctionedtime;
	
	public AuctionedBiddableItem(long auctionedtime, Player player, List<ItemStack> items, double startingprice, double bidincrement, double buynow, int duration) {
		this.auctionedtime = auctionedtime;
		this.player = player;
		this.items = items;
		this.startingprice = startingprice;
		this.bidincrement = bidincrement;
		this.buynow = buynow;
		this.duration = duration;
		currentbid = 0.00;
		bidder = null;
		List<AuctionedBiddableItem> a = auctionedbids.keySet().contains(auctionedtime) ? auctionedbids.get(auctionedtime) : new ArrayList<AuctionedBiddableItem>();
		a.add(this);
		auctionedbids.put(auctionedtime, a);
	}
	
	public void buyNow(Player purchaser) {
		currentbid = 0.00;
		auctionedbids.get(auctionedtime).remove(this);
	}
	public void bid(Player bidder) {
		currentbid += bidincrement;
		this.bidder = bidder;
	}
	public void cancel() {
		
	}
	public void addToWatchlist(Player player) {
		List<AuctionedBiddableItem> a = playersWatchlist.keySet().contains(player.getUniqueId()) ? playersWatchlist.get(player.getUniqueId()) : new ArrayList<AuctionedBiddableItem>();
		a.add(this);
		playersWatchlist.put(player.getUniqueId(), a);
	}
	public void removeFromWatchlist(Player player) {
		if(playersWatchlist.keySet().contains(player.getUniqueId())) playersWatchlist.get(player.getUniqueId()).remove(this);
	}
	
	public AuctionedBiddableItem valueOf(List<ItemStack> items) {
		for(long L : auctionedbids.keySet())
			for(AuctionedBiddableItem a : auctionedbids.get(L))
				if(a.items.equals(items))
					return a;
		return null;
	}
	public AuctionedBiddableItem valueOf(ItemStack clickeditem) {
		for(long L : auctionedbids.keySet())
			for(AuctionedBiddableItem a : auctionedbids.get(L))
				if(a.items.get(0).equals(clickeditem))
					return a;
		return null;
	}
}

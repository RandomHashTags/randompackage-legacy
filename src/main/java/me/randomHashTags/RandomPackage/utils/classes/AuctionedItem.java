package me.randomHashTags.RandomPackage.utils.classes;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackage;
import me.randomHashTags.RandomPackage.api.nearFinished.AuctionHouse;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class AuctionedItem {
	
	public static final HashMap<UUID, List<AuctionedItem>> getAuctionedItems = new HashMap<>(), getCollectionBins = new HashMap<>();
	public static final TreeMap<Long, AuctionedItem> auctionhouse = new TreeMap<>();
	private static final RandomPackage rp = RandomPackage.getPlugin;
	private static final YamlConfiguration config = AuctionHouse.getAuctionHouse().config;
	public static final HashMap<AuctionedItem, Integer> tasks = new HashMap<>();

	public final UUID seller;
	private final RPPlayerData pdata;
	private final ItemStack is;
	public final double price;
	public final long auctionedTime;
	public long listingExpiration, collectionBinExpiration;
	
	public AuctionedItem(UUID seller, ItemStack is, double price, long auctionedTime, boolean addToCollectionBin) {
		this.seller = seller;
		this.pdata = RPPlayerData.get(seller);
		this.is = is;
		this.price = price;
		this.auctionedTime = auctionedTime;
		if(addToCollectionBin) {
			if(!getCollectionBins.keySet().contains(seller)) getCollectionBins.put(seller, new ArrayList<>());
			getCollectionBins.get(seller).add(this);
			listingExpiration = -1;
			collectionBinExpiration = auctionedTime + (config.getInt("cancelled-expired-listings.expiration-settings.time") * 1000);
			final long e = (collectionBinExpiration - auctionedTime) / 1000;
			if(e <= 0) cancel(false);
			else {
				tasks.put(this, Bukkit.getScheduler().scheduleSyncDelayedTask(rp, () -> AuctionedItem.this.cancel(false), e * 20));
				pdata.collectionbin.add(this);
			}
		} else {
			if(!getAuctionedItems.keySet().contains(seller)) getAuctionedItems.put(seller, new ArrayList<>());
			getAuctionedItems.get(seller).add(this);
			listingExpiration = auctionedTime + (config.getInt("listings.expiration-settings.time") * 1000);
			collectionBinExpiration = -1;
			final long e = (listingExpiration - auctionedTime) / 1000;
			if(e <= 0) cancel(true);
			else {
				auctionhouse.put(auctionedTime, this);
				tasks.put(this, Bukkit.getScheduler().scheduleSyncDelayedTask(rp, () -> AuctionedItem.this.cancel(true), e * 20));
				pdata.auctions.add(this);
			}
		}
	}
	public void remove() {
		pdata.auctions.remove(this);
		pdata.collectionbin.remove(this);
		auctionhouse.remove(auctionedTime);
		getAuctionedItems.get(seller).remove(this);
	}
	public ItemStack getItemStack() { return is.clone(); }
	public void cancel(boolean addToCollectionBin) {
		if(!getAuctionedItems.keySet().contains(seller)) getAuctionedItems.put(seller, new ArrayList<>());
		else getAuctionedItems.get(seller).remove(this);
		if(!getCollectionBins.keySet().contains(seller)) getCollectionBins.put(seller, new ArrayList<>());
		listingExpiration = -1;
		auctionhouse.remove(auctionedTime);
		pdata.auctions.remove(this);
		if(addToCollectionBin) {
			stopTask();
			getCollectionBins.get(seller).add(this);
			final int E = config.getInt("cancelled-expired-listings.expiration-settings.time");
			collectionBinExpiration = System.currentTimeMillis() + (E * 1000);
			tasks.put(this, Bukkit.getScheduler().scheduleSyncDelayedTask(rp, () -> cancel(false), 20 * E));
			pdata.collectionbin.add(this);
		} else {
			stopTask();
			getCollectionBins.get(seller).remove(this);
			collectionBinExpiration = -1;
			pdata.collectionbin.remove(this);
		}
	}
	private void stopTask() {
		Bukkit.getScheduler().cancelTask(tasks.get(this));
		tasks.remove(this);
	}
	
	public static AuctionedItem valueOf(UUID seller, ItemStack is, boolean inCollectionBin) {
		final HashMap<UUID, List<AuctionedItem>> type = inCollectionBin ? getCollectionBins : getAuctionedItems;
		if(type.keySet().contains(seller)) {
			for(AuctionedItem i : type.get(seller))
				if(i.getItemStack().equals(is)) return i;
		}
		return null;
	}
	public static AuctionedItem valueOf(ItemStack is) {
		for(long L : auctionhouse.keySet()) {
			final AuctionedItem a = auctionhouse.get(L);
			if(a.getItemStack().equals(is))
				return a;
		}
		return null;
	}
	public static AuctionedItem valueOf(ItemStack is, double price) {
		for(long L : auctionhouse.keySet()) {
			final AuctionedItem a = auctionhouse.get(L);
			if(a.price == price && a.getItemStack().equals(is))
				return a;
		}
		return null;
	}
	public static AuctionedItem valueOf(int slot) {
		final int s = auctionhouse.keySet().size();
		return s > slot ? auctionhouse.get(auctionhouse.keySet().toArray()[slot]) : null;
	}
}

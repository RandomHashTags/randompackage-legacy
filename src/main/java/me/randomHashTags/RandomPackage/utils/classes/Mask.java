package me.randomHashTags.RandomPackage.utils.classes;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Mask {
    public static List<Mask> masks = new ArrayList<>();

    public final String path, owner, addedLore;
    private final ItemStack physicalItem;
    private final List<String> attributes;
    public Mask(String path, String owner, ItemStack physicalItem, String addedLore, List<String> attributes) {
        this.path = path;
        this.owner = owner;
        if(physicalItem != null) {
            if(owner.startsWith("http")) {
                final ItemMeta im = physicalItem.getItemMeta();
                physicalItem = getSkull(owner, im.getDisplayName(), im.getLore());
            } else {
                final SkullMeta sm = (SkullMeta) physicalItem.getItemMeta();
                sm.setOwner(owner);
                physicalItem.setItemMeta(sm);
            }
        }
        this.physicalItem = physicalItem;
        this.addedLore = ChatColor.translateAlternateColorCodes('&', addedLore);
        this.attributes = attributes;
        masks.add(this);
    }
    public ItemStack getItemStack() { return physicalItem.clone(); }
    public List<String> getAttributes() { return attributes; }

    // https://www.spigotmc.org/threads/tutorial-player-skull-with-custom-skin.143323/ , edited by RandomHashTags
    private ItemStack getSkull(String skinURL, String name, List<String> lore) {
        final ItemStack head = UMaterial.PLAYER_HEAD_ITEM.getItemStack();
        if (skinURL.isEmpty()) return head;
        final SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        headMeta.setDisplayName(name);
        headMeta.setLore(lore);
        final GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        final byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", skinURL).getBytes());
        profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
        Field profileField = null;
        try {
            profileField = headMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(headMeta, profile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        head.setItemMeta(headMeta);
        return head;
    }

    public static Mask valueOf(String path) {
        for(Mask m : masks)
            if(m.path.equals(path))
                return m;
        return null;
    }
    public static Mask valueOf(ItemStack is) {
        if(is != null && is.hasItemMeta()) {
            final ItemMeta ism = is.getItemMeta();
            final Material t = is.getType();
            final byte d = is.getData().getData();
            for(Mask m : masks) {
                final ItemStack i = m.getItemStack();
                final ItemMeta im = i.getItemMeta();
                if(im.getDisplayName().equals(ism.getDisplayName()) && im.getLore().equals(ism.getLore()) && i.getType().equals(t) && i.getData().getData() == d)
                    return m;
            }
        }
        return null;
    }
    public static Mask getOnItem(ItemStack is) {
        if(is != null && is.hasItemMeta() && is.getItemMeta().hasLore())
            for(String s : is.getItemMeta().getLore())
                for(Mask m : masks) if(m.addedLore.equals(s)) return m;
        return null;
    }
}

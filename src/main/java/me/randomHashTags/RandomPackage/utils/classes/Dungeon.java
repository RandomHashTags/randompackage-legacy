package me.randomHashTags.RandomPackage.utils.classes;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class Dungeon {
    public static final List<Dungeon> dungeons = new ArrayList<>();

    private long startedTime;
    private String fastestCompletion;

    public final String path;
    public final int slot;
    public ItemStack display, key, keyLocked, lootbag;
    public List<String> lootbagRewards;
    public Location teleportLocation;
    public Dungeon(String path, int slot, ItemStack display, ItemStack key, ItemStack keyLocked, ItemStack lootbag, List<String> lootbagRewards, Location teleportLocation) {
        this.path = path;
        this.slot = slot;
        this.display = display;
        this.key = key;
        this.keyLocked = keyLocked;
        this.lootbag = lootbag;
        this.lootbagRewards = lootbagRewards;
        this.teleportLocation = teleportLocation;
        dungeons.add(this);
    }
    public ItemStack getDisplay() { return display.clone(); }
    public ItemStack getKey() { return key.clone(); }
    public ItemStack getKeyLocked() { return keyLocked.clone(); }
    public ItemStack getLootbag() { return lootbag.clone(); }


    public static Dungeon valueOf(String path) {
        for(Dungeon d : dungeons)
            if(d.path.equals(path))
                return d;
        return null;
    }
    public static Dungeon valueOf(int slot) {
        for(Dungeon d : dungeons)
            if(d.slot == slot)
                return d;
        return null;
    }
}

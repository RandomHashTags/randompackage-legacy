package me.randomHashTags.RandomPackage.utils.classes.duels;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class DuelKit {
    public static final List<DuelKit> kits = new ArrayList<>();

    public final String path, name;
    public final int slot;
    private ItemStack display;
    public final ItemStack[] contents;
    public DuelKit(String path, int slot, String name, ItemStack display, ItemStack[] contents) {
        this.path = path;
        this.slot = slot;
        this.name = name;
        this.display = display;
        this.contents = contents;
    }
    public ItemStack display() { return display.clone(); }
}

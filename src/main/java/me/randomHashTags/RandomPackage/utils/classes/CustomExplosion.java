package me.randomHashTags.RandomPackage.utils.classes;

import me.randomHashTags.RandomPackage.RandomPackage;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import me.randomHashTags.RandomPackage.utils.universal.UVersion;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.*;

public class CustomExplosion {
    public static final List<CustomExplosion> customtnt = new ArrayList<>(), customcreepers = new ArrayList<>();
    public static final HashMap<Location, CustomExplosion> placedTNT = new HashMap<>();
    public static final HashMap<UUID, CustomExplosion> livingCreepers = new HashMap<>(), livingTNT = new HashMap<>();

    private static final Random random = new Random();
    private static final UVersion uv = UVersion.getInstance();
    private static final Plugin randompackage = RandomPackage.getPlugin, spawner = RandomPackage.spawner;
    public static int spawnerchance = 1;

    private UUID livingUUID;
    private final String path;
    private final ItemStack is;
    private final List<String> attributes;
    private String creeperName;
    public CustomExplosion(String path, ItemStack is, List<String> attributes) {
        this.path = path;
        this.is = is;
        this.attributes = attributes;
        customtnt.add(this);
    }
    public CustomExplosion(String path, ItemStack is, String creeperName, List<String> attributes) {
        this.path = path;
        this.is = is;
        this.creeperName = ChatColor.translateAlternateColorCodes('&', creeperName);
        this.attributes = attributes;
        customcreepers.add(this);
    }
    public String getPath() { return path; }
    public ItemStack getItemStack() { return is.clone(); }
    public List<String> getAttributes() { return attributes; }
    public String getCreeperName() { return creeperName; }
    public UUID getLivingUUID() { return livingUUID; }

    // https://minecraft.gamepedia.com/TNT
    public void explode(EntityExplodeEvent event, Location l) {
        if(placedTNT.keySet().contains(l))
            placedTNT.remove(l);
        if(livingUUID != null)
            (livingCreepers.keySet().contains(livingUUID) ? livingCreepers : livingTNT).remove(livingUUID);
        final List<Block> bl = getBlockList(event);
        for(Block b : bl) {
            final Location lo = b.getLocation();
            if(placedTNT.keySet().contains(lo)) {
                lo.getWorld().getBlockAt(lo).setType(Material.AIR);
                placedTNT.get(lo).ignite(lo).setFuseTicks(10+random.nextInt(21));
            }
        }
        for(String string : attributes) {
            if(!string.contains("&&") && !string.contains("||")) doExplosion(string, l, event);
            else if(string.contains("&&") && !string.contains("||"))
                for(String s : string.split("&&")) doExplosion(s, l, event);
            else if(string.contains("&&") && string.contains("||")) {
                for(String s : random.nextInt(100) < 50 ? string.split("\\|\\|")[0].split("&&") : string.split("\\|\\|")[1].split("&&"))
                    doExplosion(s, l, event);
            }
        }
    }
    private List<Block> getBlockList(EntityExplodeEvent event) {
        for(String string : attributes) {
            if(!string.contains("&&") && !string.contains("||") && string.toLowerCase().startsWith("affects_only;")) {
                return getAffectedBlocks(event, string);
            } else if(string.contains("&&") && !string.contains("||")) {
                for(String s : string.split("&&")) {
                    if(s.toLowerCase().startsWith("affects_only;"))
                        return getAffectedBlocks(event, s);
                }
            } else if(string.contains("&&") && string.contains("||")) {
                for(String s : random.nextInt(100) < 50 ? string.split("\\|\\|")[0].split("&&") : string.split("\\|\\|")[1].split("&&"))
                    if(s.toLowerCase().startsWith("affects_only;"))
                        return getAffectedBlocks(event, s);
            }
        }
        return event.blockList();
    }
    private List<Block> getAffectedBlocks(EntityExplodeEvent event, String input) {
        final Material material = UMaterial.valueOf(input.split(";")[1].toUpperCase(), (byte) 0).getType();
        for(int i = 0; i < event.blockList().size(); i++) {
            if(!event.blockList().get(i).getType().equals(material)) {
                event.blockList().remove(i);
                i--;
            }
        }
        return event.blockList();
    }
    private void doExplosion(String input, Location loc, EntityExplodeEvent event) {
        input = input.replace(" ", "").toLowerCase();
        if(input.startsWith("affects_only;")) {
            Material material = Material.getMaterial(input.split(";")[1].toUpperCase());
            for(int i = 0; i < event.blockList().size(); i++)
                if(!event.blockList().get(i).getType().equals(material)) {
                    event.blockList().remove(i);
                    i--;
                }
        } else if(input.startsWith("instant_explode;")) {
            double x = loc.getX(), y = loc.getY(), z = loc.getZ();
            String d = input.split(";")[1].toLowerCase();
            if(d.contains("-")) {
                if(d.contains("x"))      x = x - Integer.parseInt(d.split("-")[1]);
                else if(d.contains("y")) y = y - Integer.parseInt(d.split("-")[1]);
                else if(d.contains("z")) z = z - Integer.parseInt(d.split("-")[1]);
            } else if(d.contains("+")) {
                if(d.contains("x"))      x = x + Integer.parseInt(d.split("\\+")[1]);
                else if(d.contains("y")) y = y + Integer.parseInt(d.split("\\+")[1]);
                else if(d.contains("z")) z = z + Integer.parseInt(d.split("\\+")[1]);
            }
            for(int i = 1; i <= Integer.parseInt(input.split(";")[2]); i++) {
                TNTPrimed a = loc.getWorld().spawn(new Location(loc.getWorld(), x, y, z), TNTPrimed.class);
                a.setFuseTicks(0);
            }
        } else if(input.startsWith("repeat_explode;")) {
            final double x = loc.getX(), y = loc.getY(), z = loc.getZ();
            final int delay = Integer.parseInt(input.split(";")[1]);
            for(int i = 1; i <= Integer.parseInt(input.split(";")[2]); i++) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> {
                    final TNTPrimed a = loc.getWorld().spawn(new Location(loc.getWorld(), x, y, z), TNTPrimed.class);
                    a.setFuseTicks(0);
                }, 20 * delay + (delay * 20 * (i - 1)));
            }
        } else if(input.startsWith("increase_spawner_drop;")) {
            for(Block block : event.blockList()) {
                boolean mobspawner = false;
                if(block.getType().equals(UMaterial.SPAWNER.getMaterial()) && spawnerchance * Double.parseDouble(input.split(";")[1]) <= random.nextInt(100)) {
                    mobspawner = true;
                    if(spawner != null) {
                        final Location l = event.getLocation() , bl = block.getLocation();
                        final World w = l.getWorld();
                        w.dropItemNaturally(bl, UVersion.getInstance().getSpawner("Blaze"));
                    }
                    block.setType(Material.AIR);
                }
                if(!mobspawner) block.breakNaturally();
            }
        }
    }
    public static Creeper spawnCreeper(Location l, CustomExplosion ce) {
        final Creeper creeper = l.getWorld().spawn(l, Creeper.class);
        creeper.setCustomName(ce.getCreeperName());
        final UUID u = creeper.getUniqueId();
        ce.livingUUID = u;
        livingCreepers.put(u, ce);
        return creeper;
    }
    public void placeTNT(Location l, CustomExplosion ce) {
        placedTNT.put(l, ce);
    }
    public TNTPrimed spawnTNT(Location l) {
        final TNTPrimed tnt = l.getWorld().spawn(l, TNTPrimed.class);
        final UUID u = tnt.getUniqueId();
        this.livingUUID = u;
        livingTNT.put(u, this);
        return tnt;
    }
    public TNTPrimed ignite(Location l) {
        final TNTPrimed tnt = l.getWorld().spawn(l, TNTPrimed.class);
        final UUID u = tnt.getUniqueId();
        this.livingUUID = u;
        livingTNT.put(u, this);
        placedTNT.remove(l);
        return tnt;
    }


    public static CustomExplosion getCustomTNT(UUID uuid) {
        for(UUID u : livingTNT.keySet())
            if(u.equals(uuid))
                return livingTNT.get(u);
        return null;
    }
    public static CustomExplosion getCustomTNT(String path) {
        for(CustomExplosion c : customtnt)
            if(c.getPath().equals(path))
                return c;
        return null;
    }
    public static CustomExplosion getCustomTNT(ItemStack is) {
        if(is != null) {
            for(CustomExplosion c : customtnt)
                if(c.getItemStack().getItemMeta().equals(is.getItemMeta()))
                    return c;
        }
        return null;
    }
    public static CustomExplosion getCustomCreeper(UUID uuid) {
        for(UUID u : livingCreepers.keySet())
            if(u.equals(uuid))
                return livingCreepers.get(u);
        return null;
    }
    public static CustomExplosion getCustomCreeper(String path) {
        for(CustomExplosion c : customcreepers)
            if(c.getPath().equals(path))
                return c;
        return null;
    }
    public static CustomExplosion getCustomCreeper(ItemStack is) {
        if(is != null) {
            for(CustomExplosion c : customcreepers)
                if(c.getItemStack().getItemMeta().equals(is.getItemMeta()))
                    return c;
        }
        return null;
    }
}

package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.inventory.ItemStack;

@SuppressWarnings({"deprecation"})
public class BlackScroll {
	private final Random random = new Random();
	public static final List<BlackScroll> scrolls = new ArrayList<>();
	private final int number, min, max;
	private final ItemStack is;
	private final List<EnchantRarity> appliesto;
	public BlackScroll(int number, ItemStack is, int min, int max, List<EnchantRarity> appliesto) {
		this.number = number;
		this.is = is;
		this.min = min;
		this.max = max;
		this.appliesto = appliesto;
		scrolls.add(this);
	}
	public int getNumberInConfig() { return number; }
	public ItemStack getItemStack() { return is.clone(); }
	public int getMinPercent() { return min; }
	public int getMaxPercent() { return max; }
	public int getRandomPercent() { return min + random.nextInt(max - min + 1); }
	public List<EnchantRarity> getAppliesTo() { return appliesto; }
	public static BlackScroll valueOf(int number) {
		for(BlackScroll b : scrolls) if(b.getNumberInConfig() == number) return b;
		return null;
	}
	public static BlackScroll valueOf(ItemStack is) {
		if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore())
			for(BlackScroll b : scrolls)
				if(is.getType().equals(b.getItemStack().getType()) && is.getData().getData() == b.getItemStack().getData().getData()
					&& is.getItemMeta().getDisplayName().equals(b.getItemStack().getItemMeta().getDisplayName()))
					return b;
		return null;
	}
}

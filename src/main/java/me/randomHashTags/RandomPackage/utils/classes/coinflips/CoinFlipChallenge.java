package me.randomHashTags.RandomPackage.utils.classes.coinflips;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;

public class CoinFlipChallenge {
	public static final List<CoinFlipChallenge> challenges = new ArrayList<>();
	private final CoinFlipMatch match;
	private final OfflinePlayer challenger;
	private final ItemStack challengerChosen;
	private final String challengerDisplay;
	public CoinFlipChallenge(CoinFlipMatch match, OfflinePlayer challenger, ItemStack challengerChosen, String challengerDisplay) {
		this.match = match;
		this.challenger = challenger;
		this.challengerChosen = challengerChosen;
		this.challengerDisplay = challengerDisplay;
		challenges.add(this);
	}
	public CoinFlipMatch getMatch() { return match; }
	public OfflinePlayer getChallenger() { return challenger; }
	public ItemStack getChallengerChosen() { return challengerChosen; }
	public String getChallengerDisplay() { return challengerDisplay; }
	public void delete() { challenges.remove(this); }
	public static CoinFlipChallenge valueOf(OfflinePlayer player) {
		for(CoinFlipChallenge c : challenges) if(c.getMatch().getCreator() == player || c.getChallenger() == player) return c;
		return null;
	}
}
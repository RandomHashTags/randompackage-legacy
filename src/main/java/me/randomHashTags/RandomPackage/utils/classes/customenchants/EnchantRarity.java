package me.randomHashTags.RandomPackage.utils.classes.customenchants;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.ItemStack;

public class EnchantRarity {
	public static final List<EnchantRarity> rarities = new ArrayList<>();
	private final String rarityname, namecolors, applycolors;
	private final Firework firework;
	private final ItemStack revealitem, revealeditem;
	private final List<String> revealmsg;
	public EnchantRarity(String rarityname, String namecolors, String applycolors, Firework firework, ItemStack revealitem, ItemStack revealeditem, List<String> revealmsg) {
		this.rarityname = rarityname;
		this.namecolors = ChatColor.translateAlternateColorCodes('&', namecolors);
		this.applycolors = ChatColor.translateAlternateColorCodes('&', applycolors);
		this.firework = firework;
		this.revealitem = revealitem;
		this.revealeditem = revealeditem;
		this.revealmsg = revealmsg;
		rarities.add(this);
	}
	public String getName() { return rarityname; }
	public String getNameColors() { return namecolors; }
	public String getApplyColors() { return applycolors; }
	public Firework getFirework() { return firework; }
	public ItemStack getRevealItem() { return revealitem.clone(); }
	public ItemStack getRevealedItem() { return revealeditem.clone(); }
	public List<String> getRevealMessage() { return revealmsg; }
	
	public static EnchantRarity valueOf(String rarityName) {
		for(EnchantRarity e : rarities) if(e.getName().equals(rarityName)) return e;
		return null;
	}
	public static EnchantRarity valueOf(ItemStack revealitem) {
		if(revealitem != null && revealitem.hasItemMeta() && revealitem.getItemMeta().hasDisplayName() && revealitem.getItemMeta().hasLore())
			for(EnchantRarity e : rarities)
				if(e.getRevealItem().getItemMeta().equals(revealitem.getItemMeta()))
					return e;
		return null;
	}
}

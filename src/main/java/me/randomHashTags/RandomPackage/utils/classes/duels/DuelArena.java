package me.randomHashTags.RandomPackage.utils.classes.duels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class DuelArena {
	private static HashMap<UUID, DuelSettings> unrankedQueue = new HashMap<>(), rankedQueue = new HashMap<>();
	public static final List<DuelArena> arenas = new ArrayList<>();
	public final String path;
	private ItemStack is;
	public final Location location1, location2;
	public int queue = 0;
	public final int slot;
	public DuelArena(String path, int slot, ItemStack is, Location location1, Location location2) {
		this.path = path;
		this.slot = slot;
		this.is = is;
		this.location1 = location1;
		this.location2 = location2;
		arenas.add(this);
	}
	public ItemStack getItemStack() { return is.clone(); }
	public static int getSize(String duelPath) {
		for(DuelArena d : arenas)
			if(d.path.equals(duelPath))
				return d.queue;
		return 0;
	}
	public static DuelArena valueOf(int slot) {
		for(DuelArena a : arenas)
			if(a.slot == slot)
				return a;
		return null;
	}
	public static void queue(Player player, DuelSettings settings, boolean ranked) {
		settings.arena.queue += 1;
		final int q = settings.arena.queue;
		if(q == 2) {

		} else {
			(ranked ? rankedQueue : unrankedQueue).put(player.getUniqueId(), settings);
		}
	}
}

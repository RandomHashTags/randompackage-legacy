package me.randomHashTags.RandomPackage.utils.classes.duels;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class DuelSettings {
	public static final List<DuelSettings> duelSettings = new ArrayList<>();
	public final Player creator, target;
	public DuelArena arena;
	public boolean gapples, mcmmo, potions, bows, healing, foodloss, enderpearls, risksInventory, armor, weapons, envoy, deathCertificates;
	public List<String> allowedCommands;
	public DuelKit kit;
	public DuelSettings(Player creator, Player target, DuelArena arena, boolean gapples, boolean mcmmo, boolean potions, boolean bows, boolean healing, boolean foodloss, boolean enderpearls, boolean risksInventory, boolean armor, boolean weapons, List<String> allowedCommands, boolean envoy, boolean deathCertificates, DuelKit kit) {
		this.creator = creator;
		this.target = target;
		this.arena = arena;
		this.gapples = gapples;
		this.mcmmo = mcmmo;
		this.potions = potions;
		this.bows = bows;
		this.healing = healing;
		this.foodloss = foodloss;
		this.enderpearls = enderpearls;
		this.risksInventory = risksInventory;
		this.armor = armor;
		this.weapons = weapons;
		this.allowedCommands = allowedCommands;
		this.envoy = envoy;
		this.deathCertificates = deathCertificates;
		duelSettings.add(this);
	}
	public void delete() { duelSettings.remove(this); }
	public static DuelSettings valueOf(OfflinePlayer player) {
		for(DuelSettings s : duelSettings) if(s.creator.equals(player) || s.target.equals(player)) return s;
		return null;
	}
}
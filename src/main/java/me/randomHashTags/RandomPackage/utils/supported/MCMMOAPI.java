package me.randomHashTags.RandomPackage.utils.supported;

import java.util.UUID;

import com.gmail.nossr50.api.ExperienceAPI;
import com.gmail.nossr50.datatypes.skills.SkillType;
import com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent;
import com.gmail.nossr50.events.skills.abilities.McMMOPlayerAbilityActivateEvent;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.randomHashTags.RandomPackage.api.GlobalChallenges;

public class MCMMOAPI extends GlobalChallenges implements Listener {
	public boolean isEnabled = false;
	private static MCMMOAPI instance;
	public static final MCMMOAPI getMCMMOAPI() {
		if(instance == null) instance = new MCMMOAPI();
		return instance;
	}
	public ItemStack creditVoucher, levelVoucher, xpVoucher;
	public boolean gcIsEnabled = false;

	public void enable() {
		if(isEnabled) return;
		isEnabled = true;
		pluginmanager.registerEvents(this, randompackage);
		gcIsEnabled = GlobalChallenges.getChallenges().isEnabled;
		creditVoucher = d(itemsConfig, "mcmmo-vouchers.credit", 0);
		levelVoucher = d(itemsConfig, "mcmmo-vouchers.level", 0);
		xpVoucher = d(itemsConfig, "mcmmo-vouchers.xp", 0);
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		HandlerList.unregisterAll(this);
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void mcmmoPlayerXpGainEvent(McMMOPlayerXpGainEvent event) {
		if(!event.isCancelled()) {
			final Player player = event.getPlayer();
			customenchants.procPlayerArmor(event, player);
			customenchants.procPlayerItem(event, player, null);
			
			if(gcIsEnabled) {
				final UUID p = player.getUniqueId();
				final String skill = event.getSkill().name().toLowerCase();
				final float xp = event.getRawXpGained();
				increase(event, "mcmmoxpgained", p, xp);
				increase(event, "mcmmoxpgainedin_" + skill, p, xp);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	private void mcmmoAbilityActivateEvent(McMMOPlayerAbilityActivateEvent event) {
		if(!event.isCancelled() && gcIsEnabled) {
			final UUID player = event.getPlayer().getUniqueId();
			increase(event, "mcmmoabilityused", player, 1);
			increase(event, "mcmmoabilityused_" + event.getAbility().name(), player, 1);
		}
	}
	public SkillType getSkill(String skillname) {
		for(SkillType type : SkillType.values()) {
			final String s = itemsConfig.getString("mcmmo-vouchers.skill-names." + type.name().toLowerCase().replace("_skills", ""));
			if(s != null && skillname.equalsIgnoreCase(ChatColor.stripColor(s)) || skillname.equalsIgnoreCase(type.name())) return type;
		}
		return null;
	}
	public String getSkillName(SkillType skilltype) {
		final String a = itemsConfig.getString("mcmmo-vouchers.skill-names." + skilltype.name().toLowerCase().replace("_skills", ""));
		return a != null ? ChatColor.translateAlternateColorCodes('&', a) : null;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		if(event.getItem() == null || event.getItem().getType().equals(Material.AIR) || !event.getItem().hasItemMeta() || !event.getItem().getItemMeta().hasDisplayName() || !event.getItem().getItemMeta().hasLore()) { return;
		} else if(event.getItem().getItemMeta().getDisplayName().equals(creditVoucher.getItemMeta().getDisplayName())
				|| event.getItem().getItemMeta().getDisplayName().equals(levelVoucher.getItemMeta().getDisplayName())
				|| event.getItem().getItemMeta().getDisplayName().equals(xpVoucher.getItemMeta().getDisplayName())) {
			int numberslot = -1, skillslot = -1; String itemtype = null;
			if(event.getItem().getItemMeta().getDisplayName().equals(creditVoucher.getItemMeta().getDisplayName()))      itemtype = "credit";
			else if(event.getItem().getItemMeta().getDisplayName().equals(levelVoucher.getItemMeta().getDisplayName()))  itemtype = "level";
			else                                                                                                         itemtype = "xp";
			for(int i = 0; i < itemsConfig.getStringList("mcmmo-vouchers." + itemtype + ".lore").size(); i++) {
				if(itemsConfig.getStringList("mcmmo-vouchers." + itemtype + ".lore").get(i).contains("{AMOUNT}")) numberslot = i;
				if(itemsConfig.getStringList("mcmmo-vouchers." + itemtype + ".lore").get(i).contains("{SKILL}"))  skillslot = i;
			}
			if(numberslot == -1 || skillslot == -1) return;
			com.gmail.nossr50.datatypes.skills.SkillType typee = null;
			for(SkillType type : SkillType.values())
				if(event.getItem().getItemMeta().getLore().get(skillslot).equals(ChatColor.translateAlternateColorCodes('&', itemsConfig.getStringList("mcmmo-vouchers." + itemtype + ".lore").get(skillslot).replace("{SKILL}", "" + getSkillName(type)))))
					typee = type;
				
			int xp = getRemainingInt(event.getItem().getItemMeta().getLore().get(numberslot));
			event.setCancelled(true);
			event.getPlayer().updateInventory();
			if(itemtype.equals("level")) itemtype = "levels";
			for(String string : itemsConfig.getStringList("mcmmo-voucher.redeem-" + itemtype)) {
				if(string.contains("{XP}"))     string = string.replace("{XP}", formatInt(xp));
				if(string.contains("{AMOUNT}")) string = string.replace("{AMOUNT}", formatInt(xp));
				if(string.contains("{SKILL}"))  string = string.replace("{SKILL}", typee.name());
				event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', string));
			}
			if(itemtype.equals("xp"))          ExperienceAPI.addRawXP(event.getPlayer(), typee.name(), xp);
			else if(itemtype.equals("levels")) ExperienceAPI.addLevel(event.getPlayer(), typee.name(), xp);
			else if(itemtype.equals("credit")) {
				return;
			}
			removeItem(event.getPlayer(), event.getItem(), 1);
			playSound(sounds, "redeem.mcmmo-" + itemtype + "-voucher", event.getPlayer(), event.getPlayer().getLocation(), false);
			return;
		}
	}
	
}

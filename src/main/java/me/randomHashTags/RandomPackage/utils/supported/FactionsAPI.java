package me.randomHashTags.RandomPackage.utils.supported;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.api.events.faction.FactionDisbandEvent;
import me.randomHashTags.RandomPackage.api.events.faction.FactionRenameEvent;
import me.randomHashTags.RandomPackage.api.nearFinished.FactionAdditions;
import me.randomHashTags.RandomPackage.utils.classes.factionadditions.FactionUpgrade;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.RarityGem;
import me.randomHashTags.RandomPackage.utils.supported.factions.factionsUUID;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import static me.randomHashTags.RandomPackage.RandomPackage.factionPlugin;

public class FactionsAPI extends RandomPackageAPI implements Listener {
	
	private static FactionsAPI instance;
	public static final FactionsAPI getFactionsAPI() {
		if(instance == null) instance = new FactionsAPI();
		return instance;
	}
	public final HashMap<String, Double> teleportDelayMultipliers = new HashMap<>(), cropGrowthMultipliers = new HashMap<>(), enemyDamageMultipliers = new HashMap<>(), bossDamageMultipliers = new HashMap<>(), vkitLevelingChances = new HashMap<>();
	public final HashMap<String, HashMap<RarityGem, Double>> decreaseRarityGemCost = new HashMap<>();

	public List<Chunk> getWarZoneChunks() {
		if(factionPlugin == null)  return new ArrayList<>();
		else if(factionPlugin.contains("Factions")) return factionsUUID.getInstance().getWarzoneClaims();
		else return new ArrayList<>();
	}
	public String getFaction(OfflinePlayer player) {
		if(factionPlugin == null)                   return "";
		else if(factionPlugin.contains("Factions")) return factionsUUID.getInstance().getFaction(player);
		else return "";
	}
	public boolean canBreakBlock(Player player, Location blockLocation) {
		if(factionPlugin == null) return true;
		else if(factionPlugin.contains("Factions")) return factionsUUID.getInstance().canBreakBlock(player, blockLocation);
		else return true;
	}
	public String getChatMode(OfflinePlayer player) {
		if(factionPlugin == null)  return "";
		if(factionPlugin.contains("Factions")) return factionsUUID.getInstance().getChatMode(player);
		return "";
	}
	public ChatColor getRelationColor(OfflinePlayer player, Player target) {
		if(factionPlugin == null) return ChatColor.WHITE;
		else if(factionPlugin.contains("Factions")) return factionsUUID.getInstance().getRelation(player, target);
		else return ChatColor.WHITE;
	}
	public String getRole(Player player) {
		if(factionPlugin == null) return "";
		else if(factionPlugin.contains("Factions")) return factionsUUID.getInstance().getPlayerRole(player);
		else return "";
	}
	public boolean locationIsWarZone(Block block) {
		if(factionPlugin == null)                   return true;
		else if(factionPlugin.contains("Factions")) return factionsUUID.getInstance().locationIsWarZone(block);
		else return true;
	}
	public boolean relationIsEnemyOrNull(Player player1, Player player2) {
		if(factionPlugin == null)                  return true;
		else if(factionPlugin.contains("Factions"))return factionsUUID.getInstance().relationIsEnemyOrNull(player1, player2);
		else return true;
	}
	public boolean relationIsNeutral(Player player1, Player player2) {
		if(factionPlugin == null)                  return false;
		else if(factionPlugin.contains("Factions"))return factionsUUID.getInstance().relationIsNeutral(player1, player2);
		else return false;
	}
	public boolean relationIsAlly(Player player1, Player player2) {
		if(factionPlugin == null)                  return false;
		else if(factionPlugin.contains("Factions"))return factionsUUID.getInstance().relationIsAlly(player1, player2);
		else return false;
	}
	public boolean relationIsTruce(Player player1, Player player2) {
		if(factionPlugin == null)                  return false;
		else if(factionPlugin.contains("Factions"))return factionsUUID.getInstance().relationIsTruce(player1, player2);
		else return false;
	}
	public boolean relationIsMember(Player player1, Player player2) {
		if(factionPlugin == null)                  return false;
		else if(factionPlugin.contains("Factions"))return factionsUUID.getInstance().relationIsMember(player1, player2);
		else return false;
	}
	public String getFactionAt(Location l) {
		if(factionPlugin == null) return null;
		else if(factionPlugin.contains("Factions")) return factionsUUID.getInstance().getFactionAt(l);
		else return null;
	}

	public void increasePowerBoost(String factionName, double by) {
		if(factionPlugin != null) {
			if(factionPlugin.contains("Factions")) factionsUUID.getInstance().increasePower(factionName, by);
		}
	}
	public void setPowerBoost(String factionName, double value) {
		if(factionPlugin != null) {
			if(factionPlugin.contains("Factions")) factionsUUID.getInstance().setPowerBoost(factionName, value);
		}
	}
	public void resetPowerBoost(Player player) {
		if(factionPlugin != null) {
			if(factionPlugin.contains("Factions")) factionsUUID.getInstance().resetPowerBoost(player);
		}
	}

	public double getTeleportDelayMultiplier(String factionName) {
		if(factionPlugin == null || factionName == null) return 1.00;
		else if(factionPlugin.contains("Factions")) return teleportDelayMultipliers.keySet().contains(factionName) ? teleportDelayMultipliers.get(factionName) : 1.00;
		else return 1.00;
	}
	public void setTeleportDelayMultiplier(String factionName, double multiplier) {
		teleportDelayMultipliers.put(factionName, multiplier);
	}
	public void resetTeleportDelayMultiplier(String factionName) {
		teleportDelayMultipliers.put(factionName, 1.00);
	}

	public double getCropGrowthMultiplier(String factionName) {
		if(factionPlugin == null || factionName == null) return 1.00;
		else if(factionPlugin.contains("Factions")) return cropGrowthMultipliers.keySet().contains(factionName) ? cropGrowthMultipliers.get(factionName) : 1.00;
		else return 1.00;
	}
	public void setCropGrowthMultiplier(String factionName, double multiplier) {
		if(factionPlugin != null && factionName != null) {
			cropGrowthMultipliers.put(factionName, multiplier);
		}
	}

	public double getEnemyDamageMultiplier(String factionName) {
		final HashMap<String, Double> e = enemyDamageMultipliers;
		return factionName != null && e.keySet().contains(factionName) ? e.get(factionName) : 1.00;
	}
	public void setEnemyDamageMultiplier(String factionName, double multiplier) {
		if(factionName != null) enemyDamageMultipliers.put(factionName, multiplier);
	}

	public double getDecreaseRarityGemPercent(String factionName, RarityGem gem) {
		final HashMap<String, HashMap<RarityGem, Double>> d = decreaseRarityGemCost;
		return factionName != null && gem != null && d.keySet().contains(factionName) && d.get(factionName).keySet().contains(gem) ? d.get(factionName).get(gem) : 0;
	}
	public void setDecreaseRarityGemPercent(String factionName, RarityGem gem, double percent) {
		if(!decreaseRarityGemCost.keySet().contains(factionName)) decreaseRarityGemCost.put(factionName, new HashMap<>());
		decreaseRarityGemCost.get(factionName).put(gem, percent);
	}

	public double getBossDamageMultiplier(String factionName) {
		return factionName != null && bossDamageMultipliers.keySet().contains(factionName) ? bossDamageMultipliers.get(factionName) : 1.00;
	}
	public void setBossDamageMultiplier(String factionName, double multiplier) {
		if(factionName != null) {
			bossDamageMultipliers.put(factionName, multiplier);
		}
	}

	public double getVkitLevelingChance(String factionName) {
		return factionName != null && vkitLevelingChances.keySet().contains(factionName) ? vkitLevelingChances.get(factionName) : 0.00;
	}
	public void setVkitLevelingChance(String factionName, double chance) {
		if(factionName != null) {
			vkitLevelingChances.put(factionName, chance);
		}
	}

	@EventHandler
	private void factionDisbandEvent(FactionDisbandEvent event) {
		final String f = event.getFaction();
		removeMultipliers(f);
	}
	@EventHandler
	private void factionRenameEvent(FactionRenameEvent event) {
		renameFaction(event.getOldFactionName(), event.getNewFactionName());
	}
	public void putValues(String factionName, HashMap<FactionUpgrade, Integer> fupgrades, double teleportDelayMultiplier, double cropGrowthMultiplier, double enemyDamageMuliplier, double bossDamageMultiplier, double vkitLevelingChance, HashMap<RarityGem, Double> decreaseRarityGem) {
		teleportDelayMultipliers.put(factionName, teleportDelayMultiplier);
		cropGrowthMultipliers.put(factionName, cropGrowthMultiplier);
		enemyDamageMultipliers.put(factionName, enemyDamageMuliplier);
		bossDamageMultipliers.put(factionName, bossDamageMultiplier);
		vkitLevelingChances.put(factionName, vkitLevelingChance);
		decreaseRarityGemCost.put(factionName, decreaseRarityGem);
		RPPlayerData.setFactionUpgrades(factionName, fupgrades);
	}
	public void putValues(String factionName, HashMap<FactionUpgrade, Integer> fupgrades) {
		final FactionAdditions fa = FactionAdditions.getFactionAdditions();
		RPPlayerData.setFactionUpgrades(factionName, fupgrades);
		for(FactionUpgrade fu : fupgrades.keySet()) {
			final HashMap<String, String> values = fa.getValues(factionName, fu);
			for(String key : values.keySet()) {
				final double value = Double.parseDouble(values.get(key));
				if(key.equals("increasefactionpower"))
					setPowerBoost(factionName, value);
				else if(key.equals("setteleportdelaymultiplier"))
					teleportDelayMultipliers.put(factionName, value);
				else if(key.equals("setcropgrowthmultiplier"))
					cropGrowthMultipliers.put(factionName, value);
				else if(key.equals("setenemydamagemultiplier"))
					enemyDamageMultipliers.put(factionName, value);
				else if(key.equals("setbossdamagemultiplier"))
					bossDamageMultipliers.put(factionName, value);
			}
		}
	}
	public void renameFaction(String oldName, String newName) {
		putValues(newName, RPPlayerData.getFactionUpgrades(oldName), getTeleportDelayMultiplier(oldName), getCropGrowthMultiplier(oldName), getEnemyDamageMultiplier(oldName), getBossDamageMultiplier(oldName), getVkitLevelingChance(oldName), decreaseRarityGemCost.get(oldName));
		removeMultipliers(oldName);
	}
	public void removeMultipliers(String factionName) {
		teleportDelayMultipliers.remove(factionName);
		cropGrowthMultipliers.remove(factionName);
		enemyDamageMultipliers.remove(factionName);
		bossDamageMultipliers.remove(factionName);
		vkitLevelingChances.remove(factionName);
		decreaseRarityGemCost.remove(factionName);
	}
}

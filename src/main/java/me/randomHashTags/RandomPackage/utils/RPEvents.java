package me.randomHashTags.RandomPackage.utils;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.api.Fund;
import me.randomHashTags.RandomPackage.utils.classes.AuctionedItem;
import me.randomHashTags.RandomPackage.utils.classes.globalchallenges.GlobalChallengePrize;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.*;

public class RPEvents extends RandomPackageAPI {

    private static RPEvents instance;
    public static RPEvents getRPEvents() {
        if(instance == null) instance = new RPEvents();
        return instance;
    }

    public File dataF = null;
    public YamlConfiguration data = null;
    private int task;

    public void backup(boolean checkforupdate, boolean async) {
        if(async) {
            scheduler.runTaskAsynchronously(randompackage, () -> dobackup(checkforupdate));
        } else {
            dobackup(false);
        }
    }
    private void dobackup(boolean checkforupdate) {
        final long started = System.currentTimeMillis();
        if(checkforupdate) randompackage.checkForUpdate();
        int loaded = 0;
        data.set("players.array", new ArrayList<>());
        final List<String> uuids = new ArrayList<>();
        final Collection<RPPlayerData> players = RPPlayerData.players.values();
        for(RPPlayerData pdata : players) {
            loaded += 1;
            final UUID U = pdata.getUUID();
            final String uuid = U.toString();
            uuids.add(uuid);

            data.set(uuid + ".booleans", pdata.receivesAHOutbidNotications + ";" + pdata.receivesCoinflipNotifications + ";" + pdata.receivesDuelInvites + ";" + pdata.receivesJackpotNotifications + ";" + pdata.activeFilter);
            data.set(uuid + ".numbers", pdata.xpExhaustionExpiration + ";" + pdata.fundDeposits + ";" + pdata.duelELO + ";" + pdata.duelWins + ";" + pdata.duelLosses + ";" + pdata.coinflipWins + ";" + pdata.coinflipLosses + ";" + pdata.coinflipWonCash + ";" + pdata.coinflipLostCash + ";" + pdata.coinflipTaxesPaid + ";" + pdata.jackpotTicketsPurchased + ";" + pdata.jackpotWins + ";" + pdata.jackpotWinnings + ";" + pdata.teleportDelayMultiplier + ";" + pdata.maxHomeIncreaser);

            String g = "", v = "", mkits = "", CEE = "", GCP = "", titles = "", ccs = "";
            for(UUID u : pdata.customEnchantEntities) CEE = CEE + u.toString() + ";";
            data.set(uuid + ".custom enchant entities", CEE);
            final HashMap<GlobalChallengePrize, Integer> i = pdata.globalChallengePrizes;
            for(GlobalChallengePrize p : i.keySet()) GCP = GCP + p.getPlacement() + "=" + i.get(p) + ";";
            data.set(uuid + ".global challenge prizes", GCP);
            for(String m : pdata.gkitTiers.keySet()) g = g + m + "=" + pdata.gkitTiers.get(m) + ":" + pdata.getGkitCooldown(m) + ";";
            for(String m : pdata.gkitCooldowns.keySet()) {
                final int tier = pdata.getGkitTier(m);
                final long cooldown = pdata.getGkitCooldown(m);
                if(!g.contains(m + "=" + tier + ":" + cooldown + ";")) g = g + m + "=" + tier + ":" + cooldown + ";";
            }
            for(String m : pdata.vkitLevels.keySet()) v = v + m + "=" + pdata.vkitLevels.get(m) + ":" + pdata.getVkitCooldown(m) + ";";
            for(String m : pdata.vkitCooldowns.keySet()) {
                final int lvl = pdata.getVkitLevel(m);
                final long cooldown = pdata.getVkitCooldown(m);
                if(!v.contains(m + "=" + lvl + ":" + cooldown + ";")) v = v + m + "=" + lvl + ":" + cooldown + ";";
            }
            for(String m : pdata.masterykits.keySet()) mkits = mkits + m + "=" + pdata.masterykits.get(m) + ";";
            data.set(uuid + ".gkits", g);
            data.set(uuid + ".vkits", v);
            data.set(uuid + ".mkits", mkits);
            for(String m : pdata.monthlycrates) if(!ccs.contains(m)) ccs = ccs + m + "=" + pdata.redeemedMonthlyCrates.contains(m) + ";";
            for(String m : pdata.redeemedMonthlyCrates) if(!ccs.contains(m)) ccs = ccs + m + "=true;";
            data.set(uuid + ".monthly crates", ccs);
            for(String s : pdata.ownedTitles) titles = titles + s + ";";
            titles = pdata.activeTitle + ";" + titles;
            data.set(uuid + ".titles", titles);

            final List<String> homes = new ArrayList<>(), unclaimedPurchases = new ArrayList<>(), duelCollectionBin = new ArrayList<>();
            for(RPPlayerData.Home h : pdata.homes) {
                final ItemStack icon = h.icon;
                final Location l = h.location;
                homes.add(h.name + ":" + UMaterial.matchUMaterial(icon.getType().name(), icon.getData().getData()).name() + ":" + l.getWorld().getName() + ";" + l.getX() + ";" + l.getY() + ";" + l.getZ() + ";" + l.getYaw() + ";" + l.getPitch());
            }
            data.set(uuid + ".homes", homes);
            data.set(uuid + ".filtered items", pdata.getFilteredItemsString());
            for(String s : pdata.unclaimedPurchases) {
                unclaimedPurchases.add(toRPDataString(toRPDataItemStack(s)));
            }
            data.set(uuid + ".unclaimed purchases", unclaimedPurchases);

            for(ItemStack is : pdata.duelStakeCollectionBin) {
                duelCollectionBin.add(toRPDataString(is));
            }
            data.set(uuid + ".duel collection bin", duelCollectionBin);
            final HashMap<UUID, List<AuctionedItem>> ah = AuctionedItem.getAuctionedItems, cb = AuctionedItem.getCollectionBins;
            List<String> auctions = new ArrayList<>(), collectionbin = new ArrayList<>();
            if(ah.keySet().contains(U)) {
                for(AuctionedItem is : ah.get(U))
                    auctions.add(toRPDataString(is.getItemStack()) + "||" + is.price + "||" + is.auctionedTime + "||" + is.listingExpiration);
            }
            if(cb.keySet().contains(U)) {
                for(AuctionedItem is : cb.get(U))
                    collectionbin.add(toRPDataString(is.getItemStack()) + "||" + is.price + "||" + is.auctionedTime + "||" + is.collectionBinExpiration);
            }
            this.data.set(uuid + ".auctions", auctions);
            this.data.set(uuid + ".ah collection bin", collectionbin);
            List<String> showcase = new ArrayList<>();
            for(int page : pdata.showcasedItems.keySet()) {
                int size = 0;
                for(ItemStack is : pdata.showcasedItems.get(page)) {
                    size += 1;
                    showcase.add(page + ":" + pdata.showcaseSizes.get(page) + "=" + toRPDataString(is));
                }
                if(size == 0) showcase.add(page + ":" + pdata.showcaseSizes.get(page));
            }
            this.data.set(uuid + ".showcase", showcase);
        }
        data.set("players.array", uuids);
        savePlayerData();
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aBacked up " + loaded + " player data &e(took " + (System.currentTimeMillis()-started) + "ms)"));
    }
    private void savePlayerData() {
        try {
            data.save(dataF);
            dataF = new File(randompackage.getDataFolder() + File.separator + "_Data" + File.separator, "_player data.yml");
            data = YamlConfiguration.loadConfiguration(dataF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void loadBackup() {
        final long started = System.currentTimeMillis();
        scheduler.runTaskAsynchronously(randompackage, () -> {
            final long J = System.currentTimeMillis();
            final Fund f = Fund.getFund();
            int loaded = 0;
            for(String uuid : data.getStringList("players.array")) {
                loaded += 1;
                final UUID U = UUID.fromString(uuid);
                final RPPlayerData pdata = RPPlayerData.get(U);
                final String[] booleans = data.getString(uuid + ".booleans").split(";"), numbers = data.getString(uuid + ".numbers").split(";");

                pdata.receivesAHOutbidNotications = Boolean.parseBoolean(booleans[0]);
                pdata.receivesCoinflipNotifications = Boolean.parseBoolean(booleans[1]);
                pdata.receivesDuelInvites = Boolean.parseBoolean(booleans[2]);
                pdata.receivesJackpotNotifications = Boolean.parseBoolean(booleans[3]);
                pdata.activeFilter = Boolean.parseBoolean(booleans[4]);

                pdata.xpExhaustionExpiration = Long.parseLong(numbers[0]);
                pdata.fundDeposits = Double.parseDouble(numbers[1]);
                pdata.duelELO = Integer.parseInt(numbers[2]);
                pdata.duelWins = Integer.parseInt(numbers[3]);
                pdata.duelLosses = Integer.parseInt(numbers[4]);
                pdata.coinflipWins = Integer.parseInt(numbers[5]);
                pdata.coinflipLosses = Integer.parseInt(numbers[6]);
                pdata.coinflipWonCash = Double.parseDouble(numbers[7]);
                pdata.coinflipLostCash = Double.parseDouble(numbers[8]);
                pdata.coinflipTaxesPaid = Double.parseDouble(numbers[9]);
                pdata.jackpotTicketsPurchased = Integer.parseInt(numbers[10]);
                pdata.jackpotWins = Integer.parseInt(numbers[11]);
                pdata.jackpotWinnings = Double.parseDouble(numbers[12]);
                pdata.teleportDelayMultiplier = Double.parseDouble(numbers[13]);
                pdata.maxHomeIncreaser = Integer.parseInt(numbers[14]);

                f.total += pdata.fundDeposits;

                for(String s : data.getString(uuid + ".gkits").split(";")) {
                    if(!s.isEmpty()) {
                        final String gkit = s.split("=")[0], values = s.split("=")[1];
                        pdata.gkitTiers.put(gkit, Integer.parseInt(values.split(":")[0]));
                        pdata.gkitCooldowns.put(gkit, Long.parseLong(values.split(":")[1]));
                    }
                }
                for(String s : data.getString(uuid + ".vkits").split(";")) {
                    if(!s.isEmpty()) {
                        final String vkit = s.split("=")[0], values = s.split("=")[1];
                        pdata.vkitLevels.put(vkit, Integer.parseInt(values.split(":")[0]));
                        pdata.vkitCooldowns.put(vkit, Long.parseLong(values.split(":")[1]));
                    }
                }
                for(String s : data.getString(uuid + ".mkits").split(";")) {
                    if(!s.isEmpty()) {
                        pdata.masterykits.put(s.split("=")[0], Long.parseLong(s.split(";")[1]));
                    }
                }
                for(String s : data.getString(uuid + ".monthly crates").split(";")) {
                    if(!s.isEmpty()) {
                        final String m = s.split("=")[0], value = s.split("=")[1];
                        if(Boolean.parseBoolean(value) && !pdata.redeemedMonthlyCrates.contains(m)) pdata.redeemedMonthlyCrates.add(m);
                        pdata.monthlycrates.add(m);
                    }
                }
                for(String s : data.getString(uuid + ".custom enchant entities").split(";")) {
                    if(!s.isEmpty()) pdata.customEnchantEntities.add(UUID.fromString(s));
                }
                for(String s : data.getStringList(uuid + ".homes")) {
                    if(!s.isEmpty()) pdata.addHome(toLocation(s.split(":")[2]), s.split(":")[0], UMaterial.valueOf(s.split(":")[1]).getItemStack(), false, true);
                }
                for(String s : data.getStringList(uuid + ".filtered items")) {
                    if(!s.isEmpty()) pdata.addFilteredItem(toRPDataItemStack(s));
                }
                for(String s : data.getStringList(uuid + ".unclaimed purchases")) {
                    if(!s.isEmpty()) pdata.addUnclaimedPurchase(toRPDataItemStack(s));
                }
                for(String s : data.getStringList(uuid + ".duel collection bin")) {
                    if(!s.isEmpty()) pdata.duelStakeCollectionBin.add(toRPDataItemStack(s));
                }
                final String[] titles = data.getString(uuid + ".titles").split(";");
                for(int i = 0; i < titles.length; i++) {
                    final String T = titles[i];
                    if(i == 0) pdata.activeTitle = T == null ? "nil" : T;
                    else pdata.addOwnedTitle(T);
                }
                pdata.auctions.clear();
                pdata.collectionbin.clear();
                for(String w : data.getStringList(uuid + ".auctions")) {
                    if(!w.isEmpty()) {
                        final AuctionedItem a = new AuctionedItem(U, toRPDataItemStack(w.split("\\|\\|")[0]), Double.parseDouble(w.split("\\|\\|")[1]), Long.parseLong(w.split("\\|\\|")[2]), false);
                        a.listingExpiration = Long.parseLong(w.split("\\|\\|")[3]);
                        if(System.currentTimeMillis() >= a.listingExpiration) a.cancel(true);
                    }
                }
                for(String w : data.getStringList(uuid + ".ah collection bin")) {
                    if(!w.isEmpty()) {
                        final AuctionedItem a = new AuctionedItem(U, toRPDataItemStack(w.split("\\|\\|")[0]), Double.parseDouble(w.split("\\|\\|")[1]), Long.parseLong(w.split("\\|\\|")[2]), true);
                        a.collectionBinExpiration = Long.parseLong(w.split("\\|\\|")[3]);
                        if(System.currentTimeMillis() >= a.collectionBinExpiration) a.cancel(false);
                    }
                }
                for(String s : data.getStringList(uuid + ".showcase")) {
                    if(!s.isEmpty()) {
                        final int page = Integer.parseInt(s.split(":")[0]), size = Integer.parseInt(s.split(":")[1].split("=")[0]);
                        if(!pdata.showcasedItems.keySet().contains(page)) pdata.showcasedItems.put(page, new ArrayList<>());
                        pdata.showcaseSizes.put(page, size);
                        if(s.contains("=")) pdata.showcasedItems.get(page).add(toRPDataItemStack(s.split(page + ":" + size + "=")[1]));
                    }
                }
            }
            final long u = System.currentTimeMillis();
            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&6[RandomPackage] &aLoaded " + loaded + " player data &e(took " + (u-J) + "ms) [async " + (u-started) + "ms]"));
        });
    }

    public void enable() {
        dataF = new File(randompackage.getDataFolder() + File.separator + "_Data" + File.separator, "_player data.yml");
        data = YamlConfiguration.loadConfiguration(dataF);

        loadBackup();
        final long delay = randompackage.getConfig().getInt("backup")*20*60;
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(randompackage, () -> backup(true, true), delay, delay);
    }
    public void disable(boolean checkforupdate) {
        Bukkit.getScheduler().cancelTask(task);
        dobackup(checkforupdate);
    }
}

package me.randomHashTags.RandomPackage.utils;

import java.io.File;
import java.util.*;

import me.randomHashTags.RandomPackage.RPPlayerData;
import me.randomHashTags.RandomPackage.api.*;
import me.randomHashTags.RandomPackage.api.nearFinished.Masks;
import me.randomHashTags.RandomPackage.api.nearFinished.CollectionFilter;
import me.randomHashTags.RandomPackage.api.Kits;
import me.randomHashTags.RandomPackage.utils.classes.*;
import me.randomHashTags.RandomPackage.utils.classes.kits.EvolutionKit;
import me.randomHashTags.RandomPackage.utils.classes.kits.FallenHero;
import me.randomHashTags.RandomPackage.utils.classes.kits.GlobalKit;
import me.randomHashTags.RandomPackage.utils.universal.UMaterial;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.nossr50.datatypes.skills.SkillType;

import me.randomHashTags.RandomPackage.RandomPackageAPI;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.BlackScroll;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.EnchantRarity;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.EnchantmentOrb;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.Fireball;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.MagicDust;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.RandomizationScroll;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.RarityGem;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.SoulTracker;
import me.randomHashTags.RandomPackage.utils.classes.custombosses.CustomBoss;
import me.randomHashTags.RandomPackage.api.nearFinished.FactionAdditions;
import me.randomHashTags.RandomPackage.utils.supported.MCMMOAPI;
import me.randomHashTags.RandomPackage.utils.supported.VaultAPI;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

@SuppressWarnings({"deprecation"})
public class GivedpItem extends RandomPackageAPI implements Listener, TabCompleter {
	
	private static GivedpItem instance;
	public static final GivedpItem getGivedpItem() {
		if(instance == null) instance = new GivedpItem();
		return instance;
	}

	private boolean isEnabled = false;
	private int banknoteslot = -1;
	public static final HashMap<String, ItemStack> items = new HashMap<>();
	private ArrayList<UUID> itemnametag = new ArrayList<>(), itemlorecrystal = new ArrayList<>(), explosivesnowball = new ArrayList<>();
	private List<String> givedpitems = new ArrayList<>();

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		final Player player = sender instanceof Player ? (Player) sender : null;
		if(hasPermission(sender, "RandomPackage.givedp", true)) {
			if(args.length == 0 && player != null)
				viewGivedp(player);
			else if(args.length >= 2) {
				final OfflinePlayer p = Bukkit.getOfflinePlayer(args[0]);
				final String i = args[1];
				if(i.contains("spawner")) {
					final int amount = args.length >= 3 ? getRemainingInt(args[2]) : 1;
					if(player != null)
						giveSpawner(player, i, null, amount);
					else {
						if(p == null) return true;
						if(p.isOnline()) {
							giveSpawner(p.getPlayer(), i, null, amount);
						} else {
							RPPlayerData.get(p.getUniqueId()).getUnclaimedPurchases().add(item);
						}
					}
					return true;
				}
				item = checkStringForRPItem(player == null ? p : null, i);
				if(item != null) {
					if(args.length >= 3) item.setAmount(getRemainingInt(args[2]));
					if(p.isOnline())
						p.getPlayer().getInventory().addItem(item);
					else {
						RPPlayerData.get(p.getUniqueId()).addUnclaimedPurchase(item);
					}
				} else if(i.startsWith("gkit:") || i.startsWith("vkit:")) {
					final String kit = i.split(":")[1];
					final int tier = Integer.parseInt(i.split(":")[2]);
					final Player pl = p.getPlayer();
					final GlobalKit gkit = GlobalKit.valueOf(kit);
					final EvolutionKit vkit = EvolutionKit.valueOf(kit);
					final Kits kits = Kits.getKits();
					if(gkit != null || vkit != null)
						kits.give(pl, gkit != null ? gkit : vkit, tier, false, false);
				}
			}
		}
		return true;
	}
	public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
		final List<String> givedpitems = new ArrayList<>(this.givedpitems);
		if(cmd.getName().equals("givedp")) {
			if(args.length == 1) {
				givedpitems.clear();
				for(Player p : Bukkit.getOnlinePlayers()) {
					final String n = p.getName();
					if(n.toLowerCase().startsWith(args[0]))
						givedpitems.add(n);
				}
			} else if(args.length == 2) {
				if(!args[1].isEmpty()) {
					for(int i = 0; i < givedpitems.size(); i++) {
						final String s = givedpitems.get(i);
						if(!s.startsWith(args[1].toLowerCase())) {
							givedpitems.remove(s);
							i -= 1;
						}
					}
				}
			} else givedpitems.clear();
		}
		return givedpitems;
	}

	public void enable() {
		if(isEnabled) return;
		isEnabled = true;
		pluginmanager.registerEvents(this, randompackage);
		final FileConfiguration itemsConfig = YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator, "items.yml"));
		items.put("banknote", d(itemsConfig, "banknote", 0));
		items.put("christmas-candy", d(itemsConfig, "christmas-candy", 0));
		items.put("christmas-eggnog", d(itemsConfig, "christmas-eggnog", 0));
		items.put("command-reward", d(itemsConfig, "command-reward", 0));
		items.put("experience-bottle", d(itemsConfig, "experience-bottle", 0));
		items.put("explosive-cake", d(itemsConfig, "explosive-cake", 0));
		items.put("explosive-snowball", d(itemsConfig, "explosive-snowball", 0));
		items.put("halloween-candy", d(itemsConfig, "halloween-candy", 0));
		items.put("item-lore-crystal", d(itemsConfig, "item-lore-crystal", 0));
		items.put("item-name-tag", d(itemsConfig, "item-name-tag", 0));
		items.put("max-home-increase", d(itemsConfig, "max-home-increase", 0));
		items.put("mcmmo-vouchers.credit", d(itemsConfig, "mcmmo-vouchers.credit", 0));
		items.put("mcmmo-vouchers.level", d(itemsConfig, "mcmmo-vouchers.level", 0));
		items.put("mcmmo-vouchers.xp", d(itemsConfig, "mcmmo-vouchers.xp", 0));
		items.put("mystery-mob-spawner", d(itemsConfig, "mystery-mob-spawner", 0));
		items.put("showcase-expansion", d(itemsConfig, "showcase-expansion", 0));
		items.put("space-drink", d(itemsConfig, "space-drink", 0));
		items.put("space-firework", d(itemsConfig, "space-firework", 0));

		for(int i = 0; i < items.get("banknote").getItemMeta().getLore().size(); i++) {
			if(items.get("banknote").getItemMeta().getLore().get(i).contains("{AMOUNT}"))
				banknoteslot = i;
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(randompackage, () -> {
			givedpitems.addAll(Arrays.asList(
					"christmascandy", "christmaseggnog", "collectionchest",
					"dimensionweb",
					"equipmentlootbox", "enchantedobsidian", "envoysummon", "explosivecake", "explosivesnowball",
					"factioncrystal",
					"halloweencandy", "heroicfactioncrystal", "holywhitescroll",
					"itemlorecrystal", "itemnametag",
					"kothlootbag",
					"maxhomeincrease", "mysterycrate", "mysterydust", "mysterymobspawner",
					"showcaseexpanshion", "soulanvil", "soulpearl", "spacedrink", "spacefirework",
					"transmogscroll",
					"whitescroll"
			));

			for(BlackScroll bs : BlackScroll.scrolls) givedpitems.add("blackscroll:" + bs.getNumberInConfig());
			for(CustomExplosion e : CustomExplosion.customcreepers) givedpitems.add("customcreeper:" + e.getPath());
			for(CustomExplosion e : CustomExplosion.customtnt) givedpitems.add("customtnt:" + e.getPath());
			for(EnchantmentOrb orb : EnchantmentOrb.enchantmentorbs) givedpitems.add("enchantmentorb:" + orb.getNumberInConfig() + ":" + (orb.getIncrement()-1) + ":<percent>");
			for(EnchantRarity r : EnchantRarity.rarities) givedpitems.add("raritybook:" + r.getName());
			for(Fireball fb : Fireball.fireballs) givedpitems.add("fireball:" + fb.getNumberInConfig());
			for(GlobalKit gk : GlobalKit.kits) givedpitems.add("gkit:" + gk.path + ":<tier>");
			for(Mask m : Mask.masks) givedpitems.add("mask:" + m.path.replace(" ", "_"));
			for(MonthlyCrate m : MonthlyCrate.monthlycrates) givedpitems.add("monthlycrate:" + m.path);
			for(RandomizationScroll r : RandomizationScroll.scrolls) givedpitems.add("randomizationscroll:" + r.getNumberInConfig());
			for(RarityGem r : RarityGem.gems) givedpitems.add("raritygem:" + r.getPath() + ":<souls>");
			for(ServerCrate s : ServerCrate.crates) givedpitems.add("servercrate:" + s.getPath());
			for(ServerCrateFlare s : ServerCrateFlare.flares) givedpitems.add("spaceflare:" + s.serverCrate.getPath());
			for(SoulTracker s : SoulTracker.trackers) givedpitems.add("soultracker:" + s.getNumberInConfig());
			for(EvolutionKit v : EvolutionKit.kits) givedpitems.add("vkit:" + v.path + ":<level>");
		}, 5);
	}
	public void disable() {
		if(!isEnabled) return;
		isEnabled = false;
		items.clear();
		givedpitems.clear();
		HandlerList.unregisterAll(this);
	}

	public ItemStack valueOf(OfflinePlayer player, String string) {
		item = valueOf(string);
		if(item != null && player != null) {
			final String n = player.getName();
			itemMeta = item.getItemMeta(); lore.clear();
			for(String s : itemMeta.getLore())
				lore.add(s.replace("{UNLOCKED_BY}", n).replace("{PLAYER}", n));
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
		}
		return item;
	}
	public ItemStack valueOf(String string) {
		if(string == null) return null;
		final CollectionFilter cf = CollectionFilter.getCollectionFilter();
		final MonthlyCrates monthlycrates = MonthlyCrates.getMonthlyCrates();
		final FactionAdditions factionadditions = FactionAdditions.getFactionAdditions();
		final CustomArmor customarmor = CustomArmor.getCustomArmor();
		final CustomExplosions customtnt = CustomExplosions.getCustomExplosions();
		item = null;
		final String Q = string;
		String name = null;
		string = string.toLowerCase();
		int amount = 1;
		if(string.contains("amount=")) {
			amount = string.split("amount=")[1].contains("-")
					? Integer.parseInt(string.split("amount=")[1].split("-")[0]) + random.nextInt(1+Integer.parseInt(string.split("amount=")[1].split("-")[1])-Integer.parseInt(string.split("amount=")[1].split("-")[0]))
					: Integer.parseInt(string.split("amount=")[1]);
			string = string.split(";amount=")[0];
		}
		boolean savemeta = false;
		if(string.startsWith("/")) {
			item = items.get("command-reward").clone();
			itemMeta = item.getItemMeta();
			lore.clear();
			if (itemMeta.hasLore()) {
				for (String s : itemMeta.getLore())
					if (s.equals("{COMMAND}")) lore.add(string);
					else lore.add(ChatColor.translateAlternateColorCodes('&', s));
				itemMeta.setLore(lore);
				lore.clear();
				item.setItemMeta(itemMeta);
			}
		} else if(string.startsWith("equipmentlootbox")) { return customarmor.isEnabled ? customarmor.equipmentLootbox.clone() : UMaterial.CHEST.getItemStack();
        } else if(string.startsWith("enchantedbook:")) {
            Enchantment enchantment = Enchantment.getByName(string.split(":")[1].toUpperCase());
            int level = 1;
            if (string.split(":").length == 3)
                level = string.split(":")[2].equalsIgnoreCase("random") ? 1 + random.nextInt(enchantment.getMaxLevel()) : string.split(":")[2].contains("-") ? Integer.parseInt(string.split(":")[2].split("\\-")[0]) + random.nextInt(Integer.parseInt(string.split(":")[2].split("\\-")[1])) : Integer.parseInt(string.split(":")[2]);
            item = new ItemStack(Material.ENCHANTED_BOOK, amount);
            final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
            meta.addStoredEnchant(enchantment, level, true);
            item.setItemMeta(meta);
            return item;
        } else if(string.startsWith("enchantedobsidian")) {
		    return dungeons.isEnabled ? dungeons.enchantedobsidian.clone() : UMaterial.OBSIDIAN.getItemStack();
		} else if(string.startsWith("enchantmentorb")) {
			final List<EnchantmentOrb> orbs = EnchantmentOrb.enchantmentorbs;
			int min = string.split(":").length == 4 && string.split(":")[3].contains("-") ? Integer.parseInt(string.split(":")[3].split("-")[0]) : 0;
			final int percent = string.split(":").length == 2 ? random.nextInt(101) : string.split(":")[3].contains("-") ? min + random.nextInt(Integer.parseInt(string.split(":")[3].split("-")[1]) - min + 1) : getRemainingInt(string.split(":")[3]);
			EnchantmentOrb r = string.equals("enchantmentorb") || string.contains(":") && string.split(":")[1].equals("random") ? orbs.get(random.nextInt(orbs.size())) : string.split(":").length == 1 ? EnchantmentOrb.valueOf(getRemainingInt(string.split(":")[1])) : null;
			if (r == null && string.split(":").length >= 2) {
				final List<EnchantmentOrb> t = EnchantmentOrb.numbers.get(getRemainingInt(string.split(":")[1]));
				r = t.get(string.split(":")[2].equals("random") ? random.nextInt(t.size()) : getRemainingInt(string.split(":")[2]));
			}
			if (r != null) {
				item = r.getItemStack();
				if (item.hasItemMeta()) {
					itemMeta = item.getItemMeta();
					if (itemMeta.hasLore())
						for (String stringg : item.getItemMeta().getLore()) {
							if (stringg.contains("{PERCENT}"))
								stringg = stringg.replace("{PERCENT}", Integer.toString(percent));
							lore.add(stringg);
						}
					itemMeta.setLore(lore);
					lore.clear();
					item.setItemMeta(itemMeta);
				}
			}
			return r != null ? item : UMaterial.ENDER_EYE.getItemStack();
		} else if(string.startsWith("banknote:")) {
			int min = string.split(":")[1].contains("-") ? Integer.parseInt(string.split(":")[1].split("-")[0]) : 0;
			double a = string.split(":")[1].contains("-") ? min + random.nextInt(Integer.parseInt(string.split(":")[1].split("-")[1])-min+1) : round(Double.parseDouble(string.split(":")[1]), 2);
			return getBanknote(a, "{PLAYER}");
		} else if(string.startsWith("experiencebottle:") || string.startsWith("xpbottle:")) {
			int min = string.split(":")[1].contains("-") ? Integer.parseInt(string.split(":")[1].split("-")[0]) : 0;
			int a = string.split(":")[1].contains("-") ? min + random.nextInt(Integer.parseInt(string.split(":")[1].split("-")[1])-min+1) : Integer.parseInt(string.split(":")[1]);
			return getXpbottle(a, "Server");
		} else if(string.startsWith("blackscroll:")) {
			BlackScroll b = BlackScroll.valueOf(Integer.parseInt(string.split(":")[1]));
			item = b.getItemStack();
			itemMeta = item.getItemMeta();
			final int percent = string.split(":").length == 3 ? string.split(":")[2].equals("random") ? random.nextInt(101) : Integer.parseInt(string.split(":")[2]) : b.getRandomPercent();
			for(String stringg : item.getItemMeta().getLore()) {
				if(stringg.contains("{PERCENT}")) stringg = stringg.replace("{PERCENT}", "" + percent);
				lore.add(stringg);
			}
			savemeta = true;
		} else if(string.startsWith("christmascandy"))                     return items.get("christmas-candy").clone();
		else if(string.startsWith("christmaseggnog"))                      return items.get("christmas-eggnog").clone();
		else if(string.startsWith("collectionchest") && cf.isEnabled)      return cf.getCollectionChest("default");
		else if(string.startsWith("customarmor:")) return customarmor.isEnabled ? customarmor.getPiece(ArmorSet.valueOf(Q.split(":")[1]), Q.split(":")[2]) : new ItemStack(Material.DIAMOND_HELMET, 1);
		else if(string.startsWith("customboss:"))                          return CustomBoss.valueOf(Q.split(":")[1]).getSpawnItem().clone();
		else if(string.startsWith("customcreeper:"))                        return customtnt.isEnabled ? CustomExplosion.getCustomCreeper(string.split(":")[1]).getItemStack() : UMaterial.CREEPER_SPAWN_EGG.getItemStack();
		else if(string.startsWith("ce:") && string.split(":").length == 5 || string.startsWith("customenchant:") && string.split(":").length == 5) {
			final List<CustomEnchant> r = CustomEnchant.getEnchants(EnchantRarity.valueOf(Q.split(":")[1]));
			final CustomEnchant enchant = r != null ? r.get(random.nextInt(r.size())) : CustomEnchant.valueOf(Q.split(":")[1].replace("_", " "));
			if(enchant == null) return null;
			final int level = !Q.split(":")[2].equalsIgnoreCase("random") ? Integer.parseInt(Q.split(":")[2]) : 1 + random.nextInt(enchant.getMaxLevel()), success = !Q.split(":")[3].equalsIgnoreCase("random") ? Integer.parseInt(Q.split(":")[3]) : random.nextInt(101), destroy = !Q.split(":")[4].equalsIgnoreCase("random") ? Integer.parseInt(Q.split(":")[4]) : random.nextInt(101);
			return customenchants.getRevealedItem(enchant, level, success, destroy, true, true).clone();
		} else if(string.startsWith("customtnt:"))                         return customtnt.isEnabled ? CustomExplosion.getCustomTNT(string.split(":")[1]).getItemStack() : new ItemStack(Material.TNT);
		else if(string.startsWith("dimensionweb")) return dungeons.isEnabled ? dungeons.dimensionweb.clone() : UMaterial.COBWEB.getItemStack();
		else if(string.startsWith("dungeonlootbag"))  return UMaterial.ENDER_CHEST.getItemStack();
		else if(string.startsWith("dust:"))                                return MagicDust.valueOf(Integer.parseInt(string.split(":")[1])).getItemStack().clone();
		else if(string.startsWith("envoysummon")) return envoy.isEnabled ? envoy.envoySummon.clone() : UMaterial.TORCH.getItemStack();
		else if(string.startsWith("explosivecake"))                        return items.get("explosive-cake").clone();
		else if(string.startsWith("explosivesnowball"))                    return items.get("explosive-snowball").clone();
		else if(string.startsWith("factioncrystal")) return factionadditions.isEnabled ? factionadditions.factionCrystal.clone() : new ItemStack(Material.QUARTZ, 1);
		else if(string.startsWith("factionxpbooster") || string.startsWith("factionmcmmobooster"))                     return new ItemStack(Material.TORCH);
        else if(string.startsWith("fallenhero")) {
            final ArrayList<ItemStack> g = new ArrayList<>(kits.gkitFallenHeroes.values()), v = new ArrayList<>(kits.vkitFallenHeroes.values());
            item = random.nextInt(2) == 0 ? g.get(random.nextInt(g.size())).clone() : v.get(random.nextInt(v.size())).clone();
        } else if(string.startsWith("fuelcell")) return dungeons.isEnabled ? dungeons.fuelcell.clone() : UMaterial.TORCH.getItemStack();
        else if(string.startsWith("gem")) {
			final ArrayList<ItemStack> g = new ArrayList<>(kits.gkitGems.values()), v = new ArrayList<>(kits.vkitGems.values());
			item = random.nextInt(2) == 0 ? g.get(random.nextInt(g.size())).clone() : v.get(random.nextInt(v.size())).clone();
		} else if(string.startsWith("gkitfallenhero")) {
			final List<GlobalKit> gkits = GlobalKit.kits;
			return FallenHero.valueOf(string.equals("gkitfallenhero") || string.contains(":") && string.split(":")[1].equals("random") ? gkits.get(random.nextInt(gkits.size())) : GlobalKit.valueOf(Q.split(":")[1])).getSpawnItem();
		} else if(string.startsWith("vkitfallenhero")) {
			final List<EvolutionKit> evkits = EvolutionKit.kits;
			final FallenHero fh = FallenHero.valueOf(string.equals("vkitfallenhero") || string.contains(":") && string.split(":")[1].equals("random") ? evkits.get(random.nextInt(evkits.size())) : EvolutionKit.valueOf(Q.split(":")[1]));
			return fh != null ? fh.getSpawnItem() : new ItemStack(Material.AIR);
		} else if(string.startsWith("gkitgem")) {
        	final HashMap<GlobalKit, ItemStack> gems = kits.gkitGems;
        	return FallenHero.valueOf(string.equals("gkitgem") || string.contains(":") && string.split(":")[1].equals("random") ? GlobalKit.kits.get(random.nextInt(gems.keySet().size())) : GlobalKit.valueOf(Q.split(":")[1])).getGem();
        } else if(string.startsWith("vkitgem")) {
			final HashMap<EvolutionKit, ItemStack> gems = kits.vkitGems;
			return FallenHero.valueOf(string.equals("vkitgem") || string.contains(":") && string.split(":")[1].equals("random") ? EvolutionKit.kits.get(random.nextInt(gems.keySet().size())) : EvolutionKit.valueOf(Q.split(":")[1])).getGem();
        } else if(string.startsWith("halloweencandy"))                       return items.get("halloween-candy").clone();
		else if(string.startsWith("heroicfactioncrystal")) return factionadditions.isEnabled ? factionadditions.heroicFactionCrystal.clone() : UMaterial.QUARTZ.getItemStack();
		else if(string.equals("heroicfallenhero"))                     return kits.gkitHeroicFallenHeroes.get(random.nextInt(kits.gkitHeroicFallenHeroes.size())).getSpawnItem();
		else if(string.equals("heroicfallenherobundle") && kits.gkitsAreEnabled) item = kits.heroicFallenHeroBundle.clone();
		else if(string.startsWith("heroicmysterycrate") && monthlycrates.isEnabled) return monthlycrates.heroicmysterycrate.clone();
		else if(string.startsWith("holywhitescroll")) return dungeons.isEnabled ? dungeons.holywhitescroll.clone() : UMaterial.MAP.getItemStack();
		else if(string.startsWith("itemlorecrystal"))                      return items.get("item-lore-crystal").clone();
		else if(string.startsWith("itemnametag")) {                        return items.get("item-name-tag").clone();
		} else if(string.startsWith("koth") && koth.isEnabled) {
			item = koth.item_lootbag.clone();
			itemMeta = item.getItemMeta();
			lore.clear();
			for (String stringg : itemMeta.getLore()) lore.add(stringg.replace("{PLAYER}", "Server Crate"));
			savemeta = true;
		} else if(string.startsWith("mask:") && Masks.getMasks().isEnabled) { return Mask.valueOf(string.split(":")[1].replace("_", " ")).getItemStack();
		} else if(string.startsWith("maxhomeincrease"))                    return items.get("max-home-increase").clone();
		else if(string.startsWith("mcmmolevelvoucher")) {
			item = items.get("mcmmo-vouchers.level").clone(); itemMeta = item.getItemMeta(); lore.clear();
			final SkillType t = string.split(":")[1].equals("random") ? SkillType.values()[random.nextInt(SkillType.values().length)] : SkillType.valueOf(Q.toUpperCase().split(":")[1]);
			final String skill = ChatColor.translateAlternateColorCodes('&', itemsConfig.getString("mcmmo-vouchers.skill-names." + t.name().toLowerCase()));
			int min = string.split(":")[2].contains("-") ? Integer.parseInt(string.split(":")[2].split("-")[0]) : 0;
			final int amt = string.split(":")[2].contains("-") ? min + random.nextInt(Integer.parseInt(string.split(":")[2].split("-")[1])-min+1): getRemainingInt(string.split(":")[2]);
			for(String stringg : itemMeta.getLore()) {
				if(stringg.contains("{SKILL}")) stringg = stringg.replace("{SKILL}", skill);
				if(stringg.contains("{AMOUNT}")) stringg = stringg.replace("{AMOUNT}", formatInt(amt));
				if(stringg.contains("{PLAYER}")) stringg = stringg.replace("{PLAYER}", "Server Crate");
				lore.add(stringg);
			}
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
			return item;
		} else if(string.startsWith("mcmmocreditvoucher:")
			|| string.startsWith("mcmmoxpvoucher:")) {
			if(pluginmanager.getPlugin("mcMMO") == null
					|| pluginmanager.getPlugin("mcMMO") != null && !pluginmanager.getPlugin("mcMMO").isEnabled()) {
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&e&l(!)&r &eCannot use mcMMO Skill Vouchers because the plugin \"mcMMO\" doesn't exist, or isn't enabled!"));
				return null;
			}
			SkillType sk = !string.startsWith("mcmmoc") ? string.split(":").length >= 3 && string.split(":")[2].equals("random") ? SkillType.values()[random.nextInt(SkillType.values().length)] : MCMMOAPI.getMCMMOAPI().getSkill(string.split(":")[2].toUpperCase()) : null;
			sk = !string.startsWith("mcmmoc") ? MCMMOAPI.getMCMMOAPI().getSkill(string.split(":")[1]) : sk;
			String skill = !string.startsWith("mcmmoc") ? ChatColor.translateAlternateColorCodes('&', itemsConfig.getString("mcmmo-vouchers.skill-names." + sk.name().toLowerCase())) : null;
			final int min = string.split(":")[1].contains("-") ? Integer.parseInt(string.split(":")[1].split("-")[0]) : 1;
			int amt = string.split(":")[1].contains("-") ? min + random.nextInt(1+Integer.parseInt(string.split(":")[1].split("-")[1])-min) : Integer.parseInt(string.split(":")[1]);
			if(amount != -1) {
				if(string.startsWith("mcmmocreditvoucher"))     item = items.get("mcmmo-vouchers.credit").clone();
				else if(string.startsWith("mcmmoxpvoucher"))    item = items.get("mcmmo-vouchers.xp").clone();
				//
				for(String stringg : item.getItemMeta().getLore()) {
					if(stringg.contains("{SKILL}")) stringg = stringg.replace("{SKILL}", "" + skill);
					if(stringg.contains("{AMOUNT}")) stringg = stringg.replace("{AMOUNT}", formatInt(amt));
					if(stringg.contains("{PLAYER}")) stringg = stringg.replace("{PLAYER}", "Server Crate");
					lore.add(stringg);
				}
				savemeta = true;
			}
		} else if(string.startsWith("monthlycrate:")) {
			final MonthlyCrate mc = MonthlyCrate.valueOf(Q.split(":")[1]);
			if(mc != null) return mc.getItemStack();
		} else if(string.startsWith("mysterycrate") || string.startsWith("mysteryservercrate")) { return monthlycrates.isEnabled ? monthlycrates.mysterycrate.clone() : UMaterial.CHEST.getItemStack();
		} else if(string.startsWith("mysterydust")) {                      return items.get("mystery-dust").clone();
		} else if(string.startsWith("mysterymobspawner")) {                return items.get("mystery-mob-spawner").clone();
		} else if(string.toLowerCase().startsWith("randomizationscroll:")) return string.split(":")[1].startsWith("random") ? RandomizationScroll.valueOf(random.nextInt(RandomizationScroll.scrolls.size())).getItemStack() : RandomizationScroll.valueOf(Integer.parseInt(string.split(":")[1])).getItemStack();
		else if(string.toLowerCase().startsWith("raritybook:"))            return EnchantRarity.valueOf(Q.split(":")[1].split(";")[0]).getRevealItem();
		else if(string.toLowerCase().startsWith("rarityfireball:"))        return Fireball.valueOf(Integer.parseInt(string.split(":")[1])).getItemStack();
		else if(string.toLowerCase().startsWith("raritygem:")) {
			int souls = 0;
			if(Q.split(":")[2].contains("-")) {
				final int min = Integer.parseInt(Q.split(":")[2].split("-")[0]), max = Integer.parseInt(Q.split(":")[2].split("-")[1].split(";")[0]);
				souls = min+random.nextInt(max-min+1);
			} else {
				souls = Integer.parseInt(Q.split(":")[2].split(";")[0]);;
			}
			item = RarityGem.valueOf(Q.split(":")[1]).getPhysicalItem(souls); itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', itemMeta.getDisplayName()));
			item.setItemMeta(itemMeta);
			return item;
		}
		else if(string.startsWith("spacecrate:") || string.startsWith("spacechest:") || string.startsWith("serverchest:") || string.startsWith("servercrate:")) {
			return servercrates.isEnabled ? ServerCrate.valueOf(Q.split(":")[1].equals("random") ? ServerCrate.crates.get(random.nextInt(ServerCrate.crates.size())).getPath() : Q.split(":")[1]).getPhyiscalItem() : UMaterial.CHEST.getItemStack();
		} else if(string.startsWith("showcase"))                           return items.get("showcase-expansion").clone();
		else if(string.toLowerCase().startsWith("soultracker:")) {
            List<SoulTracker> p = SoulTracker.trackers;
            return p.get(string.split(":")[1].equals("random") ? random.nextInt(p.size()) : Integer.parseInt(string.split(":")[1])).getItemStack().clone();
        } else if(string.startsWith("soulanvil")) return dungeons.isEnabled ? dungeons.soulanvil.clone() : UMaterial.ANVIL.getItemStack();
		else if(string.startsWith("soulpearl"))   return dungeons.isEnabled ? dungeons.soulpearl.clone() : UMaterial.ENDER_PEARL.getItemStack();
		else if(string.startsWith("spacedrink")) {                       return items.get("space-drink").clone();
		} else if(string.startsWith("spacefirework")) {                    return items.get("space-firework").clone();
		} else if(string.startsWith("spaceflare:")) {
			return servercrates.isEnabled ? ServerCrate.valueOf(Q.split(":")[1]).getFlare().getItemStack() : new ItemStack(Material.TORCH);
		} else if(string.startsWith("title")) {
			final Titles titles = Titles.getTitles();
			string = string.equals("title") ? "" + random.nextInt(titles.titles.size()) : string.replaceAll("\\p{L}", "");
			item = titles.interactableItem.clone(); itemMeta = item.getItemMeta(); lore.clear();
			if(parseInt(string) != null) {
				final int title = parseInt(string);
				List<String> titlenames = titles.titles;
				if(title > 0 && !(title > titlenames.size()) && titlenames.get(title-1) != null) {
					name = ChatColor.translateAlternateColorCodes('&', titlenames.get(title-1));
					itemMeta.setDisplayName(item.getItemMeta().getDisplayName().replace("{TITLE}", "" + name));
					for(String stringg : item.getItemMeta().getLore()) lore.add(stringg.replace("{TITLE}", name));
					itemMeta.setLore(lore); lore.clear();
					item.setItemMeta(itemMeta);
				} else return null;
			} else return null;
		} else if(string.startsWith("transmogscroll")) {return customenchants.isEnabled ? customenchants.transmogscroll.clone() : new ItemStack(Material.PAPER);
		} else if(string.startsWith("whitescroll")) {   return customenchants.isEnabled ? customenchants.whitescroll.clone() : UMaterial.MAP.getItemStack();
		} else return null;
		if(savemeta) {
			if(item.getItemMeta().hasDisplayName()) itemMeta.setDisplayName(item.getItemMeta().getDisplayName());
			itemMeta.setDisplayName(name);
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
		}
		if(item != null) {
			item.setAmount(amount);
			return item;
		}
		return null;
	}
	
	@EventHandler
	private void playerInteractEvent(PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		final ItemStack i = event.getItem();
		if(i != null && event.getAction().name().contains("RIGHT") && i.hasItemMeta() && i.getItemMeta().hasDisplayName() && i.getItemMeta().hasLore()) {
			int k = -1;
			final ItemStack banknote = items.get("banknote");
			if(i.getItemMeta().getDisplayName().equals(banknote.getItemMeta().getDisplayName())
					&& event.getItem().getType().equals(banknote.getType())
					&& event.getItem().getItemMeta().getLore().size() == banknote.getItemMeta().getLore().size())       k = 0;
			else if(i.getItemMeta().equals(items.get("christmas-candy").getItemMeta()))                                              k = 2;
			else if(i.getItemMeta().equals(items.get("christmas-eggnog").getItemMeta()))                                             k = 3;
			else if(i.getItemMeta().getDisplayName().equals(items.get("experience-bottle").getItemMeta().getDisplayName()))          k = 4;
			else if(i.getItemMeta().equals(items.get("explosive-cake").getItemMeta()))                                               k = 5;
			else if(i.getItemMeta().equals(items.get("explosive-snowball").getItemMeta()))                                           k = 6;
			else if(i.getItemMeta().equals(items.get("halloween-candy").getItemMeta()))                                              k = 7;
			
			else if(i.getItemMeta().equals(items.get("item-lore-crystal").getItemMeta()))                                            k = 10;
			else if(i.getItemMeta().equals(items.get("item-name-tag").getItemMeta()))                                                k = 11;
			else if(i.getItemMeta().equals(items.get("max-home-increase").getItemMeta()))                                            k = 12;
			
			else if(i.getItemMeta().equals(items.get("mystery-mob-spawner").getItemMeta()))                                          k = 17;
			
			else if(i.getItemMeta().equals(items.get("space-drink").getItemMeta()))                                                  k = 21;
			else if(i.getItemMeta().equals(items.get("space-firework").getItemMeta()))                                               k = 22;
			
			else return;

			final UUID u = player.getUniqueId();
			event.setCancelled(true);
			player.updateInventory();

			if(k == 0) {
				removeItem(player, event.getItem(), 1);
				double amount = getRemainingDouble(ChatColor.stripColor(event.getItem().getItemMeta().getLore().get(banknoteslot)));
				String r = formatDouble(amount);
				r = r.contains("E") ? r.split("E")[0] : r;
				amount = getRemainingDouble(r);
				VaultAPI.economy.depositPlayer(player, amount);
				for(String string : randompackage.getConfig().getStringList("withdraw.deposit")) {
					if(string.contains("{AMOUNT}")) string = string.replace("{AMOUNT}", r);
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
				}
				playSound(sounds, "withdraw.deposit", player, player.getLocation(), false);
			} else if(k == 2) {

			} else if(k == 4) {
				for(int z = 0; z < items.get("experience-bottle").getItemMeta().getLore().size(); z++)
					if(items.get("experience-bottle").getItemMeta().getLore().get(z).contains("{AMOUNT}")) {
						removeItem(player, event.getItem(), 1);
						final int amount = getRemainingInt(ChatColor.stripColor(event.getItem().getItemMeta().getLore().get(z)));
						player.giveExp(amount);
						for(String string : randompackage.getConfig().getStringList("xpbottle.deposit")) {
							if(string.contains("{AMOUNT}")) string = string.replace("{AMOUNT}", formatInt(amount));
							if(string.contains("{BOTTLER_NAME}")) string = string.replace("{BOTTLER_NAME}", "Server");
							if(string.contains("{PLAYER}")) string = string.replace("{PLAYER}", "Server");
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
						}
						playSound(sounds, "xpbottle.redeem", player, player.getLocation(), false);
						return;
					}
			} else if(k == 5) {
				removeItem(player, event.getItem(), 1);
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "execute " + player.getName() + " " + event.getClickedBlock().getLocation().getBlockX() + " " + event.getClickedBlock().getLocation().getBlockY() + " " + event.getClickedBlock().getLocation().getBlockZ() + " particle smoke " + event.getClickedBlock().getLocation().getBlockX() + " " + event.getClickedBlock().getLocation().getBlockY() + " " + event.getClickedBlock().getLocation().getBlockZ() + " 1 1 1 1 100");
				playSound(sounds, "givedp-items.explosive-cake", event.getPlayer(), player.getLocation(), false);
			} else if(k == 6) {
				event.setCancelled(false);
				explosivesnowball.add(u);
			} else if(k == 10) {
				if(itemlorecrystal.contains(u)) {
					sendStringListMessage(event.getPlayer(), itemsConfig.getStringList("item-lore-crystal.already-in-process"));
				} else {
					itemlorecrystal.add(u);
					sendStringListMessage(player, itemsConfig.getStringList("item-lore-crystal.enter-addlore"));
					removeItem(player, event.getItem(), 1);
				}
			} else if(k == 11) {
				if(itemnametag.contains(u)) {
					sendStringListMessage(player, itemsConfig.getStringList("item-name-tag.already-in-rename-process"));
				} else {
					itemnametag.add(u);
					sendStringListMessage(player, itemsConfig.getStringList("item-name-tag.enter-rename"));
					removeItem(player, event.getItem(), 1);
				}
			} else if(k == 12) {
				RPPlayerData.get(u).maxHomeIncreaser += 1;
				removeItem(player, i, 1);
			} else if(k == 17) {
				final List<String> p = itemsConfig.getStringList("mystery-mob-spawner.obtainable-spawners");
				final String d = p.get(random.nextInt(p.size()));
				removeItem(player, event.getItem(), 1);
				giveSpawner(player, d, null, 1);
			}
		}
	}
	
	@EventHandler
	private void projectileHitEvent(ProjectileHitEvent event) {
		if(event.getEntity().getShooter() instanceof Player && explosivesnowball.contains(((Player) event.getEntity().getShooter()).getUniqueId())) {
			final Player player = (Player) event.getEntity().getShooter();
			final Location l = player.getLocation();
			explosivesnowball.remove(((Player) event.getEntity().getShooter()).getUniqueId());
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "execute " + player.getName() + " " + l.getBlockX() + " " + l.getBlockY() + " " + l.getBlockZ() + " particle smoke " + l.getBlockX() + " " + l.getBlockY() + " " + l.getBlockZ() + " 1 1 1 1 100");
			playSound(sounds, "givedp-items.explosive-cake", null, event.getEntity().getLocation(), false);
		}
	}
	@EventHandler(priority = EventPriority.LOWEST)
	private void playerChatEvent(AsyncPlayerChatEvent event) {
		if(itemnametag.contains(event.getPlayer().getUniqueId())) {
			itemnametag.remove(event.getPlayer().getUniqueId());
			event.setCancelled(true);
			String message = ChatColor.translateAlternateColorCodes('&', event.getMessage());
			item = getItemInHand(event.getPlayer());
			if(item == null || item.getType().equals(Material.AIR)) {
				sendStringListMessage(event.getPlayer(), itemsConfig.getStringList("item-name-tag.cannot-rename-air"));
				giveItem(event.getPlayer(), items.get("item-name-tag").clone());
			} else if(item.getType().name().endsWith("BOW") || item.getType().name().endsWith("_AXE") || item.getType().name().endsWith("SWORD") || item.getType().name().endsWith("HELMET") || item.getType().name().endsWith("CHESTPLATE") || item.getType().name().endsWith("LEGGINGS") || item.getType().name().endsWith("BOOTS")) {
				itemMeta = item.getItemMeta(); lore.clear();
				itemMeta.setDisplayName(message);
				item.setItemMeta(itemMeta);
				event.getPlayer().updateInventory();
				playSound(sounds, "itemnametag.rename-item", event.getPlayer(), event.getPlayer().getLocation(), false);
				for(String string : itemsConfig.getStringList("item-name-tag.rename-item")) {
					if(string.contains("{NAME}")) string = string.replace("{NAME}", itemMeta.getDisplayName());
					event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', string));
				}
			} else {
				sendStringListMessage(event.getPlayer(), itemsConfig.getStringList("item-name-tag.cannot-rename-item"));
				giveItem(event.getPlayer(), items.get("item-name-tag").clone());
			}
		} else if(itemlorecrystal.contains(event.getPlayer().getUniqueId())) {
			String apply = ChatColor.translateAlternateColorCodes('&', itemsConfig.getString("item-lore-crystal.apply"));
			item = getItemInHand(event.getPlayer());
			event.setCancelled(true);
			itemlorecrystal.remove(event.getPlayer().getUniqueId());
			if(item == null || item.getType().equals(Material.AIR)) {
				sendStringListMessage(event.getPlayer(), itemsConfig.getStringList("item-lore-crystal.cannot-addlore-air"));
				giveItem(event.getPlayer(), items.get("item-lore-crystal").clone());
			} else if(item.getType().name().endsWith("BOW") || item.getType().name().endsWith("_AXE") || item.getType().name().endsWith("SWORD") || item.getType().name().endsWith("HELMET") || item.getType().name().endsWith("CHESTPLATE") || item.getType().name().endsWith("LEGGINGS") || item.getType().name().endsWith("BOOTS")) {
				itemMeta = item.getItemMeta(); lore.clear();
				boolean did = false;
				if(itemMeta.hasLore()) {
					lore.addAll(itemMeta.getLore());
					for(int i = 0; i < lore.size(); i++) {
						if(!did && lore.get(i).startsWith(apply)) {
							did = true;
							lore.set(i, apply + ChatColor.stripColor(event.getMessage()));
						}
					}
				}
				if(!did) lore.add(apply + ChatColor.stripColor(event.getMessage()));
				itemMeta.setLore(lore); lore.clear();
				item.setItemMeta(itemMeta);
				event.getPlayer().updateInventory();
				for(String string : itemsConfig.getStringList("item-lore-crystal.add-lore")) {
					if(string.contains("{LORE}")) string = string.replace("{LORE}", apply.replace("{LORE}", ChatColor.stripColor(event.getMessage())));
					event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', string));
				}
			} else {
				sendStringListMessage(event.getPlayer(), itemsConfig.getStringList("item-lore-crystal.cannot-addlore-item"));
				giveItem(event.getPlayer(), items.get("item-lore-crystal").clone());
			}
		}
	}

	public ItemStack getBanknote(double amount, String signer) {
		item = items.get("banknote").clone(); itemMeta = item.getItemMeta(); lore.clear();
		final String am = formatDouble(round(amount, 2));
		if(itemMeta.hasLore()) {
			for(String s : itemMeta.getLore())
				lore.add(s.replace("{PLAYER}", signer).replace("{AMOUNT}", am));
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
		}
		return item;
	}
	public ItemStack getXpbottle(int amount, String bottler) {
		item = items.get("experience-bottle").clone(); itemMeta = item.getItemMeta(); lore.clear();
		if(itemMeta.hasLore()) {
			final String a = formatInt(amount);
			for(String s : itemMeta.getLore())
				lore.add(s.replace("{BOTTLER_NAME}", bottler).replace("{AMOUNT}", a));
			itemMeta.setLore(lore); lore.clear();
			item.setItemMeta(itemMeta);
		}
		return item;
	}
}

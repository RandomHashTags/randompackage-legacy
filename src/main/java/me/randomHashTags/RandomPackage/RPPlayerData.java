package me.randomHashTags.RandomPackage;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.randomHashTags.RandomPackage.utils.classes.*;
import me.randomHashTags.RandomPackage.utils.classes.duels.DuelSettings;
import me.randomHashTags.RandomPackage.utils.classes.factionadditions.ActiveFactionBooster;
import me.randomHashTags.RandomPackage.utils.classes.factionadditions.FactionUpgrade;
import me.randomHashTags.RandomPackage.utils.classes.globalchallenges.GlobalChallenge;
import me.randomHashTags.RandomPackage.utils.classes.globalchallenges.GlobalChallengePrize;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import me.randomHashTags.RandomPackage.utils.classes.customenchants.CustomEnchant;
import me.randomHashTags.RandomPackage.utils.classes.customenchants.RarityGem;
import me.randomHashTags.RandomPackage.api.events.customenchant.CustomEnchantProcEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerHomeEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerToggleRarityGemEvent;
import me.randomHashTags.RandomPackage.api.events.player.PlayerToggleRarityGemEvent.ToggleRarityGemStatus;
import me.randomHashTags.RandomPackage.api.GlobalChallenges;

@SuppressWarnings({"deprecation"})
public class RPPlayerData {
	public static final HashMap<UUID, RPPlayerData> players = new HashMap<>();

	private static final PluginManager pm = Bukkit.getPluginManager();
	private static final RandomPackageAPI api = RandomPackageAPI.getAPI();
	private static final Plugin randompackage = RandomPackage.getPlugin;
	public static final HashMap<String, HashMap<FactionUpgrade, Integer>> factionUpgrades = new HashMap<>();
	public static final HashMap<String, List<ActiveFactionBooster>> factionMultipliers = new HashMap<>();

	private final UUID uuid;
	private final OfflinePlayer player;

	public String activeTitle = null;
	public int maxHomeIncreaser = 0, duelELO = 1000, duelWins = 0, duelLosses = 0, coinflipWins = 0, coinflipLosses = 0, jackpotTicketsPurchased = 0, jackpotWins = 0, questTokens = 0;
	public boolean receivesCoinflipNotifications = true, receivesAHOutbidNotications = true, receivesDuelInvites = true, receivesJackpotNotifications = true, activeFilter = false;
	public double fundDeposits = 0.00, coinflipTaxesPaid = 0.00, coinflipWonCash = 0.00, coinflipLostCash = 0.00, jackpotWinnings = 0.00, teleportDelayMultiplier = 1.00;
	public long xpExhaustionExpiration = 0l;

	public DuelSettings lastDuelSettings = null;

	public HashMap<String, Integer> gkitTiers = new HashMap<>(), vkitLevels = new HashMap<>();
	public HashMap<String, Long> vkitCooldowns = new HashMap<>(), gkitCooldowns = new HashMap<>(), masterykits = new HashMap<>();
	public HashMap<GlobalChallengePrize, Integer> globalChallengePrizes = new HashMap<>();
	private final HashMap<RarityGem, Boolean> activeraritygems = new HashMap<>();
	private final HashMap<GlobalChallenge, Double> globalchallenges = new HashMap<>();
	private final HashMap<Integer, Integer> coinflipRecentStats = new HashMap<>();
	public final HashMap<Integer, Integer> showcaseSizes = new HashMap<>();

	public HashMap<Material, HashMap<CustomEnchant, Integer>> duelGodset = new HashMap<>();

	public List<Home> homes = new ArrayList<>();
	public List<String> ownedTitles = new ArrayList<>(), unclaimedPurchases = new ArrayList<>(), redeemedMonthlyCrates = new ArrayList<>(), monthlycrates = new ArrayList<>();
	public List<UUID> customEnchantEntities = new ArrayList<>();
	public List<ItemStack> duelStakeCollectionBin = new ArrayList<>(), filteredItems = new ArrayList<>();
	public final List<AuctionedItem> auctions = new ArrayList<>(), collectionbin = new ArrayList<>();
	public final List<AuctionedBiddableItem> biddableAuctions = new ArrayList<>();
	public HashMap<Integer, List<ItemStack>> showcasedItems = new HashMap<>();

	public RPPlayerData(OfflinePlayer player) {
		this.player = player;
		this.uuid = player.getUniqueId();
		for(int i = 1; i <= 10; i++) {
			if(api.hasPermission(player.getPlayer(), "RandomPackage.showcase." + i, false)) {
				showcasedItems.put(i, new ArrayList<>());
				showcaseSizes.put(i, 9);
			}
		}
		if(!players.keySet().contains(uuid)) {
			coinflipRecentStats.put(0, 0);
			players.put(uuid, this);
		}
	}
	public UUID getUUID() { return uuid; }
	public OfflinePlayer getOfflinePlayer() { return player; }

	public static HashMap<FactionUpgrade, Integer> getFactionUpgrades(String factionName) {
		if(!factionUpgrades.containsKey(factionName)) factionUpgrades.put(factionName, new HashMap<>());
		return factionUpgrades.get(factionName);
	}
	public static void setFactionUpgrades(String factionName, HashMap<FactionUpgrade, Integer> factionUpgrades) {
		if(factionName != null) {
			RPPlayerData.factionUpgrades.put(factionName, new HashMap<>());
			if(factionUpgrades != null) RPPlayerData.factionUpgrades.get(factionName).putAll(factionUpgrades);
		}
	}

	public static RPPlayerData get(UUID uuid) {
		return players.containsKey(uuid) ? players.get(uuid) : new RPPlayerData(Bukkit.getOfflinePlayer(uuid));
	}

	public void addGlobalChallengePrize(GlobalChallengePrize prize) {
		globalChallengePrizes.put(prize, globalChallengePrizes.keySet().contains(prize) ? globalChallengePrizes.get(prize)+1 : 1);
	}
	public void increaseGlobalChallenge(GlobalChallenge chall, double value) {
		final List<UUID> participants = chall.participants;
		if(!participants.contains(uuid)) {
			participants.add(uuid);
			globalchallenges.put(chall, 0.00);
		}
		final int ranking = GlobalChallenges.getChallenges().getRanking(player.getUniqueId(), chall);
		final double v = value+globalchallenges.get(chall);
		globalchallenges.put(chall, v);
		final String V = api.formatDouble(v), name = player.getName(), cname = chall.name, remainingTime = GlobalChallenges.getChallenges().getRemainingTime(chall);
		if(ranking == 1) {
			for(String s : GlobalChallenges.getChallenges().config.getStringList("messages.taken-lead")) {
				s = s.replace("{PLAYER}", name).replace("{NAME}", cname).replace("{NAME_L}", cname.toLowerCase()).replace("{VALUE}", V).replace("{TIME}", remainingTime);
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', s));
			}
		}
	}
	public void resetGlobalChallengeValue(GlobalChallenge chall) { globalchallenges.remove(chall); }
	public HashMap<GlobalChallenge, Double> getGlobalChallenges() { return globalchallenges; }
	
	public boolean hasActiveRarityGem(RarityGem gem) { return activeraritygems.keySet().contains(gem) ? activeraritygems.get(gem) : false; }
	public void toggleRarityGem(Event event, RarityGem gem) {
		final ToggleRarityGemStatus status = activeraritygems.keySet().contains(gem) && activeraritygems.get(gem) ? event == null ||  event instanceof CustomEnchantProcEvent ? ToggleRarityGemStatus.OFF_INTERACT : event instanceof InventoryClickEvent ? ToggleRarityGemStatus.OFF_MOVED : event instanceof PlayerDropItemEvent ? ToggleRarityGemStatus.OFF_DROPPED : ToggleRarityGemStatus.OFF_INTERACT : ToggleRarityGemStatus.ON;
		final PlayerToggleRarityGemEvent e = new PlayerToggleRarityGemEvent(player, gem, status);
		pm.callEvent(e);
		if(!e.isCancelled()) {
			final String n = status.name();
			final boolean s = n.equals("ON");
			activeraritygems.put(gem, s);
			if(player.isOnline()) {
				for(String S : s ? gem.getToggleOnMsg() : n.endsWith("INTERACT") ? gem.getToggleOffInteractMsg() : n.endsWith("DROPPED") ? gem.getToggleOffDroppedMsg() : n.endsWith("MOVED") ? gem.getToggleOffMovedMsg() : n.endsWith("RAN_OUT") ? gem.getToggleOffRanOutMsg() : gem.getToggleOffInteractMsg()) player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', S));
			}
		}
	}
	public List<RarityGem> getActiveRarityGems() {
		List<RarityGem> gems = new ArrayList<>();
		for(RarityGem g : activeraritygems.keySet()) if(activeraritygems.get(g)) gems.add(g);
		return gems;
	}

	public List<String> getFilteredItemsString() { List<String> y = new ArrayList<>(); for(ItemStack s : filteredItems) y.add(api.toRPDataString(s)); return y; }
	public void addFilteredItem(ItemStack is) { filteredItems.add(new ItemStack(is.getType(), 1, is.getData().getData())); }
	
	public List<ItemStack> getUnclaimedPurchases() { final List<ItemStack> y = new ArrayList<>(); for(String s : unclaimedPurchases) y.add(api.toRPDataItemStack(s)); return y;}
	public void setUnclaimedPurchases(List<ItemStack> items) { unclaimedPurchases.clear(); for(ItemStack is : items) unclaimedPurchases.add(api.toRPDataString(is)); }
	public void addUnclaimedPurchase(ItemStack is) { unclaimedPurchases.add(api.toRPDataString(is)); }
	public void removeUnclaimedPurchase(ItemStack is) { unclaimedPurchases.remove(api.toRPDataString(is)); }
	
	public long getGkitCooldown(String gkit) { return gkitCooldowns.keySet().contains(gkit) ? gkitCooldowns.get(gkit) : 0; }
	public int getGkitTier(String gkit) { return gkitTiers.keySet().contains(gkit) ? gkitTiers.get(gkit) : 0; }
	public long getVkitCooldown(String vkit) { return vkitCooldowns.keySet().contains(vkit) ? vkitCooldowns.get(vkit) : 0; }
	public int getVkitLevel(String vkit) { return vkitLevels.keySet().contains(vkit) ? vkitLevels.get(vkit) : 0; }

	public String getFormattedActiveTitle(boolean chat) {
		final String title = activeTitle, format = title != null ? ChatColor.translateAlternateColorCodes('&', YamlConfiguration.loadConfiguration(new File(randompackage.getDataFolder() + File.separator + "Features", "titles.yml")).getString((chat ? "chat" : "tab") + ".format")) : null;
		return title != null && !title.equals("nil") && !title.equals("null") && !title.equals("") ? " " + format.replace("{TITLE}", ChatColor.translateAlternateColorCodes('&', title)) : "";
	}
	public void addOwnedTitle(String title) { if(!ownedTitles.contains(title)) ownedTitles.add(title); }

	public int getCoinflipRecentWins() { return 0; }
	public double getCoinflipRecentWonCash() { return 0.00; }
	public int getCoinflipRecentLosses() { return 0; }
	public double getCoinflipRecentLostCash() { return 0.00; }
	public void addCoinflipWin(double woncash) {
		coinflipWins += 1;
		coinflipWonCash += woncash;
	}
	public void addCoinflipLoss(double lostcash) {
		coinflipLosses += 1;
		coinflipLostCash += lostcash;
	}

	public void addHome(Location loc, String name, ItemStack icon, boolean sendMessage, boolean bypass) {
		if(!bypass) {
			name = name == null ? randompackage.getConfig().getString("sethome.default-name") : name;
			final int homesize = homes.size(), maxhomes = getMaxHomes();
			if(sendMessage) sendHomeMessage(player, icon, name, "sethome." + (homesize < maxhomes ? "set-home" : "cannot-set-more-than"));
			if(homesize+1 > maxhomes) return;
			final PlayerHomeEvent e = new PlayerHomeEvent(Bukkit.getPlayer(uuid), name, loc, icon, false);
			pm.callEvent(e);
			if(!e.isCancelled()) new Home(name, loc, icon);
		} else {
			new Home(name, loc, icon);
		}
	}
	public int getMaxHomes() {
		final OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
		int defaultmax = randompackage.getConfig().getInt("sethome.default-max"), perm = defaultmax;
		if(player.isOnline()) for(int i = 1; i <= 100; i++) if(player.getPlayer().hasPermission("RandomPackage.sethome." + i)) perm = i;
		perm += maxHomeIncreaser;
		return perm;
	}
	public void deleteHome(Home home) {
	    final ItemStack icon = home.icon;
	    final String name = home.name;
		final PlayerHomeEvent e = new PlayerHomeEvent(Bukkit.getPlayer(uuid), name, home.location, icon, true);
		pm.callEvent(e);
		if(!e.isCancelled()) {
			sendHomeMessage(player, icon, name, "home.delete");
			homes.remove(home);
		}
	}
	private void sendHomeMessage(OfflinePlayer player, ItemStack homeicon, String homeName, String message) {
		String type = homeicon.getType().name(), icon = type.substring(0, 1) + type.substring(1).toLowerCase();
		if(type.contains("_"))
			for(int i = 0; i < type.split("_").length; i++) {
				String u = type.split("_")[i];
				icon = icon + u.substring(0, 1).toUpperCase() + u.substring(1).toLowerCase() + (i != type.split("_").length - 1 ? " " : "");
			}
        if(player.isOnline())
            for(String s : randompackage.getConfig().getStringList(message)) {
                if(s.contains("{HOME}")) s = s.replace("{HOME}", homeName);
                if(s.contains("{MAX}")) s = s.replace("{MAX}", Integer.toString(getMaxHomes()));
                if(s.contains("{ICON}")) s = s.replace("{ICON}", icon);
                player.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', s));
            }
	}

	public boolean canBypassTeleportDelay() {
		return api.hasPermission(player.getPlayer(), "RandomPackage.xpbottle.bypass-delay", false);
	}
	public boolean canBypassXPExhaustion() {
		return api.hasPermission(player.getPlayer(), "RandomPackage.xpbottle.bypass-exhaustion", false);
	}
	public boolean isXPExhausted() { return System.currentTimeMillis() < xpExhaustionExpiration; }

	public void addCustomEnchantEntity(UUID uuidOfEntity) { customEnchantEntities.add(uuidOfEntity); }
	public void removeCustomEnchantEntity(UUID uuidOfEntity) { customEnchantEntities.remove(uuidOfEntity); }
	

	public Home getHome(String homename) {
		for(Home h : homes) if(h.name.equals(homename)) return h;
		return null;
	}
	public class Home {
		public Location location;
		public String name;
		public ItemStack icon;
		public Home(String name, Location location, ItemStack icon) {
			this.name = name;
			this.location = location;
			this.icon = icon;
			boolean did = false;
			for(Home h : homes) {
				if(!did && h.name.equals(name)) {
					did = true;
					h.location = location;
					h.icon = icon;
				}
			}
			if(!did) homes.add(this);
		}
		public void remove() { homes.remove(this);  }
	}
}
